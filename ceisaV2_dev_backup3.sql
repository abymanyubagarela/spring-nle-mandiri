--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: data_pelimpahan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.data_pelimpahan (
    id_surat_kuasa character varying(255) NOT NULL,
    file character varying(255),
    filename character varying(255),
    file_date date
);


ALTER TABLE public.data_pelimpahan OWNER TO postgres;

--
-- Name: data_transporter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.data_transporter (
    id_request_booking character varying(255) NOT NULL,
    billing_code character varying(255),
    created_date timestamp without time zone,
    "timestamp" timestamp without time zone
);


ALTER TABLE public.data_transporter OWNER TO postgres;

--
-- Name: data_transporter_detil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.data_transporter_detil (
    id_data_transporter_detil integer NOT NULL,
    status character varying(255),
    billing_code character varying(255),
    bl_date character varying(255),
    bl_no character varying(255),
    booked_date character varying(255),
    company character varying(255),
    container_no character varying(255),
    container_size character varying(255),
    container_type character varying(255),
    dangerous_material character varying(255),
    dangerous_type character varying(255),
    hp_driver character varying(255),
    id_eseal character varying(255),
    id_request_booking character varying(255),
    id_platform character varying(255),
    is_finished integer DEFAULT 0,
    nama_driver character varying(255),
    nama_platform character varying(255),
    over_height integer,
    over_length integer,
    over_weight integer,
    over_width integer,
    pod character varying(255),
    pod_lat character varying(255),
    pod_lon character varying(255),
    sp2valid_date character varying(255),
    spcvalid_date character varying(255),
    temperatur integer,
    total_distance character varying(255),
    truck_plate_no character varying(255),
    url_tracking character varying(255)
);


ALTER TABLE public.data_transporter_detil OWNER TO postgres;

--
-- Name: seq_detil_data_pelimpahan; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_detil_data_pelimpahan
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_detil_data_pelimpahan OWNER TO postgres;

--
-- Name: detil_data_pelimpahan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detil_data_pelimpahan (
    id integer DEFAULT nextval('public.seq_detil_data_pelimpahan'::regclass) NOT NULL,
    bl_date date,
    bl_no character varying(255),
    created_npwp character varying(255),
    delivery_order integer DEFAULT 0,
    depo integer DEFAULT 0,
    domestic integer DEFAULT 0,
    id_surat_kuasa character varying(255),
    nama_ppjk character varying(255),
    ppjk character varying(255),
    sp2 integer DEFAULT 0,
    trucking integer DEFAULT 0,
    warehouse integer DEFAULT 0,
    document_type integer,
    kd_document_type integer
);


ALTER TABLE public.detil_data_pelimpahan OWNER TO postgres;

--
-- Name: document_do; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document_do (
    id_document_do character varying(255) NOT NULL,
    bl_date date,
    bl_no character varying(20),
    do_date_number date,
    do_number character varying(60),
    forwarder_name character varying(255),
    id_ff_ppjk character varying(30),
    id_platform character varying(5) NOT NULL,
    paid_date date,
    party integer DEFAULT 0,
    price numeric(14,2),
    request_date timestamp without time zone NOT NULL,
    shipping_name character varying(255),
    status character varying(255),
    npwp_cargo_owner character varying(30),
    bl_type character varying,
    payment_url text
);


ALTER TABLE public.document_do OWNER TO postgres;

--
-- Name: seq_document_do_container; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_document_do_container
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_document_do_container OWNER TO postgres;

--
-- Name: document_do_container; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document_do_container (
    id_do_container integer DEFAULT nextval('public.seq_document_do_container'::regclass) NOT NULL,
    container_no character varying(255),
    container_size character varying(255),
    container_type character varying(255),
    id_document_do character varying(255) NOT NULL
);


ALTER TABLE public.document_do_container OWNER TO postgres;

--
-- Name: seq_document_do_log; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_document_do_log
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_document_do_log OWNER TO postgres;

--
-- Name: document_do_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document_do_log (
    id_do_log integer DEFAULT nextval('public.seq_document_do_log'::regclass) NOT NULL,
    created_date timestamp without time zone NOT NULL,
    id_document_do character varying(255) NOT NULL,
    status character varying(255) NOT NULL
);


ALTER TABLE public.document_do_log OWNER TO postgres;

--
-- Name: seq_logtruck; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_logtruck
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_logtruck OWNER TO postgres;

--
-- Name: document_log_truck; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document_log_truck (
    id_log_truck integer DEFAULT nextval('public.seq_logtruck'::regclass) NOT NULL,
    create_date timestamp without time zone,
    id_data_transporter_detil integer,
    status character varying(255),
    id_data_transporter_detil_id_data_transporter_detil integer
);


ALTER TABLE public.document_log_truck OWNER TO postgres;

--
-- Name: document_sp2; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document_sp2 (
    id_document_sp2 character varying(255) NOT NULL,
    id_document integer NOT NULL,
    id_ff_ppjk character varying(30),
    id_platform character varying(5) NOT NULL,
    paid_thrud_date date,
    party integer DEFAULT 0,
    proforma character varying(255),
    proforma_date date,
    request_date timestamp without time zone NOT NULL,
    status character varying(255),
    terminal character varying(255),
    price numeric(14,2),
    url text,
    paid_date date,
    is_finished integer DEFAULT 0,
    payment_url text,
    gate_pass text
);


ALTER TABLE public.document_sp2 OWNER TO postgres;

--
-- Name: seq_document_sp2_container; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_document_sp2_container
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_document_sp2_container OWNER TO postgres;

--
-- Name: document_sp2_container; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document_sp2_container (
    id_sp2_container integer DEFAULT nextval('public.seq_document_sp2_container'::regclass) NOT NULL,
    container_no character varying(255) NOT NULL,
    container_size character varying(255),
    container_type character varying(255),
    id_document_sp2 character varying(255) NOT NULL
);


ALTER TABLE public.document_sp2_container OWNER TO postgres;

--
-- Name: seq_document_sp2_log; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_document_sp2_log
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_document_sp2_log OWNER TO postgres;

--
-- Name: document_sp2_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document_sp2_log (
    id_sp2_log integer DEFAULT nextval('public.seq_document_sp2_log'::regclass) NOT NULL,
    id_document_sp2 character varying(255) NOT NULL,
    status character varying(255),
    create_date timestamp without time zone
);


ALTER TABLE public.document_sp2_log OWNER TO postgres;

--
-- Name: document_trucking; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document_trucking (
    id_request_booking character varying(255) NOT NULL,
    address character varying(255),
    bl_date date,
    bl_no character varying(20),
    company character varying(255),
    email character varying(255),
    id_user character varying(60) NOT NULL,
    id_document integer NOT NULL,
    id_ff_ppjk character varying(30),
    id_platform character varying(5) NOT NULL,
    mobile character varying(255),
    party integer DEFAULT 0,
    plan_date timestamp without time zone NOT NULL,
    pod character varying(255) NOT NULL,
    pod_lat character varying(100),
    pod_lon character varying(100),
    request_date timestamp without time zone NOT NULL,
    sp2valid_date date,
    spcvalid_date date,
    total_distance integer DEFAULT 0,
    npwp character varying(60),
    term_of_payment integer
);


ALTER TABLE public.document_trucking OWNER TO postgres;

--
-- Name: seq_document_trucking_container; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_document_trucking_container
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_document_trucking_container OWNER TO postgres;

--
-- Name: document_trucking_container; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document_trucking_container (
    id_container integer DEFAULT nextval('public.seq_document_trucking_container'::regclass) NOT NULL,
    container_no character varying(255),
    container_size character varying(255),
    container_type character varying(255),
    dangerous_material character varying(255),
    dangerous_type character varying(255),
    id_request_booking character varying(255),
    over_height integer,
    over_length integer,
    over_weight integer,
    over_width integer,
    temperatur integer
);


ALTER TABLE public.document_trucking_container OWNER TO postgres;

--
-- Name: document_trucking_destination; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document_trucking_destination (
    id integer NOT NULL,
    destination character varying(255),
    id_request_booking character varying(255),
    latitude character varying(255),
    longitude character varying(255),
    urutan integer
);


ALTER TABLE public.document_trucking_destination OWNER TO postgres;

--
-- Name: document_trucking_detil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document_trucking_detil (
    id_request_booking character varying(255) NOT NULL,
    booked_date timestamp without time zone,
    booking_note character varying(255),
    created_date timestamp without time zone,
    harga_penawaran integer,
    id_service_order character varying(255),
    id_platform character varying(255),
    is_booked integer DEFAULT 0,
    paid_status integer DEFAULT 0,
    status character varying(255),
    "timestamp" timestamp without time zone,
    waktu_penawaran timestamp without time zone
);


ALTER TABLE public.document_trucking_detil OWNER TO postgres;

--
-- Name: domestic_vessel_booking; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.domestic_vessel_booking (
    id_request character varying(255) NOT NULL,
    alamat_penerima character varying(30) NOT NULL,
    alamat_pengirim character varying(30) NOT NULL,
    booking_date timestamp without time zone NOT NULL,
    deskripsi_umum_barang text,
    hp_penerima character varying(30),
    hp_pengirim character varying(30),
    id_kawasan_asal integer,
    id_kawasan_tujuan integer,
    id_platform character varying(5) NOT NULL,
    id_port_asal integer NOT NULL,
    id_port_tujuan integer NOT NULL,
    nama_penerima character varying(255) NOT NULL,
    nama_pengirim character varying(255) NOT NULL,
    order_id character varying(255),
    paid_date timestamp without time zone,
    price numeric(14,2),
    qty smallint NOT NULL,
    status character varying(255),
    tanggal_stuffing date NOT NULL,
    container_type character varying(50),
    partner character varying(255),
    service_type character varying(50),
    ship_name character varying(255),
    pelayaran character varying(255),
    email character varying(255),
    npwp character varying(30),
    partner_rating smallint
);


ALTER TABLE public.domestic_vessel_booking OWNER TO postgres;

--
-- Name: seq_domestic_vessel_log_status; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_domestic_vessel_log_status
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_domestic_vessel_log_status OWNER TO postgres;

--
-- Name: domestic_vessel_log_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.domestic_vessel_log_status (
    id_log_status integer DEFAULT nextval('public.seq_domestic_vessel_log_status'::regclass) NOT NULL,
    created_date timestamp without time zone NOT NULL,
    id_request character varying(255) NOT NULL,
    status character varying(255) NOT NULL
);


ALTER TABLE public.domestic_vessel_log_status OWNER TO postgres;

--
-- Name: seq_domestic_vessel_packing; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_domestic_vessel_packing
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_domestic_vessel_packing OWNER TO postgres;

--
-- Name: domestic_vessel_packing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.domestic_vessel_packing (
    id_packing integer DEFAULT nextval('public.seq_domestic_vessel_packing'::regclass) NOT NULL,
    berat_muatan character varying(20) NOT NULL,
    deskripsi text,
    jenis_muatan character varying(255) NOT NULL,
    jumlah_barang integer NOT NULL,
    id_request character varying(255),
    kemasan character varying(100)
);


ALTER TABLE public.domestic_vessel_packing OWNER TO postgres;

--
-- Name: seq_container_type; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_container_type
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_container_type OWNER TO postgres;

--
-- Name: mst_container_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_container_type (
    id integer DEFAULT nextval('public.seq_container_type'::regclass) NOT NULL,
    related_fields text,
    type character varying(100) NOT NULL
);


ALTER TABLE public.mst_container_type OWNER TO postgres;

--
-- Name: seq_mst_dangerous_type; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_mst_dangerous_type
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_mst_dangerous_type OWNER TO postgres;

--
-- Name: mst_dangerous_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_dangerous_type (
    id integer DEFAULT nextval('public.seq_mst_dangerous_type'::regclass) NOT NULL,
    related_fields text,
    type character varying(100) NOT NULL
);


ALTER TABLE public.mst_dangerous_type OWNER TO postgres;

--
-- Name: seq_mst_document_type; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_mst_document_type
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_mst_document_type OWNER TO postgres;

--
-- Name: mst_document_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_document_type (
    kd_document_type integer DEFAULT nextval('public.seq_mst_document_type'::regclass) NOT NULL,
    description character varying(20) NOT NULL,
    document_name character varying(20) NOT NULL,
    kd_process_type integer NOT NULL
);


ALTER TABLE public.mst_document_type OWNER TO postgres;

--
-- Name: mst_kawasan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_kawasan (
    id_kawasan bigint NOT NULL,
    nama_kawasan character varying(255),
    nama_kota character varying(255),
    id_port bigint
);


ALTER TABLE public.mst_kawasan OWNER TO postgres;

--
-- Name: seq_mst_kemasan; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_mst_kemasan
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_mst_kemasan OWNER TO postgres;

--
-- Name: mst_kemasan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_kemasan (
    id_kemasan integer DEFAULT nextval('public.seq_mst_kemasan'::regclass) NOT NULL,
    nama_kemasan character varying(255) NOT NULL
);


ALTER TABLE public.mst_kemasan OWNER TO postgres;

--
-- Name: mst_payment_services; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_payment_services (
    id_payment_services character varying(7) NOT NULL,
    id_platform character varying(5) NOT NULL,
    payment_service character varying(50) NOT NULL
);


ALTER TABLE public.mst_payment_services OWNER TO postgres;

--
-- Name: mst_platform; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_platform (
    id_platform character varying(5) NOT NULL,
    company character varying(255),
    live_tracking_url character varying(255),
    nama_platform character varying(255) NOT NULL,
    payment_url character varying(255),
    tc_url character varying(255),
    no_rekening character varying(255)
);


ALTER TABLE public.mst_platform OWNER TO postgres;

--
-- Name: seq_mst_platform_configuration; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_mst_platform_configuration
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_mst_platform_configuration OWNER TO postgres;

--
-- Name: mst_platform_configuration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_platform_configuration (
    id integer DEFAULT nextval('public.seq_mst_platform_configuration'::regclass) NOT NULL,
    category character varying(50),
    key character varying(50) NOT NULL,
    value character varying(255),
    id_platform character varying(5) NOT NULL
);


ALTER TABLE public.mst_platform_configuration OWNER TO postgres;

--
-- Name: seq_mst_platform_services; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_mst_platform_services
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_mst_platform_services OWNER TO postgres;

--
-- Name: mst_platform_services; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_platform_services (
    id_platform_services integer DEFAULT nextval('public.seq_mst_platform_services'::regclass) NOT NULL,
    id_platform character varying(5) NOT NULL,
    id_service integer NOT NULL
);


ALTER TABLE public.mst_platform_services OWNER TO postgres;

--
-- Name: mst_port; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_port (
    id_port bigint NOT NULL,
    is_asal integer,
    is_tujuan integer,
    kota character varying(255),
    port character varying(255),
    singkatan character varying(255)
);


ALTER TABLE public.mst_port OWNER TO postgres;

--
-- Name: seq_mst_process_type; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_mst_process_type
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_mst_process_type OWNER TO postgres;

--
-- Name: mst_process_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_process_type (
    kd_process_type integer DEFAULT nextval('public.seq_mst_process_type'::regclass) NOT NULL,
    process_name character varying(15) NOT NULL
);


ALTER TABLE public.mst_process_type OWNER TO postgres;

--
-- Name: mst_province; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_province (
    code character varying(2) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.mst_province OWNER TO postgres;

--
-- Name: mst_regency; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_regency (
    code character varying(4) NOT NULL,
    name character varying(255) NOT NULL,
    province_code character varying(2) NOT NULL
);


ALTER TABLE public.mst_regency OWNER TO postgres;

--
-- Name: seq_mst_services; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_mst_services
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_mst_services OWNER TO postgres;

--
-- Name: mst_services; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mst_services (
    id_service integer DEFAULT nextval('public.seq_mst_services'::regclass) NOT NULL,
    service_name character varying(255) NOT NULL
);


ALTER TABLE public.mst_services OWNER TO postgres;

--
-- Name: payment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment (
    virtual_account character varying(255) NOT NULL,
    paid_date date,
    price numeric(14,2) NOT NULL,
    status character varying(255)
);


ALTER TABLE public.payment OWNER TO postgres;

--
-- Name: seq_payment_billing; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_payment_billing
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_payment_billing OWNER TO postgres;

--
-- Name: payment_billing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment_billing (
    billing_id integer DEFAULT nextval('public.seq_payment_billing'::regclass) NOT NULL,
    amount numeric(14,2) NOT NULL,
    created_date timestamp without time zone NOT NULL,
    id_service integer,
    item_info text,
    module character varying(20) NOT NULL,
    module_unique_id character varying(60) NOT NULL,
    paid_date timestamp without time zone,
    payment_via character varying DEFAULT 'NLE'::character varying,
    service_unique_id character varying(255),
    id_platform character varying(5)
);


ALTER TABLE public.payment_billing OWNER TO postgres;

--
-- Name: payment_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment_detail (
    id_payment_detail integer NOT NULL,
    price numeric(14,2) NOT NULL
);


ALTER TABLE public.payment_detail OWNER TO postgres;

--
-- Name: seq_payment_invoice; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_payment_invoice
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_payment_invoice OWNER TO postgres;

--
-- Name: payment_invoice; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment_invoice (
    order_number character varying(20) DEFAULT lpad((nextval('public.seq_payment_invoice'::regclass))::text, 20, '0'::text) NOT NULL,
    close_period date,
    costumer_name character varying(255),
    costumer_number character varying(20),
    created_date timestamp without time zone NOT NULL,
    currency_code character varying DEFAULT 'IDR'::character varying NOT NULL,
    id_payment_services integer NOT NULL,
    invoice_account_number character varying(20),
    module character varying(20) NOT NULL,
    module_unique_id character varying(60) NOT NULL,
    open_period date,
    order_amount numeric(14,2),
    status character varying(255),
    unique_id character varying(20) NOT NULL,
    updated_date timestamp without time zone NOT NULL,
    va_created smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public.payment_invoice OWNER TO postgres;

--
-- Name: seq_payment_invoice_item; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_payment_invoice_item
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_payment_invoice_item OWNER TO postgres;

--
-- Name: payment_invoice_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment_invoice_item (
    invoice_item_id integer DEFAULT nextval('public.seq_payment_invoice_item'::regclass) NOT NULL,
    amount numeric(14,2) NOT NULL,
    id_platform character varying(5) NOT NULL,
    id_service integer NOT NULL,
    item_info text,
    quantity character varying(18),
    service_unique_id character varying(255) NOT NULL,
    order_number character varying(20) DEFAULT nextval('public.seq_payment_invoice'::regclass)
);


ALTER TABLE public.payment_invoice_item OWNER TO postgres;

--
-- Name: seq_document_trucking_destination; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_document_trucking_destination
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_document_trucking_destination OWNER TO postgres;

--
-- Name: seq_dtdetil; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_dtdetil
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_dtdetil OWNER TO postgres;

--
-- Name: seq_mst_kawasan; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_mst_kawasan
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_mst_kawasan OWNER TO postgres;

--
-- Name: seq_mst_port; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_mst_port
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_mst_port OWNER TO postgres;

--
-- Name: seq_payment_detail; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_payment_detail
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_payment_detail OWNER TO postgres;

--
-- Name: seq_td_document; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_td_document
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_td_document OWNER TO postgres;

--
-- Name: seq_td_user_document; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_td_user_document
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_td_user_document OWNER TO postgres;

--
-- Name: td_document; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.td_document (
    id_document integer DEFAULT nextval('public.seq_td_document'::regclass) NOT NULL,
    bl_date date,
    bl_no character varying(20),
    document_date date,
    document_no character varying(255),
    document_status character varying(255),
    kd_document_type integer NOT NULL,
    npwp_cargo_owner character varying(30) NOT NULL,
    finish_depo integer DEFAULT 0 NOT NULL,
    finish_do integer DEFAULT 0 NOT NULL,
    finish_sp2 integer DEFAULT 0 NOT NULL,
    finish_trucking integer DEFAULT 0 NOT NULL,
    finish_warehouse integer DEFAULT 0 NOT NULL,
    nm_cargoowner character varying(255),
    no_sppb character varying(255),
    tanggal_sppb date
);


ALTER TABLE public.td_document OWNER TO postgres;

--
-- Name: td_user_document; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.td_user_document (
    id_user_document integer DEFAULT nextval('public.seq_td_user_document'::regclass) NOT NULL,
    id_document integer NOT NULL,
    npwp character varying(30) NOT NULL,
    access_depo integer DEFAULT 1,
    access_do integer DEFAULT 1,
    access_domestic integer DEFAULT 1,
    access_sp2 integer DEFAULT 1,
    access_trucking integer DEFAULT 1,
    access_warehouse integer DEFAULT 1,
    is_ppjk integer DEFAULT 0,
    name character varying(255)
);


ALTER TABLE public.td_user_document OWNER TO postgres;

--
-- Data for Name: data_pelimpahan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.data_pelimpahan (id_surat_kuasa, file, filename, file_date) FROM stdin;
d6c6c50f-ed55-44ab-afeb-4c1e066ff316	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/b1b383dd-48a7-40fa-b9ca-b6a432884085.pdf	b1b383dd-48a7-40fa-b9ca-b6a432884085.pdf	2020-08-14
cecb4d94-8fcf-430d-b030-78744b9f9170	https://esbbcext01.beacukai.go.id:8088/SuratKuasa/viewFile/cecb4d94-8fcf-430d-b030-78744b9f9170.jpg	cecb4d94-8fcf-430d-b030-78744b9f9170.jpg	2020-08-14
2728f874-7993-4e0e-99ae-220ac554c3ea	https://esbbcext01.beacukai.go.id:8088/SuratKuasa/viewFile/2728f874-7993-4e0e-99ae-220ac554c3ea.jpg	2728f874-7993-4e0e-99ae-220ac554c3ea.jpg	2020-08-14
6ebcd5e1-fe02-457c-953e-5b67647f482f	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/6ebcd5e1-fe02-457c-953e-5b67647f482f.jpg	6ebcd5e1-fe02-457c-953e-5b67647f482f.jpg	2020-08-14
d78f2750-ee7c-4576-9bf5-cc2d805831d9	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/ddc476d3-b127-4351-ba03-82d9c5132c6e.png	ddc476d3-b127-4351-ba03-82d9c5132c6e.png	2020-07-14
f8dede8d-d06b-4204-8036-e2359e868da5	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/f8dede8d-d06b-4204-8036-e2359e868da5.png	f8dede8d-d06b-4204-8036-e2359e868da5.png	2020-07-13
1f280aae-64de-4c2b-89f1-6dd2742d7447	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/1f280aae-64de-4c2b-89f1-6dd2742d7447.png	1f280aae-64de-4c2b-89f1-6dd2742d7447.png	2020-06-18
ffc8f327-b8de-47d9-93f1-f3fb233c04a2	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/dfaafa67-0d01-4d32-973e-ac4a1829b160.png	dfaafa67-0d01-4d32-973e-ac4a1829b160.png	2020-07-23
58a46cfe-7dde-4cf2-af3e-d4b1f4bb8ba7	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/d4560ad8-3440-464f-9435-affe903b6b63.jpg	d4560ad8-3440-464f-9435-affe903b6b63.jpg	2020-08-07
9ce03d81-9ad8-4778-8313-86abf54ba4cc	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/d1fe33fe-4f34-4414-b2d7-395ad1c4cbcf.jpg	d1fe33fe-4f34-4414-b2d7-395ad1c4cbcf.jpg	2020-08-07
34e6dfc8-8a64-418b-8e7b-8d3735a13566	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/d339842f-a631-4ee4-b6c8-c51738ed0a35.jpg	d339842f-a631-4ee4-b6c8-c51738ed0a35.jpg	2020-08-07
e7f2cd1a-d1ce-493c-950f-8c423e017317	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/c8ac80e5-b615-4fd0-9327-fb9c3166ce88.jpg	c8ac80e5-b615-4fd0-9327-fb9c3166ce88.jpg	2020-08-07
680a1cb4-8793-46ac-8822-154ba1153786	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/2a33fcc5-4953-4793-8e22-1b7044799435.png	2a33fcc5-4953-4793-8e22-1b7044799435.png	2020-07-09
fcc2abcb-61a0-4072-87ba-c66da83827f8	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/5fd2b2b2-d234-4b2a-9612-e54024b6f997.png	5fd2b2b2-d234-4b2a-9612-e54024b6f997.png	2020-07-09
6e626707-4942-4151-891e-9fbb9b209d08	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/307d5f89-12d1-49d8-8826-6d70a9e6efd4.png	307d5f89-12d1-49d8-8826-6d70a9e6efd4.png	2020-07-09
141b0e8d-651b-4492-acd1-a8fbfb8c9350	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/1044c379-7811-4d9a-93a6-8226ad52e24d.png	1044c379-7811-4d9a-93a6-8226ad52e24d.png	2020-07-09
7756c3b4-1010-44fb-a2f0-9e60cf0128d4	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/5a35e01a-7f01-417c-a73e-b0e4e41747c6.jpeg	5a35e01a-7f01-417c-a73e-b0e4e41747c6.jpeg	2020-08-11
688921aa-074a-4b82-8a67-d711670dbc2a	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/27acdb8c-04b3-4fd6-98c5-f4833e704a1d.pdf	27acdb8c-04b3-4fd6-98c5-f4833e704a1d.pdf	2020-08-12
250677c0-ac91-4a9c-8a69-a0d24188ede5	https://esbbcext01.beacukai.go.id:8088/data_pelimpahan/viewFile/ef48d9ed-e043-45f8-a009-4f53426f473c.pdf	ef48d9ed-e043-45f8-a009-4f53426f473c.pdf	2020-08-14
\.


--
-- Data for Name: data_transporter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.data_transporter (id_request_booking, billing_code, created_date, "timestamp") FROM stdin;
asda929ad0920jdadkasdas	PK	2020-06-30 17:35:55.704	2020-01-24 11:30:00
u38929adsndjk29akeas8w	PK	2020-07-03 09:06:08.83	2020-01-24 11:30:00
2d3f5569-f672-4c76-baa8-948c310c278e1	PK	2020-07-20 13:53:44.233	2020-01-24 11:30:00
a8e9e638-9cb8-411e-aff3-aedeb58fedac1	PK	2020-07-20 14:36:25.449	2020-07-20 07:30:25
33b34362-4519-4bd3-a6fc-cfd87e40344b1	PK	2020-07-23 11:21:26.314	2020-07-20 07:30:25
ax82iuasndjk29akeas8w	PK	2020-08-03 15:21:10.163	2020-01-24 11:30:00
62a84586-ebda-44d8-bb19-70adfcae2b3e2	tba	2020-08-08 15:20:44.881	2020-08-08 15:20:43
4d693fca-bd52-4aca-baea-f5578b6d6f762	tba	2020-08-09 20:13:47.877	2020-08-09 20:13:46
7ae4ce09-ac66-424f-a0dd-a4f36b06fb972	tba	2020-08-10 21:54:58.071	2020-08-10 21:54:56
7b60cd9f-bb76-4469-a0f0-2bd090349edd3		2020-08-11 15:14:01.808	\N
3bd70d88-11a7-4ff6-9ad6-dd102b5668b22	tba	2020-08-11 23:08:49.476	2020-08-11 23:08:47
b3b45d95-d498-41e5-8a59-7f8dac49eaa02	tba	2020-08-12 00:49:58.476	2020-08-12 00:49:56
c2202f16-2cd2-47d7-87be-a7cc9c09ddcf2	tba	2020-08-12 00:54:22.517	2020-08-12 00:54:20
c43eb4a0-a163-4c4d-a649-d0146dbd545e2	tba	2020-08-12 01:16:23.64	2020-08-12 01:16:21
fbaf7158-5968-4411-aba8-15517a8eeea92	tba	2020-08-12 11:44:56.82	2020-08-12 11:44:53
675ebb61-bdde-4195-be83-365448ab00de2	tba	2020-08-13 12:21:41.29	2020-08-13 12:21:39
13a77a62-81c5-4aef-a0cc-f9fe4da444132	tba	2020-08-13 12:59:07.351	2020-08-13 12:59:05
2c807148-d49d-493f-8aff-6d550797ca573	PK	2020-08-14 14:31:07.573	2020-08-14 07:31:05
d050ea8f-13f6-4677-863c-0b2b5bad0dab3	PK	2020-08-14 14:48:34.698	2020-07-22 11:30:00
1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd3	PK	2020-08-14 15:00:42.595	2020-07-22 11:30:00
6e2cf3e8-9c58-4fc1-9524-06cbc5a407603	PK	2020-08-14 15:13:26.871	2020-08-14 08:13:23
bdaa653c-bf1c-4cc6-8fc1-758112cea4283	PK	2020-08-14 15:42:16.91	2020-08-14 08:42:16
437ebef7-53dd-4738-84dc-263aa239e6c73	PK	2020-08-16 14:23:59.452	2020-08-16 07:24:00
ed880f83-a3e2-4505-a043-401dbaf33c3e3	PK	2020-08-14 15:45:49.665	2020-08-14 08:45:48
c69824d3-7a04-4dab-9243-a4663e3ee4783	PK	2020-08-14 16:43:59.257	2020-08-14 09:43:57
1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	PK	2020-08-14 21:38:19.984	2020-08-14 14:38:19
50090798-dfbd-4518-8a84-78a10397747b3	PK	2020-08-17 20:27:10.96	2020-08-17 13:27:07
\.


--
-- Data for Name: data_transporter_detil; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.data_transporter_detil (id_data_transporter_detil, status, billing_code, bl_date, bl_no, booked_date, company, container_no, container_size, container_type, dangerous_material, dangerous_type, hp_driver, id_eseal, id_request_booking, id_platform, is_finished, nama_driver, nama_platform, over_height, over_length, over_weight, over_width, pod, pod_lat, pod_lon, sp2valid_date, spcvalid_date, temperatur, total_distance, truck_plate_no, url_tracking) FROM stdin;
28	\N	\N	\N	\N	\N	\N	ISMU3006513	\N	\N	\N	\N	000	tba	4d693fca-bd52-4aca-baea-f5578b6d6f762	\N	0	INDRA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B9272EB	toms-id.com/api/get_container_tracking?idRequestBooking=4d693fca-bd52-4aca-baea-f5578b6d6f762&container_no=ISMU3006513
54	\N	\N	\N	\N	\N	\N	NYKU3462589	\N	\N	\N	\N	+6281389626516	tba	13a77a62-81c5-4aef-a0cc-f9fe4da444132	\N	0	Manson Bako	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B 9842 SEH	toms-id.com/api/get_container_tracking?idRequestBooking=13a77a62-81c5-4aef-a0cc-f9fe4da444132&container_no=NYKU3462589
29	\N	\N	\N	\N	\N	\N	WHSU5363638	\N	\N	\N	\N	+6287741592086	tba	7ae4ce09-ac66-424f-a0dd-a4f36b06fb972	\N	0	Jumadi Simanulang	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B 9310 SEH	toms-id.com/api/get_container_tracking?idRequestBooking=7ae4ce09-ac66-424f-a0dd-a4f36b06fb972&container_no=WHSU5363638
55	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		d050ea8f-13f6-4677-863c-0b2b5bad0dab3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
6	Sampai di Tujuan	\N	\N	\N	\N	\N	CAAU5129690	\N	\N	\N	\N	0887654321	1b	f6b64c71-d184-4890-ac45-8661c4dec6981	\N	1	Sujadtmiko	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B 3001 XV	www.ceisa4_0.com
7	\N	\N	\N	\N	\N	\N	CAAU5129690	\N	\N	\N	\N	0887654321	1b	2d3f5569-f672-4c76-baa8-948c310c278e1	\N	0	Sujadtmiko	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B 3001 XV	www.ceisa4_0.com
8	\N	\N	\N	\N	\N	\N	KOCU4657983	\N	\N	\N	\N	0887654321	1b	2d3f5569-f672-4c76-baa8-948c310c278e1	\N	0	Sujadtmiko	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B 3001 XV	www.ceisa4_0.com
48	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	087780061591	tba	fbaf7158-5968-4411-aba8-15517a8eeea92	\N	0	SOPIAN	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B. 9056UIZ	toms-id.com/api/get_container_tracking?idRequestBooking=fbaf7158-5968-4411-aba8-15517a8eeea92&container_no=EGHU3290837
9	Sampai Tujuan	\N	\N	\N	\N	\N	BSIU9228806	\N	\N	\N	\N	081122233344	1b	a8e9e638-9cb8-411e-aff3-aedeb58fedac1	\N	1	Hadi	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B 6000 XV	www.ceisa4_0.com
11	\N	\N	\N	\N	\N	\N	KOCU4165216	\N	\N	\N	\N	tba	1b	33b34362-4519-4bd3-a6fc-cfd87e40344b1	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.ceisa4_0.com
12	\N	\N	\N	\N	\N	\N	TGBU5890303	\N	\N	\N	\N	tba	1b	33b34362-4519-4bd3-a6fc-cfd87e40344b1	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.ceisa4_0.com
40	\N	\N	\N	\N	\N	\N	TEMU2326434	\N	\N	\N	\N	tba	tba	3bd70d88-11a7-4ff6-9ad6-dd102b5668b22	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	toms-id.com/api/get_container_tracking?idRequestBooking=3bd70d88-11a7-4ff6-9ad6-dd102b5668b22&container_no=TEMU2326434
56	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
1	Menuju POD	\N	\N	\N	\N	\N	SKLU1324270	\N	\N	\N	\N	081122233344	1b	asda929ad0920jdadkasdas	\N	0	Hadi	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B 6000 XV	www.ceisa4_0.com
41	\N	\N	\N	\N	\N	\N	NYKU3462589	\N	\N	\N	\N	tba	tba	3bd70d88-11a7-4ff6-9ad6-dd102b5668b22	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	toms-id.com/api/get_container_tracking?idRequestBooking=3bd70d88-11a7-4ff6-9ad6-dd102b5668b22&container_no=NYKU3462589
57	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba		bdaa653c-bf1c-4cc6-8fc1-758112cea4283	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
5	Menuju POD	\N	\N	\N	\N	\N	CAAU5129690	\N	\N	\N	\N	0887654321	1b	u38929adsndjk29akeas8w	\N	0	Sujadtmiko	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B 3001 XV	www.ceisa4_0.com
43	\N	\N	\N	\N	\N	\N	SEGU2860274	\N	\N	\N	\N	tba	tba	3bd70d88-11a7-4ff6-9ad6-dd102b5668b22	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	toms-id.com/api/get_container_tracking?idRequestBooking=3bd70d88-11a7-4ff6-9ad6-dd102b5668b22&container_no=SEGU2860274
46	\N	\N	\N	\N	\N	\N	TGHU0968864	\N	\N	\N	\N	+6281911875246	tba	c43eb4a0-a163-4c4d-a649-d0146dbd545e2	\N	0	Meing	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B 9312 SEH	toms-id.com/api/get_container_tracking?idRequestBooking=c43eb4a0-a163-4c4d-a649-d0146dbd545e2&container_no=TGHU0968864
42	\N	\N	\N	\N	\N	\N	NYKU3785431	\N	\N	\N	\N	+6282217814545	tba	3bd70d88-11a7-4ff6-9ad6-dd102b5668b22	\N	0	Ridwan	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B 9310 SEH	toms-id.com/api/get_container_tracking?idRequestBooking=3bd70d88-11a7-4ff6-9ad6-dd102b5668b22&container_no=NYKU3785431
44	\N	\N	\N	\N	\N	\N	NYKU3889800	\N	\N	\N	\N	+6287741592086	tba	b3b45d95-d498-41e5-8a59-7f8dac49eaa02	\N	0	Jumadi Simanulang	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B 9535 SEH	toms-id.com/api/get_container_tracking?idRequestBooking=b3b45d95-d498-41e5-8a59-7f8dac49eaa02&container_no=NYKU3889800
45	\N	\N	\N	\N	\N	\N	TCLU7382520	\N	\N	\N	\N	+62895341049180	tba	c2202f16-2cd2-47d7-87be-a7cc9c09ddcf2	\N	0	Roi Martin Panggabean	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B 9311 SEH	toms-id.com/api/get_container_tracking?idRequestBooking=c2202f16-2cd2-47d7-87be-a7cc9c09ddcf2&container_no=TCLU7382520
47	\N	\N	\N	\N	\N	\N	FCIU4788164	\N	\N	\N	\N	tba	tba	c43eb4a0-a163-4c4d-a649-d0146dbd545e2	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	toms-id.com/api/get_container_tracking?idRequestBooking=c43eb4a0-a163-4c4d-a649-d0146dbd545e2&container_no=FCIU4788164
58	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba		bdaa653c-bf1c-4cc6-8fc1-758112cea4283	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
59	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
34	Port Out	\N	\N	\N	\N	\N	ISMU3006513	\N	\N	\N	\N	11111111		7b60cd9f-bb76-4469-a0f0-2bd090349edd3	\N	0	Supir Test 1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	D12345FGH	logol.co.id:8443/LogisticOnline/OrderTracker?nleBookingID=7b60cd9f-bb76-4469-a0f0-2bd090349edd3
49	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba	tba	fbaf7158-5968-4411-aba8-15517a8eeea92	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	toms-id.com/api/get_container_tracking?idRequestBooking=fbaf7158-5968-4411-aba8-15517a8eeea92&container_no=EISU2222223
60	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
61	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
62	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
4	waiting for process	\N	\N	\N	\N	\N	SKLU1324270	\N	\N	\N	\N	0887654321	1b	u38929adsndjk29akeas8w	\N	0	Sujadtmiko	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	B 3001 XV	www.ceisa4_0.com
50	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba	tba	675ebb61-bdde-4195-be83-365448ab00de2	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	toms-id.com/api/get_container_tracking?idRequestBooking=675ebb61-bdde-4195-be83-365448ab00de2&container_no=EISU2222223
51	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba	tba	675ebb61-bdde-4195-be83-365448ab00de2	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	toms-id.com/api/get_container_tracking?idRequestBooking=675ebb61-bdde-4195-be83-365448ab00de2&container_no=EISU2222223
52	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba	tba	675ebb61-bdde-4195-be83-365448ab00de2	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	toms-id.com/api/get_container_tracking?idRequestBooking=675ebb61-bdde-4195-be83-365448ab00de2&container_no=EGHU3290837
53	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba	tba	675ebb61-bdde-4195-be83-365448ab00de2	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	toms-id.com/api/get_container_tracking?idRequestBooking=675ebb61-bdde-4195-be83-365448ab00de2&container_no=EGHU3290837
63	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
64	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
65	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
66	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
67	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
68	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
69	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
70	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
71	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
72	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba		ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
73	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		c69824d3-7a04-4dab-9243-a4663e3ee4783	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
74	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		c69824d3-7a04-4dab-9243-a4663e3ee4783	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
75	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		c69824d3-7a04-4dab-9243-a4663e3ee4783	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
76	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		c69824d3-7a04-4dab-9243-a4663e3ee4783	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
77	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		c69824d3-7a04-4dab-9243-a4663e3ee4783	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
78	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		c69824d3-7a04-4dab-9243-a4663e3ee4783	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
79	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		c69824d3-7a04-4dab-9243-a4663e3ee4783	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
80	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba		1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
81	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
82	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
83	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba		1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
84	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
85	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba		1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
86	\N	\N	\N	\N	\N	\N	EISU2222223	\N	\N	\N	\N	tba		1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
87	\N	\N	\N	\N	\N	\N	EGHU3290837	\N	\N	\N	\N	tba		1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
88	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
89	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
90	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
91	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
92	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
93	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
94	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
95	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
96	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
97	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
98	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
99	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
100	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
101	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
102	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
103	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
104	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
105	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
106	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
107	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
108	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
109	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
110	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
111	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
112	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
113	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
114	\N	\N	\N	\N	\N	\N	BMOU5168025	\N	\N	\N	\N	tba		437ebef7-53dd-4738-84dc-263aa239e6c73	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
123	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
124	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
125	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
126	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
115	driver take the order	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	-		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	JAMET	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	www.clickargo.com
116	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
117	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
118	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
119	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
120	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
121	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
122	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
127	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
128	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
129	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
130	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
131	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
132	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
133	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
134	\N	\N	\N	\N	\N	\N	TCNU5064786	\N	\N	\N	\N	tba		50090798-dfbd-4518-8a84-78a10397747b3	\N	0	tba	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	tba	www.clickargo.com
\.


--
-- Data for Name: detil_data_pelimpahan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detil_data_pelimpahan (id, bl_date, bl_no, created_npwp, delivery_order, depo, domestic, id_surat_kuasa, nama_ppjk, ppjk, sp2, trucking, warehouse, document_type, kd_document_type) FROM stdin;
8	2020-06-25	HDMUSGWB0586145	018245894059000	1	1	1	f8dede8d-d06b-4204-8036-e2359e868da5	PT ZEE GLOBALTECH	012345678912	1	1	1	\N	1
9	2020-06-19	ONEYSINA41966800	018245894059000	1	0	0	2728f874-7993-4e0e-99ae-220ac554c3ea	NAGASE IMPOR-EKSPOR INDONESIA	018245894059000	1	0	0	\N	\N
10	2020-06-19	ONEYSINA41966800	018245894059000	1	0	0	6ebcd5e1-fe02-457c-953e-5b67647f482f	NAGASE IMPOR-EKSPOR INDONESIA	018245894059000	1	0	0	\N	\N
14	2020-07-08	test	018245894059000	0	0	0	680a1cb4-8793-46ac-8822-154ba1153786	GLOBAL PERKASINDO JAYA LESTARI	0	1	0	0	\N	1
16	2020-07-15	test bc 23	018245894059000	0	0	0	fcc2abcb-61a0-4072-87ba-c66da83827f8	GLOBAL PERKASINDO JAYA LESTARI	0	0	1	0	\N	2
17	2020-07-15	test bc 23	018245894059000	1	0	0	6e626707-4942-4151-891e-9fbb9b209d08	PT ZEE GLOBALTECH	012345678912	0	0	0	\N	2
18	2020-07-23	test bc 16	018245894059000	1	1	1	141b0e8d-651b-4492-acd1-a8fbfb8c9350	PT ZEE GLOBALTECH	012345678912	1	1	1	\N	3
22	2020-06-27	HDMUSGWB0586694	018245894059000	0	0	0	d78f2750-ee7c-4576-9bf5-cc2d805831d9	PT ZEE GLOBALTECH	012345678912	0	1	0	\N	1
7	2020-06-25	testedit bc16	018245894059000	1	0	0	1f280aae-64de-4c2b-89f1-6dd2742d7447	PT ZEE GLOBALTECH	012345678912	1	0	0	\N	3
23	2020-07-02	ONEYTY8AM2768800	018245894059000	0	0	0	ffc8f327-b8de-47d9-93f1-f3fb233c04a2	PT ZEE GLOBALTECH	0123456789	0	0	1	\N	1
25	2020-08-07	testtest	018245894059000	0	0	0	9ce03d81-9ad8-4778-8313-86abf54ba4cc	PT ZEE GLOBALTECH	012345678912	1	1	0	\N	1
26	2020-07-07	ttrtr	018245894059000	1	1	1	34e6dfc8-8a64-418b-8e7b-8d3735a13566	PT ZEE GLOBALTECH	012345678912	1	1	1	\N	1
27	2020-07-31	123test	018245894059000	1	1	1	e7f2cd1a-d1ce-493c-950f-8c423e017317	GLOBAL PERKASINDO JAYA LESTARI	0	1	1	1	\N	1
28	2020-07-28	ONEYTYOA66092900	010711521052000	1	1	1	7756c3b4-1010-44fb-a2f0-9e60cf0128d4	PT. GEMILANG LAJU SEJAHTERA	019926575043000	1	1	1	\N	1
29	2020-08-09	ONEYMNLA19207400	020053708056000	1	1	1	688921aa-074a-4b82-8a67-d711670dbc2a	PT. GATOTKACA TRANS SYSTEMINDO	020404224435000	1	1	1	\N	1
30	2020-08-09	MNLAST64160	020053708056000	1	1	1	250677c0-ac91-4a9c-8a69-a0d24188ede5	PT. GATOTKACA TRANS SYSTEMINDO	020404224435000	1	1	1	\N	1
31	2020-08-01	aaa	012345678912	1	1	1	d6c6c50f-ed55-44ab-afeb-4c1e066ff316	FF Demo	FF Demo	1	1	1	\N	1
\.


--
-- Data for Name: document_do; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.document_do (id_document_do, bl_date, bl_no, do_date_number, do_number, forwarder_name, id_ff_ppjk, id_platform, paid_date, party, price, request_date, shipping_name, status, npwp_cargo_owner, bl_type, payment_url) FROM stdin;
sad7932jkad29oAsA181avx	\N	232434232	2020-06-25	1234567890	PT Sejahtera		PL002	2020-06-25	1	7000000.00	2020-06-25 14:52:42.484	Cargo Ship	Proses Delivery Order	\N	\N	\N
0oa1qb7932jkaas399asdvbkl	\N	232434232	\N	\N	PT Sejahtera		PL002	\N	1	\N	2020-06-30 21:42:39.094	Cargo Ship	Add Request	\N	\N	\N
uasi299idasl2o2i23ia	\N	232434232	\N	\N	PT Sejahtera		PL002	\N	1	\N	2020-07-02 00:41:28.536	Cargo Ship	Add Request	\N	\N	\N
idDocumentDO123	2020-07-22	33224232	2020-06-25	1234567890	PT Sejahtera	1234567890	PL002	2020-07-22	1	7000000.00	2020-07-22 09:30:05.882	Cargo Ship	Menunggu Pembayaran	1234567890	House Update	\N
id_document_do_123	\N	232434232	\N	\N	PT Sejahtera		PL002	\N	1	7000000.00	2020-07-01 14:25:28.206	Cargo Ship	Menunggu Pembayaran	\N	\N	https://do.platform.com/checkout
ksiwu288a83ad020as0kal	\N	33224232	\N	\N	PT Sejahtera		PL002	\N	1	7000000.00	2020-07-06 19:56:58.954	Cargo Ship	Menunggu Pembayaran	1234567890	\N	https://do.platform.com/checkout
jsjs	\N	232434232	2020-06-25	1234567890	PT Sejahtera		PL002	2020-06-24	1	7000000.00	2020-06-30 15:05:57.921	Cargo Ship	Menunggu Pembayaran	\N	House	https://do.platform.com/checkout
3asdj83ad020as0kal	\N	232434232	2020-06-25	1234567890	PT Sejahtera		PL002	2020-06-25	1	3000000.00	2020-07-02 00:48:35.048	Cargo Ship	Menunggu Pembayaran	1234567890	House	http://
ka9289sadjisiud8as0kal	\N	33224232	2020-06-25	1234567890	PT Sejahtera		PL002	\N	1	\N	2020-07-13 17:12:53.71	Cargo Ship	Menunggu Pembayaran	1234567890	House	\N
ka9289sadjisiud8as0kalRN	\N	1234567890	2020-06-25	1234567890	PT Sejahtera		PL002	2020-06-25	1	7000000.00	2020-07-21 11:37:46.373	Cargo Ship	Proses Delivery Order	018245894059000	House	https://do.platform.com/checkout
aus83adjisiud8as0kal	\N	HLCUSIN200533094	\N	\N	PT Sejahtera		PL002	\N	1	7000000.00	2020-08-06 13:44:34.486	Cargo Ship	Menunggu Pembayaran	018245894059000	House	https://do.platform.com/checkout
5dea20f2-a6b6-4119-a1eb-7fa1a778b410	\N	TERAS123	\N	\N			PL002	\N	\N	\N	2020-08-11 23:22:45.913		Waiting for Admin Approval		MABL (Master)	\N
9eaaf20e-9bc1-4e5b-abed-e9cfd460b109	\N	23BBA22	\N	\N		018245894059000	PL002	\N	\N	\N	2020-08-11 23:27:04.109		Waiting for Admin Approval	018245894059000	MABL (Master)	\N
e963bca7-a04e-453e-b107-195082603908	\N	23BBA22	\N	\N			PL002	\N	\N	\N	2020-08-11 23:28:14.368		Waiting for Admin Approval	018245894059000	MABL (Master)	\N
e66a7d1f-3cd8-45f1-9ac3-cabfa0e5cb94	\N	2345	\N	\N			PL002	\N	\N	\N	2020-08-14 10:34:18.626		Waiting for Admin Approval	018245894059000	MABL (Master)	\N
8dc2a129-e077-4450-912d-862561dfc180	\N	764765765	\N	\N			PL002	\N	\N	\N	2020-08-14 15:21:44.858		Waiting for Admin Approval	012345678912	MABL (Master)	\N
\.


--
-- Data for Name: document_do_container; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.document_do_container (id_do_container, container_no, container_size, container_type, id_document_do) FROM stdin;
4	1234567	20	Box	jsjs
5	1234567	20	Box	0oa1qb7932jkaas399asdvbkl
6	1234567	20	Box	id_document_do_123
7	1234567	20	Box	ksiwu288a83ad020as0kal
8	1234567	20	Box	ka9289sadjisiud8as0kal
10	1234567	20	Box	ka9289sadjisiud8as0kalRN
11	1234567	20	Box	idDocumentDO123
12	1234567	20	Box	aus83adjisiud8as0kal
\.


--
-- Data for Name: document_do_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.document_do_log (id_do_log, created_date, id_document_do, status) FROM stdin;
6	2020-06-30 15:05:57.937454	jsjs	Proses Delivery Order
4	2020-06-25 14:53:15	sad7932jkad29oAsA181avx	Menunggu Pembayaran
5	2020-06-25 15:40:41	sad7932jkad29oAsA181avx	Proses Delivery Order
3	2020-06-25 14:52:42	sad7932jkad29oAsA181avx	Add Request
7	2020-06-30 21:42:39.102511	0oa1qb7932jkaas399asdvbkl	Add Request
8	2020-07-01 14:25:28.21727	id_document_do_123	Add Request
9	2020-07-02 00:41:28.566981	uasi299idasl2o2i23ia	Add Request
10	2020-07-02 00:48:35.054269	3asdj83ad020as0kal	Add Request
11	2020-07-02 00:51:44.032994	3asdj83ad020as0kal	Menunggu Pembayaran
12	2020-07-02 00:52:20.886272	3asdj83ad020as0kal	Proses Delivery Order
13	2020-07-06 19:56:58.978818	ksiwu288a83ad020as0kal	Add Request
14	2020-07-13 17:12:53.723357	ka9289sadjisiud8as0kal	Add Request
15	2020-07-13 17:19:50.30428	3asdj83ad020as0kal	Menunggu Pembayaran
17	2020-07-21 11:37:46.377858	ka9289sadjisiud8as0kalRN	Add Request
18	2020-07-22 09:30:05.882	idDocumentDO123	Add Request
19	2020-07-22 09:31:53.001	idDocumentDO123	Menunggu Pembayaran
20	2020-08-06 13:44:34.486	aus83adjisiud8as0kal	Add Request
21	2020-08-07 12:43:50.291	jsjs	Menunggu Pembayaran
22	2020-08-07 13:03:53.536	jsjs	Menunggu Pembayaran
23	2020-08-07 13:05:26.079	jsjs	Menunggu Pembayaran
24	2020-08-07 13:07:52.464	jsjs	Menunggu Pembayaran
25	2020-08-07 13:09:19.674	3asdj83ad020as0kal	Menunggu Pembayaran
26	2020-08-07 13:11:47.123	3asdj83ad020as0kal	Menunggu Pembayaran
27	2020-08-07 13:14:18.499	3asdj83ad020as0kal	Menunggu Pembayaran
28	2020-08-07 13:15:28.648	3asdj83ad020as0kal	Menunggu Pembayaran
29	2020-08-07 13:16:09.357	3asdj83ad020as0kal	Menunggu Pembayaran
30	2020-08-07 13:17:27.781	jsjs	Menunggu Pembayaran
31	2020-08-07 13:18:26.519	jsjs	Menunggu Pembayaran
32	2020-08-07 13:37:22.831	ksiwu288a83ad020as0kal	Menunggu Pembayaran
33	2020-08-11 17:20:19.753	3asdj83ad020as0kal	Menunggu Pembayaran
34	2020-08-11 17:20:54.973	ka9289sadjisiud8as0kal	Menunggu Pembayaran
35	2020-08-11 17:23:10.904	ka9289sadjisiud8as0kalRN	Menunggu Pembayaran
36	2020-08-11 17:52:10.254	ka9289sadjisiud8as0kalRN	Menunggu Pembayaran
37	2020-08-11 18:55:41.38	ka9289sadjisiud8as0kalRN	Proses Delivery Order
38	2020-08-11 20:39:53.118	aus83adjisiud8as0kal	Menunggu Pembayaran
39	2020-08-11 23:22:45.913	5dea20f2-a6b6-4119-a1eb-7fa1a778b410	Waiting for Admin Approval
40	2020-08-11 23:27:04.109	9eaaf20e-9bc1-4e5b-abed-e9cfd460b109	Waiting for Admin Approval
41	2020-08-11 23:28:14.368	e963bca7-a04e-453e-b107-195082603908	Waiting for Admin Approval
42	2020-08-14 10:34:18.626	e66a7d1f-3cd8-45f1-9ac3-cabfa0e5cb94	Waiting for Admin Approval
43	2020-08-14 15:21:44.858	8dc2a129-e077-4450-912d-862561dfc180	Waiting for Admin Approval
\.


--
-- Data for Name: document_log_truck; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.document_log_truck (id_log_truck, create_date, id_data_transporter_detil, status, id_data_transporter_detil_id_data_transporter_detil) FROM stdin;
6	2020-07-01 08:54:42.006264	1	Menuju POD	\N
9	2020-07-03 09:06:08.834123	4	\N	\N
10	2020-07-20 13:15:31.812671	5	\N	\N
11	2020-07-20 13:15:52.420009	6	\N	\N
12	2020-07-20 13:39:09.14217	6	Menuju POD	\N
13	2020-07-20 13:39:47.527813	6	Menuju Destinasi	\N
14	2020-07-20 13:40:22.117455	6	Sampai di Tujuan	\N
15	2020-07-20 13:52:41.014574	7	\N	\N
22	2020-07-23 11:20:05.009648	11	\N	\N
23	2020-07-23 11:21:26.315687	12	\N	\N
43	2020-08-09 20:13:47.879203	28	\N	\N
44	2020-08-10 21:54:58.075297	29	\N	\N
45	2020-08-10 21:58:25.757267	29	\N	\N
46	2020-08-10 21:58:56.870407	29	\N	\N
52	2020-08-11 15:14:01.808906	34	\N	\N
56	2020-08-11 18:54:22.458796	28	\N	\N
58	2020-08-11 23:08:49.477469	40	\N	\N
59	2020-08-11 23:08:49.477469	41	\N	\N
60	2020-08-11 23:08:49.477469	42	\N	\N
61	2020-08-11 23:08:49.477469	43	\N	\N
62	2020-08-11 23:31:23.604689	42	\N	\N
63	2020-08-11 23:32:21.451303	42	\N	\N
64	2020-08-12 00:49:58.477664	44	\N	\N
65	2020-08-12 00:50:42.456373	44	\N	\N
66	2020-08-12 00:54:22.519729	45	\N	\N
67	2020-08-12 00:55:07.122332	45	\N	\N
68	2020-08-12 01:16:23.642279	46	\N	\N
69	2020-08-12 01:16:23.642279	47	\N	\N
70	2020-08-12 01:29:12.609632	46	\N	\N
71	2020-08-12 09:10:37.540722	34	\N	\N
72	2020-08-12 09:12:32.088313	34	Port IN	\N
73	2020-08-12 09:12:37.01985	34	Port Out	\N
74	2020-08-12 11:44:56.822072	48	\N	\N
75	2020-08-12 11:44:56.822072	49	\N	\N
76	2020-08-12 11:47:13.215797	48	\N	\N
77	2020-08-12 14:12:25.271494	4	Menuju POD	\N
78	2020-08-12 14:17:43.79529	1	Menuju POD	\N
79	2020-08-12 16:15:14.206732	4	Menuju POD	\N
80	2020-08-12 16:30:53.536737	5	Menuju POD	\N
81	2020-08-12 16:33:11.077269	4	Menuju Destination	\N
82	2020-08-12 16:47:53.832799	4	Menuju POD	\N
83	2020-08-13 08:28:27.030328	4	driver take the order	\N
84	2020-08-13 08:55:52.374942	4	waiting for process	\N
85	2020-08-13 09:15:55.155413	4	Menuju POD	\N
86	2020-08-13 09:15:55.162	4	Menuju POD	\N
87	2020-08-13 09:16:16.076	4	Menuju POD	\N
88	2020-08-13 09:16:33.831	4	waiting for process	\N
89	2020-08-16 16:32:41.538	115	waiting for process	\N
90	2020-08-16 16:35:02.685	115	Menuju ke Depo	\N
91	2020-08-17 11:33:15.408	115	waiting for process	\N
92	2020-08-17 13:16:39.066	115	driver take the order	\N
\.


--
-- Data for Name: document_sp2; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.document_sp2 (id_document_sp2, id_document, id_ff_ppjk, id_platform, paid_thrud_date, party, proforma, proforma_date, request_date, status, terminal, price, url, paid_date, is_finished, payment_url, gate_pass) FROM stdin;
idDocumentSP2	3	\N	PL002	\N	1	Laporan Proforma	2020-07-20	2020-07-22 09:35:58.146	Proses SP2 Done	Tanjung Priok Jakarta	8000000.00	https://nle.kemenkeu.go.id	\N	0	\N	\N
489fe750-eb2f-45cb-83f3-663a197f689a	78		PL002	2020-08-11	1	Laporan Proforma	2020-08-11	2020-08-11 22:31:59.628	Complete	KOJA	433459169.00	https://nle.kemenkeu.go.id	\N	1	\N	http://123.231.237.4/cms-kiosk/app/module/pdf/202008080260.pdf
eb339182-5b80-4232-8422-470a5c261484	82	018245894059000	PL002	2020-08-12	1	Laporan Proforma	2020-07-20	2020-08-12 11:05:24.339	Complete	KOJA	395512590.00	https://nle.kemenkeu.go.id	\N	1	\N	http://123.231.237.4/cms-kiosk/app/module/pdf/202008120191.pdf
\.


--
-- Data for Name: document_sp2_container; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.document_sp2_container (id_sp2_container, container_no, container_size, container_type, id_document_sp2) FROM stdin;
3	BL001X	20	Box	idDocumentSP2
18	SUKA1234567	42	G1	489fe750-eb2f-45cb-83f3-663a197f689a
19	SUKA8965423	42	G1	eb339182-5b80-4232-8422-470a5c261484
\.


--
-- Data for Name: document_sp2_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.document_sp2_log (id_sp2_log, id_document_sp2, status, create_date) FROM stdin;
65	eb339182-5b80-4232-8422-470a5c261484	Waiting For Payment	2020-08-12 11:05:24.339
66	eb339182-5b80-4232-8422-470a5c261484	Complete	2020-08-12 11:05:49.451
3	idDocumentSP2	Proses SP2	2020-07-22 09:35:58.146
4	idDocumentSP2	Update SP2	2020-07-22 09:37:11.27
67	eb339182-5b80-4232-8422-470a5c261484	Complete	2020-08-12 11:18:18.1
11	idDocumentSP2	Proses SP2 Done	2020-08-06 15:46:58.307
61	489fe750-eb2f-45cb-83f3-663a197f689a	Waiting for Payment	2020-08-11 22:31:59.628
62	489fe750-eb2f-45cb-83f3-663a197f689a	Complete	2020-08-11 22:34:13.511
63	489fe750-eb2f-45cb-83f3-663a197f689a	Complete	2020-08-11 22:36:35.488
64	489fe750-eb2f-45cb-83f3-663a197f689a	Complete	2020-08-12 08:54:19.487
\.


--
-- Data for Name: document_trucking; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.document_trucking (id_request_booking, address, bl_date, bl_no, company, email, id_user, id_document, id_ff_ppjk, id_platform, mobile, party, plan_date, pod, pod_lat, pod_lon, request_date, sp2valid_date, spcvalid_date, total_distance, npwp, term_of_payment) FROM stdin;
asda929ad0920jdadkasdas	Jln Haji Maskur	2012-06-03	HJSCSHSR2U154700	PT Sejahtera Tbk	sejahtera@gmail.com	1234567890	3	123456	PL004	08122001211	1	2020-02-04 14:13:21	JICT	-6.1066044	106.8942988	2020-02-04 14:13:21	2020-01-22	2020-01-22	57	1234567890	30
asda929ad0920jda29329asd	Jln Haji Maskur	2012-06-04	HJSCSHSR2U154700	PT Sejahtera Tbk	sejahtera@gmail.com	1234567890	33		PL001	08122001211	1	2020-02-04 14:13:21	JICT	-6.1066044	106.8942988	2020-02-04 14:13:21	2020-01-23	2020-01-23	57	1234567890	0
d3a70aff-fbf3-4474-884e-0e48fd9901f31	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL000	021-57900391	1	2020-08-10 15:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 11:22:53	2020-08-15	2020-08-15	11	018245894059000	0
ee4e7259-8c1d-4dfc-850e-a9c3dbb67b5a1	Jakarta	2020-06-04	HLCUSIN200533094	Test	test@mail.com	018245894059000	3	coba	PL001	123455	1	2020-07-17 09:00:00	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-07-03 09:42:34	2020-07-30	2020-07-29	16	018245894059000	0
a7a46a33-f4d1-4edc-9be2-9cb799db93091	Jakarta	2020-06-04	HLCUSIN200533094	Test	test@mail.com	018245894059000	3	coba	PL001	123455	1	2020-07-03 13:43:16	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-07-03 14:46:49	2020-07-09	2020-07-09	16	018245894059000	0
b019a789-3802-4356-9091-10310cf45bbf1	Jakarta	2020-06-04	HLCUSIN200533094	Test	test@mail.com	018245894059000	3	coba	PL001	123455	1	2020-07-24 16:00:00	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-07-03 15:07:55	2020-07-22	2020-07-29	16	018245894059000	0
k10so929ad0920jda29bca021	Jln Haji Maskur	2012-06-04	HJSCSHSR2U154700	PT Sejahtera Tbk	sejahtera@gmail.com	1234567890	33		PL001	08122001211	1	2020-02-04 14:13:21	JICT	-6.1066044	106.8942988	2020-02-04 14:13:21	2020-01-23	2020-01-23	57	1234567890	0
cpq1ogi7ad0920jda29bcbl0u	Jln Haji Maskur	2012-06-04	HJSCSHSR2U154700	PT Sejahtera Tbk	sejahtera@gmail.com	1234567890	33		PL001	08122001211	1	2020-02-04 14:13:21	JICT	-6.1066044	106.8942988	2020-02-04 14:13:21	2020-01-23	2020-01-23	57	1234567890	0
590fce61-64c9-4dca-8ee7-f2e8ea76fcee1	Jakarta	2020-06-13	HDMUSGWB0585372	Test	test@mail.com	018245894059000	3	coba	PL001	123455	1	2020-07-25 10:00:00	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-07-07 09:11:37	2020-07-08	2020-07-08	16	018245894059000	0
aa170d90-9b2f-4c96-9aee-9d64648cd04e1	Jakarta	2020-06-13	HDMUSGWB0585372	Test	test@mail.com	018245894059000	3	coba	PL001	123455	1	2020-07-23 10:00:00	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-07-07 09:13:39	2020-07-08	2020-07-08	16	018245894059000	0
67889e84-403e-4379-8c5b-1877588ab1381	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-12	HLCUSIN200600570	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	3	coba	PL001	021-57900391	1	2020-07-23 10:00:00	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-07-07 09:33:39	2020-07-31	2020-07-31	16	018245894059000	0
5c849b69-0004-43c1-a31d-6148a3d33dbc1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-12	HLCUSIN200600570	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	3	coba	PL001	021-57900391	1	2020-07-24 11:00:00	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-07-07 09:35:25	2020-07-23	2020-07-29	16	018245894059000	0
e9b63d39-239b-4ba7-aa6c-f49430efeaa51	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	\N		NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	3		PL001	021-57900391	0	2020-07-08 17:09:55	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-07-08 17:10:27	2020-07-30	2020-07-16	16	018245894059000	0
f2ed1fed-3454-4c21-9904-1f11c23544c31	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	\N		NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	3		PL001	021-57900391	1	2020-07-29 11:00:00	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-07-09 09:28:09	2020-07-10	2020-07-10	16	018245894059000	0
bc3cd8cc-5384-48a5-8342-4f0361fa9c2e1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	\N		NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39	\N	PL001	021-57900391	1	2020-07-30 08:00:00	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-07-09 09:36:21	2020-07-16	2020-07-10	16	018245894059000	0
e79f8254-22f5-4d03-ad85-cd63f29b90fc1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39	\N	PL001	021-57900391	1	2020-07-21 08:00:00	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-07-09 09:39:32	2020-07-23	2020-07-23	16	018245894059000	0
3a39e7b7-9221-4257-8d17-10b067e2f04a1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39	\N	PL001	021-57900391	1	2020-07-23 08:00:00	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-07-09 09:43:58	2020-07-10	2020-07-10	16	018245894059000	0
ef74a1fb-8a10-44ed-85dc-5f37f8954c181	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39	\N	PL001	021-57900391	1	2020-07-24 12:00:00	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-07-09 11:18:35	2020-07-10	2020-07-23	16	018245894059000	0
d3a70aff-fbf3-4474-884e-0e48fd9901f32	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL001	021-57900391	1	2020-08-10 15:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 11:22:53	2020-08-15	2020-08-15	11	018245894059000	0
d3a70aff-fbf3-4474-884e-0e48fd9901f33	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL004	021-57900391	1	2020-08-10 15:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 11:22:53	2020-08-15	2020-08-15	11	018245894059000	0
149603f2-19ae-4c19-8d5b-da6acce03bcd1	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-05	JKTB4224102	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	67	019926575043000	PL000	021-45854360	1	2020-08-10 15:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 13:47:38	2020-08-11	2020-08-12	67	019926575043000	0
149603f2-19ae-4c19-8d5b-da6acce03bcd2	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-05	JKTB4224102	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	67	019926575043000	PL001	021-45854360	1	2020-08-10 15:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 13:47:38	2020-08-11	2020-08-12	67	019926575043000	0
149603f2-19ae-4c19-8d5b-da6acce03bcd3	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-05	JKTB4224102	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	67	019926575043000	PL004	021-45854360	1	2020-08-10 15:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 13:47:38	2020-08-11	2020-08-12	67	019926575043000	0
3bd70d88-11a7-4ff6-9ad6-dd102b5668b21	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL000	021-45854360	4	2020-08-12 22:58:05	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-11 23:07:13	2020-08-12	2020-08-14	16253	019926575043000	0
3bd70d88-11a7-4ff6-9ad6-dd102b5668b22	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL001	021-45854360	4	2020-08-12 22:58:05	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-11 23:07:13	2020-08-12	2020-08-14	16253	019926575043000	0
3bd70d88-11a7-4ff6-9ad6-dd102b5668b23	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL004	021-45854360	4	2020-08-12 22:58:05	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-11 23:07:13	2020-08-12	2020-08-14	16253	019926575043000	0
d1be5e65-b186-4252-94fc-95765455f5f51	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-12 11:19:40	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-12 11:21:12	2020-08-12	2020-08-13	20	018245894059000	0
d1be5e65-b186-4252-94fc-95765455f5f52	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-12 11:19:40	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-12 11:21:12	2020-08-12	2020-08-13	20	018245894059000	0
d1be5e65-b186-4252-94fc-95765455f5f53	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-12 11:19:40	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-12 11:21:12	2020-08-12	2020-08-13	20	018245894059000	0
66d6d20c-a63e-46fd-b440-748da42ce93c1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-12 16:02:37	JICT 2, Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1057443	106.8864882	2020-08-12 16:04:47	2020-08-12	2020-08-12	2	018245894059000	0
u38929adsndjk29akeas8w	Jln Haji Maskur	2012-06-04	HJSCSHSR2U154700	PT Sejahtera Tbk	sejahtera@gmail.com	018245894059000	33		PL001	08122001211	1	2020-02-04 14:13:21	JICT	-6.1066044	106.8942988	2020-02-04 14:13:21	2020-01-23	2020-01-23	57	1234567890	0
66d6d20c-a63e-46fd-b440-748da42ce93c2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-12 16:02:37	JICT 2, Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1057443	106.8864882	2020-08-12 16:04:47	2020-08-12	2020-08-12	2	018245894059000	0
66d6d20c-a63e-46fd-b440-748da42ce93c3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-12 16:02:37	JICT 2, Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1057443	106.8864882	2020-08-12 16:04:47	2020-08-12	2020-08-12	2	018245894059000	0
9c50f350-9086-4c0a-bf8d-49d3e0ef9d021	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:26:31	2020-08-12	2020-08-14	11	018245894059000	0
9c50f350-9086-4c0a-bf8d-49d3e0ef9d022	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:26:31	2020-08-12	2020-08-14	11	018245894059000	0
9c50f350-9086-4c0a-bf8d-49d3e0ef9d023	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:26:31	2020-08-12	2020-08-14	11	018245894059000	0
5a7693cb-af7f-4243-aa21-d02bb2448f371	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 10:18:13	Tanjung Pasir, Tangerang, Banten, Indonesia	-6.026038100000001	106.6615529	2020-08-13 10:21:50	2020-08-14	2020-08-15	1387	018245894059000	0
5a7693cb-af7f-4243-aa21-d02bb2448f372	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 10:18:13	Tanjung Pasir, Tangerang, Banten, Indonesia	-6.026038100000001	106.6615529	2020-08-13 10:21:50	2020-08-14	2020-08-15	1387	018245894059000	0
fe908600-552a-4e1e-9669-52fac58a5c381	JL. RAYA INTI BLOK C4 NO. 3A KAWASAN INDUSTRI BIIE KEL.SUKARESMI, KEC.CIKARANG SELATAN, BEKASI, JAWA BARAT, 17550	2020-07-05	JKTB4224102	MICS STEEL INDONESIA	commercial@mics.co.id	318194040413000	67		PL000	0218972928	1	2020-08-11 09:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 16:15:27	2020-08-11	2020-08-12	67	318194040413000	15
fe908600-552a-4e1e-9669-52fac58a5c382	JL. RAYA INTI BLOK C4 NO. 3A KAWASAN INDUSTRI BIIE KEL.SUKARESMI, KEC.CIKARANG SELATAN, BEKASI, JAWA BARAT, 17550	2020-07-05	JKTB4224102	MICS STEEL INDONESIA	commercial@mics.co.id	318194040413000	67		PL001	0218972928	1	2020-08-11 09:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 16:15:27	2020-08-11	2020-08-12	67	318194040413000	15
fe908600-552a-4e1e-9669-52fac58a5c383	JL. RAYA INTI BLOK C4 NO. 3A KAWASAN INDUSTRI BIIE KEL.SUKARESMI, KEC.CIKARANG SELATAN, BEKASI, JAWA BARAT, 17550	2020-07-05	JKTB4224102	MICS STEEL INDONESIA	commercial@mics.co.id	318194040413000	67		PL004	0218972928	1	2020-08-11 09:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 16:15:27	2020-08-11	2020-08-12	67	318194040413000	15
b3b45d95-d498-41e5-8a59-7f8dac49eaa01	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL000	021-45854360	1	2020-08-12 00:43:53	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-12 00:45:58	2020-08-12	2020-08-14	16253	019926575043000	0
b3b45d95-d498-41e5-8a59-7f8dac49eaa02	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL001	021-45854360	1	2020-08-12 00:43:53	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-12 00:45:58	2020-08-12	2020-08-14	16253	019926575043000	0
b3b45d95-d498-41e5-8a59-7f8dac49eaa03	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL004	021-45854360	1	2020-08-12 00:43:53	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-12 00:45:58	2020-08-12	2020-08-14	16253	019926575043000	0
c2202f16-2cd2-47d7-87be-a7cc9c09ddcf1	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL000	021-45854360	1	2020-08-12 00:51:14	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-12 00:52:49	2020-08-12	2020-08-14	16253	019926575043000	0
c2202f16-2cd2-47d7-87be-a7cc9c09ddcf2	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL001	021-45854360	1	2020-08-12 00:51:14	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-12 00:52:49	2020-08-12	2020-08-14	16253	019926575043000	0
c2202f16-2cd2-47d7-87be-a7cc9c09ddcf3	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL004	021-45854360	1	2020-08-12 00:51:14	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-12 00:52:49	2020-08-12	2020-08-14	16253	019926575043000	0
87efdc2a-88cb-42cb-91cc-7f235234f3ba1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-12 11:19:40	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-12 11:21:55	2020-08-12	2020-08-13	7	018245894059000	0
87efdc2a-88cb-42cb-91cc-7f235234f3ba2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-12 11:19:40	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-12 11:21:55	2020-08-12	2020-08-13	7	018245894059000	0
87efdc2a-88cb-42cb-91cc-7f235234f3ba3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-12 11:19:40	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-12 11:21:55	2020-08-12	2020-08-13	7	018245894059000	0
d78b2959-b4ee-43b2-b040-d51e200316bc1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-12 20:22:54	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 20:23:45	2020-08-12	2020-08-12	15	018245894059000	0
d78b2959-b4ee-43b2-b040-d51e200316bc2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-12 20:22:54	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 20:23:45	2020-08-12	2020-08-12	15	018245894059000	0
d78b2959-b4ee-43b2-b040-d51e200316bc3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-12 20:22:54	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 20:23:45	2020-08-12	2020-08-12	15	018245894059000	0
3f05edaa-dff3-49c0-b527-099f4936ad5d1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-12 21:39:54	JICT 2, Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1057443	106.8864882	2020-08-12 22:29:24	2020-08-12	2020-08-14	11	018245894059000	0
3f05edaa-dff3-49c0-b527-099f4936ad5d2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-12 21:39:54	JICT 2, Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1057443	106.8864882	2020-08-12 22:29:24	2020-08-12	2020-08-14	11	018245894059000	0
3f05edaa-dff3-49c0-b527-099f4936ad5d3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-12 21:39:54	JICT 2, Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1057443	106.8864882	2020-08-12 22:29:24	2020-08-12	2020-08-14	11	018245894059000	0
9f27b2cf-34b1-43ef-abb4-38f0b72f20751	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-21	HDMUSGWB0586145	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	33		PL001	021-57900391	1	2020-07-20 09:41:08	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-20 09:42:03	2020-07-21	2020-07-21	15	018245894059000	0
49b215e6-0277-48cf-a4ec-0dc05f7c77711	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-21	HDMUSGWB0586145	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	33		PL001	021-57900391	1	2020-07-20 10:03:50	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-20 10:05:22	2020-07-21	2020-07-21	15	018245894059000	0
47503e6d-ab49-4279-87ef-94e2572cd14a1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-21	HDMUSGWB0586145	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	33		PL000	021-57900391	1	2020-07-20 10:08:38	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-20 10:09:52	2020-07-22	2020-07-21	15	018245894059000	0
47503e6d-ab49-4279-87ef-94e2572cd14a2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-21	HDMUSGWB0586145	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	33		PL001	021-57900391	1	2020-07-20 10:08:38	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-20 10:09:52	2020-07-22	2020-07-21	15	018245894059000	0
47503e6d-ab49-4279-87ef-94e2572cd14a3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-21	HDMUSGWB0586145	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	33		PL004	021-57900391	1	2020-07-20 10:08:38	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-20 10:09:52	2020-07-22	2020-07-21	15	018245894059000	0
744fa35b-ccf0-4cbf-84ea-eb7ef8f80e4a1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-21	HDMUSGWB0586145	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	33		PL000	021-57900391	1	2020-07-20 10:12:40	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-20 10:13:14	2020-07-21	2020-07-28	15	018245894059000	0
744fa35b-ccf0-4cbf-84ea-eb7ef8f80e4a2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-21	HDMUSGWB0586145	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	33		PL001	021-57900391	1	2020-07-20 10:12:40	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-20 10:13:14	2020-07-21	2020-07-28	15	018245894059000	0
744fa35b-ccf0-4cbf-84ea-eb7ef8f80e4a3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-21	HDMUSGWB0586145	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	33		PL004	021-57900391	1	2020-07-20 10:12:40	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-20 10:13:14	2020-07-21	2020-07-28	15	018245894059000	0
idRequestTest123	Jln Haji Maskur	2012-06-04	HJSCSHSR2U154700	PT Sejahtera Tbk	sejahtera@gmail.com	1234567890	3		PL000	08122001211	1	2020-02-04 14:13:21	JICT	-6.1066044	106.8942988	2020-02-04 14:13:21	2020-01-23	2020-01-23	57	1234567890	0
c43eb4a0-a163-4c4d-a649-d0146dbd545e1	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL000	021-45854360	2	2020-08-12 00:55:55	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-12 01:07:05	2020-08-12	2020-08-14	16253	019926575043000	0
abcdef12345	Jln Haji Maskur	2012-06-04	HJSCSHSR2U154700	PT Sejahtera Tbk	sejahtera@gmail.com	1234567890	3		PL000	08122001211	1	2020-02-04 14:13:21	JICT	-6.1066044	106.8942988	2020-02-04 14:13:21	2020-01-23	2020-01-23	57	1234567890	0
asdfghjkl1234	Jln Haji Maskur	2012-06-04	HJSCSHSR2U154700	PT Sejahtera Tbk	sejahtera@gmail.com	1234567890	3		PL000	08122001211	1	2020-02-04 14:13:21	JICT	-6.1066044	106.8942988	2020-02-04 14:13:21	2020-01-23	2020-01-23	57	1234567890	0
c43eb4a0-a163-4c4d-a649-d0146dbd545e2	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL001	021-45854360	2	2020-08-12 00:55:55	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-12 01:07:05	2020-08-12	2020-08-14	16253	019926575043000	0
f6b64c71-d184-4890-ac45-8661c4dec6981	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-27	HDMUSGWB0586694	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	49		PL000	021-57900391	1	2020-07-20 13:03:42	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-20 13:06:26	2020-07-21	2020-07-21	15	018245894059000	0
f6b64c71-d184-4890-ac45-8661c4dec6982	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-27	HDMUSGWB0586694	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	49		PL001	021-57900391	1	2020-07-20 13:03:42	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-20 13:06:26	2020-07-21	2020-07-21	15	018245894059000	0
f6b64c71-d184-4890-ac45-8661c4dec6983	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-27	HDMUSGWB0586694	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	49		PL004	021-57900391	1	2020-07-20 13:03:42	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-20 13:06:26	2020-07-21	2020-07-21	15	018245894059000	0
2d3f5569-f672-4c76-baa8-948c310c278e1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-27	HDMUSGWB0586694	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	49		PL000	021-57900391	1	2020-07-20 13:47:23	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-07-20 13:48:03	2020-07-23	2020-07-23	11	018245894059000	0
2d3f5569-f672-4c76-baa8-948c310c278e2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-27	HDMUSGWB0586694	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	49		PL001	021-57900391	1	2020-07-20 13:47:23	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-07-20 13:48:03	2020-07-23	2020-07-23	11	018245894059000	0
2d3f5569-f672-4c76-baa8-948c310c278e3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-27	HDMUSGWB0586694	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	49		PL004	021-57900391	1	2020-07-20 13:47:23	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-07-20 13:48:03	2020-07-23	2020-07-23	11	018245894059000	0
a8e9e638-9cb8-411e-aff3-aedeb58fedac1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL000	021-57900391	1	2020-07-20 14:29:31	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-07-20 14:30:15	2020-07-21	2020-07-21	11	018245894059000	0
a8e9e638-9cb8-411e-aff3-aedeb58fedac2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL001	021-57900391	1	2020-07-20 14:29:31	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-07-20 14:30:15	2020-07-21	2020-07-21	11	018245894059000	0
a8e9e638-9cb8-411e-aff3-aedeb58fedac3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL004	021-57900391	1	2020-07-20 14:29:31	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-07-20 14:30:15	2020-07-21	2020-07-21	11	018245894059000	0
7ae4ce09-ac66-424f-a0dd-a4f36b06fb971	JL. RAYA INTI BLOK C4 NO. 3A KAWASAN INDUSTRI BIIE KEL.SUKARESMI, KEC.CIKARANG SELATAN, BEKASI, JAWA BARAT, 17550	2020-07-05	JKTB4224102	MICS STEEL INDONESIA	commercial@mics.co.id	318194040413000	67		PL000	0218972928	1	2020-08-11 10:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 21:43:20	2020-08-11	2020-08-11	67	318194040413000	15
a96402aa-cc8f-4d65-89e5-ca61440990132	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-27	HDMUSGWB0586694	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	49		PL001	021-57900391	1	2020-07-21 09:01:21	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 09:01:52	2020-07-22	2020-07-22	12	018245894059000	0
a96402aa-cc8f-4d65-89e5-ca61440990133	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-27	HDMUSGWB0586694	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	49		PL004	021-57900391	1	2020-07-21 09:01:21	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 09:01:52	2020-07-22	2020-07-22	12	018245894059000	0
4637c27d-7d01-4844-a7f9-0b286fa786441	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL000	021-57900391	1	2020-07-21 10:36:58	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 10:38:22	2020-07-22	2020-07-22	15	018245894059000	0
4637c27d-7d01-4844-a7f9-0b286fa786442	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL001	021-57900391	1	2020-07-21 10:36:58	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 10:38:22	2020-07-22	2020-07-22	15	018245894059000	0
4637c27d-7d01-4844-a7f9-0b286fa786443	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL004	021-57900391	1	2020-07-21 10:36:58	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 10:38:22	2020-07-22	2020-07-22	15	018245894059000	0
d218503c-5921-4cb9-a9db-299bfd1fa7d21	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL000	021-57900391	1	2020-07-21 10:43:26	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-07-21 10:44:20	2020-07-22	2020-07-22	9	018245894059000	0
d218503c-5921-4cb9-a9db-299bfd1fa7d22	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL001	021-57900391	1	2020-07-21 10:43:26	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-07-21 10:44:20	2020-07-22	2020-07-22	9	018245894059000	0
7ae4ce09-ac66-424f-a0dd-a4f36b06fb972	JL. RAYA INTI BLOK C4 NO. 3A KAWASAN INDUSTRI BIIE KEL.SUKARESMI, KEC.CIKARANG SELATAN, BEKASI, JAWA BARAT, 17550	2020-07-05	JKTB4224102	MICS STEEL INDONESIA	commercial@mics.co.id	318194040413000	67		PL001	0218972928	1	2020-08-11 10:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 21:43:20	2020-08-11	2020-08-11	67	318194040413000	15
d218503c-5921-4cb9-a9db-299bfd1fa7d23	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL004	021-57900391	1	2020-07-21 10:43:26	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-07-21 10:44:20	2020-07-22	2020-07-22	9	018245894059000	0
0f3531e3-682d-460d-8466-e0595d12bab41	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL000	021-57900391	1	2020-07-21 12:07:45	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 12:08:31	2020-07-22	2020-07-22	12	018245894059000	0
0f3531e3-682d-460d-8466-e0595d12bab42	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL001	021-57900391	1	2020-07-21 12:07:45	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 12:08:31	2020-07-22	2020-07-22	12	018245894059000	0
0f3531e3-682d-460d-8466-e0595d12bab43	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL004	021-57900391	1	2020-07-21 12:07:45	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 12:08:31	2020-07-22	2020-07-22	12	018245894059000	0
bc3c8e19-8ee5-4d27-b06b-09e5714bf9661	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL000	021-57900391	1	2020-07-21 12:07:45	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 12:09:06	2020-07-22	2020-07-22	12	018245894059000	0
bc3c8e19-8ee5-4d27-b06b-09e5714bf9662	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL001	021-57900391	1	2020-07-21 12:07:45	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 12:09:06	2020-07-22	2020-07-22	12	018245894059000	0
bc3c8e19-8ee5-4d27-b06b-09e5714bf9663	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL004	021-57900391	1	2020-07-21 12:07:45	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 12:09:06	2020-07-22	2020-07-22	12	018245894059000	0
85c7b7d2-66d3-4102-88fe-68916fa12a081	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL000	021-57900391	1	2020-07-21 12:07:45	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 12:10:19	2020-07-22	2020-07-22	12	018245894059000	0
85c7b7d2-66d3-4102-88fe-68916fa12a082	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL001	021-57900391	1	2020-07-21 12:07:45	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 12:10:19	2020-07-22	2020-07-22	12	018245894059000	0
85c7b7d2-66d3-4102-88fe-68916fa12a083	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL004	021-57900391	1	2020-07-21 12:07:45	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-21 12:10:19	2020-07-22	2020-07-22	12	018245894059000	0
1fb77b97-0be8-404e-adb8-a81abea7286e1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL001	021-57900391	2	2020-07-22 11:03:56	Jakarta, Indonesia	-6.2087634	106.845599	2020-07-22 11:04:47	2020-07-27	2020-07-27	9	018245894059000	0
9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a771	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL000	021-57900391	4	2020-07-23 10:47:28	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-23 10:48:16	2020-07-24	2020-07-24	15	018245894059000	0
9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a772	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL001	021-57900391	4	2020-07-23 10:47:28	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-23 10:48:16	2020-07-24	2020-07-24	15	018245894059000	0
9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a773	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL004	021-57900391	4	2020-07-23 10:47:28	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-23 10:48:16	2020-07-24	2020-07-24	15	018245894059000	0
33b34362-4519-4bd3-a6fc-cfd87e40344b1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL000	021-57900391	4	2020-07-23 10:47:28	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-23 10:48:50	2020-07-24	2020-07-24	15	018245894059000	0
33b34362-4519-4bd3-a6fc-cfd87e40344b2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL001	021-57900391	4	2020-07-23 10:47:28	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-23 10:48:50	2020-07-24	2020-07-24	15	018245894059000	0
33b34362-4519-4bd3-a6fc-cfd87e40344b3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-13	HDMUSGWB0585372	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	39		PL004	021-57900391	4	2020-07-23 10:47:28	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-07-23 10:48:50	2020-07-24	2020-07-24	15	018245894059000	0
7ae4ce09-ac66-424f-a0dd-a4f36b06fb973	JL. RAYA INTI BLOK C4 NO. 3A KAWASAN INDUSTRI BIIE KEL.SUKARESMI, KEC.CIKARANG SELATAN, BEKASI, JAWA BARAT, 17550	2020-07-05	JKTB4224102	MICS STEEL INDONESIA	commercial@mics.co.id	318194040413000	67		PL004	0218972928	1	2020-08-11 10:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 21:43:20	2020-08-11	2020-08-11	67	318194040413000	15
c43eb4a0-a163-4c4d-a649-d0146dbd545e3	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL004	021-45854360	2	2020-08-12 00:55:55	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-12 01:07:05	2020-08-12	2020-08-14	16253	019926575043000	0
a9b2b129-621d-4124-a3ae-5603179719f51	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	2	2020-08-12 11:38:46	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 11:42:18	2020-08-13	2020-08-13	\N	018245894059000	0
a9b2b129-621d-4124-a3ae-5603179719f52	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	2	2020-08-12 11:38:46	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 11:42:18	2020-08-13	2020-08-13	\N	018245894059000	0
a9b2b129-621d-4124-a3ae-5603179719f53	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	2	2020-08-12 11:38:46	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 11:42:18	2020-08-13	2020-08-13	\N	018245894059000	0
609d6724-bf89-4019-9974-e9377026880e1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-12 20:41:40	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 20:45:39	2020-08-12	2020-08-12	38	018245894059000	0
609d6724-bf89-4019-9974-e9377026880e2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-12 20:41:40	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 20:45:39	2020-08-12	2020-08-12	38	018245894059000	0
609d6724-bf89-4019-9974-e9377026880e3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	1	2020-08-12 20:41:40	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 20:45:39	2020-08-12	2020-08-12	38	018245894059000	0
609d6724-bf89-4019-9974-e9377026880e4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-12 20:41:40	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 20:45:39	2020-08-12	2020-08-12	38	018245894059000	0
e04cf392-6415-4410-8229-fd1e47189b1a1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-12 20:41:40	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 20:46:34	2020-08-12	2020-08-12	38	018245894059000	0
e04cf392-6415-4410-8229-fd1e47189b1a2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-12 20:41:40	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 20:46:34	2020-08-12	2020-08-12	38	018245894059000	0
e04cf392-6415-4410-8229-fd1e47189b1a3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	1	2020-08-12 20:41:40	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 20:46:34	2020-08-12	2020-08-12	38	018245894059000	0
e04cf392-6415-4410-8229-fd1e47189b1a4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-12 20:41:40	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 20:46:34	2020-08-12	2020-08-12	38	018245894059000	0
324bff81-6a51-4411-83ca-b7a358904ceb1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-13 09:57:24	TPK Koja, Karantina Pertanian, RW.1, North Jakarta City, Jakarta, Indonesia	-6.103061599999999	106.9036611	2020-08-13 10:04:03	2020-08-14	2020-08-14	37	018245894059000	15
324bff81-6a51-4411-83ca-b7a358904ceb2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-13 09:57:24	TPK Koja, Karantina Pertanian, RW.1, North Jakarta City, Jakarta, Indonesia	-6.103061599999999	106.9036611	2020-08-13 10:04:03	2020-08-14	2020-08-14	37	018245894059000	15
324bff81-6a51-4411-83ca-b7a358904ceb3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-13 09:57:24	TPK Koja, Karantina Pertanian, RW.1, North Jakarta City, Jakarta, Indonesia	-6.103061599999999	106.9036611	2020-08-13 10:04:03	2020-08-14	2020-08-14	37	018245894059000	15
5a7693cb-af7f-4243-aa21-d02bb2448f373	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 10:18:13	Tanjung Pasir, Tangerang, Banten, Indonesia	-6.026038100000001	106.6615529	2020-08-13 10:21:50	2020-08-14	2020-08-15	1387	018245894059000	0
cd224b4c-54e7-4f6a-94dd-d7163cb6a67f1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 13:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:36:50	2020-08-15	2020-08-15	18	018245894059000	0
cd224b4c-54e7-4f6a-94dd-d7163cb6a67f2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 13:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:36:50	2020-08-15	2020-08-15	18	018245894059000	0
25f443a1-3ba4-40e2-94c8-ad1c7e68a61a1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL000	021-57900391	1	2020-08-04 15:57:44	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-04 15:59:31	2020-08-05	2020-08-05	15	018245894059000	0
25f443a1-3ba4-40e2-94c8-ad1c7e68a61a2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL001	021-57900391	1	2020-08-04 15:57:44	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-04 15:59:31	2020-08-05	2020-08-05	15	018245894059000	0
25f443a1-3ba4-40e2-94c8-ad1c7e68a61a3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL004	021-57900391	1	2020-08-04 15:57:44	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-04 15:59:31	2020-08-05	2020-08-05	15	018245894059000	0
bd105723-8e85-4582-b795-172f30d86c741	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL000	021-57900391	1	2020-08-04 16:02:58	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-04 16:04:52	2020-08-05	2020-08-05	9	018245894059000	0
bd105723-8e85-4582-b795-172f30d86c742	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL001	021-57900391	1	2020-08-04 16:02:58	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-04 16:04:52	2020-08-05	2020-08-05	9	018245894059000	0
bd105723-8e85-4582-b795-172f30d86c743	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL004	021-57900391	1	2020-08-04 16:02:58	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-04 16:04:52	2020-08-05	2020-08-05	9	018245894059000	0
57832f2d-57b8-421a-a940-771be30c0a241	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL000	021-57900391	1	2020-08-04 16:02:58	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-04 16:05:37	2020-08-05	2020-08-05	9	018245894059000	0
57832f2d-57b8-421a-a940-771be30c0a242	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL001	021-57900391	1	2020-08-04 16:02:58	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-04 16:05:37	2020-08-05	2020-08-05	9	018245894059000	0
57832f2d-57b8-421a-a940-771be30c0a243	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL004	021-57900391	1	2020-08-04 16:02:58	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-04 16:05:37	2020-08-05	2020-08-05	9	018245894059000	0
1506a286-d5cf-4ee0-b8ce-54384e0cad401	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	2	2020-08-12 11:38:46	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 11:42:34	2020-08-13	2020-08-13	\N	018245894059000	0
1506a286-d5cf-4ee0-b8ce-54384e0cad402	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	2	2020-08-12 11:38:46	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 11:42:34	2020-08-13	2020-08-13	\N	018245894059000	0
1506a286-d5cf-4ee0-b8ce-54384e0cad403	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	2	2020-08-12 11:38:46	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 11:42:34	2020-08-13	2020-08-13	\N	018245894059000	0
130271c3-e6b8-4b5d-b7cc-22e112030afa1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-12 21:30:34	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 21:31:38	2020-08-12	2020-08-13	18	018245894059000	0
130271c3-e6b8-4b5d-b7cc-22e112030afa2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-12 21:30:34	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 21:31:38	2020-08-12	2020-08-13	18	018245894059000	0
130271c3-e6b8-4b5d-b7cc-22e112030afa3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-12 21:30:34	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 21:31:38	2020-08-12	2020-08-13	18	018245894059000	0
ca293462-4373-4cc3-98ac-c4a4f9aa9fa91	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-13 10:04:41	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-08-13 10:07:31	2020-08-14	2020-08-14	18	018245894059000	0
ca293462-4373-4cc3-98ac-c4a4f9aa9fa92	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-13 10:04:41	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-08-13 10:07:31	2020-08-14	2020-08-14	18	018245894059000	0
0ab7f929-0295-4a27-b49d-e2ab8df8db302	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL001	021-57900391	5	2020-08-14 08:20:40	Cengkareng, West Jakarta City, Jakarta, Indonesia	\N	\N	2020-08-14 08:23:26	2020-08-15	2020-08-15	0	018245894059000	0
aa0fd7e0-843b-4da6-84a1-799065767d1c1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL000	021-57900391	1	2020-08-04 16:12:28	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-04 16:12:57	2020-08-05	2020-08-05	15	018245894059000	0
aa0fd7e0-843b-4da6-84a1-799065767d1c2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL001	021-57900391	1	2020-08-04 16:12:28	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-04 16:12:57	2020-08-05	2020-08-05	15	018245894059000	0
aa0fd7e0-843b-4da6-84a1-799065767d1c3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL004	021-57900391	1	2020-08-04 16:12:28	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-04 16:12:57	2020-08-05	2020-08-05	15	018245894059000	0
d18d4675-3a3c-4ad1-a635-f41a73a9a8c51	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL000	021-57900391	1	2020-08-04 20:18:21	Jakarta, Indonesia	-6.2087634	106.845599	2020-08-04 20:18:49	2020-08-24	2020-08-30	9	018245894059000	0
d18d4675-3a3c-4ad1-a635-f41a73a9a8c52	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL001	021-57900391	1	2020-08-04 20:18:21	Jakarta, Indonesia	-6.2087634	106.845599	2020-08-04 20:18:49	2020-08-24	2020-08-30	9	018245894059000	0
d18d4675-3a3c-4ad1-a635-f41a73a9a8c53	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL004	021-57900391	1	2020-08-04 20:18:21	Jakarta, Indonesia	-6.2087634	106.845599	2020-08-04 20:18:49	2020-08-24	2020-08-30	9	018245894059000	0
fa909fc7-baef-47c1-b591-e8361ed119111	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL000	021-57900391	1	2020-08-05 09:17:39	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-05 09:18:21	2020-08-06	2020-08-06	15	018245894059000	0
fa909fc7-baef-47c1-b591-e8361ed119112	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL001	021-57900391	1	2020-08-05 09:17:39	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-05 09:18:21	2020-08-06	2020-08-06	15	018245894059000	0
fa909fc7-baef-47c1-b591-e8361ed119113	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL004	021-57900391	1	2020-08-05 09:17:39	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-05 09:18:21	2020-08-06	2020-08-06	15	018245894059000	0
fbaf7158-5968-4411-aba8-15517a8eeea91	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	2	2020-08-12 11:10:19	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 11:43:39	2020-08-13	2020-08-13	37	018245894059000	0
fbaf7158-5968-4411-aba8-15517a8eeea92	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	2	2020-08-12 11:10:19	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 11:43:39	2020-08-13	2020-08-13	37	018245894059000	0
3c94af28-9d71-4bd0-a20c-df95fbbfebd32	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL001	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:35:53	2020-08-06	2020-08-06	96	018245894059000	0
fbaf7158-5968-4411-aba8-15517a8eeea93	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	2	2020-08-12 11:10:19	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 11:43:39	2020-08-13	2020-08-13	37	018245894059000	0
fadbf713-7af9-4c0a-af44-5fc929a7f9dc1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:04:11	2020-08-12	2020-08-14	11	018245894059000	0
fadbf713-7af9-4c0a-af44-5fc929a7f9dc2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:04:11	2020-08-12	2020-08-14	11	018245894059000	0
fadbf713-7af9-4c0a-af44-5fc929a7f9dc3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:04:11	2020-08-12	2020-08-14	11	018245894059000	0
ca293462-4373-4cc3-98ac-c4a4f9aa9fa93	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-13 10:04:41	Koja District Hospital, Jalan Deli, RT.11/RW.7, Koja, North Jakarta City, Jakarta, Indonesia	-6.1087216	106.8999828	2020-08-13 10:07:31	2020-08-14	2020-08-14	18	018245894059000	0
cd224b4c-54e7-4f6a-94dd-d7163cb6a67f3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 13:00:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:36:50	2020-08-15	2020-08-15	18	018245894059000	0
a26e195e-d92f-4872-865c-9a94741a54131	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL000	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:44:11	2020-08-06	2020-08-06	96	018245894059000	0
a26e195e-d92f-4872-865c-9a94741a54132	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL001	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:44:11	2020-08-06	2020-08-06	96	018245894059000	0
a26e195e-d92f-4872-865c-9a94741a54133	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL004	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:44:11	2020-08-06	2020-08-06	96	018245894059000	0
9d16f90b-0dd2-4cf2-8b0c-3443bf1c37611	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL000	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:45:25	2020-08-06	2020-08-06	96	018245894059000	0
9d16f90b-0dd2-4cf2-8b0c-3443bf1c37612	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL001	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:45:25	2020-08-06	2020-08-06	96	018245894059000	0
9d16f90b-0dd2-4cf2-8b0c-3443bf1c37613	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL004	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:45:25	2020-08-06	2020-08-06	96	018245894059000	0
7c68b67b-c627-4d77-b671-6c3cf48930601	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL000	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:47:58	2020-08-06	2020-08-06	96	018245894059000	0
7c68b67b-c627-4d77-b671-6c3cf48930602	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL001	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:47:58	2020-08-06	2020-08-06	96	018245894059000	0
7c68b67b-c627-4d77-b671-6c3cf48930603	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL004	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:47:58	2020-08-06	2020-08-06	96	018245894059000	0
bfefd7de-9a0d-4ac5-a828-f16af511b01a1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL000	021-57900391	1	2020-08-11 11:09:05	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-11 14:54:59	2020-08-11	2020-08-11	11	018245894059000	0
bfefd7de-9a0d-4ac5-a828-f16af511b01a2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL001	021-57900391	1	2020-08-11 11:09:05	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-11 14:54:59	2020-08-11	2020-08-11	11	018245894059000	0
bfefd7de-9a0d-4ac5-a828-f16af511b01a3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL004	021-57900391	1	2020-08-11 11:09:05	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-11 14:54:59	2020-08-11	2020-08-11	11	018245894059000	0
4178de6b-a02f-4129-8174-fa372fefa7dd1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-17	CKCOKAN0008792	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	76		PL000	021-57900391	1	2020-08-12 09:31:22	Tanjung Priok, Kota Jakarta Utara, Daerah Khusus Ibukota Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 09:32:27	2020-08-12	2020-08-12	23	018245894059000	0
4178de6b-a02f-4129-8174-fa372fefa7dd2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-17	CKCOKAN0008792	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	76		PL001	021-57900391	1	2020-08-12 09:31:22	Tanjung Priok, Kota Jakarta Utara, Daerah Khusus Ibukota Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 09:32:27	2020-08-12	2020-08-12	23	018245894059000	0
4178de6b-a02f-4129-8174-fa372fefa7dd3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-17	CKCOKAN0008792	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	76		PL004	021-57900391	1	2020-08-12 09:31:22	Tanjung Priok, Kota Jakarta Utara, Daerah Khusus Ibukota Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 09:32:27	2020-08-12	2020-08-12	23	018245894059000	0
7dfbd8ec-d4c2-4e0e-a97c-5b7ada7a873f1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-12 15:31:06	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 15:43:58	2020-08-12	2020-08-12	18	018245894059000	0
7dfbd8ec-d4c2-4e0e-a97c-5b7ada7a873f2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-12 15:31:06	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 15:43:58	2020-08-12	2020-08-12	18	018245894059000	0
0ab7f929-0295-4a27-b49d-e2ab8df8db301	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL000	021-57900391	5	2020-08-14 08:20:40	Cengkareng, West Jakarta City, Jakarta, Indonesia	\N	\N	2020-08-14 08:23:26	2020-08-15	2020-08-15	0	018245894059000	0
210a9825-32e3-4f41-a87c-84ff4b3aa9301	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL000	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:46:01	2020-08-06	2020-08-06	96	018245894059000	0
210a9825-32e3-4f41-a87c-84ff4b3aa9302	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL001	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:46:01	2020-08-06	2020-08-06	96	018245894059000	0
210a9825-32e3-4f41-a87c-84ff4b3aa9303	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL004	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:46:01	2020-08-06	2020-08-06	96	018245894059000	0
e0324ebe-959a-4507-aa3f-b3259b54a2d51	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL000	021-57900391	1	2020-08-11 15:11:13	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-11 15:11:42	2020-08-11	2020-08-11	11	018245894059000	0
e0324ebe-959a-4507-aa3f-b3259b54a2d52	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL001	021-57900391	1	2020-08-11 15:11:13	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-11 15:11:42	2020-08-11	2020-08-11	11	018245894059000	0
e0324ebe-959a-4507-aa3f-b3259b54a2d53	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL004	021-57900391	1	2020-08-11 15:11:13	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-11 15:11:42	2020-08-11	2020-08-11	11	018245894059000	0
7b60cd9f-bb76-4469-a0f0-2bd090349edd1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL000	021-57900391	1	2020-08-11 15:12:09	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-11 15:13:09	2020-08-11	2020-08-11	11	018245894059000	0
7b60cd9f-bb76-4469-a0f0-2bd090349edd2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL001	021-57900391	1	2020-08-11 15:12:09	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-11 15:13:09	2020-08-11	2020-08-11	11	018245894059000	0
7b60cd9f-bb76-4469-a0f0-2bd090349edd3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL004	021-57900391	1	2020-08-11 15:12:09	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-11 15:13:09	2020-08-11	2020-08-11	11	018245894059000	0
64bb8e57-bae7-42d5-bee0-58b1d9082f8c1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-17	CKCOKAN0008792	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	76		PL000	021-57900391	1	2020-08-12 09:47:24	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-12 09:48:12	2020-08-12	2020-08-13	68	018245894059000	0
64bb8e57-bae7-42d5-bee0-58b1d9082f8c2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-17	CKCOKAN0008792	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	76		PL001	021-57900391	1	2020-08-12 09:47:24	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-12 09:48:12	2020-08-12	2020-08-13	68	018245894059000	0
64bb8e57-bae7-42d5-bee0-58b1d9082f8c3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-17	CKCOKAN0008792	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	76		PL004	021-57900391	1	2020-08-12 09:47:24	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-12 09:48:12	2020-08-12	2020-08-13	68	018245894059000	0
7dfbd8ec-d4c2-4e0e-a97c-5b7ada7a873f3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-12 15:31:06	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 15:43:58	2020-08-12	2020-08-12	18	018245894059000	0
9d96bcbb-b0b0-4c57-afff-e7bb2c71c0021	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:04:36	2020-08-12	2020-08-14	11	018245894059000	0
9d96bcbb-b0b0-4c57-afff-e7bb2c71c0022	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:04:36	2020-08-12	2020-08-14	11	018245894059000	0
9d96bcbb-b0b0-4c57-afff-e7bb2c71c0023	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:04:36	2020-08-12	2020-08-14	11	018245894059000	0
630969ec-d774-4a38-870d-a9bd1cbb41801	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-13 10:10:37	Terminal Tanjung priuk, Lorong X Timur, RT.6/RW.2, Koja, North Jakarta City, Jakarta, Indonesia	-6.1100568	106.8934459	2020-08-13 10:11:17	2020-08-15	2020-08-15	37	018245894059000	0
630969ec-d774-4a38-870d-a9bd1cbb41802	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-13 10:10:37	Terminal Tanjung priuk, Lorong X Timur, RT.6/RW.2, Koja, North Jakarta City, Jakarta, Indonesia	-6.1100568	106.8934459	2020-08-13 10:11:17	2020-08-15	2020-08-15	37	018245894059000	0
099565f0-3dfa-442b-b81a-695eba3d6a9a1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-17	CKCOKAN0008792	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	76		PL000	021-57900391	1	2020-08-12 10:27:41	Tanjung Priok, Kota Jakarta Utara, Daerah Khusus Ibukota Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 10:28:14	2020-08-12	2020-08-12	9	018245894059000	0
099565f0-3dfa-442b-b81a-695eba3d6a9a2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-17	CKCOKAN0008792	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	76		PL001	021-57900391	1	2020-08-12 10:27:41	Tanjung Priok, Kota Jakarta Utara, Daerah Khusus Ibukota Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 10:28:14	2020-08-12	2020-08-12	9	018245894059000	0
099565f0-3dfa-442b-b81a-695eba3d6a9a3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-17	CKCOKAN0008792	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	76		PL004	021-57900391	1	2020-08-12 10:27:41	Tanjung Priok, Kota Jakarta Utara, Daerah Khusus Ibukota Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 10:28:14	2020-08-12	2020-08-12	9	018245894059000	0
5d690a8a-90e8-4b89-b173-5071987732ba1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL000	021-57900391	1	2020-08-06 17:05:37	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-06 17:06:33	2020-08-07	2020-08-07	15	018245894059000	0
5d690a8a-90e8-4b89-b173-5071987732ba2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL001	021-57900391	1	2020-08-06 17:05:37	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-06 17:06:33	2020-08-07	2020-08-07	15	018245894059000	0
5d690a8a-90e8-4b89-b173-5071987732ba3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL004	021-57900391	1	2020-08-06 17:05:37	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-06 17:06:33	2020-08-07	2020-08-07	15	018245894059000	0
05f04fe5-46db-450d-a518-ad76f206c7a71	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-17	CKCOKAN0008792	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	76		PL000	021-57900391	1	2020-08-12 10:48:20	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 10:49:20	2020-08-12	2020-08-12	16	018245894059000	0
077819e9-0885-4a1e-9cda-3acaab728ee62	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL001	021-57900391	1	2020-08-07 07:15:26	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-07 07:16:12	2020-08-08	2020-08-08	23	018245894059000	0
077819e9-0885-4a1e-9cda-3acaab728ee63	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL004	021-57900391	1	2020-08-07 07:15:26	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-07 07:16:12	2020-08-08	2020-08-08	23	018245894059000	0
b75ad664-77fa-4939-99fd-bac15b2dda5a1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL000	021-57900391	1	2020-08-07 07:15:26	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-07 07:16:31	2020-08-08	2020-08-08	23	018245894059000	0
b75ad664-77fa-4939-99fd-bac15b2dda5a2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL001	021-57900391	1	2020-08-07 07:15:26	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-07 07:16:31	2020-08-08	2020-08-08	23	018245894059000	0
b75ad664-77fa-4939-99fd-bac15b2dda5a3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-02	ONEYTY8AM2768800	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	50		PL004	021-57900391	1	2020-08-07 07:15:26	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-07 07:16:31	2020-08-08	2020-08-08	23	018245894059000	0
7bc9e596-dd33-4adb-81f9-6f5dc5ef1c2b1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-01-06	SAB139732JKR	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	58	018245894059000	PL000	021-57900391	1	2020-08-08 13:34:27	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-08 13:35:20	2020-08-08	2020-08-09	97	018245894059000	0
7bc9e596-dd33-4adb-81f9-6f5dc5ef1c2b2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-01-06	SAB139732JKR	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	58	018245894059000	PL001	021-57900391	1	2020-08-08 13:34:27	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-08 13:35:20	2020-08-08	2020-08-09	97	018245894059000	0
7bc9e596-dd33-4adb-81f9-6f5dc5ef1c2b3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-01-06	SAB139732JKR	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	58	018245894059000	PL004	021-57900391	1	2020-08-08 13:34:27	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-08 13:35:20	2020-08-08	2020-08-09	97	018245894059000	0
05f04fe5-46db-450d-a518-ad76f206c7a72	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-17	CKCOKAN0008792	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	76		PL001	021-57900391	1	2020-08-12 10:48:20	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 10:49:20	2020-08-12	2020-08-12	16	018245894059000	0
05f04fe5-46db-450d-a518-ad76f206c7a73	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-17	CKCOKAN0008792	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	76		PL004	021-57900391	1	2020-08-12 10:48:20	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 10:49:20	2020-08-12	2020-08-12	16	018245894059000	0
3eeeb4e2-499d-479d-ba8a-34064bf9abc91	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-12 16:01:05	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 16:01:43	2020-08-12	2020-08-12	9	018245894059000	0
0b98a203-a5a5-48b8-97b8-a01ab7066ca91	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-01-06	SAB139732JKR	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	58	018245894059000	PL000	021-57900391	1	2020-08-08 13:41:31	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-08 13:42:24	2020-08-08	2020-08-08	14	018245894059000	0
0b98a203-a5a5-48b8-97b8-a01ab7066ca92	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-01-06	SAB139732JKR	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	58	018245894059000	PL001	021-57900391	1	2020-08-08 13:41:31	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-08 13:42:24	2020-08-08	2020-08-08	14	018245894059000	0
0b98a203-a5a5-48b8-97b8-a01ab7066ca93	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-01-06	SAB139732JKR	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	58	018245894059000	PL004	021-57900391	1	2020-08-08 13:41:31	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306119	106.956266	2020-08-08 13:42:24	2020-08-08	2020-08-08	14	018245894059000	0
4be64d2d-950c-44ab-9eac-cb24aed1ff8e1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-01-06	SAB139732JKR	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	58	018245894059000	PL000	021-57900391	1	2020-08-08 13:34:27	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-08 13:42:58	2020-08-08	2020-08-09	97	018245894059000	0
4be64d2d-950c-44ab-9eac-cb24aed1ff8e2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-01-06	SAB139732JKR	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	58	018245894059000	PL001	021-57900391	1	2020-08-08 13:34:27	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-08 13:42:58	2020-08-08	2020-08-09	97	018245894059000	0
4be64d2d-950c-44ab-9eac-cb24aed1ff8e3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-01-06	SAB139732JKR	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	58	018245894059000	PL004	021-57900391	1	2020-08-08 13:34:27	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-08 13:42:58	2020-08-08	2020-08-09	97	018245894059000	0
logolTest2Booking104	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-01-06	SAB139732JKR	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	58	018245894059000	PL004	021-57900391	1	2020-08-08 13:34:27	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-08 13:42:58	2020-08-08	2020-08-09	97	018245894059000	0
3c94af28-9d71-4bd0-a20c-df95fbbfebd33	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL004	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:35:53	2020-08-06	2020-08-06	96	018245894059000	0
8b0dbcd5-a60b-44b1-bb92-a3f33b3f2c171	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL000	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:41:01	2020-08-06	2020-08-06	96	018245894059000	0
8b0dbcd5-a60b-44b1-bb92-a3f33b3f2c172	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL001	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:41:01	2020-08-06	2020-08-06	96	018245894059000	0
8b0dbcd5-a60b-44b1-bb92-a3f33b3f2c173	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL004	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:41:01	2020-08-06	2020-08-06	96	018245894059000	0
1fceb782-b52b-46ac-a475-1a272d04a3bf1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL000	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:43:28	2020-08-06	2020-08-06	96	018245894059000	0
1fceb782-b52b-46ac-a475-1a272d04a3bf2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL001	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:43:28	2020-08-06	2020-08-06	96	018245894059000	0
1fceb782-b52b-46ac-a475-1a272d04a3bf3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL004	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:43:28	2020-08-06	2020-08-06	96	018245894059000	0
65ba261d-1638-4146-ac5b-c050775bd0601	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL000	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:46:59	2020-08-06	2020-08-06	96	018245894059000	0
65ba261d-1638-4146-ac5b-c050775bd0602	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL001	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:46:59	2020-08-06	2020-08-06	96	018245894059000	0
65ba261d-1638-4146-ac5b-c050775bd0603	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL004	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:46:59	2020-08-06	2020-08-06	96	018245894059000	0
723299d0-6747-4b0e-b481-d1a35a89fe881	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL000	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:48:49	2020-08-06	2020-08-06	96	018245894059000	0
723299d0-6747-4b0e-b481-d1a35a89fe882	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL001	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:48:49	2020-08-06	2020-08-06	96	018245894059000	0
723299d0-6747-4b0e-b481-d1a35a89fe883	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-06-04	HLCUSIN200533094	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	36		PL004	021-57900391	1	2020-08-06 10:34:55	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-06 10:48:49	2020-08-06	2020-08-06	96	018245894059000	0
logolTest2Booking105	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-01-06	SAB139732JKR	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	58	018245894059000	PL004	021-57900391	1	2020-08-08 13:34:27	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-08 13:42:58	2020-08-08	2020-08-09	97	018245894059000	0
4d693fca-bd52-4aca-baea-f5578b6d6f761	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL000	021-57900391	1	2020-08-09 20:11:43	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-09 20:12:15	2020-08-09	2020-08-09	96	018245894059000	0
4d693fca-bd52-4aca-baea-f5578b6d6f762	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL001	021-57900391	1	2020-08-09 20:11:43	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-09 20:12:15	2020-08-09	2020-08-09	96	018245894059000	0
4d693fca-bd52-4aca-baea-f5578b6d6f763	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL004	021-57900391	1	2020-08-09 20:11:43	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-09 20:12:15	2020-08-09	2020-08-09	96	018245894059000	0
debeec10-2b9f-4f75-92a6-59404c83bac61	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL000	021-57900391	1	2020-08-09 20:21:48	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-09 20:22:11	2020-08-09	2020-08-09	52	018245894059000	0
debeec10-2b9f-4f75-92a6-59404c83bac62	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL001	021-57900391	1	2020-08-09 20:21:48	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-09 20:22:11	2020-08-09	2020-08-09	52	018245894059000	0
debeec10-2b9f-4f75-92a6-59404c83bac63	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL004	021-57900391	1	2020-08-09 20:21:48	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-09 20:22:11	2020-08-09	2020-08-09	52	018245894059000	0
b4c7b959-182a-4635-872f-4f1cd84c0ce71	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL000	021-57900391	1	2020-08-09 21:25:01	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-09 21:25:27	2020-08-09	2020-08-09	16	018245894059000	0
b4c7b959-182a-4635-872f-4f1cd84c0ce72	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL001	021-57900391	1	2020-08-09 21:25:01	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-09 21:25:27	2020-08-09	2020-08-09	16	018245894059000	0
b4c7b959-182a-4635-872f-4f1cd84c0ce73	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL004	021-57900391	1	2020-08-09 21:25:01	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-09 21:25:27	2020-08-09	2020-08-09	16	018245894059000	0
0b4a223f-fa95-439d-bdd9-069ee6e2be951	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL000	021-57900391	1	2020-08-10 08:34:56	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-10 08:35:17	2020-08-10	2020-08-10	7	018245894059000	0
0b4a223f-fa95-439d-bdd9-069ee6e2be952	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL001	021-57900391	1	2020-08-10 08:34:56	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-10 08:35:17	2020-08-10	2020-08-10	7	018245894059000	0
0b4a223f-fa95-439d-bdd9-069ee6e2be953	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL004	021-57900391	1	2020-08-10 08:34:56	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-10 08:35:17	2020-08-10	2020-08-10	7	018245894059000	0
aaf22fd8-b72a-46c4-a3d7-73e2affa38ec1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL000	021-57900391	1	2020-08-10 09:32:54	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-10 09:33:35	2020-08-10	2020-08-10	7	018245894059000	0
aaf22fd8-b72a-46c4-a3d7-73e2affa38ec2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL001	021-57900391	1	2020-08-10 09:32:54	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-10 09:33:35	2020-08-10	2020-08-10	7	018245894059000	0
aaf22fd8-b72a-46c4-a3d7-73e2affa38ec3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL004	021-57900391	1	2020-08-10 09:32:54	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-10 09:33:35	2020-08-10	2020-08-10	7	018245894059000	0
d1ca6c91-6434-48b8-968a-6d937c0903001	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL000	021-57900391	1	2020-08-10 09:49:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 09:50:01	2020-08-15	2020-08-15	11	018245894059000	0
d1ca6c91-6434-48b8-968a-6d937c0903002	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL001	021-57900391	1	2020-08-10 09:49:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 09:50:01	2020-08-15	2020-08-15	11	018245894059000	0
d1ca6c91-6434-48b8-968a-6d937c0903003	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-26	02PENJKT2007016	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	65		PL004	021-57900391	1	2020-08-10 09:49:00	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-10 09:50:01	2020-08-15	2020-08-15	11	018245894059000	0
c1b5a2f2-7e0e-42fe-84b7-10e87ccabba61	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL000	021-57900391	2	2020-08-12 10:02:55	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 10:43:33	2020-08-12	2020-08-13	34	018245894059000	0
c1b5a2f2-7e0e-42fe-84b7-10e87ccabba62	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL001	021-57900391	2	2020-08-12 10:02:55	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 10:43:33	2020-08-12	2020-08-13	34	018245894059000	0
c1b5a2f2-7e0e-42fe-84b7-10e87ccabba63	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL004	021-57900391	2	2020-08-12 10:02:55	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-12 10:43:33	2020-08-12	2020-08-13	34	018245894059000	0
3eeeb4e2-499d-479d-ba8a-34064bf9abc92	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-12 16:01:05	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 16:01:43	2020-08-12	2020-08-12	9	018245894059000	0
3eeeb4e2-499d-479d-ba8a-34064bf9abc93	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-12 16:01:05	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-12 16:01:43	2020-08-12	2020-08-12	9	018245894059000	0
5ad42164-30e0-477e-bca0-53df257c85771	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:24:39	2020-08-12	2020-08-14	11	018245894059000	0
5ad42164-30e0-477e-bca0-53df257c85772	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:24:39	2020-08-12	2020-08-14	11	018245894059000	0
5ad42164-30e0-477e-bca0-53df257c85773	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:24:39	2020-08-12	2020-08-14	11	018245894059000	0
8311f5c9-e80f-4888-bdb7-1ed85dfd889a1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:25:47	2020-08-12	2020-08-14	11	018245894059000	0
8311f5c9-e80f-4888-bdb7-1ed85dfd889a2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:25:47	2020-08-12	2020-08-14	11	018245894059000	0
8311f5c9-e80f-4888-bdb7-1ed85dfd889a3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-12 21:39:54	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-12 22:25:47	2020-08-12	2020-08-14	11	018245894059000	0
630969ec-d774-4a38-870d-a9bd1cbb41803	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-13 10:10:37	Terminal Tanjung priuk, Lorong X Timur, RT.6/RW.2, Koja, North Jakarta City, Jakarta, Indonesia	-6.1100568	106.8934459	2020-08-13 10:11:17	2020-08-15	2020-08-15	37	018245894059000	0
6bc3fd96-267c-46f7-953d-85b068c3d6291	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:50:31	2020-08-13	2020-08-13	11	018245894059000	0
6bc3fd96-267c-46f7-953d-85b068c3d6292	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:50:31	2020-08-13	2020-08-13	11	018245894059000	0
6bc3fd96-267c-46f7-953d-85b068c3d6293	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:50:31	2020-08-13	2020-08-13	11	018245894059000	0
c128f8e8-5519-4373-898d-f23a6125a4521	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:51:30	2020-08-13	2020-08-13	11	018245894059000	0
c128f8e8-5519-4373-898d-f23a6125a4522	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:51:30	2020-08-13	2020-08-13	11	018245894059000	0
c128f8e8-5519-4373-898d-f23a6125a4523	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:51:30	2020-08-13	2020-08-13	11	018245894059000	0
fd08da2d-ce43-4d99-92f7-153b732b79301	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:52:33	2020-08-13	2020-08-13	11	018245894059000	0
fd08da2d-ce43-4d99-92f7-153b732b79302	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:52:33	2020-08-13	2020-08-13	11	018245894059000	0
fd08da2d-ce43-4d99-92f7-153b732b79303	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:52:33	2020-08-13	2020-08-13	11	018245894059000	0
3d384c0c-574a-4c00-a918-9e76e59fad8c1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:56:07	2020-08-13	2020-08-13	11	018245894059000	0
3d384c0c-574a-4c00-a918-9e76e59fad8c2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:56:07	2020-08-13	2020-08-13	11	018245894059000	0
3d384c0c-574a-4c00-a918-9e76e59fad8c3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 10:56:07	2020-08-13	2020-08-13	11	018245894059000	0
393deb31-8169-4cf2-8ba9-ec90f20dcdc11	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 11:01:39	2020-08-13	2020-08-13	11	018245894059000	0
393deb31-8169-4cf2-8ba9-ec90f20dcdc12	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 11:01:39	2020-08-13	2020-08-13	11	018245894059000	0
393deb31-8169-4cf2-8ba9-ec90f20dcdc13	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 10:47:41	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 11:01:39	2020-08-13	2020-08-13	11	018245894059000	0
1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 10:47:41	JICT 2, Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 11:08:59	2020-08-13	2020-08-13	11	018245894059000	0
1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 10:47:41	JICT 2, Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 11:08:59	2020-08-13	2020-08-13	11	018245894059000	0
1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 10:47:41	JICT 2, Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-13 11:08:59	2020-08-13	2020-08-13	11	018245894059000	0
adc33ecb-2c20-414c-b33d-8578e66c3f8e1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 10:47:41	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-13 11:09:39	2020-08-13	2020-08-13	10	018245894059000	0
adc33ecb-2c20-414c-b33d-8578e66c3f8e2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 10:47:41	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-13 11:09:39	2020-08-13	2020-08-13	10	018245894059000	0
adc33ecb-2c20-414c-b33d-8578e66c3f8e3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 10:47:41	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-13 11:09:39	2020-08-13	2020-08-13	10	018245894059000	0
3bb5f3cf-f3df-4bc1-b885-3bcd03f113501	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 13:00:00	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-13 11:25:08	2020-08-15	2020-08-15	19	018245894059000	0
3bb5f3cf-f3df-4bc1-b885-3bcd03f113502	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 13:00:00	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-13 11:25:08	2020-08-15	2020-08-15	19	018245894059000	0
3bb5f3cf-f3df-4bc1-b885-3bcd03f113503	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 13:00:00	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-13 11:25:08	2020-08-15	2020-08-15	19	018245894059000	0
675ebb61-bdde-4195-be83-365448ab00de1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 12:07:13	JICT 2, Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1057443	106.8864882	2020-08-13 12:09:33	2020-08-13	2020-08-13	11	018245894059000	0
675ebb61-bdde-4195-be83-365448ab00de2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 12:07:13	JICT 2, Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1057443	106.8864882	2020-08-13 12:09:33	2020-08-13	2020-08-13	11	018245894059000	0
675ebb61-bdde-4195-be83-365448ab00de3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 12:07:13	JICT 2, Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1057443	106.8864882	2020-08-13 12:09:33	2020-08-13	2020-08-13	11	018245894059000	0
13a77a62-81c5-4aef-a0cc-f9fe4da444131	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL000	021-45854360	1	2020-08-13 12:55:41	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-13 12:57:23	2020-08-13	2020-08-14	95	019926575043000	0
13a77a62-81c5-4aef-a0cc-f9fe4da444132	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL001	021-45854360	1	2020-08-13 12:55:41	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-13 12:57:23	2020-08-13	2020-08-14	95	019926575043000	0
13a77a62-81c5-4aef-a0cc-f9fe4da444133	KOMP. GADING BUKIT INDAH BLOK C17 LT. 2 \r\nJL. BUKIT GADING RAYA, KELAPA GADING JAKARTA 14240	2020-07-28	ONEYTYOA66092900	PT. GEMILANG LAJU SEJAHTERA	jonathan@gemilangls.com	019926575043000	79	019926575043000	PL004	021-45854360	1	2020-08-13 12:55:41	New Priok Container Terminal One (NPCT1), North Jakarta City, Jakarta, Indonesia	-6.0948999	106.9230484	2020-08-13 12:57:23	2020-08-13	2020-08-14	95	019926575043000	0
31db3546-d72b-4b12-8f06-228fdf5787ab1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	2	2020-08-15 13:00:00	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306108	106.956208	2020-08-13 14:17:47	2020-08-14	2020-08-14	23	018245894059000	0
31db3546-d72b-4b12-8f06-228fdf5787ab2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	2	2020-08-15 13:00:00	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306108	106.956208	2020-08-13 14:17:47	2020-08-14	2020-08-14	23	018245894059000	0
31db3546-d72b-4b12-8f06-228fdf5787ab3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	2	2020-08-15 13:00:00	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306108	106.956208	2020-08-13 14:17:47	2020-08-14	2020-08-14	23	018245894059000	0
31db3546-d72b-4b12-8f06-228fdf5787ab4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	2	2020-08-15 13:00:00	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306108	106.956208	2020-08-13 14:17:47	2020-08-14	2020-08-14	23	018245894059000	0
c41d9cfe-d43b-4684-8e47-f024cf38745a1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	2	2020-08-15 13:00:00	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306108	106.956208	2020-08-13 14:18:34	2020-08-14	2020-08-14	23	018245894059000	0
c41d9cfe-d43b-4684-8e47-f024cf38745a2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	2	2020-08-15 13:00:00	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306108	106.956208	2020-08-13 14:18:34	2020-08-14	2020-08-14	23	018245894059000	0
c41d9cfe-d43b-4684-8e47-f024cf38745a3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	2	2020-08-15 13:00:00	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306108	106.956208	2020-08-13 14:18:34	2020-08-14	2020-08-14	23	018245894059000	0
c41d9cfe-d43b-4684-8e47-f024cf38745a4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	2	2020-08-15 13:00:00	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306108	106.956208	2020-08-13 14:18:34	2020-08-14	2020-08-14	23	018245894059000	0
5d4d1702-cea0-48a2-806a-e49dece6fcc21	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	2	2020-08-15 13:00:00	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306108	106.956208	2020-08-13 14:18:39	2020-08-14	2020-08-14	23	018245894059000	0
5d4d1702-cea0-48a2-806a-e49dece6fcc22	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	2	2020-08-15 13:00:00	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306108	106.956208	2020-08-13 14:18:39	2020-08-14	2020-08-14	23	018245894059000	0
5d4d1702-cea0-48a2-806a-e49dece6fcc23	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	2	2020-08-15 13:00:00	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306108	106.956208	2020-08-13 14:18:39	2020-08-14	2020-08-14	23	018245894059000	0
5d4d1702-cea0-48a2-806a-e49dece6fcc24	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	2	2020-08-15 13:00:00	Jalan Koja I No.Koja 1, RT.002/RW.007, Jatiasih, Bekasi City, West Java, Indonesia	-6.306108	106.956208	2020-08-13 14:18:39	2020-08-14	2020-08-14	23	018245894059000	0
b1ee4420-1ec8-4632-bc23-f7884f4a01551	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 14:34:06	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:34:41	2020-08-13	2020-08-14	50	018245894059000	0
b1ee4420-1ec8-4632-bc23-f7884f4a01552	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 14:34:06	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:34:41	2020-08-13	2020-08-14	50	018245894059000	0
b1ee4420-1ec8-4632-bc23-f7884f4a01553	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	4	2020-08-13 14:34:06	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:34:41	2020-08-13	2020-08-14	50	018245894059000	0
b1ee4420-1ec8-4632-bc23-f7884f4a01554	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 14:34:06	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:34:41	2020-08-13	2020-08-14	50	018245894059000	0
1f5e6e70-d88d-4c77-8aaf-7fcf969f0f891	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 14:35:19	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:35:53	2020-08-13	2020-08-14	50	018245894059000	0
1f5e6e70-d88d-4c77-8aaf-7fcf969f0f892	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 14:35:19	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:35:53	2020-08-13	2020-08-14	50	018245894059000	0
1f5e6e70-d88d-4c77-8aaf-7fcf969f0f893	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	4	2020-08-13 14:35:19	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:35:53	2020-08-13	2020-08-14	50	018245894059000	0
1f5e6e70-d88d-4c77-8aaf-7fcf969f0f894	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 14:35:19	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:35:53	2020-08-13	2020-08-14	50	018245894059000	0
11731f42-a238-4d5a-98e4-ea2bce56a4511	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 14:37:43	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:38:51	2020-08-14	2020-08-14	38	018245894059000	0
11731f42-a238-4d5a-98e4-ea2bce56a4512	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 14:37:43	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:38:51	2020-08-14	2020-08-14	38	018245894059000	0
11731f42-a238-4d5a-98e4-ea2bce56a4513	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	4	2020-08-13 14:37:43	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:38:51	2020-08-14	2020-08-14	38	018245894059000	0
11731f42-a238-4d5a-98e4-ea2bce56a4514	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 14:37:43	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:38:51	2020-08-14	2020-08-14	38	018245894059000	0
2b8e75a5-7fdd-461b-96df-ecf9236f7b7e1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	2	2020-08-13 14:55:46	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:56:39	2020-08-13	2020-08-14	81	018245894059000	0
2b8e75a5-7fdd-461b-96df-ecf9236f7b7e2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	2	2020-08-13 14:55:46	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:56:39	2020-08-13	2020-08-14	81	018245894059000	0
2b8e75a5-7fdd-461b-96df-ecf9236f7b7e3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	2	2020-08-13 14:55:46	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:56:39	2020-08-13	2020-08-14	81	018245894059000	0
2b8e75a5-7fdd-461b-96df-ecf9236f7b7e4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	2	2020-08-13 14:55:46	Tanjung Priok, Jakarta Lor, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-13 14:56:39	2020-08-13	2020-08-14	81	018245894059000	0
e4f6d78c-6d48-436b-a387-6406950aee9b1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 20:25:32	Jalan Palmerah Utara, RT.2/RW.1, Gelora, West Jakarta City, Jakarta, Indonesia	-6.2043898	106.7970079	2020-08-13 20:34:39	2020-08-13	2020-08-13	1861	018245894059000	0
e4f6d78c-6d48-436b-a387-6406950aee9b2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 20:25:32	Jalan Palmerah Utara, RT.2/RW.1, Gelora, West Jakarta City, Jakarta, Indonesia	-6.2043898	106.7970079	2020-08-13 20:34:39	2020-08-13	2020-08-13	1861	018245894059000	0
e4f6d78c-6d48-436b-a387-6406950aee9b3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	4	2020-08-13 20:25:32	Jalan Palmerah Utara, RT.2/RW.1, Gelora, West Jakarta City, Jakarta, Indonesia	-6.2043898	106.7970079	2020-08-13 20:34:39	2020-08-13	2020-08-13	1861	018245894059000	0
e4f6d78c-6d48-436b-a387-6406950aee9b4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 20:25:32	Jalan Palmerah Utara, RT.2/RW.1, Gelora, West Jakarta City, Jakarta, Indonesia	-6.2043898	106.7970079	2020-08-13 20:34:39	2020-08-13	2020-08-13	1861	018245894059000	0
03a1f255-8904-448c-a71b-aa588b3211581	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-13 20:25:32	Jalan Palmerah Utara, RT.2/RW.1, Gelora, West Jakarta City, Jakarta, Indonesia	-6.2043898	106.7970079	2020-08-13 20:38:32	2020-08-13	2020-08-13	1861	018245894059000	0
03a1f255-8904-448c-a71b-aa588b3211582	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-13 20:25:32	Jalan Palmerah Utara, RT.2/RW.1, Gelora, West Jakarta City, Jakarta, Indonesia	-6.2043898	106.7970079	2020-08-13 20:38:32	2020-08-13	2020-08-13	1861	018245894059000	0
03a1f255-8904-448c-a71b-aa588b3211583	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	4	2020-08-13 20:25:32	Jalan Palmerah Utara, RT.2/RW.1, Gelora, West Jakarta City, Jakarta, Indonesia	-6.2043898	106.7970079	2020-08-13 20:38:32	2020-08-13	2020-08-13	1861	018245894059000	0
03a1f255-8904-448c-a71b-aa588b3211584	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-13 20:25:32	Jalan Palmerah Utara, RT.2/RW.1, Gelora, West Jakarta City, Jakarta, Indonesia	-6.2043898	106.7970079	2020-08-13 20:38:32	2020-08-13	2020-08-13	1861	018245894059000	0
a3efbc31-a9d2-4a3d-8328-9ae89d0460991	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-14 07:33:02	Cengkareng, West Jakarta City, Jakarta, Indonesia	-6.1486651	106.7352584	2020-08-14 07:41:15	2020-08-15	2020-08-15	13	018245894059000	0
a3efbc31-a9d2-4a3d-8328-9ae89d0460992	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-14 07:33:02	Cengkareng, West Jakarta City, Jakarta, Indonesia	-6.1486651	106.7352584	2020-08-14 07:41:15	2020-08-15	2020-08-15	13	018245894059000	0
a3efbc31-a9d2-4a3d-8328-9ae89d0460993	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	1	2020-08-14 07:33:02	Cengkareng, West Jakarta City, Jakarta, Indonesia	-6.1486651	106.7352584	2020-08-14 07:41:15	2020-08-15	2020-08-15	13	018245894059000	0
a3efbc31-a9d2-4a3d-8328-9ae89d0460994	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-14 07:33:02	Cengkareng, West Jakarta City, Jakarta, Indonesia	-6.1486651	106.7352584	2020-08-14 07:41:15	2020-08-15	2020-08-15	13	018245894059000	0
0ab7f929-0295-4a27-b49d-e2ab8df8db303	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL002	021-57900391	5	2020-08-14 08:20:40	Cengkareng, West Jakarta City, Jakarta, Indonesia	\N	\N	2020-08-14 08:23:26	2020-08-15	2020-08-15	0	018245894059000	0
0ab7f929-0295-4a27-b49d-e2ab8df8db304	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL004	021-57900391	5	2020-08-14 08:20:40	Cengkareng, West Jakarta City, Jakarta, Indonesia	\N	\N	2020-08-14 08:23:26	2020-08-15	2020-08-15	0	018245894059000	0
d6791664-e59a-45e5-b5ee-5a1f5eb6b5111	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-14 10:00:40	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-14 10:01:23	2020-08-14	2020-08-17	11	018245894059000	0
d6791664-e59a-45e5-b5ee-5a1f5eb6b5112	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-14 10:00:40	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-14 10:01:23	2020-08-14	2020-08-17	11	018245894059000	0
d6791664-e59a-45e5-b5ee-5a1f5eb6b5113	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	4	2020-08-14 10:00:40	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-14 10:01:23	2020-08-14	2020-08-17	11	018245894059000	0
d6791664-e59a-45e5-b5ee-5a1f5eb6b5114	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-14 10:00:40	JICT, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.105955300000001	106.8991097	2020-08-14 10:01:23	2020-08-14	2020-08-17	11	018245894059000	0
fd79d7ee-fe24-47ea-b81c-f41953d398b91	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-14 09:52:10	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 10:37:31	2020-08-14	2020-08-15	56	018245894059000	0
fd79d7ee-fe24-47ea-b81c-f41953d398b92	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-14 09:52:10	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 10:37:31	2020-08-14	2020-08-15	56	018245894059000	0
fd79d7ee-fe24-47ea-b81c-f41953d398b93	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	1	2020-08-14 09:52:10	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 10:37:31	2020-08-14	2020-08-15	56	018245894059000	0
fd79d7ee-fe24-47ea-b81c-f41953d398b94	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-14 09:52:10	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 10:37:31	2020-08-14	2020-08-15	56	018245894059000	0
2c807148-d49d-493f-8aff-6d550797ca571	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	2	2020-08-14 14:28:57	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 14:30:36	2020-08-14	2020-08-14	661	018245894059000	0
2c807148-d49d-493f-8aff-6d550797ca572	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	2	2020-08-14 14:28:57	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 14:30:36	2020-08-14	2020-08-14	661	018245894059000	0
2c807148-d49d-493f-8aff-6d550797ca573	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	2	2020-08-14 14:28:57	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 14:30:36	2020-08-14	2020-08-14	661	018245894059000	0
2c807148-d49d-493f-8aff-6d550797ca574	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	2	2020-08-14 14:28:57	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 14:30:36	2020-08-14	2020-08-14	661	018245894059000	0
d050ea8f-13f6-4677-863c-0b2b5bad0dab1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-14 14:40:45	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 14:44:50	2020-08-14	2020-08-14	58	018245894059000	0
d050ea8f-13f6-4677-863c-0b2b5bad0dab2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-14 14:40:45	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 14:44:50	2020-08-14	2020-08-14	58	018245894059000	0
d050ea8f-13f6-4677-863c-0b2b5bad0dab3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	1	2020-08-14 14:40:45	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 14:44:50	2020-08-14	2020-08-14	58	018245894059000	0
d050ea8f-13f6-4677-863c-0b2b5bad0dab4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-14 14:40:45	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 14:44:50	2020-08-14	2020-08-14	58	018245894059000	0
1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-14 14:58:24	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 14:59:20	2020-08-14	2020-08-14	19	018245894059000	0
1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-14 14:58:24	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 14:59:20	2020-08-14	2020-08-14	19	018245894059000	0
1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	1	2020-08-14 14:58:24	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 14:59:20	2020-08-14	2020-08-14	19	018245894059000	0
1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-14 14:58:24	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 14:59:20	2020-08-14	2020-08-14	19	018245894059000	0
8c689fea-dc94-4e79-add1-a100863461991	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-14 15:06:16	THE PRIME Office Suites, Jalan Yos Sudarso, RT.10/RW.6, Bambu River, North Jakarta City, Jakarta, Indonesia	-6.1428143	106.8899555	2020-08-14 15:07:02	2020-08-14	2020-08-14	53	018245894059000	0
8c689fea-dc94-4e79-add1-a100863461992	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-14 15:06:16	THE PRIME Office Suites, Jalan Yos Sudarso, RT.10/RW.6, Bambu River, North Jakarta City, Jakarta, Indonesia	-6.1428143	106.8899555	2020-08-14 15:07:02	2020-08-14	2020-08-14	53	018245894059000	0
8c689fea-dc94-4e79-add1-a100863461993	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	1	2020-08-14 15:06:16	THE PRIME Office Suites, Jalan Yos Sudarso, RT.10/RW.6, Bambu River, North Jakarta City, Jakarta, Indonesia	-6.1428143	106.8899555	2020-08-14 15:07:02	2020-08-14	2020-08-14	53	018245894059000	0
8c689fea-dc94-4e79-add1-a100863461994	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-14 15:06:16	THE PRIME Office Suites, Jalan Yos Sudarso, RT.10/RW.6, Bambu River, North Jakarta City, Jakarta, Indonesia	-6.1428143	106.8899555	2020-08-14 15:07:02	2020-08-14	2020-08-14	53	018245894059000	0
6e2cf3e8-9c58-4fc1-9524-06cbc5a407601	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-14 15:06:16	THE PRIME Office Suites, Jalan Yos Sudarso, RT.10/RW.6, Bambu River, North Jakarta City, Jakarta, Indonesia	-6.1428143	106.8899555	2020-08-14 15:12:57	2020-08-14	2020-08-14	53	018245894059000	0
6e2cf3e8-9c58-4fc1-9524-06cbc5a407602	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-14 15:06:16	THE PRIME Office Suites, Jalan Yos Sudarso, RT.10/RW.6, Bambu River, North Jakarta City, Jakarta, Indonesia	-6.1428143	106.8899555	2020-08-14 15:12:57	2020-08-14	2020-08-14	53	018245894059000	0
6e2cf3e8-9c58-4fc1-9524-06cbc5a407603	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	1	2020-08-14 15:06:16	THE PRIME Office Suites, Jalan Yos Sudarso, RT.10/RW.6, Bambu River, North Jakarta City, Jakarta, Indonesia	-6.1428143	106.8899555	2020-08-14 15:12:57	2020-08-14	2020-08-14	53	018245894059000	0
6e2cf3e8-9c58-4fc1-9524-06cbc5a407604	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-14 15:06:16	THE PRIME Office Suites, Jalan Yos Sudarso, RT.10/RW.6, Bambu River, North Jakarta City, Jakarta, Indonesia	-6.1428143	106.8899555	2020-08-14 15:12:57	2020-08-14	2020-08-14	53	018245894059000	0
e64169f7-5a28-4596-963a-c82f7518ef821	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-14 15:40:23	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:41:39	2020-08-15	2020-08-15	37	018245894059000	0
e64169f7-5a28-4596-963a-c82f7518ef822	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-14 15:40:23	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:41:39	2020-08-15	2020-08-15	37	018245894059000	0
e64169f7-5a28-4596-963a-c82f7518ef823	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	4	2020-08-14 15:40:23	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:41:39	2020-08-15	2020-08-15	37	018245894059000	0
e64169f7-5a28-4596-963a-c82f7518ef824	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-14 15:40:23	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:41:39	2020-08-15	2020-08-15	37	018245894059000	0
bdaa653c-bf1c-4cc6-8fc1-758112cea4281	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-14 15:41:05	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:41:44	2020-08-14	2020-08-14	58	018245894059000	0
bdaa653c-bf1c-4cc6-8fc1-758112cea4282	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-14 15:41:05	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:41:44	2020-08-14	2020-08-14	58	018245894059000	0
bdaa653c-bf1c-4cc6-8fc1-758112cea4283	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	1	2020-08-14 15:41:05	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:41:44	2020-08-14	2020-08-14	58	018245894059000	0
bdaa653c-bf1c-4cc6-8fc1-758112cea4284	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-14 15:41:05	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:41:44	2020-08-14	2020-08-14	58	018245894059000	0
ed880f83-a3e2-4505-a043-401dbaf33c3e1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	2	2020-08-14 15:44:34	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:45:12	2020-08-14	2020-08-14	58	018245894059000	0
ed880f83-a3e2-4505-a043-401dbaf33c3e2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	2	2020-08-14 15:44:34	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:45:12	2020-08-14	2020-08-14	58	018245894059000	0
ed880f83-a3e2-4505-a043-401dbaf33c3e3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	2	2020-08-14 15:44:34	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:45:12	2020-08-14	2020-08-14	58	018245894059000	0
ed880f83-a3e2-4505-a043-401dbaf33c3e4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	2	2020-08-14 15:44:34	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:45:12	2020-08-14	2020-08-14	58	018245894059000	0
b099e47b-2c40-4280-b3b9-d3679d2f8c621	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-14 15:47:55	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:48:19	2020-08-14	2020-08-15	58	018245894059000	0
b099e47b-2c40-4280-b3b9-d3679d2f8c622	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-14 15:47:55	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:48:19	2020-08-14	2020-08-15	58	018245894059000	0
b099e47b-2c40-4280-b3b9-d3679d2f8c623	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	1	2020-08-14 15:47:55	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:48:19	2020-08-14	2020-08-15	58	018245894059000	0
b099e47b-2c40-4280-b3b9-d3679d2f8c624	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-14 15:47:55	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 15:48:19	2020-08-14	2020-08-15	58	018245894059000	0
e34ba684-8663-41ed-9f80-a1d5bea85f0f1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-14 15:52:46	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-14 15:54:05	2020-08-15	2020-08-15	36	018245894059000	0
e34ba684-8663-41ed-9f80-a1d5bea85f0f2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-14 15:52:46	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-14 15:54:05	2020-08-15	2020-08-15	36	018245894059000	0
e34ba684-8663-41ed-9f80-a1d5bea85f0f3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	4	2020-08-14 15:52:46	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-14 15:54:05	2020-08-15	2020-08-15	36	018245894059000	0
e34ba684-8663-41ed-9f80-a1d5bea85f0f4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-14 15:52:46	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-14 15:54:05	2020-08-15	2020-08-15	36	018245894059000	0
d00708c9-c77a-42a9-b15b-abd76e2495411	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-14 15:54:32	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-14 15:56:02	2020-08-16	2020-08-15	36	018245894059000	0
d00708c9-c77a-42a9-b15b-abd76e2495412	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-14 15:54:32	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-14 15:56:02	2020-08-16	2020-08-15	36	018245894059000	0
d00708c9-c77a-42a9-b15b-abd76e2495413	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	4	2020-08-14 15:54:32	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-14 15:56:02	2020-08-16	2020-08-15	36	018245894059000	0
d00708c9-c77a-42a9-b15b-abd76e2495414	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-14 15:54:32	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-14 15:56:02	2020-08-16	2020-08-15	36	018245894059000	0
60b0c35d-a58b-4bc8-a2e3-249a820a3f241	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-14 16:39:35	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 16:40:42	2020-08-14	2020-08-15	58	018245894059000	0
60b0c35d-a58b-4bc8-a2e3-249a820a3f242	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-14 16:39:35	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 16:40:42	2020-08-14	2020-08-15	58	018245894059000	0
60b0c35d-a58b-4bc8-a2e3-249a820a3f243	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	1	2020-08-14 16:39:35	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 16:40:42	2020-08-14	2020-08-15	58	018245894059000	0
60b0c35d-a58b-4bc8-a2e3-249a820a3f244	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-14 16:39:35	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 16:40:42	2020-08-14	2020-08-15	58	018245894059000	0
d8beea02-0ae4-4bf3-bf90-089a2382b1e11	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-14 16:39:35	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 16:41:12	2020-08-14	2020-08-15	58	018245894059000	0
d8beea02-0ae4-4bf3-bf90-089a2382b1e12	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-14 16:39:35	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 16:41:12	2020-08-14	2020-08-15	58	018245894059000	0
d8beea02-0ae4-4bf3-bf90-089a2382b1e13	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	1	2020-08-14 16:39:35	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 16:41:12	2020-08-14	2020-08-15	58	018245894059000	0
d8beea02-0ae4-4bf3-bf90-089a2382b1e14	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-14 16:39:35	TPK Koja, Digul, RW.1, Koja, North Jakarta City, Jakarta, Indonesia	-6.1021909	106.9042129	2020-08-14 16:41:12	2020-08-14	2020-08-15	58	018245894059000	0
c69824d3-7a04-4dab-9243-a4663e3ee4781	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	1	2020-08-14 16:39:35	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 16:42:31	2020-08-14	2020-08-15	54	018245894059000	0
c69824d3-7a04-4dab-9243-a4663e3ee4782	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	1	2020-08-14 16:39:35	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 16:42:31	2020-08-14	2020-08-15	54	018245894059000	0
c69824d3-7a04-4dab-9243-a4663e3ee4783	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	1	2020-08-14 16:39:35	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 16:42:31	2020-08-14	2020-08-15	54	018245894059000	0
c69824d3-7a04-4dab-9243-a4663e3ee4784	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	1	2020-08-14 16:39:35	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 16:42:31	2020-08-14	2020-08-15	54	018245894059000	0
89a90d27-3410-4d0e-b990-32ea7f1aac031	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	4	2020-08-14 19:00:00	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-14 17:53:41	2020-08-15	2020-08-15	19	018245894059000	0
89a90d27-3410-4d0e-b990-32ea7f1aac032	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	4	2020-08-14 19:00:00	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-14 17:53:41	2020-08-15	2020-08-15	19	018245894059000	0
89a90d27-3410-4d0e-b990-32ea7f1aac033	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	4	2020-08-14 19:00:00	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-14 17:53:41	2020-08-15	2020-08-15	19	018245894059000	0
89a90d27-3410-4d0e-b990-32ea7f1aac034	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	4	2020-08-14 19:00:00	Koja, North Jakarta City, Jakarta, Indonesia	-6.117663500000001	106.9063491	2020-08-14 17:53:41	2020-08-15	2020-08-15	19	018245894059000	0
1ec0c1fe-6b84-44fe-924e-6d416f35f04d1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL000	021-57900391	2	2020-08-14 21:32:36	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 21:36:58	2020-08-14	2020-08-14	54	018245894059000	0
1ec0c1fe-6b84-44fe-924e-6d416f35f04d2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL001	021-57900391	2	2020-08-14 21:32:36	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 21:36:58	2020-08-14	2020-08-14	54	018245894059000	0
1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL002	021-57900391	2	2020-08-14 21:32:36	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 21:36:58	2020-08-14	2020-08-14	54	018245894059000	0
1ec0c1fe-6b84-44fe-924e-6d416f35f04d4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-07-19	EGLV003090915504	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	82		PL004	021-57900391	2	2020-08-14 21:32:36	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-14 21:36:58	2020-08-14	2020-08-14	54	018245894059000	0
437ebef7-53dd-4738-84dc-263aa239e6c71	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL000	021-57900391	1	2020-08-16 13:31:21	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-16 13:34:22	2020-08-16	2020-08-16	55	018245894059000	0
437ebef7-53dd-4738-84dc-263aa239e6c72	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL001	021-57900391	1	2020-08-16 13:31:21	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-16 13:34:22	2020-08-16	2020-08-16	55	018245894059000	0
437ebef7-53dd-4738-84dc-263aa239e6c73	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL002	021-57900391	1	2020-08-16 13:31:21	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-16 13:34:22	2020-08-16	2020-08-16	55	018245894059000	0
437ebef7-53dd-4738-84dc-263aa239e6c74	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL004	021-57900391	1	2020-08-16 13:31:21	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-16 13:34:22	2020-08-16	2020-08-16	55	018245894059000	0
f65452ac-5fe1-45c9-b14f-f27da4d788941	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL000	021-57900391	2	2020-08-16 14:22:44	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-16 14:23:51	2020-08-16	2020-08-16	55	018245894059000	0
f65452ac-5fe1-45c9-b14f-f27da4d788942	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL001	021-57900391	2	2020-08-16 14:22:44	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-16 14:23:51	2020-08-16	2020-08-16	55	018245894059000	0
f65452ac-5fe1-45c9-b14f-f27da4d788943	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL002	021-57900391	2	2020-08-16 14:22:44	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-16 14:23:51	2020-08-16	2020-08-16	55	018245894059000	0
f65452ac-5fe1-45c9-b14f-f27da4d788944	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL004	021-57900391	2	2020-08-16 14:22:44	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-16 14:23:51	2020-08-16	2020-08-16	55	018245894059000	0
50090798-dfbd-4518-8a84-78a10397747b1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL000	021-57900391	1	2020-08-16 14:25:40	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-16 14:48:32	2020-08-16	2020-08-17	55	018245894059000	0
50090798-dfbd-4518-8a84-78a10397747b2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL001	021-57900391	1	2020-08-16 14:25:40	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-16 14:48:32	2020-08-16	2020-08-17	55	018245894059000	0
50090798-dfbd-4518-8a84-78a10397747b3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL002	021-57900391	1	2020-08-16 14:25:40	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-16 14:48:32	2020-08-16	2020-08-17	55	018245894059000	0
50090798-dfbd-4518-8a84-78a10397747b4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL004	021-57900391	1	2020-08-16 14:25:40	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-16 14:48:32	2020-08-16	2020-08-17	55	018245894059000	0
ad119148-6875-4a87-8ec2-e044f2e3913e1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL000	021-57900391	1	2020-08-17 20:27:42	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 20:30:20	2020-08-17	2020-08-18	54	018245894059000	0
ad119148-6875-4a87-8ec2-e044f2e3913e2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL001	021-57900391	1	2020-08-17 20:27:42	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 20:30:20	2020-08-17	2020-08-18	54	018245894059000	0
ad119148-6875-4a87-8ec2-e044f2e3913e3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL002	021-57900391	1	2020-08-17 20:27:42	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 20:30:20	2020-08-17	2020-08-18	54	018245894059000	0
ad119148-6875-4a87-8ec2-e044f2e3913e4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL004	021-57900391	1	2020-08-17 20:27:42	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 20:30:20	2020-08-17	2020-08-18	54	018245894059000	0
5fe18cfa-b431-465b-8555-7880de1d66e71	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL000	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:02:32	2020-08-17	2020-08-18	54	018245894059000	0
5fe18cfa-b431-465b-8555-7880de1d66e72	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL001	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:02:32	2020-08-17	2020-08-18	54	018245894059000	0
5fe18cfa-b431-465b-8555-7880de1d66e73	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL002	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:02:32	2020-08-17	2020-08-18	54	018245894059000	0
5fe18cfa-b431-465b-8555-7880de1d66e74	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL004	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:02:32	2020-08-17	2020-08-18	54	018245894059000	0
47676dff-207c-46af-bc29-a7fb93afaf801	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL000	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:03:19	2020-08-17	2020-08-18	54	018245894059000	0
47676dff-207c-46af-bc29-a7fb93afaf802	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL001	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:03:19	2020-08-17	2020-08-18	54	018245894059000	0
47676dff-207c-46af-bc29-a7fb93afaf803	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL002	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:03:19	2020-08-17	2020-08-18	54	018245894059000	0
47676dff-207c-46af-bc29-a7fb93afaf804	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL004	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:03:19	2020-08-17	2020-08-18	54	018245894059000	0
04fc413c-3a4a-43d3-9fd3-3c5966ff50501	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL000	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:04:03	2020-08-17	2020-08-18	54	018245894059000	0
04fc413c-3a4a-43d3-9fd3-3c5966ff50502	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL001	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:04:03	2020-08-17	2020-08-18	54	018245894059000	0
04fc413c-3a4a-43d3-9fd3-3c5966ff50503	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL002	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:04:03	2020-08-17	2020-08-18	54	018245894059000	0
04fc413c-3a4a-43d3-9fd3-3c5966ff50504	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL004	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:04:03	2020-08-17	2020-08-18	54	018245894059000	0
d0db336c-d39c-467d-86ce-0fae0180bb6e1	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL000	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:04:20	2020-08-17	2020-08-18	54	018245894059000	0
d0db336c-d39c-467d-86ce-0fae0180bb6e2	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL001	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:04:20	2020-08-17	2020-08-18	54	018245894059000	0
d0db336c-d39c-467d-86ce-0fae0180bb6e3	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL002	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:04:20	2020-08-17	2020-08-18	54	018245894059000	0
d0db336c-d39c-467d-86ce-0fae0180bb6e4	WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0	2020-08-03	HDMUSGWB0589660	NAGASE IMPOR-EKSPOR INDONESIA	ari@nagase.co.id	018245894059000	81		PL004	021-57900391	1	2020-08-17 23:01:03	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	-6.1320555	106.8714848	2020-08-17 23:04:20	2020-08-17	2020-08-18	54	018245894059000	0
\.


--
-- Data for Name: document_trucking_container; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.document_trucking_container (id_container, container_no, container_size, container_type, dangerous_material, dangerous_type, id_request_booking, over_height, over_length, over_weight, over_width, temperatur) FROM stdin;
3	SKLU1324270	20	General/ Dry Cargo	Class 1: Coba	Dangerous	asda929ad0920jdadkasdas	\N	\N	\N	\N	\N
6	SKLU1324270	20	General/ Dry Cargo	Class 1: Coba	Dangerous	asda929ad0920jda29329asd	\N	\N	\N	\N	\N
7	SKLU1324270	20	General/ Dry Cargo	Class 1: Coba	Dangerous	u38929adsndjk29akeas8w	\N	\N	\N	\N	\N
9	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	ee4e7259-8c1d-4dfc-850e-a9c3dbb67b5a1	\N	\N	\N	\N	\N
10	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	a7a46a33-f4d1-4edc-9be2-9cb799db93091	\N	\N	\N	\N	\N
11	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	b019a789-3802-4356-9091-10310cf45bbf1	\N	\N	\N	\N	\N
12	SKLU1324270	20	General/ Dry Cargo	Class 1: Coba	Dangerous	k10so929ad0920jda29bca021	\N	\N	\N	\N	\N
13	SKLU1435201	20	General/ Dry Cargo	Class 1: Coba	Dangerous	k10so929ad0920jda29bca021	\N	\N	\N	\N	\N
14	SKLU1324581	20	General/ Dry Cargo	Class 1: Coba	Dangerous	cpq1ogi7ad0920jda29bcbl0u	\N	\N	\N	\N	\N
15	BSIU9228806	40	General/ Dry Cargo	\N	Non Dangerous	590fce61-64c9-4dca-8ee7-f2e8ea76fcee1	\N	\N	\N	\N	\N
16	BSIU9228806	40	General/ Dry Cargo	\N	Non Dangerous	aa170d90-9b2f-4c96-9aee-9d64648cd04e1	\N	\N	\N	\N	\N
17	TCLU4569117	40	General/ Dry Cargo	\N	Non Dangerous	67889e84-403e-4379-8c5b-1877588ab1381	\N	\N	\N	\N	\N
18	TCLU4569117	40	General/ Dry Cargo	\N	Non Dangerous	5c849b69-0004-43c1-a31d-6148a3d33dbc1	\N	\N	\N	\N	\N
19	SKLU1435201	20	General/ Dry Cargo	Class 1: Coba	Dangerous	k10so929ad0920jda29bca021	\N	\N	\N	\N	\N
20	SKLU1324270	20	General/ Dry Cargo	Class 1: Coba	Dangerous	k10so929ad0920jda29bca021	\N	\N	\N	\N	\N
21	SKLU1324581	20	General/ Dry Cargo	Class 1: Coba	Dangerous	cpq1ogi7ad0920jda29bcbl0u	\N	\N	\N	\N	\N
22	BSIU9228806	40	General/ Dry Cargo	\N	Non Dangerous	f2ed1fed-3454-4c21-9904-1f11c23544c31	\N	\N	\N	\N	\N
23	BSIU9228806	40	General/ Dry Cargo	\N	Non Dangerous	bc3cd8cc-5384-48a5-8342-4f0361fa9c2e1	\N	\N	\N	\N	\N
24	KOCU4102840	40	General/ Dry Cargo	\N	Non Dangerous	e79f8254-22f5-4d03-ad85-cd63f29b90fc1	\N	\N	\N	\N	\N
25	BSIU9228806	40	General/ Dry Cargo	\N	Non Dangerous	3a39e7b7-9221-4257-8d17-10b067e2f04a1	\N	\N	\N	\N	\N
26	KOCU4165216	40	General/ Dry Cargo	\N	Non Dangerous	ef74a1fb-8a10-44ed-85dc-5f37f8954c181	\N	\N	\N	\N	\N
274	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	d3a70aff-fbf3-4474-884e-0e48fd9901f31	\N	\N	\N	\N	\N
275	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	d3a70aff-fbf3-4474-884e-0e48fd9901f32	\N	\N	\N	\N	\N
276	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	d3a70aff-fbf3-4474-884e-0e48fd9901f33	\N	\N	\N	\N	\N
358	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	c1b5a2f2-7e0e-42fe-84b7-10e87ccabba61	\N	\N	\N	\N	\N
359	BMOU5168025	40	General/ Dry Cargo	\N	Non Dangerous	c1b5a2f2-7e0e-42fe-84b7-10e87ccabba61	\N	\N	\N	\N	\N
360	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	c1b5a2f2-7e0e-42fe-84b7-10e87ccabba62	\N	\N	\N	\N	\N
361	BMOU5168025	40	General/ Dry Cargo	\N	Non Dangerous	c1b5a2f2-7e0e-42fe-84b7-10e87ccabba62	\N	\N	\N	\N	\N
362	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	c1b5a2f2-7e0e-42fe-84b7-10e87ccabba63	\N	\N	\N	\N	\N
363	BMOU5168025	40	General/ Dry Cargo	\N	Non Dangerous	c1b5a2f2-7e0e-42fe-84b7-10e87ccabba63	\N	\N	\N	\N	\N
397	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1506a286-d5cf-4ee0-b8ce-54384e0cad401	\N	\N	\N	\N	\N
398	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1506a286-d5cf-4ee0-b8ce-54384e0cad401	\N	\N	\N	\N	\N
399	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1506a286-d5cf-4ee0-b8ce-54384e0cad402	\N	\N	\N	\N	\N
400	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1506a286-d5cf-4ee0-b8ce-54384e0cad402	\N	\N	\N	\N	\N
401	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1506a286-d5cf-4ee0-b8ce-54384e0cad403	\N	\N	\N	\N	\N
402	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1506a286-d5cf-4ee0-b8ce-54384e0cad403	\N	\N	\N	\N	\N
424	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	66d6d20c-a63e-46fd-b440-748da42ce93c1	\N	\N	\N	\N	\N
425	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	66d6d20c-a63e-46fd-b440-748da42ce93c2	\N	\N	\N	\N	\N
426	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	66d6d20c-a63e-46fd-b440-748da42ce93c3	\N	\N	\N	\N	\N
438	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	130271c3-e6b8-4b5d-b7cc-22e112030afa1	\N	\N	\N	\N	\N
439	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	130271c3-e6b8-4b5d-b7cc-22e112030afa2	\N	\N	\N	\N	\N
440	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	130271c3-e6b8-4b5d-b7cc-22e112030afa3	\N	\N	\N	\N	\N
456	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0021	\N	\N	\N	\N	\N
457	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0022	\N	\N	\N	\N	\N
458	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0022	\N	\N	\N	\N	\N
459	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0022	\N	\N	\N	\N	\N
460	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0022	\N	\N	\N	\N	\N
461	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0023	\N	\N	\N	\N	\N
462	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0023	\N	\N	\N	\N	\N
463	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0023	\N	\N	\N	\N	\N
464	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0023	\N	\N	\N	\N	\N
482	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	8311f5c9-e80f-4888-bdb7-1ed85dfd889a2	\N	\N	\N	\N	\N
483	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	8311f5c9-e80f-4888-bdb7-1ed85dfd889a2	\N	\N	\N	\N	\N
484	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	8311f5c9-e80f-4888-bdb7-1ed85dfd889a2	\N	\N	\N	\N	\N
485	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	8311f5c9-e80f-4888-bdb7-1ed85dfd889a3	\N	\N	\N	\N	\N
486	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	8311f5c9-e80f-4888-bdb7-1ed85dfd889a3	\N	\N	\N	\N	\N
487	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	8311f5c9-e80f-4888-bdb7-1ed85dfd889a3	\N	\N	\N	\N	\N
488	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	8311f5c9-e80f-4888-bdb7-1ed85dfd889a3	\N	\N	\N	\N	\N
489	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	9c50f350-9086-4c0a-bf8d-49d3e0ef9d021	\N	\N	\N	\N	\N
490	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	9c50f350-9086-4c0a-bf8d-49d3e0ef9d021	\N	\N	\N	\N	\N
491	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	9c50f350-9086-4c0a-bf8d-49d3e0ef9d021	\N	\N	\N	\N	\N
277	WHSU5363638	40	General/ Dry Cargo	\N	Non Dangerous	149603f2-19ae-4c19-8d5b-da6acce03bcd1	\N	\N	\N	\N	\N
278	WHSU5363638	40	General/ Dry Cargo	\N	Non Dangerous	149603f2-19ae-4c19-8d5b-da6acce03bcd2	\N	\N	\N	\N	\N
70	CAIU7132926	40	General/ Dry Cargo	\N	Non Dangerous	9f27b2cf-34b1-43ef-abb4-38f0b72f20751	\N	\N	\N	\N	\N
71	CAIU7132926	40	General/ Dry Cargo	\N	Non Dangerous	49b215e6-0277-48cf-a4ec-0dc05f7c77711	\N	\N	\N	\N	\N
72	CAIU7132926	40	General/ Dry Cargo	\N	Non Dangerous	47503e6d-ab49-4279-87ef-94e2572cd14a1	\N	\N	\N	\N	\N
73	CAIU7132926	40	General/ Dry Cargo	\N	Non Dangerous	47503e6d-ab49-4279-87ef-94e2572cd14a2	\N	\N	\N	\N	\N
74	CAIU7132926	40	General/ Dry Cargo	\N	Non Dangerous	47503e6d-ab49-4279-87ef-94e2572cd14a3	\N	\N	\N	\N	\N
75	CAIU7132926	40	General/ Dry Cargo	\N	Non Dangerous	744fa35b-ccf0-4cbf-84ea-eb7ef8f80e4a1	\N	\N	\N	\N	\N
76	CAIU7132926	40	General/ Dry Cargo	\N	Non Dangerous	744fa35b-ccf0-4cbf-84ea-eb7ef8f80e4a2	\N	\N	\N	\N	\N
77	CAIU7132926	40	General/ Dry Cargo	\N	Non Dangerous	744fa35b-ccf0-4cbf-84ea-eb7ef8f80e4a3	\N	\N	\N	\N	\N
78	SKLU1324270	20	General/ Dry Cargo	Class 1: Coba	Dangerous	idRequestTest123	\N	\N	\N	\N	\N
279	WHSU5363638	40	General/ Dry Cargo	\N	Non Dangerous	149603f2-19ae-4c19-8d5b-da6acce03bcd3	\N	\N	\N	\N	\N
88	SKLU1324270	20	General/ Dry Cargo	Class 1: Coba	Dangerous	abcdef12345	\N	\N	\N	\N	\N
89	SKLU1435201	20	General/ Dry Cargo	Class 1: Coba	Dangerous	abcdef12345	\N	\N	\N	\N	\N
90	SKLU1324581	20	General/ Dry Cargo	Class 1: Coba	Dangerous	asdfghjkl1234	\N	\N	\N	\N	\N
91	SKLU1435201	20	General/ Dry Cargo	Class 1: Coba	Dangerous	abcdef12345	\N	\N	\N	\N	\N
92	SKLU1324270	20	General/ Dry Cargo	Class 1: Coba	Dangerous	abcdef12345	\N	\N	\N	\N	\N
93	SKLU1324581	20	General/ Dry Cargo	Class 1: Coba	Dangerous	asdfghjkl1234	\N	\N	\N	\N	\N
367	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d1be5e65-b186-4252-94fc-95765455f5f51	\N	\N	\N	\N	\N
368	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d1be5e65-b186-4252-94fc-95765455f5f51	\N	\N	\N	\N	\N
369	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d1be5e65-b186-4252-94fc-95765455f5f51	\N	\N	\N	\N	\N
370	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d1be5e65-b186-4252-94fc-95765455f5f51	\N	\N	\N	\N	\N
371	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d1be5e65-b186-4252-94fc-95765455f5f52	\N	\N	\N	\N	\N
372	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d1be5e65-b186-4252-94fc-95765455f5f52	\N	\N	\N	\N	\N
373	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d1be5e65-b186-4252-94fc-95765455f5f52	\N	\N	\N	\N	\N
374	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d1be5e65-b186-4252-94fc-95765455f5f52	\N	\N	\N	\N	\N
112	KOCU4657983	40	General/ Dry Cargo	\N	Non Dangerous	2d3f5569-f672-4c76-baa8-948c310c278e1	\N	\N	\N	\N	\N
113	KOCU4657983	40	General/ Dry Cargo	\N	Non Dangerous	2d3f5569-f672-4c76-baa8-948c310c278e2	\N	\N	\N	\N	\N
114	KOCU4657983	40	General/ Dry Cargo	\N	Non Dangerous	2d3f5569-f672-4c76-baa8-948c310c278e3	\N	\N	\N	\N	\N
115	BSIU9228806	40	General/ Dry Cargo	\N	Non Dangerous	a8e9e638-9cb8-411e-aff3-aedeb58fedac1	\N	\N	\N	\N	\N
116	BSIU9228806	40	General/ Dry Cargo	\N	Non Dangerous	a8e9e638-9cb8-411e-aff3-aedeb58fedac2	\N	\N	\N	\N	\N
117	BSIU9228806	40	General/ Dry Cargo	\N	Non Dangerous	a8e9e638-9cb8-411e-aff3-aedeb58fedac3	\N	\N	\N	\N	\N
375	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d1be5e65-b186-4252-94fc-95765455f5f53	\N	\N	\N	\N	\N
376	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d1be5e65-b186-4252-94fc-95765455f5f53	\N	\N	\N	\N	\N
377	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d1be5e65-b186-4252-94fc-95765455f5f53	\N	\N	\N	\N	\N
121	SEGU5850726	40	General/ Dry Cargo	\N	Non Dangerous	4637c27d-7d01-4844-a7f9-0b286fa786441	\N	\N	\N	\N	\N
122	SEGU5850726	40	General/ Dry Cargo	\N	Non Dangerous	4637c27d-7d01-4844-a7f9-0b286fa786442	\N	\N	\N	\N	\N
123	SEGU5850726	40	General/ Dry Cargo	\N	Non Dangerous	4637c27d-7d01-4844-a7f9-0b286fa786443	\N	\N	\N	\N	\N
124	KOCU4102840	40	General/ Dry Cargo	\N	Non Dangerous	d218503c-5921-4cb9-a9db-299bfd1fa7d21	\N	\N	\N	\N	\N
125	KOCU4102840	40	General/ Dry Cargo	\N	Non Dangerous	d218503c-5921-4cb9-a9db-299bfd1fa7d22	\N	\N	\N	\N	\N
126	KOCU4102840	40	General/ Dry Cargo	\N	Non Dangerous	d218503c-5921-4cb9-a9db-299bfd1fa7d23	\N	\N	\N	\N	\N
127	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	0f3531e3-682d-460d-8466-e0595d12bab41	\N	\N	\N	\N	\N
128	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	0f3531e3-682d-460d-8466-e0595d12bab42	\N	\N	\N	\N	\N
129	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	0f3531e3-682d-460d-8466-e0595d12bab43	\N	\N	\N	\N	\N
130	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	bc3c8e19-8ee5-4d27-b06b-09e5714bf9661	\N	\N	\N	\N	\N
131	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	bc3c8e19-8ee5-4d27-b06b-09e5714bf9662	\N	\N	\N	\N	\N
132	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	bc3c8e19-8ee5-4d27-b06b-09e5714bf9663	\N	\N	\N	\N	\N
133	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	85c7b7d2-66d3-4102-88fe-68916fa12a081	\N	\N	\N	\N	\N
134	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	85c7b7d2-66d3-4102-88fe-68916fa12a082	\N	\N	\N	\N	\N
135	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	85c7b7d2-66d3-4102-88fe-68916fa12a083	\N	\N	\N	\N	\N
136	KOCU4102840	40	General/ Dry Cargo	\N	Non Dangerous	1fb77b97-0be8-404e-adb8-a81abea7286e1	\N	\N	\N	\N	\N
137	KOCU4165216	40	General/ Dry Cargo	\N	Non Dangerous	1fb77b97-0be8-404e-adb8-a81abea7286e1	\N	\N	\N	\N	\N
138	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a771	\N	\N	\N	\N	\N
139	KOCU4165216	40	General/ Dry Cargo	\N	Non Dangerous	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a771	\N	\N	\N	\N	\N
140	KOCU4102840	40	General/ Dry Cargo	\N	Non Dangerous	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a771	\N	\N	\N	\N	\N
141	SEGU5850726	40	General/ Dry Cargo	\N	Non Dangerous	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a771	\N	\N	\N	\N	\N
142	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a772	\N	\N	\N	\N	\N
143	SEGU5850726	40	General/ Dry Cargo	\N	Non Dangerous	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a772	\N	\N	\N	\N	\N
144	KOCU4102840	40	General/ Dry Cargo	\N	Non Dangerous	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a772	\N	\N	\N	\N	\N
145	KOCU4165216	40	General/ Dry Cargo	\N	Non Dangerous	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a772	\N	\N	\N	\N	\N
146	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a773	\N	\N	\N	\N	\N
147	KOCU4102840	40	General/ Dry Cargo	\N	Non Dangerous	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a773	\N	\N	\N	\N	\N
148	KOCU4165216	40	General/ Dry Cargo	\N	Non Dangerous	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a773	\N	\N	\N	\N	\N
149	SEGU5850726	40	General/ Dry Cargo	\N	Non Dangerous	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a773	\N	\N	\N	\N	\N
150	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	33b34362-4519-4bd3-a6fc-cfd87e40344b1	\N	\N	\N	\N	\N
151	KOCU4102840	40	General/ Dry Cargo	\N	Non Dangerous	33b34362-4519-4bd3-a6fc-cfd87e40344b1	\N	\N	\N	\N	\N
152	KOCU4165216	40	General/ Dry Cargo	\N	Non Dangerous	33b34362-4519-4bd3-a6fc-cfd87e40344b1	\N	\N	\N	\N	\N
153	SEGU5850726	40	General/ Dry Cargo	\N	Non Dangerous	33b34362-4519-4bd3-a6fc-cfd87e40344b1	\N	\N	\N	\N	\N
154	SEGU5850726	40	General/ Dry Cargo	\N	Non Dangerous	33b34362-4519-4bd3-a6fc-cfd87e40344b2	\N	\N	\N	\N	\N
155	KOCU4102840	40	General/ Dry Cargo	\N	Non Dangerous	33b34362-4519-4bd3-a6fc-cfd87e40344b2	\N	\N	\N	\N	\N
156	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	33b34362-4519-4bd3-a6fc-cfd87e40344b2	\N	\N	\N	\N	\N
157	KOCU4165216	40	General/ Dry Cargo	\N	Non Dangerous	33b34362-4519-4bd3-a6fc-cfd87e40344b2	\N	\N	\N	\N	\N
158	TGBU5890303	40	General/ Dry Cargo	\N	Non Dangerous	33b34362-4519-4bd3-a6fc-cfd87e40344b3	\N	\N	\N	\N	\N
159	KOCU4102840	40	General/ Dry Cargo	\N	Non Dangerous	33b34362-4519-4bd3-a6fc-cfd87e40344b3	\N	\N	\N	\N	\N
160	SEGU5850726	40	General/ Dry Cargo	\N	Non Dangerous	33b34362-4519-4bd3-a6fc-cfd87e40344b3	\N	\N	\N	\N	\N
161	KOCU4165216	40	General/ Dry Cargo	\N	Non Dangerous	33b34362-4519-4bd3-a6fc-cfd87e40344b3	\N	\N	\N	\N	\N
280	WHSU5363638	40	General/ Dry Cargo	\N	Non Dangerous	fe908600-552a-4e1e-9669-52fac58a5c381	\N	\N	\N	\N	\N
281	WHSU5363638	40	General/ Dry Cargo	\N	Non Dangerous	fe908600-552a-4e1e-9669-52fac58a5c382	\N	\N	\N	\N	\N
282	WHSU5363638	40	General/ Dry Cargo	\N	Non Dangerous	fe908600-552a-4e1e-9669-52fac58a5c383	\N	\N	\N	\N	\N
319	TEMU2326434	20	General/ Dry Cargo	\N	Non Dangerous	3bd70d88-11a7-4ff6-9ad6-dd102b5668b21	\N	\N	\N	\N	\N
320	SEGU2860274	20	General/ Dry Cargo	\N	Non Dangerous	3bd70d88-11a7-4ff6-9ad6-dd102b5668b21	\N	\N	\N	\N	\N
321	NYKU3785431	20	General/ Dry Cargo	\N	Non Dangerous	3bd70d88-11a7-4ff6-9ad6-dd102b5668b21	\N	\N	\N	\N	\N
322	NYKU3462589	20	General/ Dry Cargo	\N	Non Dangerous	3bd70d88-11a7-4ff6-9ad6-dd102b5668b21	\N	\N	\N	\N	\N
323	TEMU2326434	20	General/ Dry Cargo	\N	Non Dangerous	3bd70d88-11a7-4ff6-9ad6-dd102b5668b22	\N	\N	\N	\N	\N
324	NYKU3785431	20	General/ Dry Cargo	\N	Non Dangerous	3bd70d88-11a7-4ff6-9ad6-dd102b5668b22	\N	\N	\N	\N	\N
325	NYKU3462589	20	General/ Dry Cargo	\N	Non Dangerous	3bd70d88-11a7-4ff6-9ad6-dd102b5668b22	\N	\N	\N	\N	\N
326	SEGU2860274	20	General/ Dry Cargo	\N	Non Dangerous	3bd70d88-11a7-4ff6-9ad6-dd102b5668b22	\N	\N	\N	\N	\N
327	TEMU2326434	20	General/ Dry Cargo	\N	Non Dangerous	3bd70d88-11a7-4ff6-9ad6-dd102b5668b23	\N	\N	\N	\N	\N
328	NYKU3785431	20	General/ Dry Cargo	\N	Non Dangerous	3bd70d88-11a7-4ff6-9ad6-dd102b5668b23	\N	\N	\N	\N	\N
329	SEGU2860274	20	General/ Dry Cargo	\N	Non Dangerous	3bd70d88-11a7-4ff6-9ad6-dd102b5668b23	\N	\N	\N	\N	\N
330	NYKU3462589	20	General/ Dry Cargo	\N	Non Dangerous	3bd70d88-11a7-4ff6-9ad6-dd102b5668b23	\N	\N	\N	\N	\N
349	BEAU2589221	20	General/ Dry Cargo	\N	Non Dangerous	4178de6b-a02f-4129-8174-fa372fefa7dd1	\N	\N	\N	\N	\N
350	BEAU2589221	20	General/ Dry Cargo	\N	Non Dangerous	4178de6b-a02f-4129-8174-fa372fefa7dd2	\N	\N	\N	\N	\N
351	BEAU2589221	20	General/ Dry Cargo	\N	Non Dangerous	4178de6b-a02f-4129-8174-fa372fefa7dd3	\N	\N	\N	\N	\N
378	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d1be5e65-b186-4252-94fc-95765455f5f53	\N	\N	\N	\N	\N
403	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fbaf7158-5968-4411-aba8-15517a8eeea91	\N	\N	\N	\N	\N
404	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fbaf7158-5968-4411-aba8-15517a8eeea91	\N	\N	\N	\N	\N
405	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fbaf7158-5968-4411-aba8-15517a8eeea92	\N	\N	\N	\N	\N
406	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fbaf7158-5968-4411-aba8-15517a8eeea92	\N	\N	\N	\N	\N
188	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	25f443a1-3ba4-40e2-94c8-ad1c7e68a61a1	\N	\N	\N	\N	\N
189	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	25f443a1-3ba4-40e2-94c8-ad1c7e68a61a2	\N	\N	\N	\N	\N
190	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	25f443a1-3ba4-40e2-94c8-ad1c7e68a61a3	\N	\N	\N	\N	\N
191	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	bd105723-8e85-4582-b795-172f30d86c741	\N	\N	\N	\N	\N
192	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	bd105723-8e85-4582-b795-172f30d86c742	\N	\N	\N	\N	\N
193	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	bd105723-8e85-4582-b795-172f30d86c743	\N	\N	\N	\N	\N
194	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	57832f2d-57b8-421a-a940-771be30c0a241	\N	\N	\N	\N	\N
407	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fbaf7158-5968-4411-aba8-15517a8eeea93	\N	\N	\N	\N	\N
408	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fbaf7158-5968-4411-aba8-15517a8eeea93	\N	\N	\N	\N	\N
195	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	57832f2d-57b8-421a-a940-771be30c0a242	\N	\N	\N	\N	\N
196	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	57832f2d-57b8-421a-a940-771be30c0a243	\N	\N	\N	\N	\N
301	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	bfefd7de-9a0d-4ac5-a828-f16af511b01a1	\N	\N	\N	\N	\N
302	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	bfefd7de-9a0d-4ac5-a828-f16af511b01a2	\N	\N	\N	\N	\N
303	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	bfefd7de-9a0d-4ac5-a828-f16af511b01a3	\N	\N	\N	\N	\N
331	NYKU3889800	20	General/ Dry Cargo	\N	Non Dangerous	b3b45d95-d498-41e5-8a59-7f8dac49eaa01	\N	\N	\N	\N	\N
332	NYKU3889800	20	General/ Dry Cargo	\N	Non Dangerous	b3b45d95-d498-41e5-8a59-7f8dac49eaa02	\N	\N	\N	\N	\N
333	NYKU3889800	20	General/ Dry Cargo	\N	Non Dangerous	b3b45d95-d498-41e5-8a59-7f8dac49eaa03	\N	\N	\N	\N	\N
334	TCLU7382520	20	General/ Dry Cargo	\N	Non Dangerous	c2202f16-2cd2-47d7-87be-a7cc9c09ddcf1	\N	\N	\N	\N	\N
335	TCLU7382520	20	General/ Dry Cargo	\N	Non Dangerous	c2202f16-2cd2-47d7-87be-a7cc9c09ddcf2	\N	\N	\N	\N	\N
336	TCLU7382520	20	General/ Dry Cargo	\N	Non Dangerous	c2202f16-2cd2-47d7-87be-a7cc9c09ddcf3	\N	\N	\N	\N	\N
352	BEAU2589221	20	General/ Dry Cargo	\N	Non Dangerous	64bb8e57-bae7-42d5-bee0-58b1d9082f8c1	\N	\N	\N	\N	\N
353	BEAU2589221	20	General/ Dry Cargo	\N	Non Dangerous	64bb8e57-bae7-42d5-bee0-58b1d9082f8c2	\N	\N	\N	\N	\N
354	BEAU2589221	20	General/ Dry Cargo	\N	Non Dangerous	64bb8e57-bae7-42d5-bee0-58b1d9082f8c3	\N	\N	\N	\N	\N
379	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	87efdc2a-88cb-42cb-91cc-7f235234f3ba1	\N	\N	\N	\N	\N
380	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	87efdc2a-88cb-42cb-91cc-7f235234f3ba1	\N	\N	\N	\N	\N
381	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	87efdc2a-88cb-42cb-91cc-7f235234f3ba1	\N	\N	\N	\N	\N
382	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	87efdc2a-88cb-42cb-91cc-7f235234f3ba1	\N	\N	\N	\N	\N
383	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	87efdc2a-88cb-42cb-91cc-7f235234f3ba2	\N	\N	\N	\N	\N
384	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	87efdc2a-88cb-42cb-91cc-7f235234f3ba2	\N	\N	\N	\N	\N
385	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	87efdc2a-88cb-42cb-91cc-7f235234f3ba2	\N	\N	\N	\N	\N
386	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	87efdc2a-88cb-42cb-91cc-7f235234f3ba2	\N	\N	\N	\N	\N
387	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	87efdc2a-88cb-42cb-91cc-7f235234f3ba3	\N	\N	\N	\N	\N
388	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	87efdc2a-88cb-42cb-91cc-7f235234f3ba3	\N	\N	\N	\N	\N
389	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	87efdc2a-88cb-42cb-91cc-7f235234f3ba3	\N	\N	\N	\N	\N
390	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	87efdc2a-88cb-42cb-91cc-7f235234f3ba3	\N	\N	\N	\N	\N
409	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	7dfbd8ec-d4c2-4e0e-a97c-5b7ada7a873f1	\N	\N	\N	\N	\N
410	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	7dfbd8ec-d4c2-4e0e-a97c-5b7ada7a873f2	\N	\N	\N	\N	\N
411	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	7dfbd8ec-d4c2-4e0e-a97c-5b7ada7a873f3	\N	\N	\N	\N	\N
427	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d78b2959-b4ee-43b2-b040-d51e200316bc1	\N	\N	\N	\N	\N
428	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d78b2959-b4ee-43b2-b040-d51e200316bc2	\N	\N	\N	\N	\N
429	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d78b2959-b4ee-43b2-b040-d51e200316bc3	\N	\N	\N	\N	\N
441	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fadbf713-7af9-4c0a-af44-5fc929a7f9dc1	\N	\N	\N	\N	\N
442	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fadbf713-7af9-4c0a-af44-5fc929a7f9dc1	\N	\N	\N	\N	\N
443	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fadbf713-7af9-4c0a-af44-5fc929a7f9dc1	\N	\N	\N	\N	\N
444	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fadbf713-7af9-4c0a-af44-5fc929a7f9dc1	\N	\N	\N	\N	\N
445	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fadbf713-7af9-4c0a-af44-5fc929a7f9dc2	\N	\N	\N	\N	\N
446	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fadbf713-7af9-4c0a-af44-5fc929a7f9dc2	\N	\N	\N	\N	\N
447	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fadbf713-7af9-4c0a-af44-5fc929a7f9dc2	\N	\N	\N	\N	\N
448	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fadbf713-7af9-4c0a-af44-5fc929a7f9dc2	\N	\N	\N	\N	\N
449	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fadbf713-7af9-4c0a-af44-5fc929a7f9dc3	\N	\N	\N	\N	\N
450	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fadbf713-7af9-4c0a-af44-5fc929a7f9dc3	\N	\N	\N	\N	\N
451	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fadbf713-7af9-4c0a-af44-5fc929a7f9dc3	\N	\N	\N	\N	\N
452	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fadbf713-7af9-4c0a-af44-5fc929a7f9dc3	\N	\N	\N	\N	\N
465	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5ad42164-30e0-477e-bca0-53df257c85771	\N	\N	\N	\N	\N
466	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5ad42164-30e0-477e-bca0-53df257c85771	\N	\N	\N	\N	\N
467	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5ad42164-30e0-477e-bca0-53df257c85771	\N	\N	\N	\N	\N
468	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5ad42164-30e0-477e-bca0-53df257c85771	\N	\N	\N	\N	\N
469	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5ad42164-30e0-477e-bca0-53df257c85772	\N	\N	\N	\N	\N
470	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5ad42164-30e0-477e-bca0-53df257c85772	\N	\N	\N	\N	\N
471	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5ad42164-30e0-477e-bca0-53df257c85772	\N	\N	\N	\N	\N
472	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5ad42164-30e0-477e-bca0-53df257c85772	\N	\N	\N	\N	\N
473	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5ad42164-30e0-477e-bca0-53df257c85773	\N	\N	\N	\N	\N
474	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5ad42164-30e0-477e-bca0-53df257c85773	\N	\N	\N	\N	\N
475	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5ad42164-30e0-477e-bca0-53df257c85773	\N	\N	\N	\N	\N
476	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5ad42164-30e0-477e-bca0-53df257c85773	\N	\N	\N	\N	\N
477	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	8311f5c9-e80f-4888-bdb7-1ed85dfd889a1	\N	\N	\N	\N	\N
478	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	8311f5c9-e80f-4888-bdb7-1ed85dfd889a1	\N	\N	\N	\N	\N
479	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	8311f5c9-e80f-4888-bdb7-1ed85dfd889a1	\N	\N	\N	\N	\N
480	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	8311f5c9-e80f-4888-bdb7-1ed85dfd889a1	\N	\N	\N	\N	\N
481	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	8311f5c9-e80f-4888-bdb7-1ed85dfd889a2	\N	\N	\N	\N	\N
197	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	aa0fd7e0-843b-4da6-84a1-799065767d1c1	\N	\N	\N	\N	\N
198	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	aa0fd7e0-843b-4da6-84a1-799065767d1c2	\N	\N	\N	\N	\N
199	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	aa0fd7e0-843b-4da6-84a1-799065767d1c3	\N	\N	\N	\N	\N
200	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	d18d4675-3a3c-4ad1-a635-f41a73a9a8c51	\N	\N	\N	\N	\N
201	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	d18d4675-3a3c-4ad1-a635-f41a73a9a8c52	\N	\N	\N	\N	\N
202	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	d18d4675-3a3c-4ad1-a635-f41a73a9a8c53	\N	\N	\N	\N	\N
203	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	fa909fc7-baef-47c1-b591-e8361ed119111	\N	\N	\N	\N	\N
204	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	fa909fc7-baef-47c1-b591-e8361ed119112	\N	\N	\N	\N	\N
205	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	fa909fc7-baef-47c1-b591-e8361ed119113	\N	\N	\N	\N	\N
207	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	3c94af28-9d71-4bd0-a20c-df95fbbfebd32	\N	\N	\N	\N	\N
208	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	3c94af28-9d71-4bd0-a20c-df95fbbfebd33	\N	\N	\N	\N	\N
209	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	8b0dbcd5-a60b-44b1-bb92-a3f33b3f2c171	\N	\N	\N	\N	\N
210	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	8b0dbcd5-a60b-44b1-bb92-a3f33b3f2c172	\N	\N	\N	\N	\N
211	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	8b0dbcd5-a60b-44b1-bb92-a3f33b3f2c173	\N	\N	\N	\N	\N
212	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	1fceb782-b52b-46ac-a475-1a272d04a3bf1	\N	\N	\N	\N	\N
213	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	1fceb782-b52b-46ac-a475-1a272d04a3bf2	\N	\N	\N	\N	\N
214	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	1fceb782-b52b-46ac-a475-1a272d04a3bf3	\N	\N	\N	\N	\N
215	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	a26e195e-d92f-4872-865c-9a94741a54131	\N	\N	\N	\N	\N
216	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	a26e195e-d92f-4872-865c-9a94741a54132	\N	\N	\N	\N	\N
217	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	a26e195e-d92f-4872-865c-9a94741a54133	\N	\N	\N	\N	\N
218	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	9d16f90b-0dd2-4cf2-8b0c-3443bf1c37611	\N	\N	\N	\N	\N
219	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	9d16f90b-0dd2-4cf2-8b0c-3443bf1c37612	\N	\N	\N	\N	\N
220	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	9d16f90b-0dd2-4cf2-8b0c-3443bf1c37613	\N	\N	\N	\N	\N
221	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	210a9825-32e3-4f41-a87c-84ff4b3aa9301	\N	\N	\N	\N	\N
222	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	210a9825-32e3-4f41-a87c-84ff4b3aa9302	\N	\N	\N	\N	\N
223	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	210a9825-32e3-4f41-a87c-84ff4b3aa9303	\N	\N	\N	\N	\N
224	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	65ba261d-1638-4146-ac5b-c050775bd0601	\N	\N	\N	\N	\N
225	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	65ba261d-1638-4146-ac5b-c050775bd0602	\N	\N	\N	\N	\N
226	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	65ba261d-1638-4146-ac5b-c050775bd0603	\N	\N	\N	\N	\N
227	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	7c68b67b-c627-4d77-b671-6c3cf48930601	\N	\N	\N	\N	\N
228	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	7c68b67b-c627-4d77-b671-6c3cf48930602	\N	\N	\N	\N	\N
229	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	7c68b67b-c627-4d77-b671-6c3cf48930603	\N	\N	\N	\N	\N
230	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	723299d0-6747-4b0e-b481-d1a35a89fe881	\N	\N	\N	\N	\N
231	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	723299d0-6747-4b0e-b481-d1a35a89fe882	\N	\N	\N	\N	\N
232	FSCU4855805	40	General/ Dry Cargo	\N	Non Dangerous	723299d0-6747-4b0e-b481-d1a35a89fe883	\N	\N	\N	\N	\N
233	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	5d690a8a-90e8-4b89-b173-5071987732ba1	\N	\N	\N	\N	\N
234	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	5d690a8a-90e8-4b89-b173-5071987732ba2	\N	\N	\N	\N	\N
235	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	5d690a8a-90e8-4b89-b173-5071987732ba3	\N	\N	\N	\N	\N
304	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	e0324ebe-959a-4507-aa3f-b3259b54a2d51	\N	\N	\N	\N	\N
239	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	b75ad664-77fa-4939-99fd-bac15b2dda5a1	\N	\N	\N	\N	\N
240	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	b75ad664-77fa-4939-99fd-bac15b2dda5a2	\N	\N	\N	\N	\N
241	NYKU3604261	20	General/ Dry Cargo	\N	Non Dangerous	b75ad664-77fa-4939-99fd-bac15b2dda5a3	\N	\N	\N	\N	\N
256	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	4d693fca-bd52-4aca-baea-f5578b6d6f761	\N	\N	\N	\N	\N
257	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	4d693fca-bd52-4aca-baea-f5578b6d6f762	\N	\N	\N	\N	\N
258	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	4d693fca-bd52-4aca-baea-f5578b6d6f763	\N	\N	\N	\N	\N
259	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	debeec10-2b9f-4f75-92a6-59404c83bac61	\N	\N	\N	\N	\N
305	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	e0324ebe-959a-4507-aa3f-b3259b54a2d52	\N	\N	\N	\N	\N
306	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	e0324ebe-959a-4507-aa3f-b3259b54a2d53	\N	\N	\N	\N	\N
260	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	debeec10-2b9f-4f75-92a6-59404c83bac62	\N	\N	\N	\N	\N
261	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	debeec10-2b9f-4f75-92a6-59404c83bac63	\N	\N	\N	\N	\N
262	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	b4c7b959-182a-4635-872f-4f1cd84c0ce71	\N	\N	\N	\N	\N
263	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	b4c7b959-182a-4635-872f-4f1cd84c0ce72	\N	\N	\N	\N	\N
264	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	b4c7b959-182a-4635-872f-4f1cd84c0ce73	\N	\N	\N	\N	\N
265	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	0b4a223f-fa95-439d-bdd9-069ee6e2be951	\N	\N	\N	\N	\N
266	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	0b4a223f-fa95-439d-bdd9-069ee6e2be952	\N	\N	\N	\N	\N
267	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	0b4a223f-fa95-439d-bdd9-069ee6e2be953	\N	\N	\N	\N	\N
268	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	aaf22fd8-b72a-46c4-a3d7-73e2affa38ec1	\N	\N	\N	\N	\N
269	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	aaf22fd8-b72a-46c4-a3d7-73e2affa38ec2	\N	\N	\N	\N	\N
270	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	aaf22fd8-b72a-46c4-a3d7-73e2affa38ec3	\N	\N	\N	\N	\N
271	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	d1ca6c91-6434-48b8-968a-6d937c0903001	\N	\N	\N	\N	\N
272	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	d1ca6c91-6434-48b8-968a-6d937c0903002	\N	\N	\N	\N	\N
273	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	d1ca6c91-6434-48b8-968a-6d937c0903003	\N	\N	\N	\N	\N
289	WHSU5363638	40	General/ Dry Cargo	\N	Non Dangerous	7ae4ce09-ac66-424f-a0dd-a4f36b06fb971	\N	\N	\N	\N	\N
290	WHSU5363638	40	General/ Dry Cargo	\N	Non Dangerous	7ae4ce09-ac66-424f-a0dd-a4f36b06fb972	\N	\N	\N	\N	\N
291	WHSU5363638	40	General/ Dry Cargo	\N	Non Dangerous	7ae4ce09-ac66-424f-a0dd-a4f36b06fb973	\N	\N	\N	\N	\N
307	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	7b60cd9f-bb76-4469-a0f0-2bd090349edd1	\N	\N	\N	\N	\N
308	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	7b60cd9f-bb76-4469-a0f0-2bd090349edd2	\N	\N	\N	\N	\N
309	ISMU3006513	20	General/ Dry Cargo	\N	Non Dangerous	7b60cd9f-bb76-4469-a0f0-2bd090349edd3	\N	\N	\N	\N	\N
337	FCIU4788164	20	General/ Dry Cargo	\N	Non Dangerous	c43eb4a0-a163-4c4d-a649-d0146dbd545e1	\N	\N	\N	\N	\N
338	TGHU0968864	20	General/ Dry Cargo	\N	Non Dangerous	c43eb4a0-a163-4c4d-a649-d0146dbd545e1	\N	\N	\N	\N	\N
339	FCIU4788164	20	General/ Dry Cargo	\N	Non Dangerous	c43eb4a0-a163-4c4d-a649-d0146dbd545e2	\N	\N	\N	\N	\N
340	TGHU0968864	20	General/ Dry Cargo	\N	Non Dangerous	c43eb4a0-a163-4c4d-a649-d0146dbd545e2	\N	\N	\N	\N	\N
341	TGHU0968864	20	General/ Dry Cargo	\N	Non Dangerous	c43eb4a0-a163-4c4d-a649-d0146dbd545e3	\N	\N	\N	\N	\N
342	FCIU4788164	20	General/ Dry Cargo	\N	Non Dangerous	c43eb4a0-a163-4c4d-a649-d0146dbd545e3	\N	\N	\N	\N	\N
355	BEAU2589221	20	General/ Dry Cargo	\N	Non Dangerous	099565f0-3dfa-442b-b81a-695eba3d6a9a1	\N	\N	\N	\N	\N
356	BEAU2589221	20	General/ Dry Cargo	\N	Non Dangerous	099565f0-3dfa-442b-b81a-695eba3d6a9a2	\N	\N	\N	\N	\N
357	BEAU2589221	20	General/ Dry Cargo	\N	Non Dangerous	099565f0-3dfa-442b-b81a-695eba3d6a9a3	\N	\N	\N	\N	\N
364	BEAU2589221	20	General/ Dry Cargo	\N	Non Dangerous	05f04fe5-46db-450d-a518-ad76f206c7a71	\N	\N	\N	\N	\N
365	BEAU2589221	20	General/ Dry Cargo	\N	Non Dangerous	05f04fe5-46db-450d-a518-ad76f206c7a72	\N	\N	\N	\N	\N
366	BEAU2589221	20	General/ Dry Cargo	\N	Non Dangerous	05f04fe5-46db-450d-a518-ad76f206c7a73	\N	\N	\N	\N	\N
391	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	a9b2b129-621d-4124-a3ae-5603179719f51	\N	\N	\N	\N	\N
392	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	a9b2b129-621d-4124-a3ae-5603179719f51	\N	\N	\N	\N	\N
393	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	a9b2b129-621d-4124-a3ae-5603179719f52	\N	\N	\N	\N	\N
394	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	a9b2b129-621d-4124-a3ae-5603179719f52	\N	\N	\N	\N	\N
395	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	a9b2b129-621d-4124-a3ae-5603179719f53	\N	\N	\N	\N	\N
396	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	a9b2b129-621d-4124-a3ae-5603179719f53	\N	\N	\N	\N	\N
412	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3eeeb4e2-499d-479d-ba8a-34064bf9abc91	\N	\N	\N	\N	\N
413	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3eeeb4e2-499d-479d-ba8a-34064bf9abc91	\N	\N	\N	\N	\N
414	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3eeeb4e2-499d-479d-ba8a-34064bf9abc91	\N	\N	\N	\N	\N
415	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3eeeb4e2-499d-479d-ba8a-34064bf9abc91	\N	\N	\N	\N	\N
416	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3eeeb4e2-499d-479d-ba8a-34064bf9abc92	\N	\N	\N	\N	\N
417	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3eeeb4e2-499d-479d-ba8a-34064bf9abc92	\N	\N	\N	\N	\N
418	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3eeeb4e2-499d-479d-ba8a-34064bf9abc92	\N	\N	\N	\N	\N
419	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3eeeb4e2-499d-479d-ba8a-34064bf9abc92	\N	\N	\N	\N	\N
420	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3eeeb4e2-499d-479d-ba8a-34064bf9abc93	\N	\N	\N	\N	\N
421	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3eeeb4e2-499d-479d-ba8a-34064bf9abc93	\N	\N	\N	\N	\N
422	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3eeeb4e2-499d-479d-ba8a-34064bf9abc93	\N	\N	\N	\N	\N
423	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3eeeb4e2-499d-479d-ba8a-34064bf9abc93	\N	\N	\N	\N	\N
430	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	609d6724-bf89-4019-9974-e9377026880e1	\N	\N	\N	\N	\N
431	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	609d6724-bf89-4019-9974-e9377026880e2	\N	\N	\N	\N	\N
432	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	609d6724-bf89-4019-9974-e9377026880e3	\N	\N	\N	\N	\N
433	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	609d6724-bf89-4019-9974-e9377026880e4	\N	\N	\N	\N	\N
434	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e04cf392-6415-4410-8229-fd1e47189b1a1	\N	\N	\N	\N	\N
435	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e04cf392-6415-4410-8229-fd1e47189b1a2	\N	\N	\N	\N	\N
436	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e04cf392-6415-4410-8229-fd1e47189b1a3	\N	\N	\N	\N	\N
437	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e04cf392-6415-4410-8229-fd1e47189b1a4	\N	\N	\N	\N	\N
453	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0021	\N	\N	\N	\N	\N
454	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0021	\N	\N	\N	\N	\N
455	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0021	\N	\N	\N	\N	\N
492	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	9c50f350-9086-4c0a-bf8d-49d3e0ef9d021	\N	\N	\N	\N	\N
493	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	9c50f350-9086-4c0a-bf8d-49d3e0ef9d022	\N	\N	\N	\N	\N
494	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	9c50f350-9086-4c0a-bf8d-49d3e0ef9d022	\N	\N	\N	\N	\N
495	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	9c50f350-9086-4c0a-bf8d-49d3e0ef9d022	\N	\N	\N	\N	\N
496	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	9c50f350-9086-4c0a-bf8d-49d3e0ef9d022	\N	\N	\N	\N	\N
497	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	9c50f350-9086-4c0a-bf8d-49d3e0ef9d023	\N	\N	\N	\N	\N
498	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	9c50f350-9086-4c0a-bf8d-49d3e0ef9d023	\N	\N	\N	\N	\N
499	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	9c50f350-9086-4c0a-bf8d-49d3e0ef9d023	\N	\N	\N	\N	\N
500	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	9c50f350-9086-4c0a-bf8d-49d3e0ef9d023	\N	\N	\N	\N	\N
501	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3f05edaa-dff3-49c0-b527-099f4936ad5d1	\N	\N	\N	\N	\N
502	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3f05edaa-dff3-49c0-b527-099f4936ad5d1	\N	\N	\N	\N	\N
503	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3f05edaa-dff3-49c0-b527-099f4936ad5d1	\N	\N	\N	\N	\N
504	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3f05edaa-dff3-49c0-b527-099f4936ad5d1	\N	\N	\N	\N	\N
505	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3f05edaa-dff3-49c0-b527-099f4936ad5d2	\N	\N	\N	\N	\N
506	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3f05edaa-dff3-49c0-b527-099f4936ad5d2	\N	\N	\N	\N	\N
507	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3f05edaa-dff3-49c0-b527-099f4936ad5d2	\N	\N	\N	\N	\N
508	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3f05edaa-dff3-49c0-b527-099f4936ad5d2	\N	\N	\N	\N	\N
509	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3f05edaa-dff3-49c0-b527-099f4936ad5d3	\N	\N	\N	\N	\N
510	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3f05edaa-dff3-49c0-b527-099f4936ad5d3	\N	\N	\N	\N	\N
511	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3f05edaa-dff3-49c0-b527-099f4936ad5d3	\N	\N	\N	\N	\N
512	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3f05edaa-dff3-49c0-b527-099f4936ad5d3	\N	\N	\N	\N	\N
513	EGHU3290837	20	ISO Tank	\N	Non Dangerous	324bff81-6a51-4411-83ca-b7a358904ceb1	\N	\N	\N	\N	\N
514	EGHU3290837	20	ISO Tank	\N	Non Dangerous	324bff81-6a51-4411-83ca-b7a358904ceb2	\N	\N	\N	\N	\N
515	EGHU3290837	20	ISO Tank	\N	Non Dangerous	324bff81-6a51-4411-83ca-b7a358904ceb3	\N	\N	\N	\N	\N
516	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	ca293462-4373-4cc3-98ac-c4a4f9aa9fa91	\N	\N	\N	\N	\N
517	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	ca293462-4373-4cc3-98ac-c4a4f9aa9fa92	\N	\N	\N	\N	\N
518	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	ca293462-4373-4cc3-98ac-c4a4f9aa9fa93	\N	\N	\N	\N	\N
519	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	630969ec-d774-4a38-870d-a9bd1cbb41801	\N	\N	\N	\N	\N
520	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	630969ec-d774-4a38-870d-a9bd1cbb41802	\N	\N	\N	\N	\N
521	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	630969ec-d774-4a38-870d-a9bd1cbb41803	\N	\N	\N	\N	\N
522	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5a7693cb-af7f-4243-aa21-d02bb2448f371	\N	\N	\N	\N	\N
523	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5a7693cb-af7f-4243-aa21-d02bb2448f371	\N	\N	\N	\N	\N
524	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5a7693cb-af7f-4243-aa21-d02bb2448f371	\N	\N	\N	\N	\N
525	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5a7693cb-af7f-4243-aa21-d02bb2448f371	\N	\N	\N	\N	\N
526	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5a7693cb-af7f-4243-aa21-d02bb2448f372	\N	\N	\N	\N	\N
527	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5a7693cb-af7f-4243-aa21-d02bb2448f372	\N	\N	\N	\N	\N
528	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5a7693cb-af7f-4243-aa21-d02bb2448f372	\N	\N	\N	\N	\N
529	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5a7693cb-af7f-4243-aa21-d02bb2448f372	\N	\N	\N	\N	\N
530	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5a7693cb-af7f-4243-aa21-d02bb2448f373	\N	\N	\N	\N	\N
531	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5a7693cb-af7f-4243-aa21-d02bb2448f373	\N	\N	\N	\N	\N
532	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5a7693cb-af7f-4243-aa21-d02bb2448f373	\N	\N	\N	\N	\N
533	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5a7693cb-af7f-4243-aa21-d02bb2448f373	\N	\N	\N	\N	\N
534	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f1	\N	\N	\N	\N	\N
535	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f1	\N	\N	\N	\N	\N
536	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f1	\N	\N	\N	\N	\N
537	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f1	\N	\N	\N	\N	\N
538	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f2	\N	\N	\N	\N	\N
539	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f2	\N	\N	\N	\N	\N
540	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f2	\N	\N	\N	\N	\N
541	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f2	\N	\N	\N	\N	\N
542	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f3	\N	\N	\N	\N	\N
543	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f3	\N	\N	\N	\N	\N
544	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f3	\N	\N	\N	\N	\N
545	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f3	\N	\N	\N	\N	\N
546	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	6bc3fd96-267c-46f7-953d-85b068c3d6291	\N	\N	\N	\N	\N
547	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	6bc3fd96-267c-46f7-953d-85b068c3d6291	\N	\N	\N	\N	\N
548	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	6bc3fd96-267c-46f7-953d-85b068c3d6291	\N	\N	\N	\N	\N
549	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	6bc3fd96-267c-46f7-953d-85b068c3d6291	\N	\N	\N	\N	\N
550	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	6bc3fd96-267c-46f7-953d-85b068c3d6292	\N	\N	\N	\N	\N
551	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	6bc3fd96-267c-46f7-953d-85b068c3d6292	\N	\N	\N	\N	\N
552	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	6bc3fd96-267c-46f7-953d-85b068c3d6292	\N	\N	\N	\N	\N
553	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	6bc3fd96-267c-46f7-953d-85b068c3d6292	\N	\N	\N	\N	\N
554	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	6bc3fd96-267c-46f7-953d-85b068c3d6293	\N	\N	\N	\N	\N
555	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	6bc3fd96-267c-46f7-953d-85b068c3d6293	\N	\N	\N	\N	\N
556	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	6bc3fd96-267c-46f7-953d-85b068c3d6293	\N	\N	\N	\N	\N
557	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	6bc3fd96-267c-46f7-953d-85b068c3d6293	\N	\N	\N	\N	\N
558	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	c128f8e8-5519-4373-898d-f23a6125a4521	\N	\N	\N	\N	\N
559	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	c128f8e8-5519-4373-898d-f23a6125a4521	\N	\N	\N	\N	\N
560	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c128f8e8-5519-4373-898d-f23a6125a4521	\N	\N	\N	\N	\N
561	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c128f8e8-5519-4373-898d-f23a6125a4521	\N	\N	\N	\N	\N
562	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c128f8e8-5519-4373-898d-f23a6125a4522	\N	\N	\N	\N	\N
563	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	c128f8e8-5519-4373-898d-f23a6125a4522	\N	\N	\N	\N	\N
564	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	c128f8e8-5519-4373-898d-f23a6125a4522	\N	\N	\N	\N	\N
565	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c128f8e8-5519-4373-898d-f23a6125a4522	\N	\N	\N	\N	\N
566	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	c128f8e8-5519-4373-898d-f23a6125a4523	\N	\N	\N	\N	\N
567	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c128f8e8-5519-4373-898d-f23a6125a4523	\N	\N	\N	\N	\N
568	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c128f8e8-5519-4373-898d-f23a6125a4523	\N	\N	\N	\N	\N
569	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	c128f8e8-5519-4373-898d-f23a6125a4523	\N	\N	\N	\N	\N
570	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fd08da2d-ce43-4d99-92f7-153b732b79301	\N	\N	\N	\N	\N
571	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fd08da2d-ce43-4d99-92f7-153b732b79301	\N	\N	\N	\N	\N
572	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fd08da2d-ce43-4d99-92f7-153b732b79301	\N	\N	\N	\N	\N
573	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fd08da2d-ce43-4d99-92f7-153b732b79301	\N	\N	\N	\N	\N
574	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fd08da2d-ce43-4d99-92f7-153b732b79302	\N	\N	\N	\N	\N
575	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fd08da2d-ce43-4d99-92f7-153b732b79302	\N	\N	\N	\N	\N
576	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fd08da2d-ce43-4d99-92f7-153b732b79302	\N	\N	\N	\N	\N
577	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fd08da2d-ce43-4d99-92f7-153b732b79302	\N	\N	\N	\N	\N
578	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fd08da2d-ce43-4d99-92f7-153b732b79303	\N	\N	\N	\N	\N
579	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fd08da2d-ce43-4d99-92f7-153b732b79303	\N	\N	\N	\N	\N
580	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fd08da2d-ce43-4d99-92f7-153b732b79303	\N	\N	\N	\N	\N
581	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	fd08da2d-ce43-4d99-92f7-153b732b79303	\N	\N	\N	\N	\N
582	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3d384c0c-574a-4c00-a918-9e76e59fad8c1	\N	\N	\N	\N	\N
583	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3d384c0c-574a-4c00-a918-9e76e59fad8c1	\N	\N	\N	\N	\N
584	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3d384c0c-574a-4c00-a918-9e76e59fad8c1	\N	\N	\N	\N	\N
585	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3d384c0c-574a-4c00-a918-9e76e59fad8c1	\N	\N	\N	\N	\N
586	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3d384c0c-574a-4c00-a918-9e76e59fad8c2	\N	\N	\N	\N	\N
587	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3d384c0c-574a-4c00-a918-9e76e59fad8c2	\N	\N	\N	\N	\N
588	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3d384c0c-574a-4c00-a918-9e76e59fad8c2	\N	\N	\N	\N	\N
589	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3d384c0c-574a-4c00-a918-9e76e59fad8c2	\N	\N	\N	\N	\N
590	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3d384c0c-574a-4c00-a918-9e76e59fad8c3	\N	\N	\N	\N	\N
591	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3d384c0c-574a-4c00-a918-9e76e59fad8c3	\N	\N	\N	\N	\N
592	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3d384c0c-574a-4c00-a918-9e76e59fad8c3	\N	\N	\N	\N	\N
593	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3d384c0c-574a-4c00-a918-9e76e59fad8c3	\N	\N	\N	\N	\N
594	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	393deb31-8169-4cf2-8ba9-ec90f20dcdc11	\N	\N	\N	\N	\N
595	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	393deb31-8169-4cf2-8ba9-ec90f20dcdc11	\N	\N	\N	\N	\N
596	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	393deb31-8169-4cf2-8ba9-ec90f20dcdc11	\N	\N	\N	\N	\N
597	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	393deb31-8169-4cf2-8ba9-ec90f20dcdc11	\N	\N	\N	\N	\N
598	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	393deb31-8169-4cf2-8ba9-ec90f20dcdc12	\N	\N	\N	\N	\N
599	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	393deb31-8169-4cf2-8ba9-ec90f20dcdc12	\N	\N	\N	\N	\N
600	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	393deb31-8169-4cf2-8ba9-ec90f20dcdc12	\N	\N	\N	\N	\N
601	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	393deb31-8169-4cf2-8ba9-ec90f20dcdc12	\N	\N	\N	\N	\N
602	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	393deb31-8169-4cf2-8ba9-ec90f20dcdc13	\N	\N	\N	\N	\N
603	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	393deb31-8169-4cf2-8ba9-ec90f20dcdc13	\N	\N	\N	\N	\N
604	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	393deb31-8169-4cf2-8ba9-ec90f20dcdc13	\N	\N	\N	\N	\N
605	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	393deb31-8169-4cf2-8ba9-ec90f20dcdc13	\N	\N	\N	\N	\N
606	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece1	\N	\N	\N	\N	\N
607	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece1	\N	\N	\N	\N	\N
608	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece1	\N	\N	\N	\N	\N
609	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece1	\N	\N	\N	\N	\N
610	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece2	\N	\N	\N	\N	\N
611	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece2	\N	\N	\N	\N	\N
612	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece2	\N	\N	\N	\N	\N
613	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece2	\N	\N	\N	\N	\N
614	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece3	\N	\N	\N	\N	\N
615	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece3	\N	\N	\N	\N	\N
616	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece3	\N	\N	\N	\N	\N
617	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece3	\N	\N	\N	\N	\N
618	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	adc33ecb-2c20-414c-b33d-8578e66c3f8e1	\N	\N	\N	\N	\N
619	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	adc33ecb-2c20-414c-b33d-8578e66c3f8e1	\N	\N	\N	\N	\N
620	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	adc33ecb-2c20-414c-b33d-8578e66c3f8e1	\N	\N	\N	\N	\N
621	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	adc33ecb-2c20-414c-b33d-8578e66c3f8e1	\N	\N	\N	\N	\N
622	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	adc33ecb-2c20-414c-b33d-8578e66c3f8e2	\N	\N	\N	\N	\N
623	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	adc33ecb-2c20-414c-b33d-8578e66c3f8e2	\N	\N	\N	\N	\N
624	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	adc33ecb-2c20-414c-b33d-8578e66c3f8e2	\N	\N	\N	\N	\N
625	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	adc33ecb-2c20-414c-b33d-8578e66c3f8e2	\N	\N	\N	\N	\N
626	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	adc33ecb-2c20-414c-b33d-8578e66c3f8e3	\N	\N	\N	\N	\N
627	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	adc33ecb-2c20-414c-b33d-8578e66c3f8e3	\N	\N	\N	\N	\N
628	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	adc33ecb-2c20-414c-b33d-8578e66c3f8e3	\N	\N	\N	\N	\N
629	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	adc33ecb-2c20-414c-b33d-8578e66c3f8e3	\N	\N	\N	\N	\N
630	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3bb5f3cf-f3df-4bc1-b885-3bcd03f113501	\N	\N	\N	\N	\N
631	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3bb5f3cf-f3df-4bc1-b885-3bcd03f113501	\N	\N	\N	\N	\N
632	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3bb5f3cf-f3df-4bc1-b885-3bcd03f113501	\N	\N	\N	\N	\N
633	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3bb5f3cf-f3df-4bc1-b885-3bcd03f113501	\N	\N	\N	\N	\N
634	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3bb5f3cf-f3df-4bc1-b885-3bcd03f113502	\N	\N	\N	\N	\N
635	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3bb5f3cf-f3df-4bc1-b885-3bcd03f113502	\N	\N	\N	\N	\N
636	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3bb5f3cf-f3df-4bc1-b885-3bcd03f113502	\N	\N	\N	\N	\N
637	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3bb5f3cf-f3df-4bc1-b885-3bcd03f113502	\N	\N	\N	\N	\N
638	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3bb5f3cf-f3df-4bc1-b885-3bcd03f113503	\N	\N	\N	\N	\N
639	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3bb5f3cf-f3df-4bc1-b885-3bcd03f113503	\N	\N	\N	\N	\N
640	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	3bb5f3cf-f3df-4bc1-b885-3bcd03f113503	\N	\N	\N	\N	\N
641	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	3bb5f3cf-f3df-4bc1-b885-3bcd03f113503	\N	\N	\N	\N	\N
642	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	675ebb61-bdde-4195-be83-365448ab00de1	\N	\N	\N	\N	\N
643	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	675ebb61-bdde-4195-be83-365448ab00de1	\N	\N	\N	\N	\N
644	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	675ebb61-bdde-4195-be83-365448ab00de1	\N	\N	\N	\N	\N
645	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	675ebb61-bdde-4195-be83-365448ab00de1	\N	\N	\N	\N	\N
646	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	675ebb61-bdde-4195-be83-365448ab00de2	\N	\N	\N	\N	\N
647	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	675ebb61-bdde-4195-be83-365448ab00de2	\N	\N	\N	\N	\N
648	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	675ebb61-bdde-4195-be83-365448ab00de2	\N	\N	\N	\N	\N
649	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	675ebb61-bdde-4195-be83-365448ab00de2	\N	\N	\N	\N	\N
650	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	675ebb61-bdde-4195-be83-365448ab00de3	\N	\N	\N	\N	\N
651	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	675ebb61-bdde-4195-be83-365448ab00de3	\N	\N	\N	\N	\N
652	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	675ebb61-bdde-4195-be83-365448ab00de3	\N	\N	\N	\N	\N
653	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	675ebb61-bdde-4195-be83-365448ab00de3	\N	\N	\N	\N	\N
654	NYKU3462589	20	General/ Dry Cargo	\N	Non Dangerous	13a77a62-81c5-4aef-a0cc-f9fe4da444131	\N	\N	\N	\N	\N
655	NYKU3462589	20	General/ Dry Cargo	\N	Non Dangerous	13a77a62-81c5-4aef-a0cc-f9fe4da444132	\N	\N	\N	\N	\N
656	NYKU3462589	20	General/ Dry Cargo	\N	Non Dangerous	13a77a62-81c5-4aef-a0cc-f9fe4da444133	\N	\N	\N	\N	\N
657	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	31db3546-d72b-4b12-8f06-228fdf5787ab1	\N	\N	\N	\N	\N
658	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	31db3546-d72b-4b12-8f06-228fdf5787ab1	\N	\N	\N	\N	\N
659	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	31db3546-d72b-4b12-8f06-228fdf5787ab2	\N	\N	\N	\N	\N
660	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	31db3546-d72b-4b12-8f06-228fdf5787ab2	\N	\N	\N	\N	\N
661	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	31db3546-d72b-4b12-8f06-228fdf5787ab3	\N	\N	\N	\N	\N
662	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	31db3546-d72b-4b12-8f06-228fdf5787ab3	\N	\N	\N	\N	\N
663	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	31db3546-d72b-4b12-8f06-228fdf5787ab4	\N	\N	\N	\N	\N
664	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	31db3546-d72b-4b12-8f06-228fdf5787ab4	\N	\N	\N	\N	\N
665	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	c41d9cfe-d43b-4684-8e47-f024cf38745a1	\N	\N	\N	\N	\N
666	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c41d9cfe-d43b-4684-8e47-f024cf38745a1	\N	\N	\N	\N	\N
667	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	c41d9cfe-d43b-4684-8e47-f024cf38745a2	\N	\N	\N	\N	\N
668	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c41d9cfe-d43b-4684-8e47-f024cf38745a2	\N	\N	\N	\N	\N
669	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	c41d9cfe-d43b-4684-8e47-f024cf38745a3	\N	\N	\N	\N	\N
670	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c41d9cfe-d43b-4684-8e47-f024cf38745a3	\N	\N	\N	\N	\N
671	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	c41d9cfe-d43b-4684-8e47-f024cf38745a4	\N	\N	\N	\N	\N
672	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c41d9cfe-d43b-4684-8e47-f024cf38745a4	\N	\N	\N	\N	\N
673	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5d4d1702-cea0-48a2-806a-e49dece6fcc21	\N	\N	\N	\N	\N
674	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5d4d1702-cea0-48a2-806a-e49dece6fcc21	\N	\N	\N	\N	\N
675	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5d4d1702-cea0-48a2-806a-e49dece6fcc22	\N	\N	\N	\N	\N
676	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5d4d1702-cea0-48a2-806a-e49dece6fcc22	\N	\N	\N	\N	\N
677	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5d4d1702-cea0-48a2-806a-e49dece6fcc23	\N	\N	\N	\N	\N
678	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5d4d1702-cea0-48a2-806a-e49dece6fcc23	\N	\N	\N	\N	\N
679	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	5d4d1702-cea0-48a2-806a-e49dece6fcc24	\N	\N	\N	\N	\N
680	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	5d4d1702-cea0-48a2-806a-e49dece6fcc24	\N	\N	\N	\N	\N
681	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01551	\N	\N	\N	\N	\N
682	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01551	\N	\N	\N	\N	\N
683	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01551	\N	\N	\N	\N	\N
684	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01551	\N	\N	\N	\N	\N
685	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01552	\N	\N	\N	\N	\N
686	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01552	\N	\N	\N	\N	\N
687	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01552	\N	\N	\N	\N	\N
688	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01552	\N	\N	\N	\N	\N
689	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01553	\N	\N	\N	\N	\N
690	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01553	\N	\N	\N	\N	\N
691	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01553	\N	\N	\N	\N	\N
692	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01553	\N	\N	\N	\N	\N
693	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01554	\N	\N	\N	\N	\N
694	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01554	\N	\N	\N	\N	\N
695	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01554	\N	\N	\N	\N	\N
696	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	b1ee4420-1ec8-4632-bc23-f7884f4a01554	\N	\N	\N	\N	\N
713	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4511	\N	\N	\N	\N	\N
714	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4511	\N	\N	\N	\N	\N
715	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4511	\N	\N	\N	\N	\N
716	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4511	\N	\N	\N	\N	\N
717	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4512	\N	\N	\N	\N	\N
718	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4512	\N	\N	\N	\N	\N
719	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4512	\N	\N	\N	\N	\N
720	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4512	\N	\N	\N	\N	\N
721	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4513	\N	\N	\N	\N	\N
722	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4513	\N	\N	\N	\N	\N
723	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4513	\N	\N	\N	\N	\N
724	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4513	\N	\N	\N	\N	\N
725	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4514	\N	\N	\N	\N	\N
726	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4514	\N	\N	\N	\N	\N
727	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4514	\N	\N	\N	\N	\N
728	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	11731f42-a238-4d5a-98e4-ea2bce56a4514	\N	\N	\N	\N	\N
697	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f891	\N	\N	\N	\N	\N
698	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f891	\N	\N	\N	\N	\N
699	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f891	\N	\N	\N	\N	\N
700	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f891	\N	\N	\N	\N	\N
701	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f892	\N	\N	\N	\N	\N
702	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f892	\N	\N	\N	\N	\N
703	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f892	\N	\N	\N	\N	\N
704	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f892	\N	\N	\N	\N	\N
705	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f893	\N	\N	\N	\N	\N
706	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f893	\N	\N	\N	\N	\N
707	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f893	\N	\N	\N	\N	\N
708	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f893	\N	\N	\N	\N	\N
709	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f894	\N	\N	\N	\N	\N
710	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f894	\N	\N	\N	\N	\N
711	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f894	\N	\N	\N	\N	\N
712	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f894	\N	\N	\N	\N	\N
729	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e1	\N	\N	\N	\N	\N
730	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e1	\N	\N	\N	\N	\N
731	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e2	\N	\N	\N	\N	\N
732	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e2	\N	\N	\N	\N	\N
733	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e3	\N	\N	\N	\N	\N
734	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e3	\N	\N	\N	\N	\N
735	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e4	\N	\N	\N	\N	\N
736	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e4	\N	\N	\N	\N	\N
737	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b1	\N	\N	\N	\N	\N
738	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b1	\N	\N	\N	\N	\N
739	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b1	\N	\N	\N	\N	\N
740	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b1	\N	\N	\N	\N	\N
741	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b2	\N	\N	\N	\N	\N
742	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b2	\N	\N	\N	\N	\N
743	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b2	\N	\N	\N	\N	\N
744	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b2	\N	\N	\N	\N	\N
745	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b3	\N	\N	\N	\N	\N
746	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b3	\N	\N	\N	\N	\N
747	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b3	\N	\N	\N	\N	\N
748	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b3	\N	\N	\N	\N	\N
749	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b4	\N	\N	\N	\N	\N
750	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b4	\N	\N	\N	\N	\N
751	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b4	\N	\N	\N	\N	\N
752	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e4f6d78c-6d48-436b-a387-6406950aee9b4	\N	\N	\N	\N	\N
753	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211581	\N	\N	\N	\N	\N
754	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211581	\N	\N	\N	\N	\N
755	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211581	\N	\N	\N	\N	\N
756	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211581	\N	\N	\N	\N	\N
757	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211582	\N	\N	\N	\N	\N
758	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211582	\N	\N	\N	\N	\N
759	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211582	\N	\N	\N	\N	\N
760	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211582	\N	\N	\N	\N	\N
761	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211583	\N	\N	\N	\N	\N
762	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211583	\N	\N	\N	\N	\N
763	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211583	\N	\N	\N	\N	\N
764	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211583	\N	\N	\N	\N	\N
765	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211584	\N	\N	\N	\N	\N
766	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211584	\N	\N	\N	\N	\N
767	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211584	\N	\N	\N	\N	\N
768	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	03a1f255-8904-448c-a71b-aa588b3211584	\N	\N	\N	\N	\N
769	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	a3efbc31-a9d2-4a3d-8328-9ae89d0460991	\N	\N	\N	\N	\N
770	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	a3efbc31-a9d2-4a3d-8328-9ae89d0460992	\N	\N	\N	\N	\N
771	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	a3efbc31-a9d2-4a3d-8328-9ae89d0460993	\N	\N	\N	\N	\N
772	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	a3efbc31-a9d2-4a3d-8328-9ae89d0460994	\N	\N	\N	\N	\N
773	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db301	\N	\N	\N	\N	\N
774	HDMU6721182	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db301	\N	\N	\N	\N	\N
775	BMOU5168025	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db301	\N	\N	\N	\N	\N
776	CAIU4439082	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db301	\N	\N	\N	\N	\N
777	TCNU5064786	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db301	\N	\N	\N	\N	\N
778	BMOU5168025	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db302	\N	\N	\N	\N	\N
779	HDMU6721182	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db302	\N	\N	\N	\N	\N
780	TCNU5064786	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db302	\N	\N	\N	\N	\N
781	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db302	\N	\N	\N	\N	\N
782	CAIU4439082	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db302	\N	\N	\N	\N	\N
783	HDMU6721182	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db303	\N	\N	\N	\N	\N
784	CAIU4439082	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db303	\N	\N	\N	\N	\N
785	TCNU5064786	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db303	\N	\N	\N	\N	\N
786	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db303	\N	\N	\N	\N	\N
787	BMOU5168025	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db303	\N	\N	\N	\N	\N
788	HDMU6721182	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db304	\N	\N	\N	\N	\N
789	CAIU4439082	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db304	\N	\N	\N	\N	\N
790	BMOU5168025	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db304	\N	\N	\N	\N	\N
791	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db304	\N	\N	\N	\N	\N
792	TCNU5064786	40	General/ Dry Cargo	\N	Non Dangerous	0ab7f929-0295-4a27-b49d-e2ab8df8db304	\N	\N	\N	\N	\N
793	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5111	\N	\N	\N	\N	\N
794	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5111	\N	\N	\N	\N	\N
795	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5111	\N	\N	\N	\N	\N
796	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5111	\N	\N	\N	\N	\N
797	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5112	\N	\N	\N	\N	\N
798	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5112	\N	\N	\N	\N	\N
799	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5112	\N	\N	\N	\N	\N
800	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5112	\N	\N	\N	\N	\N
801	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5113	\N	\N	\N	\N	\N
802	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5113	\N	\N	\N	\N	\N
803	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5113	\N	\N	\N	\N	\N
804	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5113	\N	\N	\N	\N	\N
805	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5114	\N	\N	\N	\N	\N
806	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5114	\N	\N	\N	\N	\N
807	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5114	\N	\N	\N	\N	\N
808	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d6791664-e59a-45e5-b5ee-5a1f5eb6b5114	\N	\N	\N	\N	\N
809	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fd79d7ee-fe24-47ea-b81c-f41953d398b91	\N	\N	\N	\N	\N
810	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fd79d7ee-fe24-47ea-b81c-f41953d398b92	\N	\N	\N	\N	\N
811	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fd79d7ee-fe24-47ea-b81c-f41953d398b93	\N	\N	\N	\N	\N
812	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	fd79d7ee-fe24-47ea-b81c-f41953d398b94	\N	\N	\N	\N	\N
813	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	2c807148-d49d-493f-8aff-6d550797ca571	\N	\N	\N	\N	\N
814	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	2c807148-d49d-493f-8aff-6d550797ca571	\N	\N	\N	\N	\N
815	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	2c807148-d49d-493f-8aff-6d550797ca572	\N	\N	\N	\N	\N
816	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	2c807148-d49d-493f-8aff-6d550797ca572	\N	\N	\N	\N	\N
817	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	2c807148-d49d-493f-8aff-6d550797ca573	\N	\N	\N	\N	\N
818	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	2c807148-d49d-493f-8aff-6d550797ca573	\N	\N	\N	\N	\N
819	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	2c807148-d49d-493f-8aff-6d550797ca574	\N	\N	\N	\N	\N
820	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	2c807148-d49d-493f-8aff-6d550797ca574	\N	\N	\N	\N	\N
821	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d050ea8f-13f6-4677-863c-0b2b5bad0dab1	\N	\N	\N	\N	\N
822	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d050ea8f-13f6-4677-863c-0b2b5bad0dab2	\N	\N	\N	\N	\N
823	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d050ea8f-13f6-4677-863c-0b2b5bad0dab3	\N	\N	\N	\N	\N
824	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d050ea8f-13f6-4677-863c-0b2b5bad0dab4	\N	\N	\N	\N	\N
825	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd1	\N	\N	\N	\N	\N
826	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd2	\N	\N	\N	\N	\N
827	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd3	\N	\N	\N	\N	\N
828	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd4	\N	\N	\N	\N	\N
829	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	8c689fea-dc94-4e79-add1-a100863461991	\N	\N	\N	\N	\N
830	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	8c689fea-dc94-4e79-add1-a100863461992	\N	\N	\N	\N	\N
831	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	8c689fea-dc94-4e79-add1-a100863461993	\N	\N	\N	\N	\N
832	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	8c689fea-dc94-4e79-add1-a100863461994	\N	\N	\N	\N	\N
833	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	6e2cf3e8-9c58-4fc1-9524-06cbc5a407601	\N	\N	\N	\N	\N
834	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	6e2cf3e8-9c58-4fc1-9524-06cbc5a407602	\N	\N	\N	\N	\N
835	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	6e2cf3e8-9c58-4fc1-9524-06cbc5a407603	\N	\N	\N	\N	\N
836	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	6e2cf3e8-9c58-4fc1-9524-06cbc5a407604	\N	\N	\N	\N	\N
837	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef821	\N	\N	\N	\N	\N
838	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef821	\N	\N	\N	\N	\N
839	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef821	\N	\N	\N	\N	\N
840	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef821	\N	\N	\N	\N	\N
841	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef822	\N	\N	\N	\N	\N
842	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef822	\N	\N	\N	\N	\N
843	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef822	\N	\N	\N	\N	\N
844	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef822	\N	\N	\N	\N	\N
845	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef823	\N	\N	\N	\N	\N
846	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef823	\N	\N	\N	\N	\N
847	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef823	\N	\N	\N	\N	\N
848	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef823	\N	\N	\N	\N	\N
849	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef824	\N	\N	\N	\N	\N
850	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef824	\N	\N	\N	\N	\N
851	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef824	\N	\N	\N	\N	\N
852	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e64169f7-5a28-4596-963a-c82f7518ef824	\N	\N	\N	\N	\N
853	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	bdaa653c-bf1c-4cc6-8fc1-758112cea4281	\N	\N	\N	\N	\N
854	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	bdaa653c-bf1c-4cc6-8fc1-758112cea4282	\N	\N	\N	\N	\N
855	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	bdaa653c-bf1c-4cc6-8fc1-758112cea4283	\N	\N	\N	\N	\N
856	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	bdaa653c-bf1c-4cc6-8fc1-758112cea4284	\N	\N	\N	\N	\N
857	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	ed880f83-a3e2-4505-a043-401dbaf33c3e1	\N	\N	\N	\N	\N
858	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	ed880f83-a3e2-4505-a043-401dbaf33c3e1	\N	\N	\N	\N	\N
859	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	ed880f83-a3e2-4505-a043-401dbaf33c3e2	\N	\N	\N	\N	\N
860	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	ed880f83-a3e2-4505-a043-401dbaf33c3e2	\N	\N	\N	\N	\N
861	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	\N	\N	\N	\N
862	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	ed880f83-a3e2-4505-a043-401dbaf33c3e3	\N	\N	\N	\N	\N
863	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	ed880f83-a3e2-4505-a043-401dbaf33c3e4	\N	\N	\N	\N	\N
864	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	ed880f83-a3e2-4505-a043-401dbaf33c3e4	\N	\N	\N	\N	\N
865	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	b099e47b-2c40-4280-b3b9-d3679d2f8c621	\N	\N	\N	\N	\N
866	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	b099e47b-2c40-4280-b3b9-d3679d2f8c622	\N	\N	\N	\N	\N
867	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	b099e47b-2c40-4280-b3b9-d3679d2f8c623	\N	\N	\N	\N	\N
868	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	b099e47b-2c40-4280-b3b9-d3679d2f8c624	\N	\N	\N	\N	\N
869	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f1	\N	\N	\N	\N	\N
870	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f1	\N	\N	\N	\N	\N
871	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f1	\N	\N	\N	\N	\N
872	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f1	\N	\N	\N	\N	\N
873	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f2	\N	\N	\N	\N	\N
874	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f2	\N	\N	\N	\N	\N
875	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f2	\N	\N	\N	\N	\N
876	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f2	\N	\N	\N	\N	\N
877	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f3	\N	\N	\N	\N	\N
878	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f3	\N	\N	\N	\N	\N
879	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f3	\N	\N	\N	\N	\N
880	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f3	\N	\N	\N	\N	\N
881	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f4	\N	\N	\N	\N	\N
882	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f4	\N	\N	\N	\N	\N
883	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f4	\N	\N	\N	\N	\N
884	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	e34ba684-8663-41ed-9f80-a1d5bea85f0f4	\N	\N	\N	\N	\N
885	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495411	\N	\N	\N	\N	\N
886	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495411	\N	\N	\N	\N	\N
887	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495411	\N	\N	\N	\N	\N
888	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495411	\N	\N	\N	\N	\N
889	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495412	\N	\N	\N	\N	\N
890	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495412	\N	\N	\N	\N	\N
891	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495412	\N	\N	\N	\N	\N
892	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495412	\N	\N	\N	\N	\N
893	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495413	\N	\N	\N	\N	\N
894	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495413	\N	\N	\N	\N	\N
895	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495413	\N	\N	\N	\N	\N
896	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495413	\N	\N	\N	\N	\N
897	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495414	\N	\N	\N	\N	\N
898	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495414	\N	\N	\N	\N	\N
899	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495414	\N	\N	\N	\N	\N
900	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	d00708c9-c77a-42a9-b15b-abd76e2495414	\N	\N	\N	\N	\N
901	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	60b0c35d-a58b-4bc8-a2e3-249a820a3f241	\N	\N	\N	\N	\N
902	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	60b0c35d-a58b-4bc8-a2e3-249a820a3f242	\N	\N	\N	\N	\N
903	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	60b0c35d-a58b-4bc8-a2e3-249a820a3f243	\N	\N	\N	\N	\N
904	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	60b0c35d-a58b-4bc8-a2e3-249a820a3f244	\N	\N	\N	\N	\N
905	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d8beea02-0ae4-4bf3-bf90-089a2382b1e11	\N	\N	\N	\N	\N
906	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d8beea02-0ae4-4bf3-bf90-089a2382b1e12	\N	\N	\N	\N	\N
907	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d8beea02-0ae4-4bf3-bf90-089a2382b1e13	\N	\N	\N	\N	\N
908	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	d8beea02-0ae4-4bf3-bf90-089a2382b1e14	\N	\N	\N	\N	\N
909	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c69824d3-7a04-4dab-9243-a4663e3ee4781	\N	\N	\N	\N	\N
910	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c69824d3-7a04-4dab-9243-a4663e3ee4782	\N	\N	\N	\N	\N
911	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c69824d3-7a04-4dab-9243-a4663e3ee4783	\N	\N	\N	\N	\N
912	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	c69824d3-7a04-4dab-9243-a4663e3ee4784	\N	\N	\N	\N	\N
913	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac031	\N	\N	\N	\N	\N
914	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac031	\N	\N	\N	\N	\N
915	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac031	\N	\N	\N	\N	\N
916	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac031	\N	\N	\N	\N	\N
917	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac032	\N	\N	\N	\N	\N
918	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac032	\N	\N	\N	\N	\N
919	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac032	\N	\N	\N	\N	\N
920	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac032	\N	\N	\N	\N	\N
921	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac033	\N	\N	\N	\N	\N
922	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac033	\N	\N	\N	\N	\N
923	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac033	\N	\N	\N	\N	\N
924	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac033	\N	\N	\N	\N	\N
925	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac034	\N	\N	\N	\N	\N
926	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac034	\N	\N	\N	\N	\N
927	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac034	\N	\N	\N	\N	\N
928	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	89a90d27-3410-4d0e-b990-32ea7f1aac034	\N	\N	\N	\N	\N
929	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1ec0c1fe-6b84-44fe-924e-6d416f35f04d1	\N	\N	\N	\N	\N
930	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1ec0c1fe-6b84-44fe-924e-6d416f35f04d1	\N	\N	\N	\N	\N
931	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1ec0c1fe-6b84-44fe-924e-6d416f35f04d2	\N	\N	\N	\N	\N
932	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1ec0c1fe-6b84-44fe-924e-6d416f35f04d2	\N	\N	\N	\N	\N
933	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	\N	\N	\N	\N	\N
934	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	\N	\N	\N	\N	\N
935	EISU2222223	20	General/ Dry Cargo	\N	Non Dangerous	1ec0c1fe-6b84-44fe-924e-6d416f35f04d4	\N	\N	\N	\N	\N
936	EGHU3290837	20	General/ Dry Cargo	\N	Non Dangerous	1ec0c1fe-6b84-44fe-924e-6d416f35f04d4	\N	\N	\N	\N	\N
937	BMOU5168025	40	General/ Dry Cargo	\N	Non Dangerous	437ebef7-53dd-4738-84dc-263aa239e6c71	\N	\N	\N	\N	\N
938	BMOU5168025	40	General/ Dry Cargo	\N	Non Dangerous	437ebef7-53dd-4738-84dc-263aa239e6c72	\N	\N	\N	\N	\N
939	BMOU5168025	40	General/ Dry Cargo	\N	Non Dangerous	437ebef7-53dd-4738-84dc-263aa239e6c73	\N	\N	\N	\N	\N
940	BMOU5168025	40	General/ Dry Cargo	\N	Non Dangerous	437ebef7-53dd-4738-84dc-263aa239e6c74	\N	\N	\N	\N	\N
941	TCNU5064786	40	General/ Dry Cargo	\N	Non Dangerous	f65452ac-5fe1-45c9-b14f-f27da4d788941	\N	\N	\N	\N	\N
942	HDMU6721182	40	General/ Dry Cargo	\N	Non Dangerous	f65452ac-5fe1-45c9-b14f-f27da4d788941	\N	\N	\N	\N	\N
943	TCNU5064786	40	General/ Dry Cargo	\N	Non Dangerous	f65452ac-5fe1-45c9-b14f-f27da4d788942	\N	\N	\N	\N	\N
944	HDMU6721182	40	General/ Dry Cargo	\N	Non Dangerous	f65452ac-5fe1-45c9-b14f-f27da4d788942	\N	\N	\N	\N	\N
945	TCNU5064786	40	General/ Dry Cargo	\N	Non Dangerous	f65452ac-5fe1-45c9-b14f-f27da4d788943	\N	\N	\N	\N	\N
946	HDMU6721182	40	General/ Dry Cargo	\N	Non Dangerous	f65452ac-5fe1-45c9-b14f-f27da4d788943	\N	\N	\N	\N	\N
947	HDMU6721182	40	General/ Dry Cargo	\N	Non Dangerous	f65452ac-5fe1-45c9-b14f-f27da4d788944	\N	\N	\N	\N	\N
948	TCNU5064786	40	General/ Dry Cargo	\N	Non Dangerous	f65452ac-5fe1-45c9-b14f-f27da4d788944	\N	\N	\N	\N	\N
949	TCNU5064786	40	General/ Dry Cargo	\N	Non Dangerous	50090798-dfbd-4518-8a84-78a10397747b1	\N	\N	\N	\N	\N
950	TCNU5064786	40	General/ Dry Cargo	\N	Non Dangerous	50090798-dfbd-4518-8a84-78a10397747b2	\N	\N	\N	\N	\N
951	TCNU5064786	40	General/ Dry Cargo	\N	Non Dangerous	50090798-dfbd-4518-8a84-78a10397747b3	\N	\N	\N	\N	\N
952	TCNU5064786	40	General/ Dry Cargo	\N	Non Dangerous	50090798-dfbd-4518-8a84-78a10397747b4	\N	\N	\N	\N	\N
953	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	ad119148-6875-4a87-8ec2-e044f2e3913e1	\N	\N	\N	\N	\N
954	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	ad119148-6875-4a87-8ec2-e044f2e3913e2	\N	\N	\N	\N	\N
955	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	ad119148-6875-4a87-8ec2-e044f2e3913e3	\N	\N	\N	\N	\N
956	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	ad119148-6875-4a87-8ec2-e044f2e3913e4	\N	\N	\N	\N	\N
957	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	5fe18cfa-b431-465b-8555-7880de1d66e71	\N	\N	\N	\N	\N
958	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	5fe18cfa-b431-465b-8555-7880de1d66e72	\N	\N	\N	\N	\N
959	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	5fe18cfa-b431-465b-8555-7880de1d66e73	\N	\N	\N	\N	\N
960	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	5fe18cfa-b431-465b-8555-7880de1d66e74	\N	\N	\N	\N	\N
961	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	47676dff-207c-46af-bc29-a7fb93afaf801	\N	\N	\N	\N	\N
962	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	47676dff-207c-46af-bc29-a7fb93afaf802	\N	\N	\N	\N	\N
963	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	47676dff-207c-46af-bc29-a7fb93afaf803	\N	\N	\N	\N	\N
964	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	47676dff-207c-46af-bc29-a7fb93afaf804	\N	\N	\N	\N	\N
965	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	04fc413c-3a4a-43d3-9fd3-3c5966ff50501	\N	\N	\N	\N	\N
966	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	04fc413c-3a4a-43d3-9fd3-3c5966ff50502	\N	\N	\N	\N	\N
967	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	04fc413c-3a4a-43d3-9fd3-3c5966ff50503	\N	\N	\N	\N	\N
968	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	04fc413c-3a4a-43d3-9fd3-3c5966ff50504	\N	\N	\N	\N	\N
969	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	d0db336c-d39c-467d-86ce-0fae0180bb6e1	\N	\N	\N	\N	\N
970	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	d0db336c-d39c-467d-86ce-0fae0180bb6e2	\N	\N	\N	\N	\N
971	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	d0db336c-d39c-467d-86ce-0fae0180bb6e3	\N	\N	\N	\N	\N
972	BSIU9300069	40	General/ Dry Cargo	\N	Non Dangerous	d0db336c-d39c-467d-86ce-0fae0180bb6e4	\N	\N	\N	\N	\N
\.


--
-- Data for Name: document_trucking_destination; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.document_trucking_destination (id, destination, id_request_booking, latitude, longitude, urutan) FROM stdin;
3	cek2222	asda929ad0920jdadkasdas	-6.157519	106.948607	1
6	cek2222	asda929ad0920jda29329asd	-6.157519	106.948607	1
7	cek2222	u38929adsndjk29akeas8w	-6.157519	106.948607	1
9	Cawang, East Jakarta City, Jakarta, Indonesia	ee4e7259-8c1d-4dfc-850e-a9c3dbb67b5a1	-6	106	1
10	Cawang, East Jakarta City, Jakarta, Indonesia	a7a46a33-f4d1-4edc-9be2-9cb799db93091	-6	106	1
11	Cawang, East Jakarta City, Jakarta, Indonesia	b019a789-3802-4356-9091-10310cf45bbf1	-6	106	1
12	cek2222	asda929ad0920jda29329asd	-6.157519	106.948607	1
13	cek2222	k10so929ad0920jda29bca021	-6.157519	106.948607	1
14	Cawang, East Jakarta City, Jakarta, Indonesia	590fce61-64c9-4dca-8ee7-f2e8ea76fcee1	-6	106	1
15	Cawang, East Jakarta City, Jakarta, Indonesia	aa170d90-9b2f-4c96-9aee-9d64648cd04e1	-6	106	1
16	Cawang, East Jakarta City, Jakarta, Indonesia	67889e84-403e-4379-8c5b-1877588ab1381	-6	106	1
17	Cawang, East Jakarta City, Jakarta, Indonesia	5c849b69-0004-43c1-a31d-6148a3d33dbc1	-6	106	1
18	cek2222	asda929ad0920jda29329asd	-6.157519	106.948607	1
19	cek2222	k10so929ad0920jda29bca021	-6.157519	106.948607	1
20	Cawang, East Jakarta City, Jakarta, Indonesia	e9b63d39-239b-4ba7-aa6c-f49430efeaa51	-6	106	1
21	Cawang, East Jakarta City, Jakarta, Indonesia	f2ed1fed-3454-4c21-9904-1f11c23544c31	-6	106	1
22	Cawang, East Jakarta City, Jakarta, Indonesia	bc3cd8cc-5384-48a5-8342-4f0361fa9c2e1	-6	106	1
23	Cawang, East Jakarta City, Jakarta, Indonesia	e79f8254-22f5-4d03-ad85-cd63f29b90fc1	-6	106	1
24	Cawang, East Jakarta City, Jakarta, Indonesia	3a39e7b7-9221-4257-8d17-10b067e2f04a1	-6	106	1
25	Cawang, East Jakarta City, Jakarta, Indonesia	ef74a1fb-8a10-44ed-85dc-5f37f8954c181	-6	106	1
285	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	d3a70aff-fbf3-4474-884e-0e48fd9901f31	-6.2065725	106.8752798	1
286	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	d3a70aff-fbf3-4474-884e-0e48fd9901f32	-6.2065725	106.8752798	1
287	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	d3a70aff-fbf3-4474-884e-0e48fd9901f33	-6.2065725	106.8752798	1
363	PT. Puninar Yusen Logistics Indonesia, Jalan Cakung Cilincing Barat, RT.17/RW.7, West Cakung, East Jakarta City, Jakarta, Indonesia	b3b45d95-d498-41e5-8a59-7f8dac49eaa01	40.7689625	-74.93334639999999	2
364	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	b3b45d95-d498-41e5-8a59-7f8dac49eaa01	-6.4198055	107.4060214	1
365	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	b3b45d95-d498-41e5-8a59-7f8dac49eaa02	-6.4198055	107.4060214	1
366	PT. Puninar Yusen Logistics Indonesia, Jalan Cakung Cilincing Barat, RT.17/RW.7, West Cakung, East Jakarta City, Jakarta, Indonesia	b3b45d95-d498-41e5-8a59-7f8dac49eaa02	40.7689625	-74.93334639999999	2
367	PT. Puninar Yusen Logistics Indonesia, Jalan Cakung Cilincing Barat, RT.17/RW.7, West Cakung, East Jakarta City, Jakarta, Indonesia	b3b45d95-d498-41e5-8a59-7f8dac49eaa03	40.7689625	-74.93334639999999	2
368	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	b3b45d95-d498-41e5-8a59-7f8dac49eaa03	-6.4198055	107.4060214	1
369	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	c2202f16-2cd2-47d7-87be-a7cc9c09ddcf1	-6.4198055	107.4060214	1
370	PT. Puninar Yusen Logistics Indonesia, Jalan Cakung Cilincing Barat, RT.17/RW.7, West Cakung, East Jakarta City, Jakarta, Indonesia	c2202f16-2cd2-47d7-87be-a7cc9c09ddcf1	40.7689625	-74.93334639999999	2
371	PT. Puninar Yusen Logistics Indonesia, Jalan Cakung Cilincing Barat, RT.17/RW.7, West Cakung, East Jakarta City, Jakarta, Indonesia	c2202f16-2cd2-47d7-87be-a7cc9c09ddcf2	40.7689625	-74.93334639999999	2
372	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	c2202f16-2cd2-47d7-87be-a7cc9c09ddcf2	-6.4198055	107.4060214	1
373	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	c2202f16-2cd2-47d7-87be-a7cc9c09ddcf3	-6.4198055	107.4060214	1
374	PT. Puninar Yusen Logistics Indonesia, Jalan Cakung Cilincing Barat, RT.17/RW.7, West Cakung, East Jakarta City, Jakarta, Indonesia	c2202f16-2cd2-47d7-87be-a7cc9c09ddcf3	40.7689625	-74.93334639999999	2
68	Cawang, East Jakarta City, Jakarta, Indonesia	9f27b2cf-34b1-43ef-abb4-38f0b72f20751	-6	106	1
69	Cawang, East Jakarta City, Jakarta, Indonesia	49b215e6-0277-48cf-a4ec-0dc05f7c77711	-6	106	1
70	Cawang, East Jakarta City, Jakarta, Indonesia	47503e6d-ab49-4279-87ef-94e2572cd14a1	-6	106	1
71	Cawang, East Jakarta City, Jakarta, Indonesia	47503e6d-ab49-4279-87ef-94e2572cd14a2	-6	106	1
72	Cawang, East Jakarta City, Jakarta, Indonesia	47503e6d-ab49-4279-87ef-94e2572cd14a3	-6	106	1
73	Cawang, East Jakarta City, Jakarta, Indonesia	744fa35b-ccf0-4cbf-84ea-eb7ef8f80e4a1	-6	106	1
74	Cawang, East Jakarta City, Jakarta, Indonesia	744fa35b-ccf0-4cbf-84ea-eb7ef8f80e4a2	-6	106	1
75	Cawang, East Jakarta City, Jakarta, Indonesia	744fa35b-ccf0-4cbf-84ea-eb7ef8f80e4a3	-6	106	1
76	cek2222	idRequestTest123	-6.157519	106.948607	1
288	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	149603f2-19ae-4c19-8d5b-da6acce03bcd1	-6.119959199999999	106.9451223	2
289	PT MICS Steel Indonesia, Jalan Raya Inti, Sukaresmi, Bekasi, West Java, Indonesia	149603f2-19ae-4c19-8d5b-da6acce03bcd1	-6.3361129	107.1304075	1
290	PT MICS Steel Indonesia, Jalan Raya Inti, Sukaresmi, Bekasi, West Java, Indonesia	149603f2-19ae-4c19-8d5b-da6acce03bcd2	-6.3361129	107.1304075	1
291	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	149603f2-19ae-4c19-8d5b-da6acce03bcd2	-6.119959199999999	106.9451223	2
292	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	149603f2-19ae-4c19-8d5b-da6acce03bcd3	-6.119959199999999	106.9451223	2
293	PT MICS Steel Indonesia, Jalan Raya Inti, Sukaresmi, Bekasi, West Java, Indonesia	149603f2-19ae-4c19-8d5b-da6acce03bcd3	-6.3361129	107.1304075	1
85	cek2222	abcdef12345	-6.157519	106.948607	1
86	cek2222	asdfghjkl1234	-6.157519	106.948607	1
87	cek2222	abcdef12345	-6.157519	106.948607	1
88	cek2222	asdfghjkl1234	-6.157519	106.948607	1
393	Bekasi, Kota Bekasi, Jawa Barat, Indonesia	4178de6b-a02f-4129-8174-fa372fefa7dd1	-6.2382699	106.9755726	2
394	Jakarta, Daerah Khusus Ibukota Jakarta, Indonesia	4178de6b-a02f-4129-8174-fa372fefa7dd1	-6.2087634	106.845599	1
395	Bekasi, Kota Bekasi, Jawa Barat, Indonesia	4178de6b-a02f-4129-8174-fa372fefa7dd2	-6.2382699	106.9755726	2
396	Jakarta, Daerah Khusus Ibukota Jakarta, Indonesia	4178de6b-a02f-4129-8174-fa372fefa7dd2	-6.2087634	106.845599	1
397	Jakarta, Daerah Khusus Ibukota Jakarta, Indonesia	4178de6b-a02f-4129-8174-fa372fefa7dd3	-6.2087634	106.845599	1
398	Bekasi, Kota Bekasi, Jawa Barat, Indonesia	4178de6b-a02f-4129-8174-fa372fefa7dd3	-6.2382699	106.9755726	2
107	Jatinegara, East Jakarta City, Jakarta, Indonesia	2d3f5569-f672-4c76-baa8-948c310c278e1	-6	106	1
108	Jatinegara, East Jakarta City, Jakarta, Indonesia	2d3f5569-f672-4c76-baa8-948c310c278e2	-6	106	1
109	Jatinegara, East Jakarta City, Jakarta, Indonesia	2d3f5569-f672-4c76-baa8-948c310c278e3	-6	106	1
110	Jatinegara, East Jakarta City, Jakarta, Indonesia	a8e9e638-9cb8-411e-aff3-aedeb58fedac1	-6	106	1
111	Jatinegara, East Jakarta City, Jakarta, Indonesia	a8e9e638-9cb8-411e-aff3-aedeb58fedac2	-6	106	1
112	Jatinegara, East Jakarta City, Jakarta, Indonesia	a8e9e638-9cb8-411e-aff3-aedeb58fedac3	-6	106	1
405	Jakarta, Daerah Khusus Ibukota Jakarta, Indonesia	099565f0-3dfa-442b-b81a-695eba3d6a9a1	-6.2087634	106.845599	1
406	Jakarta, Daerah Khusus Ibukota Jakarta, Indonesia	099565f0-3dfa-442b-b81a-695eba3d6a9a2	-6.2087634	106.845599	1
407	Jakarta, Daerah Khusus Ibukota Jakarta, Indonesia	099565f0-3dfa-442b-b81a-695eba3d6a9a3	-6.2087634	106.845599	1
116	Cawang, East Jakarta City, Jakarta, Indonesia	4637c27d-7d01-4844-a7f9-0b286fa786441	-6.2490679	106.8679899	1
117	Cawang, East Jakarta City, Jakarta, Indonesia	4637c27d-7d01-4844-a7f9-0b286fa786442	-6.2490679	106.8679899	1
118	Cawang, East Jakarta City, Jakarta, Indonesia	4637c27d-7d01-4844-a7f9-0b286fa786443	-6.2490679	106.8679899	1
119	Jakarta, Indonesia	d218503c-5921-4cb9-a9db-299bfd1fa7d21	-6.2087634	106.845599	1
120	Jakarta, Indonesia	d218503c-5921-4cb9-a9db-299bfd1fa7d22	-6.2087634	106.845599	1
121	Jakarta, Indonesia	d218503c-5921-4cb9-a9db-299bfd1fa7d23	-6.2087634	106.845599	1
122	Jatinegara, East Jakarta City, Jakarta, Indonesia	0f3531e3-682d-460d-8466-e0595d12bab41	-6.2307016	106.8827427	1
123	Jatinegara, East Jakarta City, Jakarta, Indonesia	0f3531e3-682d-460d-8466-e0595d12bab42	-6.2307016	106.8827427	1
124	Jatinegara, East Jakarta City, Jakarta, Indonesia	0f3531e3-682d-460d-8466-e0595d12bab43	-6.2307016	106.8827427	1
125	Jatinegara, East Jakarta City, Jakarta, Indonesia	bc3c8e19-8ee5-4d27-b06b-09e5714bf9661	-6.2307016	106.8827427	1
126	Jatinegara, East Jakarta City, Jakarta, Indonesia	bc3c8e19-8ee5-4d27-b06b-09e5714bf9662	-6.2307016	106.8827427	1
127	Jatinegara, East Jakarta City, Jakarta, Indonesia	bc3c8e19-8ee5-4d27-b06b-09e5714bf9663	-6.2307016	106.8827427	1
128	Jatinegara, East Jakarta City, Jakarta, Indonesia	85c7b7d2-66d3-4102-88fe-68916fa12a081	-6.2307016	106.8827427	1
129	Jatinegara, East Jakarta City, Jakarta, Indonesia	85c7b7d2-66d3-4102-88fe-68916fa12a082	-6.2307016	106.8827427	1
130	Jatinegara, East Jakarta City, Jakarta, Indonesia	85c7b7d2-66d3-4102-88fe-68916fa12a083	-6.2307016	106.8827427	1
131	Tanjung Priok, North Jakarta City, Jakarta, Indonesia	1fb77b97-0be8-404e-adb8-a81abea7286e1	-6	106	1
414	Bekasi, Bekasi City, West Java, Indonesia	05f04fe5-46db-450d-a518-ad76f206c7a71	-6.2382699	106.9755726	1
415	Bekasi, Bekasi City, West Java, Indonesia	05f04fe5-46db-450d-a518-ad76f206c7a72	-6.2382699	106.9755726	1
416	Bekasi, Bekasi City, West Java, Indonesia	05f04fe5-46db-450d-a518-ad76f206c7a73	-6.2382699	106.9755726	1
432	\N	1506a286-d5cf-4ee0-b8ce-54384e0cad401	\N	\N	2
433	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	1506a286-d5cf-4ee0-b8ce-54384e0cad401	-6.244397500000001	106.9919047	1
434	\N	1506a286-d5cf-4ee0-b8ce-54384e0cad402	\N	\N	2
435	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	1506a286-d5cf-4ee0-b8ce-54384e0cad402	-6.244397500000001	106.9919047	1
436	\N	1506a286-d5cf-4ee0-b8ce-54384e0cad403	\N	\N	2
132	Cawang, East Jakarta City, Jakarta, Indonesia	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a771	-6.2490679	106.8679899	1
133	Cawang, East Jakarta City, Jakarta, Indonesia	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a772	-6.2490679	106.8679899	1
134	Cawang, East Jakarta City, Jakarta, Indonesia	9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a773	-6.2490679	106.8679899	1
135	Cawang, East Jakarta City, Jakarta, Indonesia	33b34362-4519-4bd3-a6fc-cfd87e40344b1	-6.2490679	106.8679899	1
136	Cawang, East Jakarta City, Jakarta, Indonesia	33b34362-4519-4bd3-a6fc-cfd87e40344b2	-6.2490679	106.8679899	1
137	Cawang, East Jakarta City, Jakarta, Indonesia	33b34362-4519-4bd3-a6fc-cfd87e40344b3	-6.2490679	106.8679899	1
294	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	fe908600-552a-4e1e-9669-52fac58a5c381	-6.119959199999999	106.9451223	2
295	PT MICS Steel Indonesia, Jalan Raya Inti, Sukaresmi, Bekasi, West Java, Indonesia	fe908600-552a-4e1e-9669-52fac58a5c381	-6.3361129	107.1304075	1
296	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	fe908600-552a-4e1e-9669-52fac58a5c382	-6.119959199999999	106.9451223	2
297	PT MICS Steel Indonesia, Jalan Raya Inti, Sukaresmi, Bekasi, West Java, Indonesia	fe908600-552a-4e1e-9669-52fac58a5c382	-6.3361129	107.1304075	1
298	PT MICS Steel Indonesia, Jalan Raya Inti, Sukaresmi, Bekasi, West Java, Indonesia	fe908600-552a-4e1e-9669-52fac58a5c383	-6.3361129	107.1304075	1
299	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	fe908600-552a-4e1e-9669-52fac58a5c383	-6.119959199999999	106.9451223	2
375	PT. Puninar Yusen Logistics Indonesia, Jalan Cakung Cilincing Barat, RT.17/RW.7, West Cakung, East Jakarta City, Jakarta, Indonesia	c43eb4a0-a163-4c4d-a649-d0146dbd545e1	40.7689625	-74.93334639999999	2
376	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	c43eb4a0-a163-4c4d-a649-d0146dbd545e1	-6.4198055	107.4060214	1
444	Jakarta, Indonesia	3eeeb4e2-499d-479d-ba8a-34064bf9abc91	-6.2087634	106.845599	1
445	Jakarta, Indonesia	3eeeb4e2-499d-479d-ba8a-34064bf9abc92	-6.2087634	106.845599	1
446	Jakarta, Indonesia	3eeeb4e2-499d-479d-ba8a-34064bf9abc93	-6.2087634	106.845599	1
594	Jakarta, Indonesia	a3efbc31-a9d2-4a3d-8328-9ae89d0460991	-6.2087634	106.845599	1
595	Jakarta, Indonesia	a3efbc31-a9d2-4a3d-8328-9ae89d0460992	-6.2087634	106.845599	1
158	Cawang, East Jakarta City, Jakarta, Indonesia	25f443a1-3ba4-40e2-94c8-ad1c7e68a61a1	-6.2490679	106.8679899	1
159	Cawang, East Jakarta City, Jakarta, Indonesia	25f443a1-3ba4-40e2-94c8-ad1c7e68a61a2	-6.2490679	106.8679899	1
160	Cawang, East Jakarta City, Jakarta, Indonesia	25f443a1-3ba4-40e2-94c8-ad1c7e68a61a3	-6.2490679	106.8679899	1
161	Jakarta, Indonesia	bd105723-8e85-4582-b795-172f30d86c741	-6.2087634	106.845599	1
162	Jakarta, Indonesia	bd105723-8e85-4582-b795-172f30d86c742	-6.2087634	106.845599	1
163	Jakarta, Indonesia	bd105723-8e85-4582-b795-172f30d86c743	-6.2087634	106.845599	1
164	Jakarta, Indonesia	57832f2d-57b8-421a-a940-771be30c0a241	-6.2087634	106.845599	1
165	Jakarta, Indonesia	57832f2d-57b8-421a-a940-771be30c0a242	-6.2087634	106.845599	1
166	Jakarta, Indonesia	57832f2d-57b8-421a-a940-771be30c0a243	-6.2087634	106.845599	1
167	Cawang, East Jakarta City, Jakarta, Indonesia	aa0fd7e0-843b-4da6-84a1-799065767d1c1	-6.2490679	106.8679899	1
168	Cawang, East Jakarta City, Jakarta, Indonesia	aa0fd7e0-843b-4da6-84a1-799065767d1c2	-6.2490679	106.8679899	1
169	Cawang, East Jakarta City, Jakarta, Indonesia	aa0fd7e0-843b-4da6-84a1-799065767d1c3	-6.2490679	106.8679899	1
170	tanjung priok	d18d4675-3a3c-4ad1-a635-f41a73a9a8c51	-6.1320555	106.8714848	1
171	tanjung priok	d18d4675-3a3c-4ad1-a635-f41a73a9a8c52	-6.1320555	106.8714848	1
172	tanjung priok	d18d4675-3a3c-4ad1-a635-f41a73a9a8c53	-6.1320555	106.8714848	1
173	Cawang, East Jakarta City, Jakarta, Indonesia	fa909fc7-baef-47c1-b591-e8361ed119111	-6.2490679	106.8679899	1
174	Cawang, East Jakarta City, Jakarta, Indonesia	fa909fc7-baef-47c1-b591-e8361ed119112	-6.2490679	106.8679899	1
175	Cawang, East Jakarta City, Jakarta, Indonesia	fa909fc7-baef-47c1-b591-e8361ed119113	-6.2490679	106.8679899	1
596	Jakarta, Indonesia	a3efbc31-a9d2-4a3d-8328-9ae89d0460993	-6.2087634	106.845599	1
597	Jakarta, Indonesia	a3efbc31-a9d2-4a3d-8328-9ae89d0460994	-6.2087634	106.845599	1
178	Bekasi, Bekasi City, West Java, Indonesia	3c94af28-9d71-4bd0-a20c-df95fbbfebd32	-6.2382699	106.9755726	2
179	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	3c94af28-9d71-4bd0-a20c-df95fbbfebd32	-6.597146899999999	106.8060388	1
180	Bekasi, Bekasi City, West Java, Indonesia	3c94af28-9d71-4bd0-a20c-df95fbbfebd33	-6.2382699	106.9755726	2
181	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	3c94af28-9d71-4bd0-a20c-df95fbbfebd33	-6.597146899999999	106.8060388	1
182	Bekasi, Bekasi City, West Java, Indonesia	8b0dbcd5-a60b-44b1-bb92-a3f33b3f2c171	-6.2382699	106.9755726	2
183	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	8b0dbcd5-a60b-44b1-bb92-a3f33b3f2c171	-6.597146899999999	106.8060388	1
184	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	8b0dbcd5-a60b-44b1-bb92-a3f33b3f2c172	-6.597146899999999	106.8060388	1
185	Bekasi, Bekasi City, West Java, Indonesia	8b0dbcd5-a60b-44b1-bb92-a3f33b3f2c172	-6.2382699	106.9755726	2
186	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	8b0dbcd5-a60b-44b1-bb92-a3f33b3f2c173	-6.597146899999999	106.8060388	1
187	Bekasi, Bekasi City, West Java, Indonesia	8b0dbcd5-a60b-44b1-bb92-a3f33b3f2c173	-6.2382699	106.9755726	2
188	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	1fceb782-b52b-46ac-a475-1a272d04a3bf1	-6.597146899999999	106.8060388	1
189	Bekasi, Bekasi City, West Java, Indonesia	1fceb782-b52b-46ac-a475-1a272d04a3bf1	-6.2382699	106.9755726	2
190	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	1fceb782-b52b-46ac-a475-1a272d04a3bf2	-6.597146899999999	106.8060388	1
191	Bekasi, Bekasi City, West Java, Indonesia	1fceb782-b52b-46ac-a475-1a272d04a3bf2	-6.2382699	106.9755726	2
192	Bekasi, Bekasi City, West Java, Indonesia	1fceb782-b52b-46ac-a475-1a272d04a3bf3	-6.2382699	106.9755726	2
193	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	1fceb782-b52b-46ac-a475-1a272d04a3bf3	-6.597146899999999	106.8060388	1
194	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	a26e195e-d92f-4872-865c-9a94741a54131	-6.597146899999999	106.8060388	1
195	Bekasi, Bekasi City, West Java, Indonesia	a26e195e-d92f-4872-865c-9a94741a54131	-6.2382699	106.9755726	2
196	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	a26e195e-d92f-4872-865c-9a94741a54132	-6.597146899999999	106.8060388	1
197	Bekasi, Bekasi City, West Java, Indonesia	a26e195e-d92f-4872-865c-9a94741a54132	-6.2382699	106.9755726	2
198	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	a26e195e-d92f-4872-865c-9a94741a54133	-6.597146899999999	106.8060388	1
199	Bekasi, Bekasi City, West Java, Indonesia	a26e195e-d92f-4872-865c-9a94741a54133	-6.2382699	106.9755726	2
200	Bekasi, Bekasi City, West Java, Indonesia	9d16f90b-0dd2-4cf2-8b0c-3443bf1c37611	-6.2382699	106.9755726	2
201	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	9d16f90b-0dd2-4cf2-8b0c-3443bf1c37611	-6.597146899999999	106.8060388	1
202	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	9d16f90b-0dd2-4cf2-8b0c-3443bf1c37612	-6.597146899999999	106.8060388	1
203	Bekasi, Bekasi City, West Java, Indonesia	9d16f90b-0dd2-4cf2-8b0c-3443bf1c37612	-6.2382699	106.9755726	2
204	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	9d16f90b-0dd2-4cf2-8b0c-3443bf1c37613	-6.597146899999999	106.8060388	1
205	Bekasi, Bekasi City, West Java, Indonesia	9d16f90b-0dd2-4cf2-8b0c-3443bf1c37613	-6.2382699	106.9755726	2
206	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	210a9825-32e3-4f41-a87c-84ff4b3aa9301	-6.597146899999999	106.8060388	1
207	Bekasi, Bekasi City, West Java, Indonesia	210a9825-32e3-4f41-a87c-84ff4b3aa9301	-6.2382699	106.9755726	2
208	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	210a9825-32e3-4f41-a87c-84ff4b3aa9302	-6.597146899999999	106.8060388	1
209	Bekasi, Bekasi City, West Java, Indonesia	210a9825-32e3-4f41-a87c-84ff4b3aa9302	-6.2382699	106.9755726	2
210	Bekasi, Bekasi City, West Java, Indonesia	210a9825-32e3-4f41-a87c-84ff4b3aa9303	-6.2382699	106.9755726	2
211	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	210a9825-32e3-4f41-a87c-84ff4b3aa9303	-6.597146899999999	106.8060388	1
218	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	7c68b67b-c627-4d77-b671-6c3cf48930601	-6.597146899999999	106.8060388	1
219	Bekasi, Bekasi City, West Java, Indonesia	7c68b67b-c627-4d77-b671-6c3cf48930601	-6.2382699	106.9755726	2
220	Bekasi, Bekasi City, West Java, Indonesia	7c68b67b-c627-4d77-b671-6c3cf48930602	-6.2382699	106.9755726	2
221	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	7c68b67b-c627-4d77-b671-6c3cf48930602	-6.597146899999999	106.8060388	1
222	Bekasi, Bekasi City, West Java, Indonesia	7c68b67b-c627-4d77-b671-6c3cf48930603	-6.2382699	106.9755726	2
223	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	7c68b67b-c627-4d77-b671-6c3cf48930603	-6.597146899999999	106.8060388	1
336	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	bfefd7de-9a0d-4ac5-a828-f16af511b01a1	-6.2065725	106.8752798	1
337	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	bfefd7de-9a0d-4ac5-a828-f16af511b01a2	-6.2065725	106.8752798	1
338	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	bfefd7de-9a0d-4ac5-a828-f16af511b01a3	-6.2065725	106.8752798	1
357	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	3bd70d88-11a7-4ff6-9ad6-dd102b5668b21	-6.4198055	107.4060214	1
358	PT. Puninar Yusen Logistics Indonesia, Jalan Cakung Cilincing Barat, RT.17/RW.7, West Cakung, East Jakarta City, Jakarta, Indonesia	3bd70d88-11a7-4ff6-9ad6-dd102b5668b21	40.7689625	-74.93334639999999	2
359	PT. Puninar Yusen Logistics Indonesia, Jalan Cakung Cilincing Barat, RT.17/RW.7, West Cakung, East Jakarta City, Jakarta, Indonesia	3bd70d88-11a7-4ff6-9ad6-dd102b5668b22	40.7689625	-74.93334639999999	2
360	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	3bd70d88-11a7-4ff6-9ad6-dd102b5668b22	-6.4198055	107.4060214	1
361	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	3bd70d88-11a7-4ff6-9ad6-dd102b5668b23	-6.4198055	107.4060214	1
212	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	65ba261d-1638-4146-ac5b-c050775bd0601	-6.597146899999999	106.8060388	1
213	Bekasi, Bekasi City, West Java, Indonesia	65ba261d-1638-4146-ac5b-c050775bd0601	-6.2382699	106.9755726	2
214	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	65ba261d-1638-4146-ac5b-c050775bd0602	-6.597146899999999	106.8060388	1
215	Bekasi, Bekasi City, West Java, Indonesia	65ba261d-1638-4146-ac5b-c050775bd0602	-6.2382699	106.9755726	2
216	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	65ba261d-1638-4146-ac5b-c050775bd0603	-6.597146899999999	106.8060388	1
217	Bekasi, Bekasi City, West Java, Indonesia	65ba261d-1638-4146-ac5b-c050775bd0603	-6.2382699	106.9755726	2
224	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	723299d0-6747-4b0e-b481-d1a35a89fe881	-6.597146899999999	106.8060388	1
225	Bekasi, Bekasi City, West Java, Indonesia	723299d0-6747-4b0e-b481-d1a35a89fe881	-6.2382699	106.9755726	2
226	Bekasi, Bekasi City, West Java, Indonesia	723299d0-6747-4b0e-b481-d1a35a89fe882	-6.2382699	106.9755726	2
227	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	723299d0-6747-4b0e-b481-d1a35a89fe882	-6.597146899999999	106.8060388	1
228	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	723299d0-6747-4b0e-b481-d1a35a89fe883	-6.597146899999999	106.8060388	1
229	Bekasi, Bekasi City, West Java, Indonesia	723299d0-6747-4b0e-b481-d1a35a89fe883	-6.2382699	106.9755726	2
230	Cawang, East Jakarta City, Jakarta, Indonesia	5d690a8a-90e8-4b89-b173-5071987732ba1	-6.2490679	106.8679899	1
231	Cawang, East Jakarta City, Jakarta, Indonesia	5d690a8a-90e8-4b89-b173-5071987732ba2	-6.2490679	106.8679899	1
232	Cawang, East Jakarta City, Jakarta, Indonesia	5d690a8a-90e8-4b89-b173-5071987732ba3	-6.2490679	106.8679899	1
312	PT MICS Steel Indonesia, Jalan Raya Inti, Sukaresmi, Bekasi, West Java, Indonesia	7ae4ce09-ac66-424f-a0dd-a4f36b06fb971	-6.3361129	107.1304075	1
313	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	7ae4ce09-ac66-424f-a0dd-a4f36b06fb971	-6.119959199999999	106.9451223	2
314	PT MICS Steel Indonesia, Jalan Raya Inti, Sukaresmi, Bekasi, West Java, Indonesia	7ae4ce09-ac66-424f-a0dd-a4f36b06fb972	-6.3361129	107.1304075	1
236	Cikarang, Bekasi, West Java, Indonesia	b75ad664-77fa-4939-99fd-bac15b2dda5a1	-6.307923199999999	107.172085	1
237	Cikarang, Bekasi, West Java, Indonesia	b75ad664-77fa-4939-99fd-bac15b2dda5a2	-6.307923199999999	107.172085	1
238	Cikarang, Bekasi, West Java, Indonesia	b75ad664-77fa-4939-99fd-bac15b2dda5a3	-6.307923199999999	107.172085	1
315	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	7ae4ce09-ac66-424f-a0dd-a4f36b06fb972	-6.119959199999999	106.9451223	2
399	Cikarang, Bekasi, West Java, Indonesia	64bb8e57-bae7-42d5-bee0-58b1d9082f8c1	-6.307923199999999	107.172085	1
400	Depo Container SPIL Marunda, Jalan Marunda Makmur, RT.5/RW.1, Marunda, North Jakarta City, Jakarta, Indonesia	64bb8e57-bae7-42d5-bee0-58b1d9082f8c1	-6.1067677	106.9655551	2
401	Depo Container SPIL Marunda, Jalan Marunda Makmur, RT.5/RW.1, Marunda, North Jakarta City, Jakarta, Indonesia	64bb8e57-bae7-42d5-bee0-58b1d9082f8c2	-6.1067677	106.9655551	2
402	Cikarang, Bekasi, West Java, Indonesia	64bb8e57-bae7-42d5-bee0-58b1d9082f8c2	-6.307923199999999	107.172085	1
438	Cikarang, Bekasi, West Java, Indonesia	fbaf7158-5968-4411-aba8-15517a8eeea91	-6.307923199999999	107.172085	1
439	Cikarang, Bekasi, West Java, Indonesia	fbaf7158-5968-4411-aba8-15517a8eeea92	-6.307923199999999	107.172085	1
440	Cikarang, Bekasi, West Java, Indonesia	fbaf7158-5968-4411-aba8-15517a8eeea93	-6.307923199999999	107.172085	1
447	Koja, North Jakarta City, Jakarta, Indonesia	66d6d20c-a63e-46fd-b440-748da42ce93c1	-6.117663500000001	106.9063491	1
448	Koja, North Jakarta City, Jakarta, Indonesia	66d6d20c-a63e-46fd-b440-748da42ce93c2	-6.117663500000001	106.9063491	1
449	Koja, North Jakarta City, Jakarta, Indonesia	66d6d20c-a63e-46fd-b440-748da42ce93c3	-6.117663500000001	106.9063491	1
450	Cengkareng, West Jakarta City, Jakarta, Indonesia	d78b2959-b4ee-43b2-b040-d51e200316bc1	-6.1486651	106.7352584	1
451	Cengkareng, West Jakarta City, Jakarta, Indonesia	d78b2959-b4ee-43b2-b040-d51e200316bc2	-6.1486651	106.7352584	1
452	Cengkareng, West Jakarta City, Jakarta, Indonesia	d78b2959-b4ee-43b2-b040-d51e200316bc3	-6.1486651	106.7352584	1
491	Cikarang, Bekasi, West Java, Indonesia	324bff81-6a51-4411-83ca-b7a358904ceb2	-6.307923199999999	107.172085	1
492	Cikarang, Bekasi, West Java, Indonesia	324bff81-6a51-4411-83ca-b7a358904ceb3	-6.307923199999999	107.172085	1
496	Cikarang, Bekasi, West Java, Indonesia	630969ec-d774-4a38-870d-a9bd1cbb41801	-6.307923199999999	107.172085	1
497	Cikarang, Bekasi, West Java, Indonesia	630969ec-d774-4a38-870d-a9bd1cbb41802	-6.307923199999999	107.172085	1
498	Cikarang, Bekasi, West Java, Indonesia	630969ec-d774-4a38-870d-a9bd1cbb41803	-6.307923199999999	107.172085	1
526	Cengkareng, West Jakarta City, Jakarta, Indonesia	3bb5f3cf-f3df-4bc1-b885-3bcd03f113501	-6.1486651	106.7352584	1
527	Cengkareng, West Jakarta City, Jakarta, Indonesia	3bb5f3cf-f3df-4bc1-b885-3bcd03f113502	-6.1486651	106.7352584	1
528	Cengkareng, West Jakarta City, Jakarta, Indonesia	3bb5f3cf-f3df-4bc1-b885-3bcd03f113503	-6.1486651	106.7352584	1
539	Cikarang, Bekasi, West Java, Indonesia	31db3546-d72b-4b12-8f06-228fdf5787ab2	-6.307923199999999	107.172085	1
540	Cikarang, Bekasi, West Java, Indonesia	31db3546-d72b-4b12-8f06-228fdf5787ab3	-6.307923199999999	107.172085	1
541	Cikarang, Bekasi, West Java, Indonesia	31db3546-d72b-4b12-8f06-228fdf5787ab4	-6.307923199999999	107.172085	1
542	Cikarang, Bekasi, West Java, Indonesia	c41d9cfe-d43b-4684-8e47-f024cf38745a1	-6.307923199999999	107.172085	1
543	Cikarang, Bekasi, West Java, Indonesia	c41d9cfe-d43b-4684-8e47-f024cf38745a2	-6.307923199999999	107.172085	1
544	Cikarang, Bekasi, West Java, Indonesia	c41d9cfe-d43b-4684-8e47-f024cf38745a3	-6.307923199999999	107.172085	1
545	Cikarang, Bekasi, West Java, Indonesia	c41d9cfe-d43b-4684-8e47-f024cf38745a4	-6.307923199999999	107.172085	1
546	Cikarang, Bekasi, West Java, Indonesia	5d4d1702-cea0-48a2-806a-e49dece6fcc21	-6.307923199999999	107.172085	1
264	Bekasi, Bekasi City, West Java, Indonesia	4d693fca-bd52-4aca-baea-f5578b6d6f761	-6.2382699	106.9755726	2
265	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	4d693fca-bd52-4aca-baea-f5578b6d6f761	-6.597146899999999	106.8060388	1
266	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	4d693fca-bd52-4aca-baea-f5578b6d6f762	-6.597146899999999	106.8060388	1
267	Bekasi, Bekasi City, West Java, Indonesia	4d693fca-bd52-4aca-baea-f5578b6d6f762	-6.2382699	106.9755726	2
268	Bekasi, Bekasi City, West Java, Indonesia	4d693fca-bd52-4aca-baea-f5578b6d6f763	-6.2382699	106.9755726	2
269	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	4d693fca-bd52-4aca-baea-f5578b6d6f763	-6.597146899999999	106.8060388	1
270	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	debeec10-2b9f-4f75-92a6-59404c83bac61	-6.597146899999999	106.8060388	1
271	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	debeec10-2b9f-4f75-92a6-59404c83bac62	-6.597146899999999	106.8060388	1
272	Bogor, Kampung Parung Jambu, Bogor City, West Java, Indonesia	debeec10-2b9f-4f75-92a6-59404c83bac63	-6.597146899999999	106.8060388	1
273	Bekasi, Bekasi City, West Java, Indonesia	b4c7b959-182a-4635-872f-4f1cd84c0ce71	-6.2382699	106.9755726	1
274	Bekasi, Bekasi City, West Java, Indonesia	b4c7b959-182a-4635-872f-4f1cd84c0ce72	-6.2382699	106.9755726	1
275	Bekasi, Bekasi City, West Java, Indonesia	b4c7b959-182a-4635-872f-4f1cd84c0ce73	-6.2382699	106.9755726	1
276	Rawamangun, East Jakarta City, Jakarta, Indonesia	0b4a223f-fa95-439d-bdd9-069ee6e2be951	-6.1976024	106.879792	1
277	Rawamangun, East Jakarta City, Jakarta, Indonesia	0b4a223f-fa95-439d-bdd9-069ee6e2be952	-6.1976024	106.879792	1
278	Rawamangun, East Jakarta City, Jakarta, Indonesia	0b4a223f-fa95-439d-bdd9-069ee6e2be953	-6.1976024	106.879792	1
279	Rawamangun, East Jakarta City, Jakarta, Indonesia	aaf22fd8-b72a-46c4-a3d7-73e2affa38ec1	-6.1976024	106.879792	1
280	Rawamangun, East Jakarta City, Jakarta, Indonesia	aaf22fd8-b72a-46c4-a3d7-73e2affa38ec2	-6.1976024	106.879792	1
281	Rawamangun, East Jakarta City, Jakarta, Indonesia	aaf22fd8-b72a-46c4-a3d7-73e2affa38ec3	-6.1976024	106.879792	1
282	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	d1ca6c91-6434-48b8-968a-6d937c0903001	-6.2065725	106.8752798	1
283	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	d1ca6c91-6434-48b8-968a-6d937c0903002	-6.2065725	106.8752798	1
284	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	d1ca6c91-6434-48b8-968a-6d937c0903003	-6.2065725	106.8752798	1
316	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	7ae4ce09-ac66-424f-a0dd-a4f36b06fb973	-6.119959199999999	106.9451223	2
317	PT MICS Steel Indonesia, Jalan Raya Inti, Sukaresmi, Bekasi, West Java, Indonesia	7ae4ce09-ac66-424f-a0dd-a4f36b06fb973	-6.3361129	107.1304075	1
339	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	e0324ebe-959a-4507-aa3f-b3259b54a2d51	-6.2065725	106.8752798	1
340	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	e0324ebe-959a-4507-aa3f-b3259b54a2d52	-6.2065725	106.8752798	1
341	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	e0324ebe-959a-4507-aa3f-b3259b54a2d53	-6.2065725	106.8752798	1
342	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	7b60cd9f-bb76-4469-a0f0-2bd090349edd1	-6.2065725	106.8752798	1
343	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	7b60cd9f-bb76-4469-a0f0-2bd090349edd2	-6.2065725	106.8752798	1
344	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	7b60cd9f-bb76-4469-a0f0-2bd090349edd3	-6.2065725	106.8752798	1
362	PT. Puninar Yusen Logistics Indonesia, Jalan Cakung Cilincing Barat, RT.17/RW.7, West Cakung, East Jakarta City, Jakarta, Indonesia	3bd70d88-11a7-4ff6-9ad6-dd102b5668b23	40.7689625	-74.93334639999999	2
377	PT. Puninar Yusen Logistics Indonesia, Jalan Cakung Cilincing Barat, RT.17/RW.7, West Cakung, East Jakarta City, Jakarta, Indonesia	c43eb4a0-a163-4c4d-a649-d0146dbd545e2	40.7689625	-74.93334639999999	2
378	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	c43eb4a0-a163-4c4d-a649-d0146dbd545e2	-6.4198055	107.4060214	1
379	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	c43eb4a0-a163-4c4d-a649-d0146dbd545e3	-6.4198055	107.4060214	1
380	PT. Puninar Yusen Logistics Indonesia, Jalan Cakung Cilincing Barat, RT.17/RW.7, West Cakung, East Jakarta City, Jakarta, Indonesia	c43eb4a0-a163-4c4d-a649-d0146dbd545e3	40.7689625	-74.93334639999999	2
403	Depo Container SPIL Marunda, Jalan Marunda Makmur, RT.5/RW.1, Marunda, North Jakarta City, Jakarta, Indonesia	64bb8e57-bae7-42d5-bee0-58b1d9082f8c3	-6.1067677	106.9655551	2
404	Cikarang, Bekasi, West Java, Indonesia	64bb8e57-bae7-42d5-bee0-58b1d9082f8c3	-6.307923199999999	107.172085	1
408	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	c1b5a2f2-7e0e-42fe-84b7-10e87ccabba61	-6.244397500000001	106.9919047	1
409	Depo Container SPIL Marunda, Jalan Marunda Makmur, RT.5/RW.1, Marunda, North Jakarta City, Jakarta, Indonesia	c1b5a2f2-7e0e-42fe-84b7-10e87ccabba61	-6.1067677	106.9655551	2
410	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	c1b5a2f2-7e0e-42fe-84b7-10e87ccabba62	-6.244397500000001	106.9919047	1
411	Depo Container SPIL Marunda, Jalan Marunda Makmur, RT.5/RW.1, Marunda, North Jakarta City, Jakarta, Indonesia	c1b5a2f2-7e0e-42fe-84b7-10e87ccabba62	-6.1067677	106.9655551	2
412	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	c1b5a2f2-7e0e-42fe-84b7-10e87ccabba63	-6.244397500000001	106.9919047	1
413	Depo Container SPIL Marunda, Jalan Marunda Makmur, RT.5/RW.1, Marunda, North Jakarta City, Jakarta, Indonesia	c1b5a2f2-7e0e-42fe-84b7-10e87ccabba63	-6.1067677	106.9655551	2
417	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	d1be5e65-b186-4252-94fc-95765455f5f51	-6.244397500000001	106.9919047	1
418	Depo Container SPIL Marunda, Jalan Malaka 1 Gang Veteran, Marunda, North Jakarta City, Jakarta, Indonesia	d1be5e65-b186-4252-94fc-95765455f5f51	-6.1370993	106.9547514	2
419	Depo Container SPIL Marunda, Jalan Malaka 1 Gang Veteran, Marunda, North Jakarta City, Jakarta, Indonesia	d1be5e65-b186-4252-94fc-95765455f5f52	-6.1370993	106.9547514	2
420	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	d1be5e65-b186-4252-94fc-95765455f5f52	-6.244397500000001	106.9919047	1
421	Depo Container SPIL Marunda, Jalan Malaka 1 Gang Veteran, Marunda, North Jakarta City, Jakarta, Indonesia	d1be5e65-b186-4252-94fc-95765455f5f53	-6.1370993	106.9547514	2
422	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	d1be5e65-b186-4252-94fc-95765455f5f53	-6.244397500000001	106.9919047	1
423	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	87efdc2a-88cb-42cb-91cc-7f235234f3ba1	-6.244397500000001	106.9919047	1
424	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	87efdc2a-88cb-42cb-91cc-7f235234f3ba2	-6.244397500000001	106.9919047	1
425	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	87efdc2a-88cb-42cb-91cc-7f235234f3ba3	-6.244397500000001	106.9919047	1
426	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	a9b2b129-621d-4124-a3ae-5603179719f51	-6.244397500000001	106.9919047	1
427	\N	a9b2b129-621d-4124-a3ae-5603179719f51	\N	\N	2
428	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	a9b2b129-621d-4124-a3ae-5603179719f52	-6.244397500000001	106.9919047	1
429	\N	a9b2b129-621d-4124-a3ae-5603179719f52	\N	\N	2
430	\N	a9b2b129-621d-4124-a3ae-5603179719f53	\N	\N	2
431	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	a9b2b129-621d-4124-a3ae-5603179719f53	-6.244397500000001	106.9919047	1
437	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	1506a286-d5cf-4ee0-b8ce-54384e0cad403	-6.244397500000001	106.9919047	1
441	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	7dfbd8ec-d4c2-4e0e-a97c-5b7ada7a873f1	-6.244397500000001	106.9919047	1
442	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	7dfbd8ec-d4c2-4e0e-a97c-5b7ada7a873f2	-6.244397500000001	106.9919047	1
443	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	7dfbd8ec-d4c2-4e0e-a97c-5b7ada7a873f3	-6.244397500000001	106.9919047	1
453	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	609d6724-bf89-4019-9974-e9377026880e1	-6.119959199999999	106.9451223	2
454	Cengkareng, West Jakarta City, Jakarta, Indonesia	609d6724-bf89-4019-9974-e9377026880e1	-6.1486651	106.7352584	1
455	Cengkareng, West Jakarta City, Jakarta, Indonesia	609d6724-bf89-4019-9974-e9377026880e2	-6.1486651	106.7352584	1
456	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	609d6724-bf89-4019-9974-e9377026880e2	-6.119959199999999	106.9451223	2
457	Cengkareng, West Jakarta City, Jakarta, Indonesia	609d6724-bf89-4019-9974-e9377026880e3	-6.1486651	106.7352584	1
458	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	609d6724-bf89-4019-9974-e9377026880e3	-6.119959199999999	106.9451223	2
459	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	609d6724-bf89-4019-9974-e9377026880e4	-6.119959199999999	106.9451223	2
460	Cengkareng, West Jakarta City, Jakarta, Indonesia	609d6724-bf89-4019-9974-e9377026880e4	-6.1486651	106.7352584	1
461	Cengkareng, West Jakarta City, Jakarta, Indonesia	e04cf392-6415-4410-8229-fd1e47189b1a1	-6.1486651	106.7352584	1
462	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	e04cf392-6415-4410-8229-fd1e47189b1a1	-6.119959199999999	106.9451223	2
463	Cengkareng, West Jakarta City, Jakarta, Indonesia	e04cf392-6415-4410-8229-fd1e47189b1a2	-6.1486651	106.7352584	1
464	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	e04cf392-6415-4410-8229-fd1e47189b1a2	-6.119959199999999	106.9451223	2
465	Cengkareng, West Jakarta City, Jakarta, Indonesia	e04cf392-6415-4410-8229-fd1e47189b1a3	-6.1486651	106.7352584	1
466	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	e04cf392-6415-4410-8229-fd1e47189b1a3	-6.119959199999999	106.9451223	2
467	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	e04cf392-6415-4410-8229-fd1e47189b1a4	-6.119959199999999	106.9451223	2
468	Cengkareng, West Jakarta City, Jakarta, Indonesia	e04cf392-6415-4410-8229-fd1e47189b1a4	-6.1486651	106.7352584	1
469	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	130271c3-e6b8-4b5d-b7cc-22e112030afa1	-6.244397500000001	106.9919047	1
470	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	130271c3-e6b8-4b5d-b7cc-22e112030afa2	-6.244397500000001	106.9919047	1
471	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	130271c3-e6b8-4b5d-b7cc-22e112030afa3	-6.244397500000001	106.9919047	1
472	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	fadbf713-7af9-4c0a-af44-5fc929a7f9dc1	-6.2065725	106.8752798	1
473	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	fadbf713-7af9-4c0a-af44-5fc929a7f9dc2	-6.2065725	106.8752798	1
474	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	fadbf713-7af9-4c0a-af44-5fc929a7f9dc3	-6.2065725	106.8752798	1
475	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0021	-6.2065725	106.8752798	1
476	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0022	-6.2065725	106.8752798	1
477	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	9d96bcbb-b0b0-4c57-afff-e7bb2c71c0023	-6.2065725	106.8752798	1
478	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	5ad42164-30e0-477e-bca0-53df257c85771	-6.2065725	106.8752798	1
479	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	5ad42164-30e0-477e-bca0-53df257c85772	-6.2065725	106.8752798	1
480	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	5ad42164-30e0-477e-bca0-53df257c85773	-6.2065725	106.8752798	1
481	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	8311f5c9-e80f-4888-bdb7-1ed85dfd889a1	-6.2065725	106.8752798	1
482	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	8311f5c9-e80f-4888-bdb7-1ed85dfd889a2	-6.2065725	106.8752798	1
483	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	8311f5c9-e80f-4888-bdb7-1ed85dfd889a3	-6.2065725	106.8752798	1
484	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	9c50f350-9086-4c0a-bf8d-49d3e0ef9d021	-6.2065725	106.8752798	1
485	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	9c50f350-9086-4c0a-bf8d-49d3e0ef9d022	-6.2065725	106.8752798	1
486	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	9c50f350-9086-4c0a-bf8d-49d3e0ef9d023	-6.2065725	106.8752798	1
490	Cikarang, Bekasi, West Java, Indonesia	324bff81-6a51-4411-83ca-b7a358904ceb1	-6.307923199999999	107.172085	1
487	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	3f05edaa-dff3-49c0-b527-099f4936ad5d1	-6.2065725	106.8752798	1
488	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	3f05edaa-dff3-49c0-b527-099f4936ad5d2	-6.2065725	106.8752798	1
489	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	3f05edaa-dff3-49c0-b527-099f4936ad5d3	-6.2065725	106.8752798	1
493	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	ca293462-4373-4cc3-98ac-c4a4f9aa9fa91	-6.244397500000001	106.9919047	1
494	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	ca293462-4373-4cc3-98ac-c4a4f9aa9fa92	-6.244397500000001	106.9919047	1
495	Panasonic Gobel Indonesia Service Center Bekasi, RT.001/RW.005, Kayuringin Jaya, Bekasi City, West Java, Indonesia	ca293462-4373-4cc3-98ac-c4a4f9aa9fa93	-6.244397500000001	106.9919047	1
499	Jalan Letjen Suprapto No.3e, A U R, Medan City, North Sumatra, Indonesia	5a7693cb-af7f-4243-aa21-d02bb2448f371	3.5807804	98.68180559999999	1
500	Jalan Letjen Suprapto No.3e, A U R, Medan City, North Sumatra, Indonesia	5a7693cb-af7f-4243-aa21-d02bb2448f372	3.5807804	98.68180559999999	1
501	Jalan Letjen Suprapto No.3e, A U R, Medan City, North Sumatra, Indonesia	5a7693cb-af7f-4243-aa21-d02bb2448f373	3.5807804	98.68180559999999	1
502	Cengkareng, West Jakarta City, Jakarta, Indonesia	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f1	-6.1486651	106.7352584	1
503	Cengkareng, West Jakarta City, Jakarta, Indonesia	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f2	-6.1486651	106.7352584	1
504	Cengkareng, West Jakarta City, Jakarta, Indonesia	cd224b4c-54e7-4f6a-94dd-d7163cb6a67f3	-6.1486651	106.7352584	1
505	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	6bc3fd96-267c-46f7-953d-85b068c3d6291	-6.2065725	106.8752798	1
506	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	6bc3fd96-267c-46f7-953d-85b068c3d6292	-6.2065725	106.8752798	1
507	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	6bc3fd96-267c-46f7-953d-85b068c3d6293	-6.2065725	106.8752798	1
508	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	c128f8e8-5519-4373-898d-f23a6125a4521	-6.2065725	106.8752798	1
509	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	c128f8e8-5519-4373-898d-f23a6125a4522	-6.2065725	106.8752798	1
510	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	c128f8e8-5519-4373-898d-f23a6125a4523	-6.2065725	106.8752798	1
511	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	fd08da2d-ce43-4d99-92f7-153b732b79301	-6.2065725	106.8752798	1
512	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	fd08da2d-ce43-4d99-92f7-153b732b79302	-6.2065725	106.8752798	1
513	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	fd08da2d-ce43-4d99-92f7-153b732b79303	-6.2065725	106.8752798	1
514	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	3d384c0c-574a-4c00-a918-9e76e59fad8c1	-6.2065725	106.8752798	1
515	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	3d384c0c-574a-4c00-a918-9e76e59fad8c2	-6.2065725	106.8752798	1
516	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	3d384c0c-574a-4c00-a918-9e76e59fad8c3	-6.2065725	106.8752798	1
517	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	393deb31-8169-4cf2-8ba9-ec90f20dcdc11	-6.2065725	106.8752798	1
518	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	393deb31-8169-4cf2-8ba9-ec90f20dcdc12	-6.2065725	106.8752798	1
519	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	393deb31-8169-4cf2-8ba9-ec90f20dcdc13	-6.2065725	106.8752798	1
520	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece1	-6.2065725	106.8752798	1
521	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece2	-6.2065725	106.8752798	1
522	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece3	-6.2065725	106.8752798	1
523	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	adc33ecb-2c20-414c-b33d-8578e66c3f8e1	-6.2065725	106.8752798	1
524	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	adc33ecb-2c20-414c-b33d-8578e66c3f8e2	-6.2065725	106.8752798	1
525	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	adc33ecb-2c20-414c-b33d-8578e66c3f8e3	-6.2065725	106.8752798	1
529	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	675ebb61-bdde-4195-be83-365448ab00de1	-6.2065725	106.8752798	1
530	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	675ebb61-bdde-4195-be83-365448ab00de2	-6.2065725	106.8752798	1
538	Cikarang, Bekasi, West Java, Indonesia	31db3546-d72b-4b12-8f06-228fdf5787ab1	-6.307923199999999	107.172085	1
531	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	675ebb61-bdde-4195-be83-365448ab00de3	-6.2065725	106.8752798	1
532	Puninar Yusen Logistics Indonesia, PT, Simpangan, Bekasi, West Java, Indonesia	13a77a62-81c5-4aef-a0cc-f9fe4da444131	-6.277000999999999	107.165712	2
533	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	13a77a62-81c5-4aef-a0cc-f9fe4da444131	-6.4198055	107.4060214	1
534	Puninar Yusen Logistics Indonesia, PT, Simpangan, Bekasi, West Java, Indonesia	13a77a62-81c5-4aef-a0cc-f9fe4da444132	-6.277000999999999	107.165712	2
535	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	13a77a62-81c5-4aef-a0cc-f9fe4da444132	-6.4198055	107.4060214	1
536	PT Sumi Rubber Indonesia, Kalihurip, Karawang Regency, West Java, Indonesia	13a77a62-81c5-4aef-a0cc-f9fe4da444133	-6.4198055	107.4060214	1
537	Puninar Yusen Logistics Indonesia, PT, Simpangan, Bekasi, West Java, Indonesia	13a77a62-81c5-4aef-a0cc-f9fe4da444133	-6.277000999999999	107.165712	2
547	Cikarang, Bekasi, West Java, Indonesia	5d4d1702-cea0-48a2-806a-e49dece6fcc22	-6.307923199999999	107.172085	1
548	Cikarang, Bekasi, West Java, Indonesia	5d4d1702-cea0-48a2-806a-e49dece6fcc23	-6.307923199999999	107.172085	1
549	Cikarang, Bekasi, West Java, Indonesia	5d4d1702-cea0-48a2-806a-e49dece6fcc24	-6.307923199999999	107.172085	1
550	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	b1ee4420-1ec8-4632-bc23-f7884f4a01551	-6.119959199999999	106.9451223	2
551	Cibitung, Bekasi, West Java, Indonesia	b1ee4420-1ec8-4632-bc23-f7884f4a01551	-6.2335134	107.1071	1
552	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	b1ee4420-1ec8-4632-bc23-f7884f4a01552	-6.119959199999999	106.9451223	2
553	Cibitung, Bekasi, West Java, Indonesia	b1ee4420-1ec8-4632-bc23-f7884f4a01552	-6.2335134	107.1071	1
554	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	b1ee4420-1ec8-4632-bc23-f7884f4a01553	-6.119959199999999	106.9451223	2
555	Cibitung, Bekasi, West Java, Indonesia	b1ee4420-1ec8-4632-bc23-f7884f4a01553	-6.2335134	107.1071	1
556	Cibitung, Bekasi, West Java, Indonesia	b1ee4420-1ec8-4632-bc23-f7884f4a01554	-6.2335134	107.1071	1
557	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	b1ee4420-1ec8-4632-bc23-f7884f4a01554	-6.119959199999999	106.9451223	2
566	Cikarang, Bekasi, West Java, Indonesia	11731f42-a238-4d5a-98e4-ea2bce56a4511	-6.307923199999999	107.172085	1
567	Cikarang, Bekasi, West Java, Indonesia	11731f42-a238-4d5a-98e4-ea2bce56a4512	-6.307923199999999	107.172085	1
568	Cikarang, Bekasi, West Java, Indonesia	11731f42-a238-4d5a-98e4-ea2bce56a4513	-6.307923199999999	107.172085	1
569	Cikarang, Bekasi, West Java, Indonesia	11731f42-a238-4d5a-98e4-ea2bce56a4514	-6.307923199999999	107.172085	1
558	Cibitung, Bekasi, West Java, Indonesia	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f891	-6.2335134	107.1071	1
559	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f891	-6.119959199999999	106.9451223	2
560	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f892	-6.119959199999999	106.9451223	2
561	Cibitung, Bekasi, West Java, Indonesia	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f892	-6.2335134	107.1071	1
562	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f893	-6.119959199999999	106.9451223	2
563	Cibitung, Bekasi, West Java, Indonesia	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f893	-6.2335134	107.1071	1
564	Cibitung, Bekasi, West Java, Indonesia	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f894	-6.2335134	107.1071	1
565	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	1f5e6e70-d88d-4c77-8aaf-7fcf969f0f894	-6.119959199999999	106.9451223	2
570	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e1	-6.119959199999999	106.9451223	2
571	Cibinong City Mall, Jalan Tegar Beriman, Pakansari, Bogor, West Java, Indonesia	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e1	-6.4842002	106.8423123	1
572	Cibinong City Mall, Jalan Tegar Beriman, Pakansari, Bogor, West Java, Indonesia	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e2	-6.4842002	106.8423123	1
573	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e2	-6.119959199999999	106.9451223	2
574	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e3	-6.119959199999999	106.9451223	2
575	Cibinong City Mall, Jalan Tegar Beriman, Pakansari, Bogor, West Java, Indonesia	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e3	-6.4842002	106.8423123	1
576	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e4	-6.119959199999999	106.9451223	2
577	Cibinong City Mall, Jalan Tegar Beriman, Pakansari, Bogor, West Java, Indonesia	2b8e75a5-7fdd-461b-96df-ecf9236f7b7e4	-6.4842002	106.8423123	1
578	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	e4f6d78c-6d48-436b-a387-6406950aee9b1	-6.119959199999999	106.9451223	2
579	Jalan Kemayoran No.RT. 01, East Air Tawar, Padang City, West Sumatra, Indonesia	e4f6d78c-6d48-436b-a387-6406950aee9b1	-0.8907149999999999	100.35576	1
580	Jalan Kemayoran No.RT. 01, East Air Tawar, Padang City, West Sumatra, Indonesia	e4f6d78c-6d48-436b-a387-6406950aee9b2	-0.8907149999999999	100.35576	1
581	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	e4f6d78c-6d48-436b-a387-6406950aee9b2	-6.119959199999999	106.9451223	2
582	Jalan Kemayoran No.RT. 01, East Air Tawar, Padang City, West Sumatra, Indonesia	e4f6d78c-6d48-436b-a387-6406950aee9b3	-0.8907149999999999	100.35576	1
583	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	e4f6d78c-6d48-436b-a387-6406950aee9b3	-6.119959199999999	106.9451223	2
584	Jalan Kemayoran No.RT. 01, East Air Tawar, Padang City, West Sumatra, Indonesia	e4f6d78c-6d48-436b-a387-6406950aee9b4	-0.8907149999999999	100.35576	1
585	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	e4f6d78c-6d48-436b-a387-6406950aee9b4	-6.119959199999999	106.9451223	2
586	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	03a1f255-8904-448c-a71b-aa588b3211581	-6.119959199999999	106.9451223	2
587	Jalan Kemayoran No.RT. 01, East Air Tawar, Padang City, West Sumatra, Indonesia	03a1f255-8904-448c-a71b-aa588b3211581	-0.8907149999999999	100.35576	1
588	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	03a1f255-8904-448c-a71b-aa588b3211582	-6.119959199999999	106.9451223	2
589	Jalan Kemayoran No.RT. 01, East Air Tawar, Padang City, West Sumatra, Indonesia	03a1f255-8904-448c-a71b-aa588b3211582	-0.8907149999999999	100.35576	1
590	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	03a1f255-8904-448c-a71b-aa588b3211583	-6.119959199999999	106.9451223	2
591	Jalan Kemayoran No.RT. 01, East Air Tawar, Padang City, West Sumatra, Indonesia	03a1f255-8904-448c-a71b-aa588b3211583	-0.8907149999999999	100.35576	1
592	Forway Logistics, Jalan Medan, RW.2, Cilincing, North Jakarta City, Jakarta, Indonesia	03a1f255-8904-448c-a71b-aa588b3211584	-6.119959199999999	106.9451223	2
593	Jalan Kemayoran No.RT. 01, East Air Tawar, Padang City, West Sumatra, Indonesia	03a1f255-8904-448c-a71b-aa588b3211584	-0.8907149999999999	100.35576	1
598	Kebayoran Lama, South Jakarta City, Jakarta, Indonesia	0ab7f929-0295-4a27-b49d-e2ab8df8db301	-6.2443916	106.7765443	1
599	Kebayoran Lama, South Jakarta City, Jakarta, Indonesia	0ab7f929-0295-4a27-b49d-e2ab8df8db302	-6.2443916	106.7765443	1
600	Kebayoran Lama, South Jakarta City, Jakarta, Indonesia	0ab7f929-0295-4a27-b49d-e2ab8df8db303	-6.2443916	106.7765443	1
601	Kebayoran Lama, South Jakarta City, Jakarta, Indonesia	0ab7f929-0295-4a27-b49d-e2ab8df8db304	-6.2443916	106.7765443	1
602	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	d6791664-e59a-45e5-b5ee-5a1f5eb6b5111	-6.2065725	106.8752798	1
603	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	d6791664-e59a-45e5-b5ee-5a1f5eb6b5112	-6.2065725	106.8752798	1
604	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	d6791664-e59a-45e5-b5ee-5a1f5eb6b5113	-6.2065725	106.8752798	1
605	Directorate General of Customs and Excise, Jalan Jendral Ahmad Yani By Pass, RT.12/RW.5, Rawamangun, East Jakarta City, Jakarta, Indonesia	d6791664-e59a-45e5-b5ee-5a1f5eb6b5114	-6.2065725	106.8752798	1
606	VILLA TAJUR, RT.04/RW.08, Sindangrasa, Bogor City, West Java, Indonesia	fd79d7ee-fe24-47ea-b81c-f41953d398b91	-6.6367563	106.8356681	1
607	VILLA TAJUR, RT.04/RW.08, Sindangrasa, Bogor City, West Java, Indonesia	fd79d7ee-fe24-47ea-b81c-f41953d398b92	-6.6367563	106.8356681	1
608	VILLA TAJUR, RT.04/RW.08, Sindangrasa, Bogor City, West Java, Indonesia	fd79d7ee-fe24-47ea-b81c-f41953d398b93	-6.6367563	106.8356681	1
609	VILLA TAJUR, RT.04/RW.08, Sindangrasa, Bogor City, West Java, Indonesia	fd79d7ee-fe24-47ea-b81c-f41953d398b94	-6.6367563	106.8356681	1
610	Grand City Mall Surabaya, Jalan Walikota Mustajab, Ketabang, Surabaya City, East Java, Indonesia	2c807148-d49d-493f-8aff-6d550797ca571	-7.261965099999999	112.7498109	1
611	Grand City Mall Surabaya, Jalan Walikota Mustajab, Ketabang, Surabaya City, East Java, Indonesia	2c807148-d49d-493f-8aff-6d550797ca572	-7.261965099999999	112.7498109	1
612	Grand City Mall Surabaya, Jalan Walikota Mustajab, Ketabang, Surabaya City, East Java, Indonesia	2c807148-d49d-493f-8aff-6d550797ca573	-7.261965099999999	112.7498109	1
613	Grand City Mall Surabaya, Jalan Walikota Mustajab, Ketabang, Surabaya City, East Java, Indonesia	2c807148-d49d-493f-8aff-6d550797ca574	-7.261965099999999	112.7498109	1
614	Sumber Karya Indah - SKI Bogor, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	d050ea8f-13f6-4677-863c-0b2b5bad0dab1	-6.6217646	106.8214054	1
615	Sumber Karya Indah - SKI Bogor, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	d050ea8f-13f6-4677-863c-0b2b5bad0dab2	-6.6217646	106.8214054	1
616	Sumber Karya Indah - SKI Bogor, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	d050ea8f-13f6-4677-863c-0b2b5bad0dab3	-6.6217646	106.8214054	1
617	Sumber Karya Indah - SKI Bogor, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	d050ea8f-13f6-4677-863c-0b2b5bad0dab4	-6.6217646	106.8214054	1
618	Cengkareng, West Jakarta City, Jakarta, Indonesia	1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd1	-6.1486651	106.7352584	1
619	Cengkareng, West Jakarta City, Jakarta, Indonesia	1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd2	-6.1486651	106.7352584	1
620	Cengkareng, West Jakarta City, Jakarta, Indonesia	1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd3	-6.1486651	106.7352584	1
621	Cengkareng, West Jakarta City, Jakarta, Indonesia	1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd4	-6.1486651	106.7352584	1
622	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	8c689fea-dc94-4e79-add1-a100863461991	-6.6228706	106.8223211	1
623	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	8c689fea-dc94-4e79-add1-a100863461992	-6.6228706	106.8223211	1
624	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	8c689fea-dc94-4e79-add1-a100863461993	-6.6228706	106.8223211	1
625	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	8c689fea-dc94-4e79-add1-a100863461994	-6.6228706	106.8223211	1
626	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	6e2cf3e8-9c58-4fc1-9524-06cbc5a407601	-6.6228706	106.8223211	1
627	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	6e2cf3e8-9c58-4fc1-9524-06cbc5a407602	-6.6228706	106.8223211	1
628	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	6e2cf3e8-9c58-4fc1-9524-06cbc5a407603	-6.6228706	106.8223211	1
629	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	6e2cf3e8-9c58-4fc1-9524-06cbc5a407604	-6.6228706	106.8223211	1
630	Cikarang, Bekasi, West Java, Indonesia	e64169f7-5a28-4596-963a-c82f7518ef821	-6.307923199999999	107.172085	1
631	Cikarang, Bekasi, West Java, Indonesia	e64169f7-5a28-4596-963a-c82f7518ef822	-6.307923199999999	107.172085	1
632	Cikarang, Bekasi, West Java, Indonesia	e64169f7-5a28-4596-963a-c82f7518ef823	-6.307923199999999	107.172085	1
633	Cikarang, Bekasi, West Java, Indonesia	e64169f7-5a28-4596-963a-c82f7518ef824	-6.307923199999999	107.172085	1
634	Perumahan Graha Pajajaran, RT.05/RW.15, Katulampa, Bogor City, West Java, Indonesia	bdaa653c-bf1c-4cc6-8fc1-758112cea4281	-6.6195205	106.8335602	1
635	Perumahan Graha Pajajaran, RT.05/RW.15, Katulampa, Bogor City, West Java, Indonesia	bdaa653c-bf1c-4cc6-8fc1-758112cea4282	-6.6195205	106.8335602	1
636	Perumahan Graha Pajajaran, RT.05/RW.15, Katulampa, Bogor City, West Java, Indonesia	bdaa653c-bf1c-4cc6-8fc1-758112cea4283	-6.6195205	106.8335602	1
637	Perumahan Graha Pajajaran, RT.05/RW.15, Katulampa, Bogor City, West Java, Indonesia	bdaa653c-bf1c-4cc6-8fc1-758112cea4284	-6.6195205	106.8335602	1
638	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	ed880f83-a3e2-4505-a043-401dbaf33c3e1	-6.6228706	106.8223211	1
639	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	ed880f83-a3e2-4505-a043-401dbaf33c3e2	-6.6228706	106.8223211	1
640	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	ed880f83-a3e2-4505-a043-401dbaf33c3e3	-6.6228706	106.8223211	1
641	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	ed880f83-a3e2-4505-a043-401dbaf33c3e4	-6.6228706	106.8223211	1
642	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	b099e47b-2c40-4280-b3b9-d3679d2f8c621	-6.6228706	106.8223211	1
643	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	b099e47b-2c40-4280-b3b9-d3679d2f8c622	-6.6228706	106.8223211	1
644	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	b099e47b-2c40-4280-b3b9-d3679d2f8c623	-6.6228706	106.8223211	1
645	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	b099e47b-2c40-4280-b3b9-d3679d2f8c624	-6.6228706	106.8223211	1
646	Cikarang, Bekasi, West Java, Indonesia	e34ba684-8663-41ed-9f80-a1d5bea85f0f1	-6.307923199999999	107.172085	1
647	Cikarang, Bekasi, West Java, Indonesia	e34ba684-8663-41ed-9f80-a1d5bea85f0f2	-6.307923199999999	107.172085	1
648	Cikarang, Bekasi, West Java, Indonesia	e34ba684-8663-41ed-9f80-a1d5bea85f0f3	-6.307923199999999	107.172085	1
649	Cikarang, Bekasi, West Java, Indonesia	e34ba684-8663-41ed-9f80-a1d5bea85f0f4	-6.307923199999999	107.172085	1
650	Cikarang, Bekasi, West Java, Indonesia	d00708c9-c77a-42a9-b15b-abd76e2495411	-6.307923199999999	107.172085	1
651	Cikarang, Bekasi, West Java, Indonesia	d00708c9-c77a-42a9-b15b-abd76e2495412	-6.307923199999999	107.172085	1
652	Cikarang, Bekasi, West Java, Indonesia	d00708c9-c77a-42a9-b15b-abd76e2495413	-6.307923199999999	107.172085	1
653	Cikarang, Bekasi, West Java, Indonesia	d00708c9-c77a-42a9-b15b-abd76e2495414	-6.307923199999999	107.172085	1
654	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	60b0c35d-a58b-4bc8-a2e3-249a820a3f241	-6.6228706	106.8223211	1
655	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	60b0c35d-a58b-4bc8-a2e3-249a820a3f242	-6.6228706	106.8223211	1
656	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	60b0c35d-a58b-4bc8-a2e3-249a820a3f243	-6.6228706	106.8223211	1
657	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	60b0c35d-a58b-4bc8-a2e3-249a820a3f244	-6.6228706	106.8223211	1
658	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	d8beea02-0ae4-4bf3-bf90-089a2382b1e11	-6.6228706	106.8223211	1
659	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	d8beea02-0ae4-4bf3-bf90-089a2382b1e12	-6.6228706	106.8223211	1
660	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	d8beea02-0ae4-4bf3-bf90-089a2382b1e13	-6.6228706	106.8223211	1
661	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	d8beea02-0ae4-4bf3-bf90-089a2382b1e14	-6.6228706	106.8223211	1
662	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	c69824d3-7a04-4dab-9243-a4663e3ee4781	-6.6228706	106.8223211	1
663	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	c69824d3-7a04-4dab-9243-a4663e3ee4782	-6.6228706	106.8223211	1
664	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	c69824d3-7a04-4dab-9243-a4663e3ee4783	-6.6228706	106.8223211	1
665	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	c69824d3-7a04-4dab-9243-a4663e3ee4784	-6.6228706	106.8223211	1
666	Cengkareng, West Jakarta City, Jakarta, Indonesia	89a90d27-3410-4d0e-b990-32ea7f1aac031	-6.1486651	106.7352584	1
667	Cengkareng, West Jakarta City, Jakarta, Indonesia	89a90d27-3410-4d0e-b990-32ea7f1aac032	-6.1486651	106.7352584	1
668	Cengkareng, West Jakarta City, Jakarta, Indonesia	89a90d27-3410-4d0e-b990-32ea7f1aac033	-6.1486651	106.7352584	1
669	Cengkareng, West Jakarta City, Jakarta, Indonesia	89a90d27-3410-4d0e-b990-32ea7f1aac034	-6.1486651	106.7352584	1
670	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	1ec0c1fe-6b84-44fe-924e-6d416f35f04d1	-6.6228706	106.8223211	1
671	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	1ec0c1fe-6b84-44fe-924e-6d416f35f04d2	-6.6228706	106.8223211	1
672	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	-6.6228706	106.8223211	1
673	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	1ec0c1fe-6b84-44fe-924e-6d416f35f04d4	-6.6228706	106.8223211	1
674	Sekolah Islam Ibnu Hajar (SIIHA), Jalan Katulampa Raya, RT.01/RW.01, Katulampa, Bogor City, West Java, Indonesia	437ebef7-53dd-4738-84dc-263aa239e6c71	-6.628542500000001	106.8285257	1
675	Sekolah Islam Ibnu Hajar (SIIHA), Jalan Katulampa Raya, RT.01/RW.01, Katulampa, Bogor City, West Java, Indonesia	437ebef7-53dd-4738-84dc-263aa239e6c72	-6.628542500000001	106.8285257	1
676	Sekolah Islam Ibnu Hajar (SIIHA), Jalan Katulampa Raya, RT.01/RW.01, Katulampa, Bogor City, West Java, Indonesia	437ebef7-53dd-4738-84dc-263aa239e6c73	-6.628542500000001	106.8285257	1
677	Sekolah Islam Ibnu Hajar (SIIHA), Jalan Katulampa Raya, RT.01/RW.01, Katulampa, Bogor City, West Java, Indonesia	437ebef7-53dd-4738-84dc-263aa239e6c74	-6.628542500000001	106.8285257	1
678	Bogor Riverside, RT.04/RW.10, Katulampa, Bogor City, West Java, Indonesia	f65452ac-5fe1-45c9-b14f-f27da4d788941	-6.629473700000001	106.8305149	1
679	Bogor Riverside, RT.04/RW.10, Katulampa, Bogor City, West Java, Indonesia	f65452ac-5fe1-45c9-b14f-f27da4d788942	-6.629473700000001	106.8305149	1
680	Bogor Riverside, RT.04/RW.10, Katulampa, Bogor City, West Java, Indonesia	f65452ac-5fe1-45c9-b14f-f27da4d788943	-6.629473700000001	106.8305149	1
681	Bogor Riverside, RT.04/RW.10, Katulampa, Bogor City, West Java, Indonesia	f65452ac-5fe1-45c9-b14f-f27da4d788944	-6.629473700000001	106.8305149	1
682	Bogor Riverside, RT.04/RW.10, Katulampa, Bogor City, West Java, Indonesia	50090798-dfbd-4518-8a84-78a10397747b1	-6.629473700000001	106.8305149	1
683	Bogor Riverside, RT.04/RW.10, Katulampa, Bogor City, West Java, Indonesia	50090798-dfbd-4518-8a84-78a10397747b2	-6.629473700000001	106.8305149	1
684	Bogor Riverside, RT.04/RW.10, Katulampa, Bogor City, West Java, Indonesia	50090798-dfbd-4518-8a84-78a10397747b3	-6.629473700000001	106.8305149	1
685	Bogor Riverside, RT.04/RW.10, Katulampa, Bogor City, West Java, Indonesia	50090798-dfbd-4518-8a84-78a10397747b4	-6.629473700000001	106.8305149	1
686	WAROS, Gang Pemuda, RT.02/RW.01, Katulampa, Bogor City, West Java, Indonesia	ad119148-6875-4a87-8ec2-e044f2e3913e1	-6.6246047	106.8265643	1
687	WAROS, Gang Pemuda, RT.02/RW.01, Katulampa, Bogor City, West Java, Indonesia	ad119148-6875-4a87-8ec2-e044f2e3913e2	-6.6246047	106.8265643	1
688	WAROS, Gang Pemuda, RT.02/RW.01, Katulampa, Bogor City, West Java, Indonesia	ad119148-6875-4a87-8ec2-e044f2e3913e3	-6.6246047	106.8265643	1
689	WAROS, Gang Pemuda, RT.02/RW.01, Katulampa, Bogor City, West Java, Indonesia	ad119148-6875-4a87-8ec2-e044f2e3913e4	-6.6246047	106.8265643	1
690	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	5fe18cfa-b431-465b-8555-7880de1d66e71	-6.6228706	106.8223211	1
691	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	5fe18cfa-b431-465b-8555-7880de1d66e72	-6.6228706	106.8223211	1
692	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	5fe18cfa-b431-465b-8555-7880de1d66e73	-6.6228706	106.8223211	1
693	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	5fe18cfa-b431-465b-8555-7880de1d66e74	-6.6228706	106.8223211	1
694	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	47676dff-207c-46af-bc29-a7fb93afaf801	-6.6228706	106.8223211	1
695	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	47676dff-207c-46af-bc29-a7fb93afaf802	-6.6228706	106.8223211	1
696	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	47676dff-207c-46af-bc29-a7fb93afaf803	-6.6228706	106.8223211	1
697	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	47676dff-207c-46af-bc29-a7fb93afaf804	-6.6228706	106.8223211	1
698	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	04fc413c-3a4a-43d3-9fd3-3c5966ff50501	-6.6228706	106.8223211	1
699	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	04fc413c-3a4a-43d3-9fd3-3c5966ff50502	-6.6228706	106.8223211	1
700	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	04fc413c-3a4a-43d3-9fd3-3c5966ff50503	-6.6228706	106.8223211	1
701	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	04fc413c-3a4a-43d3-9fd3-3c5966ff50504	-6.6228706	106.8223211	1
702	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	d0db336c-d39c-467d-86ce-0fae0180bb6e1	-6.6228706	106.8223211	1
703	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	d0db336c-d39c-467d-86ce-0fae0180bb6e2	-6.6228706	106.8223211	1
704	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	d0db336c-d39c-467d-86ce-0fae0180bb6e3	-6.6228706	106.8223211	1
705	Water Park SKI, Wisata SKI Tajur, Jalan Katulampa Raya, Katulampa, Bogor City, West Java, Indonesia	d0db336c-d39c-467d-86ce-0fae0180bb6e4	-6.6228706	106.8223211	1
\.


--
-- Data for Name: document_trucking_detil; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.document_trucking_detil (id_request_booking, booked_date, booking_note, created_date, harga_penawaran, id_service_order, id_platform, is_booked, paid_status, status, "timestamp", waktu_penawaran) FROM stdin;
9e6c11c4-690e-4bc8-8b1f-6eacd17132ce1	\N	\N	2020-07-22 11:46:51.712	5000000	1	PL000	0	0	\N	2020-07-22 04:43:42	2020-07-22 04:43:42
a96402aa-cc8f-4d65-89e5-ca61440990131	2020-07-21 09:05:20.89		2020-07-21 09:05:02.112	5000000	1	PL000	1	1	Menunggu konfirmasi provider	2020-07-21 02:01:52	2020-07-21 02:01:52
4637c27d-7d01-4844-a7f9-0b286fa786441	\N	\N	2020-07-21 10:41:33.551	5000000	1	PL000	0	0	\N	2020-07-21 03:38:23	2020-07-21 03:38:23
d218503c-5921-4cb9-a9db-299bfd1fa7d21	\N	\N	2020-07-21 10:47:34.397	5000000	1	PL000	0	0	\N	2020-07-21 03:44:22	2020-07-21 03:44:22
0f3531e3-682d-460d-8466-e0595d12bab41	\N	\N	2020-07-21 12:12:33.572	5000000	1	PL000	0	0	\N	2020-07-21 05:09:22	2020-07-21 05:09:22
85c7b7d2-66d3-4102-88fe-68916fa12a081	\N	\N	2020-07-21 12:13:29.695	5000000	1	PL000	0	0	\N	2020-07-21 05:10:20	2020-07-21 05:10:20
c3b09897-4ea1-4315-87a4-64e84456582e1	\N	\N	2020-07-22 09:27:27.154	5000000	1	PL000	0	0	\N	2020-07-22 02:24:16	2020-07-22 02:24:16
20bb4554-530c-43d1-a2e7-fb506c647a191	\N	\N	2020-07-22 09:28:03.218	5000000	1	PL000	0	0	\N	2020-07-22 02:24:53	2020-07-22 02:24:53
3d57fd22-e94b-4d04-a521-b1a11b7944981	\N	\N	2020-07-22 15:08:34.644	5000000	1	PL000	\N	\N	\N	2020-07-22 08:05:24	2020-07-22 08:05:24
asda929ad0920jdadkasdas	\N	\N	2020-06-30 17:35:04.781	5000000	1	PL001	1	1	Menunggu Pembayaran	2019-09-08 14:00:00	2019-10-07 14:00:00
d0d11f22-96f5-4c8e-97c2-200c448d0d211	\N	\N	2020-07-22 09:32:54.427	5000000	1	PL000	\N	\N	\N	2020-07-22 02:29:44	2020-07-22 02:29:44
f2cab17b-b482-43d6-b653-f669f9909ddc1	\N	\N	2020-07-22 09:34:02.376	5000000	1	PL000	\N	\N	\N	2020-07-22 02:30:52	2020-07-22 02:30:52
955b8b18-c775-48a3-a969-15701dcf13c51	\N	\N	2020-07-22 15:21:02.438	5000000	1	PL000	\N	\N	\N	2020-07-22 08:17:52	2020-07-22 08:17:52
ee9bb38c-6b0a-4e7a-970a-1cbd53c7a9021	\N	\N	2020-07-22 11:17:02.411	5000000	1	PL000	\N	\N	\N	2020-07-22 04:13:52	2020-07-22 04:13:52
abcdef12345	\N	\N	2020-07-20 12:45:41.69	5000000	1	PL000	\N	\N	\N	2020-07-20 05:45:40	2020-07-20 05:45:40
asdfghjkl1234	\N	\N	2020-07-20 12:45:41.779	5000000	1	PL000	\N	\N	\N	2020-07-20 05:45:40	2020-07-20 05:45:40
825f1fac-90c1-407a-b6c8-39189ba144201	\N	\N	2020-07-22 11:23:45.812	5000000	1	PL000	\N	\N	\N	2020-07-22 04:20:36	2020-07-22 04:20:36
b8e8b957-e401-4433-9429-1d88d3ee700c1	\N	\N	2020-07-20 12:53:34.021	5000000	1	PL000	\N	\N	\N	2020-07-20 05:50:25	2020-07-20 05:50:25
62834227-31a5-45d3-ae7e-885b658fc7761	\N	\N	2020-07-22 12:02:14.463	5000000	1	PL000	\N	\N	\N	2020-07-22 04:59:05	2020-07-22 04:59:05
08e143b4-a69b-4ecd-8596-236851c581b01	\N	\N	2020-07-22 11:26:54.656	5000000	1	PL000	\N	\N	\N	2020-07-22 04:23:45	2020-07-22 04:23:45
13c775ba-7934-4c55-a60a-57405de284651	\N	\N	2020-07-22 11:48:36.988	5000000	1	PL000	\N	\N	\N	2020-07-22 04:45:27	2020-07-22 04:45:27
2d3f5569-f672-4c76-baa8-948c310c278e1	2020-07-20 13:52:33.027	catatan booking	2020-07-20 13:51:16.533	5000000	1	PL000	1	0	Menunggu konfirmasi provider	2020-07-20 06:48:03	2020-07-20 06:48:03
f6b64c71-d184-4890-ac45-8661c4dec6981	2020-07-20 13:10:06.348	catatan booking	2020-07-20 13:09:38.257	5000000	1	PL000	1	1	\N	2020-07-20 06:06:27	2020-07-20 06:06:27
dd274863-365b-451c-a7fb-e9642d72bb051	\N	\N	2020-07-20 14:33:34.767	5000000	1	PL000	0	0	\N	2020-07-20 07:30:25	2020-07-20 07:30:25
279d15e9-98bf-49ef-948f-e69a98db4df41	\N	\N	2020-07-22 11:36:38.762	5000000	1	PL000	\N	\N	\N	2020-07-22 04:33:29	2020-07-22 04:33:29
6c515280-29d3-4ceb-9740-ead068f7fd011	\N	\N	2020-07-22 11:39:41.857	5000000	1	PL000	0	0	\N	2020-07-22 04:36:32	2020-07-22 04:36:32
92753af6-466a-497e-81ca-7ee43cfb27ca1	\N	\N	2020-07-20 14:37:42.059	5000000	1	PL000	0	0	\N	2020-07-20 07:34:30	2020-07-20 07:34:30
c32a07be-4ad3-43d6-99ed-223f4cd2f9eb1	\N	\N	2020-07-20 14:38:48.645	5000000	1	PL000	0	0	\N	2020-07-20 07:35:39	2020-07-20 07:35:39
ec24568b-039d-4bcb-a71b-3d1b57c5f5ca1	\N	\N	2020-07-20 14:39:08.798	5000000	1	PL000	0	0	\N	2020-07-20 07:35:59	2020-07-20 07:35:59
a8e9e638-9cb8-411e-aff3-aedeb58fedac1	2020-07-20 14:36:18.244	ctt	2020-07-20 14:33:25.569	5000000	1	PL000	1	1	Driver Ditemukan	2020-07-20 07:30:16	2020-07-20 07:30:16
ef3c78ed-a0ca-4568-9146-c630561c1f6b1	\N	\N	2020-07-20 14:56:57.829	5000000	1	PL000	0	0	\N	2020-07-20 07:53:46	2020-07-20 07:53:46
c18c53f8-1d40-4f55-b3c0-47116162f6611	\N	\N	2020-07-20 14:58:03.51	5000000	1	PL000	0	0	\N	2020-07-20 07:54:53	2020-07-20 07:54:53
a6ce1813-1fb7-4d62-a5ce-19b7dc7ba6c01	\N	\N	2020-07-20 14:59:12.421	5000000	1	PL000	0	0	\N	2020-07-20 07:56:03	2020-07-20 07:56:03
a1fa4d75-8851-4a43-80da-6c76849ff9491	\N	\N	2020-07-20 14:59:33.168	5000000	1	PL000	0	0	\N	2020-07-20 07:56:24	2020-07-20 07:56:24
3b26eb1e-e53e-4d3f-955d-cca7178196e81	\N	\N	2020-07-20 15:01:28.549	5000000	1	PL000	0	0	\N	2020-07-20 07:58:19	2020-07-20 07:58:19
6553fb0e-a096-44bf-b538-5d736884a44a1	\N	\N	2020-07-20 15:01:59.984	5000000	1	PL000	0	0	\N	2020-07-20 07:58:51	2020-07-20 07:58:51
ca19fd8b-ff55-42d0-9453-deda8ae5f8ac1	\N	\N	2020-07-20 15:11:51.994	5000000	1	PL000	0	0	\N	2020-07-20 08:08:43	2020-07-20 08:08:43
94153f67-316c-4c11-9ac7-435722e9a4651	\N	\N	2020-07-22 11:41:46.487	5000000	1	PL000	0	0	\N	2020-07-22 04:38:36	2020-07-22 04:38:36
25c4ed3d-b17d-4b12-a9ec-7df00f4770901	\N	\N	2020-07-22 11:42:43.079	5000000	1	PL000	\N	\N	\N	2020-07-22 04:39:33	2020-07-22 04:39:33
e1fb9b0e-3936-463f-9c71-d80572d6e63d1	\N	\N	2020-07-22 11:43:40.407	5000000	1	PL000	\N	\N	\N	2020-07-22 04:40:30	2020-07-22 04:40:30
46d3119b-c97f-4ada-beb5-0a9d7705eaa01	\N	\N	2020-07-22 11:48:56.637	5000000	1	PL000	\N	\N	\N	2020-07-22 04:45:47	2020-07-22 04:45:47
abd2fa72-c18f-489f-b999-5ffc13532d691	\N	\N	2020-07-22 12:03:15.036	5000000	1	PL000	\N	\N	\N	2020-07-22 05:00:05	2020-07-22 05:00:05
2c8789e5-4b15-472e-8e71-5269ac7354141	\N	\N	2020-07-22 11:50:14.402	5000000	1	PL000	\N	\N	\N	2020-07-22 04:47:04	2020-07-22 04:47:04
9d98a2d1-fcfb-4962-8e33-96d354155b401	\N	\N	2020-07-22 11:51:31.042	5000000	1	PL000	\N	\N	\N	2020-07-22 04:48:21	2020-07-22 04:48:21
94931173-815e-4f33-8104-465aa9bff6851	\N	\N	2020-07-22 11:52:53.877	5000000	1	PL000	\N	\N	\N	2020-07-22 04:49:44	2020-07-22 04:49:44
1a401b86-2b21-41b8-9e96-edec11bf796d1	\N	\N	2020-07-22 11:53:14.301	5000000	1	PL000	\N	\N	\N	2020-07-22 04:50:04	2020-07-22 04:50:04
4f2304cc-4296-414c-9cda-67f66afc45c91	\N	\N	2020-07-22 11:55:15.427	5000000	1	PL000	\N	\N	\N	2020-07-22 04:52:05	2020-07-22 04:52:05
03718619-4145-4d91-8f9c-682337a3805e1	\N	\N	2020-07-22 11:58:04.729	5000000	1	PL000	\N	\N	\N	2020-07-22 04:54:55	2020-07-22 04:54:55
30ae4933-526e-4e1c-8a28-9295c2fb710e1	\N	\N	2020-07-22 12:03:36.257	5000000	1	PL000	\N	\N	\N	2020-07-22 05:00:26	2020-07-22 05:00:26
077a9391-1346-4d82-896d-f5ba159db2fe1	\N	\N	2020-07-22 12:04:06.756	5000000	1	PL000	\N	\N	\N	2020-07-22 05:00:57	2020-07-22 05:00:57
7694dff7-53ca-4686-8579-6f59b0b2d0481	\N	\N	2020-07-22 12:04:32.896	5000000	1	PL000	0	0	\N	2020-07-22 05:01:23	2020-07-22 05:01:23
46273680-1300-4e05-adca-b446e9e0dd0f1	\N	\N	2020-07-22 12:04:53.246	5000000	1	PL000	\N	\N	\N	2020-07-22 05:01:43	2020-07-22 05:01:43
939c8f85-480d-45e1-a546-d13ab9cf28931	\N	\N	2020-07-22 12:09:10.777	5000000	1	PL000	\N	\N	\N	2020-07-22 05:06:01	2020-07-22 05:06:01
bdf6ef5f-f5ae-4b49-995b-792f037e36761	\N	\N	2020-07-22 12:36:29.381	5000000	1	PL000	\N	\N	\N	2020-07-22 05:33:19	2020-07-22 05:33:19
10f2f18d-8aea-46d6-b2de-7480debbaecc1	\N	\N	2020-07-22 12:36:43.746	5000000	1	PL000	\N	\N	\N	2020-07-22 05:33:34	2020-07-22 05:33:34
3cfe789d-ab16-4fa6-b584-37e3caa8e0d11	\N	\N	2020-07-22 12:37:03.831	5000000	1	PL000	\N	\N	\N	2020-07-22 05:33:54	2020-07-22 05:33:54
6cfa1874-2baa-407d-8c7c-05bef6486d201	\N	\N	2020-07-22 15:00:53.021	5000000	1	PL000	\N	\N	\N	2020-07-22 07:57:42	2020-07-22 07:57:42
462b4736-fc09-4f2a-8ae1-adb48fe161791	\N	\N	2020-07-22 15:04:21.402	5000000	1	PL000	\N	\N	\N	2020-07-22 08:01:10	2020-07-22 08:01:10
c829319d-abc3-4bfb-b8c9-a9f670c082261	\N	\N	2020-07-22 15:04:28.65	5000000	1	PL000	\N	\N	\N	2020-07-22 08:01:18	2020-07-22 08:01:18
2cc792fa-785d-4779-a89f-b34e0712d08c1	\N	\N	2020-07-22 15:04:58.994	5000000	1	PL000	\N	\N	\N	2020-07-22 08:01:48	2020-07-22 08:01:48
661a385c-8b84-42f2-9a36-548fb90c212a1	\N	\N	2020-07-22 15:05:19.766	5000000	1	PL000	\N	\N	\N	2020-07-22 08:02:09	2020-07-22 08:02:09
27a71444-86a9-4e57-9b4c-302cfc68e5e91	\N	\N	2020-07-22 15:05:45.461	5000000	1	PL000	\N	\N	\N	2020-07-22 08:02:35	2020-07-22 08:02:35
98feeadc-27da-4ab6-8c70-c0b2fe7aa3651	\N	\N	2020-07-22 15:22:49.356	5000000	1	PL000	\N	\N	\N	2020-07-22 08:19:39	2020-07-22 08:19:39
a6e57ec3-b902-4962-8256-db74a1ffdd751	\N	\N	2020-07-22 16:46:58.128	5000000	1	PL000	\N	\N	\N	2020-07-22 09:43:47	2020-07-22 09:43:47
f1564570-6ea6-44cf-83ff-6785e9ffd23e1	\N	\N	2020-07-22 15:07:40.271	5000000	1	PL000	\N	\N	\N	2020-07-22 08:04:30	2020-07-22 08:04:30
b14b4abd-b527-46cd-a60d-f06e8d6ee4df1	\N	\N	2020-07-22 15:14:26.874	5000000	1	PL000	\N	\N	\N	2020-07-22 08:11:16	2020-07-22 08:11:16
6f37ebd3-d4f0-4264-84a4-91be5d8699a81	\N	\N	2020-07-22 15:07:51.105	5000000	1	PL000	\N	\N	\N	2020-07-22 08:04:41	2020-07-22 08:04:41
f3957065-7fb6-4ad7-84d1-83890f74ffec3	\N	\N	2020-08-13 10:16:53.14	33498213	31913	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
c2220b86-cc5c-4cb0-a05e-07cc60613b481	\N	\N	2020-07-22 16:47:11.199	5000000	1	PL000	\N	\N	\N	2020-07-22 09:44:01	2020-07-22 09:44:01
7c1ca7bf-2815-4215-aa49-6a33f8495a121	\N	\N	2020-08-04 08:37:03.53	5000000	1	PL000	0	0	\N	2020-08-04 01:33:46	2020-08-04 01:33:46
af0cb8a3-125d-4223-9017-b503546351871	\N	\N	2020-08-04 08:50:41.467	5000000	1	PL000	0	0	\N	2020-08-04 01:47:23	2020-08-04 01:47:23
27577122-6b09-4f82-b448-b7ff41e3573c1	\N	\N	2020-07-22 15:17:29.698	5000000	1	PL000	\N	\N	\N	2020-07-22 08:14:19	2020-07-22 08:14:19
5d690a8a-90e8-4b89-b173-5071987732ba1	\N	\N	2020-08-06 17:09:53.152	5000000	1	PL000	0	0	\N	2020-08-06 10:06:34	2020-08-06 10:06:34
6c1cfefc-4286-4379-ab7c-463d136f85d21	\N	\N	2020-07-22 16:46:26.937	5000000	1	PL000	\N	\N	\N	2020-07-22 09:43:16	2020-07-22 09:43:16
a23ab739-da3c-4dba-87e3-722e49db0ba03	\N	\N	2020-08-09 13:30:17.984	46833456	31573	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
d81b285f-be88-41ee-b5a7-0ee9e395beeb3	\N	\N	2020-08-09 15:26:22.982	46833456	31601	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
21e98ab2-34fd-4a5a-9ac5-e1ba081fd9e93	\N	\N	2020-08-09 16:24:32.361	46833456	31614	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
5f222960-3ebd-460b-8e2c-c4fb6c46c47b3	\N	\N	2020-08-09 16:25:14.398	46833456	31615	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
b8c88aa0-25aa-4300-b1e0-5ba999fa53393	\N	\N	2020-08-09 16:58:27.375	46833456	31628	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
d5c6e3d1-2d65-46cf-bb7c-f7279f0281133	\N	\N	2020-08-09 17:02:37.304	46833456	31629	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
0b4a223f-fa95-439d-bdd9-069ee6e2be952	\N	\N	2020-08-10 08:35:19.403	166250	31635	PL001	0	0	\N	2020-08-09 22:21:13	2020-08-09 22:21:13
d3a70aff-fbf3-4474-884e-0e48fd9901f33	\N	\N	2020-08-10 11:22:53.231	4000000	1	PL004	0	0	\N	\N	2020-08-10 11:22:21
149603f2-19ae-4c19-8d5b-da6acce03bcd2	\N	\N	2020-08-10 13:47:41.926	1591250	31658	PL001	0	0	\N	2020-08-10 06:21:14	2020-08-10 06:21:14
eaa37d02-6db3-46cf-bfaa-2a8a28992f743	\N	\N	2020-08-10 20:28:18.728	21470247	31699	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
4263c552-db16-4fc0-b1a2-3981aaceb7db3	\N	\N	2020-08-10 20:34:18.903	21470247	31700	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
2b5f0c6b-2a52-4485-9888-5feeaa1e8a243	\N	\N	2020-08-10 20:43:03.234	21470247	31701	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
c3401ce4-e89a-4226-ae50-167fbf4adac93	\N	\N	2020-08-10 20:52:06.645	21470247	31702	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
1d56a727-f8f2-49ae-bf5d-71cff1ef31763	\N	\N	2020-08-10 21:31:35.566	21470247	31707	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
0fd2dc2d-7dad-4bb4-9b2d-e1f7b3ca4bc73	\N	\N	2020-08-10 21:44:33.873	21470247	31714	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
d88d4277-b1f7-4619-8d19-5ad2889ee7fb3	\N	\N	2020-08-10 21:53:27.051	21470247	31717	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
211cf67a-0f46-412a-951d-df93ed86ce483	\N	\N	2020-08-10 21:56:24.822	21470247	31719	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
6c5a0087-6993-4cad-8ef8-e6cf9e78e76f3	\N	\N	2020-08-10 21:59:33.711	21470247	31721	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
15d5abb9-da6e-4b06-aee8-fe96739b346d3	\N	\N	2020-08-10 22:00:41.352	21470247	31723	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
088ff457-e961-4ced-a798-1d0e1742d8503	\N	\N	2020-08-10 22:29:17.688	21470247	31730	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
6a339b54-cfb3-4ef1-9e5a-4c4682ca66cd3	\N	\N	2020-08-11 14:09:24.69	6394640	31738	PL001	0	0	\N	2020-08-11 06:21:15	2020-08-11 06:21:15
e0324ebe-959a-4507-aa3f-b3259b54a2d52	\N	\N	2020-08-11 15:12:08.8	261250	31749	PL001	0	0	\N	2020-08-11 06:21:15	2020-08-11 06:21:15
7b60cd9f-bb76-4469-a0f0-2bd090349edd3	2020-08-11 15:14:01.596		2020-08-11 15:13:35.635	1400000	1	PL004	1	1	Finished	\N	2020-08-11 15:12:57
8811d595-1463-4e54-88ff-2061217ef1d23	\N	\N	2020-08-12 09:56:12.227	1246162	31775	PL001	0	0	\N	2020-08-11 22:21:16	2020-08-11 22:21:16
d1be5e65-b186-4252-94fc-95765455f5f52	\N	\N	2020-08-12 11:21:16.051	1900000	31780	PL001	0	0	\N	2020-08-11 22:21:16	2020-08-11 22:21:16
46aabdb2-747e-4a41-943c-c417b45f31593	\N	\N	2020-08-13 10:17:23.483	32522537	31915	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
d1be5e65-b186-4252-94fc-95765455f5f53	\N	\N	2020-08-12 11:21:16.21	16000000	1	PL004	0	0	\N	\N	2020-08-12 11:20:33
fbaf7158-5968-4411-aba8-15517a8eeea93	\N	\N	2020-08-12 11:43:38.253	3400000	1	PL004	0	0	\N	\N	2020-08-12 11:42:55
d3fbf528-2dfd-49e3-83dd-34dcb0e4001d3	\N	\N	2020-08-13 10:20:39.242	32522537	31916	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
c2202f16-2cd2-47d7-87be-a7cc9c09ddcf3	\N	\N	2020-08-12 00:52:50.043	2200000	1	PL004	0	0	\N	\N	2020-08-12 00:52:09
2d8df9b6-ad31-4b5c-bc3f-1074a1128cd83	\N	\N	2020-08-12 08:16:57.884	1400000	1	PL004	0	0	\N	\N	2020-08-12 08:16:15
cc46772d-e1ea-49ed-9432-0b4dc204f5963	\N	\N	2020-08-12 14:39:14.236	60420	31797	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
c128f8e8-5519-4373-898d-f23a6125a4522	\N	\N	2020-08-13 10:51:58.552	1045000	31922	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
7dfbd8ec-d4c2-4e0e-a97c-5b7ada7a873f3	\N	\N	2020-08-12 15:43:58.784	1500000	1	PL004	0	0	\N	\N	2020-08-12 15:43:15
f05ea194-9fd7-4455-9f4b-cbf68af939143	\N	\N	2020-08-12 15:58:30.934	30210	31819	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
3de2bee0-3dad-4b78-8282-6dd21bc7f1fa3	\N	\N	2020-08-12 16:11:19.196	30210	31834	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
6aa7c22e-b1fb-4047-abbd-fa45ed5b7a023	\N	\N	2020-08-12 16:33:39.013	60420	31842	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
f73041c4-52e1-480a-8634-7fcff3e58b533	\N	\N	2020-08-12 16:46:42.005	60420	31852	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
25ab7dc9-2332-4790-a80f-73e1b04107833	\N	\N	2020-08-12 16:55:16.437	29355	31860	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
609d6724-bf89-4019-9974-e9377026880e2	\N	\N	2020-08-12 20:45:40.009	902500	31898	PL001	0	0	\N	2020-08-12 14:21:16	2020-08-12 14:21:16
fadbf713-7af9-4c0a-af44-5fc929a7f9dc2	\N	\N	2020-08-12 22:04:37.99	1045000	31901	PL001	0	0	\N	2020-08-12 14:21:16	2020-08-12 14:21:16
9d96bcbb-b0b0-4c57-afff-e7bb2c71c0022	\N	\N	2020-08-12 22:05:03.5	1045000	31902	PL001	0	0	\N	2020-08-12 14:21:16	2020-08-12 14:21:16
3f05edaa-dff3-49c0-b527-099f4936ad5d2	\N	\N	2020-08-12 22:29:51.158	1045000	31906	PL001	0	0	\N	2020-08-12 14:21:16	2020-08-12 14:21:16
5a647b19-6981-4e16-a398-ec3e45db00b53	\N	\N	2020-08-13 11:06:39.536	63602	31926	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
1dea26d4-1c0d-4368-9a1c-2b1eea1e4ece2	\N	\N	2020-08-13 11:09:26.764	1045000	31928	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
adc33ecb-2c20-414c-b33d-8578e66c3f8e2	\N	\N	2020-08-13 11:10:07.281	950000	31929	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
24f5ec2a-ea35-49b2-808d-943a90e368953	\N	\N	2020-08-13 11:10:23.61	61750	31930	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
ee36e19a-0e5e-4e73-b6e8-c320c4b58dd03	\N	\N	2020-08-13 11:16:04.943	61750	31932	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
3a1d846b-7d01-42e7-a58f-bde2d405533f3	\N	\N	2020-08-13 11:41:35.472	61750	31935	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
54742ee5-2c14-4126-8baf-e016116de3853	\N	\N	2020-08-13 13:19:19.757	61750	31939	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
675ebb61-bdde-4195-be83-365448ab00de2	2020-08-13 12:12:25.064		2020-08-13 12:10:01.53	1045000	31937	PL001	1	0	Menunggu konfirmasi provider	2020-08-12 22:21:17	2020-08-12 22:21:17
2606ccef-6ed1-4dad-8a0f-3cb3209ccec93	\N	\N	2020-08-13 13:19:45.787	61750	31940	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
5ec99d4a-66bd-497b-b1b4-80b335c216e93	\N	\N	2020-08-13 13:50:19.88	16649989	31942	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
51a81e19-9e3b-438d-b8c1-6900106e0ef41	\N	\N	2020-07-22 15:05:56.718	5000000	1	PL000	\N	\N	\N	2020-07-22 08:02:46	2020-07-22 08:02:46
6c9a1930-015a-4c23-b046-71ef385cafd11	\N	\N	2020-07-22 15:06:23.582	5000000	1	PL000	0	0	\N	2020-07-22 08:03:13	2020-07-22 08:03:13
171f75b1-5b54-440e-8237-52f9021d9b041	\N	\N	2020-07-22 15:09:57.032	5000000	1	PL000	0	0	\N	2020-07-22 08:06:47	2020-07-22 08:06:47
c98685bd-074a-461b-aa4c-739ae5c5f4521	\N	\N	2020-07-22 15:06:50.905	5000000	1	PL000	\N	\N	\N	2020-07-22 08:03:40	2020-07-22 08:03:40
944eba67-e2f9-44f6-8c80-9ac30bbe49101	\N	\N	2020-07-22 15:30:47.897	5000000	1	PL000	\N	\N	\N	2020-07-22 08:27:37	2020-07-22 08:27:37
01dde4be-8c3c-4286-b05b-6a522e129e351	\N	\N	2020-07-22 15:11:15.385	5000000	1	PL000	\N	\N	\N	2020-07-22 08:08:04	2020-07-22 08:08:04
a38905bf-d5ed-4fb9-97ca-65e0d577e5b01	\N	\N	2020-07-22 15:07:02.089	5000000	1	PL000	\N	\N	\N	2020-07-22 08:03:52	2020-07-22 08:03:52
dfb001a9-abb5-4f4f-8816-67f75bee59591	\N	\N	2020-08-10 09:07:33.362	272887	31636	PL001	0	0	\N	2020-08-09 22:21:13	2020-08-09 22:21:13
d3a70aff-fbf3-4474-884e-0e48fd9901f32	\N	\N	2020-08-10 11:22:53.305	261250	31648	PL001	0	0	\N	2020-08-09 22:21:13	2020-08-09 22:21:13
223e7945-0b8d-48d5-9c42-f3b5433a8c621	\N	\N	2020-07-22 15:12:01.719	5000000	1	PL000	\N	\N	\N	2020-07-22 08:08:51	2020-07-22 08:08:51
fe908600-552a-4e1e-9669-52fac58a5c382	\N	\N	2020-08-10 16:15:27.868	1638987	31684	PL001	0	0	\N	2020-08-10 06:21:14	2020-08-10 06:21:14
e1c202d0-8e5b-4da9-96f7-767064804dfb3	\N	\N	2020-08-10 20:56:26.855	21470247	31703	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
3849a8f2-b4a8-4b99-881a-4574213afb313	\N	\N	2020-08-10 21:34:18.045	21470247	31709	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
862131e7-557b-4ac4-835c-80e0837d88961	\N	\N	2020-07-22 16:24:59.758	5000000	1	PL000	\N	\N	\N	2020-07-22 09:21:49	2020-07-22 09:21:49
5c9cec4d-4bb7-427c-b654-dd998610e74f1	\N	\N	2020-07-22 15:13:51.668	5000000	1	PL000	\N	\N	\N	2020-07-22 08:10:41	2020-07-22 08:10:41
b73bd59a-73b2-4928-a1a1-f32e87a977f61	\N	\N	2020-07-22 16:47:31.453	5000000	1	PL000	\N	\N	\N	2020-07-22 09:44:21	2020-07-22 09:44:21
9fc6abd8-21a1-4d8d-8ef0-9f3cbc083a771	\N	\N	2020-07-23 10:51:43.186	5000000	1	PL000	0	0	\N	2020-07-23 03:48:25	2020-07-23 03:48:25
c1b5a2f2-7e0e-42fe-84b7-10e87ccabba63	\N	\N	2020-08-12 10:45:29.931	3200000	1	PL004	0	0	\N	\N	2020-08-12 10:44:47
de52e138-307a-48a9-861a-05523552ef081	\N	\N	2020-07-22 16:45:47.263	5000000	1	PL000	\N	\N	\N	2020-07-22 09:42:37	2020-07-22 09:42:37
33b34362-4519-4bd3-a6fc-cfd87e40344b1	2020-07-23 10:52:29.394		2020-07-23 10:52:01.116	5000000	1	PL000	1	1	Menunggu konfirmasi provider	2020-07-23 03:48:50	2020-07-23 03:48:50
4bb000fc-e792-4476-a0d7-a92325268ebf1	\N	\N	2020-07-22 16:46:49.648	5000000	1	PL000	\N	\N	\N	2020-07-22 09:43:39	2020-07-22 09:43:39
2dd4e91f-df8a-4468-a496-657354d545c33	\N	\N	2020-08-10 21:50:41.367	21470247	31716	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
8f8c039e-b673-4257-a123-a689f5a7d3c73	\N	\N	2020-08-12 14:46:11.7	60420	31798	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
e063c4fd-d22c-4428-af52-c9218e0549941	\N	\N	2020-08-04 09:44:39.108	5000000	1	PL000	0	0	\N	2020-08-04 02:41:20	2020-08-04 02:41:20
logolTest2Booking101	\N	\N	2020-08-08 12:28:26.157	4000000	1	PL004	0	0	\N	\N	2020-08-08 12:28:05
239e3f88-dd49-4589-9ab6-461cddc03e043	\N	\N	2020-08-10 21:58:37.353	21470247	31720	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
29e9f09c-58d9-419a-98fb-d2acbd098f8c3	\N	\N	2020-08-10 22:00:09.29	21470247	31722	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
dd7e5b5f-03c3-480b-bef7-b8954cd341c23	\N	\N	2020-08-08 13:44:38.2	444837	31549	PL001	0	1	\N	2020-08-08 06:21:07	2020-08-08 06:21:07
7dfbd8ec-d4c2-4e0e-a97c-5b7ada7a873f2	\N	\N	2020-08-12 15:43:59.115	427500	31808	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
7abe53b3-c6e8-442c-b9af-1550878df6933	\N	\N	2020-08-10 22:03:14.129	21470247	31724	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
1b2c5dc4-8acc-45d7-9d5f-5cc97d7f3cdc3	\N	\N	2020-08-10 22:08:00.216	21470247	31725	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
7ae4ce09-ac66-424f-a0dd-a4f36b06fb972	2020-08-10 21:43:36.646		2020-08-10 21:43:22.478	1638987	31713	PL001	1	1	Menunggu konfirmasi provider	2020-08-10 14:21:14	2020-08-10 14:21:14
3eeeb4e2-499d-479d-ba8a-34064bf9abc92	\N	\N	2020-08-12 16:01:46.026	855000	31823	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
0cd7e1f8-70e9-4900-b415-4c00b7bc6e583	\N	\N	2020-08-09 13:36:23.224	46833456	31575	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
a747677c-4077-4727-9853-95c44e56c5233	\N	\N	2020-08-09 13:38:28.946	46833456	31576	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
3408cf42-c1c6-4b5b-970e-3677d301c4b43	\N	\N	2020-08-09 13:42:53.568	46833456	31578	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
f304d22b-26ed-4c8e-8ece-2ab8b14d83413	\N	\N	2020-08-09 13:47:36.567	46833456	31582	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
6a6aecec-0eb3-4bf8-a1f8-e0fa786f144b3	\N	\N	2020-08-09 15:27:00.928	46833456	31602	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
6ef1be6a-b9b4-4b4b-b8a2-b525eb58d9c13	\N	\N	2020-08-09 15:27:06.525	46833456	31603	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
4c7d5c0f-5424-40d8-b6f0-4c005446846a3	\N	\N	2020-08-09 16:25:40.179	46833456	31616	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
bf4b74b5-96ff-4907-a842-1a5ae8acc06c3	\N	\N	2020-08-11 11:11:29.638	3197320	31734	PL001	0	0	\N	2020-08-10 22:21:14	2020-08-10 22:21:14
4d693fca-bd52-4aca-baea-f5578b6d6f762	2020-08-09 20:12:33.073		2020-08-09 20:12:18.52	2280000	31632	PL001	1	0	Menunggu konfirmasi provider	2020-08-09 06:21:10	2020-08-09 06:21:10
bfefd7de-9a0d-4ac5-a828-f16af511b01a3	\N	\N	2020-08-11 14:55:25.352	4000000	1	PL004	0	0	\N	\N	2020-08-11 14:54:47
7b60cd9f-bb76-4469-a0f0-2bd090349edd2	\N	\N	2020-08-11 15:13:35.588	261250	31750	PL001	0	0	\N	2020-08-11 06:21:15	2020-08-11 06:21:15
928e6589-83f2-4a8f-8a48-dcf932d6cb3c3	\N	\N	2020-08-12 16:36:37.452	60420	31845	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
324bff81-6a51-4411-83ca-b7a358904ceb2	\N	\N	2020-08-13 10:04:04.348	905112	31909	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
4178de6b-a02f-4129-8174-fa372fefa7dd2	\N	\N	2020-08-12 09:32:28.907	546250	31773	PL001	0	0	\N	2020-08-11 22:21:16	2020-08-11 22:21:16
099565f0-3dfa-442b-b81a-695eba3d6a9a2	\N	\N	2020-08-12 10:28:16.354	213750	31776	PL001	0	0	\N	2020-08-11 22:21:16	2020-08-11 22:21:16
c2202f16-2cd2-47d7-87be-a7cc9c09ddcf2	2020-08-12 00:54:07.955		2020-08-12 00:52:50.101	386008750	31769	PL001	1	1	Menunggu konfirmasi provider	2020-08-11 14:21:15	2020-08-11 14:21:15
6f8f55fe-9b97-42d4-90f1-58c47e0055e83	\N	\N	2020-08-12 17:22:34.04	30210	31888	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
fbaf7158-5968-4411-aba8-15517a8eeea92	2020-08-12 11:44:26.916		2020-08-12 11:43:38.513	1757500	31782	PL001	1	0	Menunggu konfirmasi provider	2020-08-11 22:21:16	2020-08-11 22:21:16
e04cf392-6415-4410-8229-fd1e47189b1a2	\N	\N	2020-08-12 20:46:34.96	902500	31899	PL001	0	0	\N	2020-08-12 14:21:16	2020-08-12 14:21:16
u38929adsndjk29akeas8w	2020-07-16 09:24:22.355		2020-07-03 08:29:51.32	5000000	1	PL001	1	1	accepted	2019-09-08 14:00:00	2019-10-07 14:00:00
ca293462-4373-4cc3-98ac-c4a4f9aa9fa92	\N	\N	2020-08-13 10:07:31.075	427500	31910	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
e04cf392-6415-4410-8229-fd1e47189b1a3	2020-08-12 20:47:47.281		2020-08-12 20:46:37.544	200000	1	PL002	1	0	Menunggu konfirmasi provider	2020-08-12 13:46:37	2020-08-12 13:46:37
5ad42164-30e0-477e-bca0-53df257c85772	\N	\N	2020-08-12 22:25:07.217	1045000	31903	PL001	0	0	\N	2020-08-12 14:21:16	2020-08-12 14:21:16
8311f5c9-e80f-4888-bdb7-1ed85dfd889a2	\N	\N	2020-08-12 22:26:14.467	1045000	31904	PL001	0	0	\N	2020-08-12 14:21:16	2020-08-12 14:21:16
630969ec-d774-4a38-870d-a9bd1cbb41802	\N	\N	2020-08-13 10:11:18.216	878750	31911	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
5a7693cb-af7f-4243-aa21-d02bb2448f372	\N	\N	2020-08-13 10:21:51.52	131765000	31918	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
cd224b4c-54e7-4f6a-94dd-d7163cb6a67f2	\N	\N	2020-08-13 10:36:50.331	1710000	31920	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
fd08da2d-ce43-4d99-92f7-153b732b79302	\N	\N	2020-08-13 10:53:00.666	1045000	31923	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
3d384c0c-574a-4c00-a918-9e76e59fad8c2	\N	\N	2020-08-13 10:56:34.878	1045000	31924	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
f5050710-1831-4b6a-a6ec-73b36f9e3bd13	\N	\N	2020-08-13 11:08:41.411	61750	31927	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
c53cebcd-926d-467a-afcf-eafc280378aa3	\N	\N	2020-08-11 15:59:31.611	20844900	31757	PL001	0	0	\N	2020-08-11 06:21:15	2020-08-11 06:21:15
0386facd-cff2-4a57-8da2-0900f283d93e1	\N	\N	2020-07-22 15:11:43.763	5000000	1	PL000	\N	\N	\N	2020-07-22 08:08:33	2020-07-22 08:08:33
bd463735-360b-4174-a281-4922244ca97b3	\N	\N	2020-08-11 16:00:19.221	1509075	31758	PL001	0	0	\N	2020-08-11 06:21:15	2020-08-11 06:21:15
f6d04a8c-796b-4396-9ed0-63de03e096ef1	\N	\N	2020-07-22 16:14:35.457	5000000	1	PL000	\N	\N	\N	2020-07-22 09:11:16	2020-07-22 09:11:16
90394be6-6492-4966-88d9-02ff0556ddab1	2020-07-23 13:09:58.866		2020-07-23 13:09:23.58	5000000	1	PL000	1	1	Menunggu konfirmasi provider	2020-07-23 06:06:11	2020-07-23 06:06:11
d4e6129b-407c-4a24-8eca-b21f4f9c9dae1	\N	\N	2020-08-04 08:27:07.634	5000000	1	PL000	0	0	\N	2020-08-04 01:23:50	2020-08-04 01:23:50
fa909fc7-baef-47c1-b591-e8361ed119111	\N	\N	2020-08-05 09:21:46.011	5000000	1	PL000	0	0	\N	2020-08-05 02:18:22	2020-08-05 02:18:22
77ef7ff4-00ff-4df3-935a-398ff8490d321	\N	\N	2020-08-05 09:24:12.539	5000000	1	PL000	0	0	\N	2020-08-05 02:20:53	2020-08-05 02:20:53
25a2f0d3-e746-45ff-bf87-62e47f0b2db01	\N	\N	2020-07-22 15:16:04.205	5000000	1	PL000	\N	\N	\N	2020-07-22 08:12:54	2020-07-22 08:12:54
logolTest1Booking10	\N	\N	2020-08-08 13:33:15.47	500000000	1	PL004	0	0	\N	2019-09-08 14:00:00	2019-10-07 14:00:00
39347b0a-5cb7-4ab7-b8c7-6150de9cafe63	\N	\N	2020-08-11 20:36:45.61	60420	31764	PL001	0	0	\N	2020-08-11 14:21:15	2020-08-11 14:21:15
17f5527a-84df-427c-be62-811560b4af3e3	\N	\N	2020-08-12 15:02:47.05	60420	31801	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
f9736882-27df-44dc-8f80-21a5b60c71731	\N	\N	2020-08-09 12:08:06.581	2342462	31565	PL001	0	0	\N	2020-08-08 22:21:09	2020-08-08 22:21:09
23a4c9fa-4309-4fb5-88f4-c6c1b254bca51	\N	\N	2020-07-22 16:46:17.87	5000000	1	PL000	\N	\N	\N	2020-07-22 09:43:07	2020-07-22 09:43:07
f83a151a-c29b-456c-b989-6082f48cd98e1	\N	\N	2020-08-09 12:08:23.807	2342462	31566	PL001	0	0	\N	2020-08-08 22:21:09	2020-08-08 22:21:09
b4498807-e985-48be-8b09-7e7c7f1e7a463	\N	\N	2020-08-09 15:12:24.198	46833456	31591	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
0d924925-690c-4f25-84a8-5a0c97f805e63	\N	\N	2020-08-09 15:13:44.699	46833456	31592	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
e3db3f9d-2a87-441f-add3-794e47e933bd3	\N	\N	2020-08-09 15:15:09.496	46833456	31593	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
dbd38770-e3d6-49f7-bdfa-e85efdbf4da93	\N	\N	2020-08-09 15:18:12.509	46833456	31594	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
bfaa4cb2-6963-4fbe-9b50-f103f1600a403	\N	\N	2020-08-09 15:19:38.283	46833456	31595	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
f3b73add-5dc5-4e37-8990-8db2bd8e90763	\N	\N	2020-08-09 15:21:30.722	46833456	31596	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
dcd95b5b-a597-4896-b415-6f96f9bb2b5b3	\N	\N	2020-08-09 15:23:21.492	46833456	31597	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
b747e408-0bd2-46fe-9355-d141ad3f82d03	\N	\N	2020-08-09 15:24:18.328	46833456	31598	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
8252fb3e-2b43-4b5c-9260-5f5131ec23b53	\N	\N	2020-08-09 15:29:22.951	46833456	31604	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
9eb68147-0934-46d0-8bc5-01bebf80f9c73	\N	\N	2020-08-09 15:30:22	46833456	31605	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
1985714d-4923-454c-a2df-00fc9a2647563	\N	\N	2020-08-09 15:40:31.405	46833456	31606	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
dc10c865-ed63-41bf-815d-aee16370ce113	\N	\N	2020-08-09 15:42:40.677	46833456	31607	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
96c68007-9c1a-4f41-a6dc-8fbb878544853	\N	\N	2020-08-09 16:30:09.752	46833456	31617	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
5a71da19-8ceb-4bec-9391-a4633ae114eb3	\N	\N	2020-08-09 16:30:32.743	46833456	31618	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
cb9c74b3-7a01-49ee-9d02-d1cdde8d4e4b3	\N	\N	2020-08-09 16:31:18.764	46833456	31619	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
aa6648f7-7d12-402d-a4db-62950855dcae3	\N	\N	2020-08-09 16:37:11.288	46833456	31620	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
fcc51fb3-8a35-4353-9b18-59164b8dbef93	\N	\N	2020-08-09 16:41:01.945	46833456	31621	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
f76a7211-284d-4302-be8c-b1f5b0ceee793	\N	\N	2020-08-09 16:41:14.007	46833456	31622	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
4806202c-1d73-4ba7-842e-72454c14589d3	\N	\N	2020-08-09 16:42:10.534	46833456	31623	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
04881a34-9b84-4dac-a240-a7c65d3aa15d3	\N	\N	2020-08-09 16:42:53.489	46833456	31624	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
2ad84504-558b-4774-b921-1d1b32d982fa3	\N	\N	2020-08-09 16:48:39.104	46833456	31625	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
be3a188e-9309-44c8-98fa-cf6a6480a16e3	\N	\N	2020-08-09 16:49:54.662	46833456	31626	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
debeec10-2b9f-4f75-92a6-59404c83bac62	\N	\N	2020-08-09 20:22:14.414	1235000	31633	PL001	0	0	\N	2020-08-09 14:21:11	2020-08-09 14:21:11
aaf22fd8-b72a-46c4-a3d7-73e2affa38ec2	\N	\N	2020-08-10 09:33:37.348	166250	31637	PL001	0	0	\N	2020-08-09 22:21:13	2020-08-09 22:21:13
93277b57-3fdc-4c18-bf58-ec96d8288b341	\N	\N	2020-08-10 12:15:38.697	267900	31653	PL001	0	0	\N	2020-08-09 22:21:13	2020-08-09 22:21:13
fe908600-552a-4e1e-9669-52fac58a5c383	\N	\N	2020-08-10 16:15:28.72	1800000	1	PL004	0	0	\N	\N	2020-08-10 16:14:54
d8aac32c-a820-4481-a4d2-5f3daf2b8b223	\N	\N	2020-08-10 19:48:40.326	21470247	31696	PL001	0	0	\N	2020-08-10 06:21:14	2020-08-10 06:21:14
4b05dd2d-0bda-4fde-a2c8-279a1e01130c3	\N	\N	2020-08-10 19:49:22.051	21470247	31698	PL001	0	0	\N	2020-08-10 06:21:14	2020-08-10 06:21:14
aa50a645-cdc7-4c8f-b115-266dc94931283	\N	\N	2020-08-10 20:59:45.381	21470247	31704	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
ccaa217a-d399-47d2-a78d-a21f299376bd3	\N	\N	2020-08-10 21:36:14.632	21470247	31710	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
bcc505c6-63ff-46c8-9ba5-0308681323373	\N	\N	2020-08-10 21:37:12.92	21470247	31711	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
304a8cc7-d641-4405-86a7-ae58c07c4d9b3	\N	\N	2020-08-10 22:23:45.721	21470247	31726	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
3bd70d88-11a7-4ff6-9ad6-dd102b5668b23	\N	\N	2020-08-11 23:07:14.302	8800000	1	PL004	0	0	\N	\N	2020-08-11 23:06:34
87efdc2a-88cb-42cb-91cc-7f235234f3ba3	\N	\N	2020-08-12 11:21:58.797	6000000	1	PL004	0	0	\N	\N	2020-08-12 11:21:16
967e2b1e-6b85-43a4-a876-1654472b99b53	\N	\N	2020-08-11 11:17:18.394	6394640	31735	PL001	0	0	\N	2020-08-10 22:21:14	2020-08-10 22:21:14
bfefd7de-9a0d-4ac5-a828-f16af511b01a2	\N	\N	2020-08-11 14:55:25.467	261250	31748	PL001	0	0	\N	2020-08-11 06:21:15	2020-08-11 06:21:15
c43eb4a0-a163-4c4d-a649-d0146dbd545e2	2020-08-12 01:08:58.941		2020-08-12 01:07:06.273	772017500	31770	PL001	1	1	Menunggu konfirmasi provider	2020-08-11 14:21:15	2020-08-11 14:21:15
3bd70d88-11a7-4ff6-9ad6-dd102b5668b22	2020-08-11 23:08:03.911		2020-08-11 23:07:14.333	1544035000	31767	PL001	1	0	Menunggu konfirmasi provider	2020-08-11 14:21:15	2020-08-11 14:21:15
c43eb4a0-a163-4c4d-a649-d0146dbd545e3	\N	\N	2020-08-12 01:07:05.91	4400000	1	PL004	0	0	\N	\N	2020-08-12 01:06:25
64bb8e57-bae7-42d5-bee0-58b1d9082f8c3	\N	\N	2020-08-12 09:50:09.003	1300000	1	PL004	0	0	\N	\N	2020-08-12 09:49:26
9e4c66ce-da48-4796-89e0-32746ffed9c03	\N	\N	2020-08-12 11:44:48.382	31920	31783	PL001	0	0	\N	2020-08-11 22:21:16	2020-08-11 22:21:16
c1b5a2f2-7e0e-42fe-84b7-10e87ccabba62	\N	\N	2020-08-12 10:45:29.646	1615000	31777	PL001	0	0	\N	2020-08-11 22:21:16	2020-08-11 22:21:16
1d3c83d1-f077-4e9d-8a16-bf4982be70ae3	\N	\N	2020-08-12 15:02:16.524	60420	31799	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
c4749633-3e4e-47d8-be1c-852f6d6324a73	\N	\N	2020-08-12 15:02:30.44	60420	31800	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
22e68972-5134-48f8-9349-b0490fc3e2903	\N	\N	2020-08-12 15:04:08.005	30210	31802	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
502b3042-bac4-4dcb-bbae-1c284c6336953	\N	\N	2020-08-12 15:04:49.238	30210	31804	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
a39f8e96-f1c4-42e8-9c6e-fd6f201889a63	\N	\N	2020-08-12 15:07:35.58	30210	31805	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
e525b01c-edd1-464d-9f68-3535cd4f16643	\N	\N	2020-08-12 15:54:59.864	30210	31813	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
66d6d20c-a63e-46fd-b440-748da42ce93c2	\N	\N	2020-08-12 16:04:50.075	47500	31826	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
66d6d20c-a63e-46fd-b440-748da42ce93c3	\N	\N	2020-08-12 16:04:50.228	1000000	1	PL004	0	0	\N	\N	2020-08-12 16:04:06
1b781de5-5534-49b4-b69d-4672beca3cf41	\N	\N	2020-07-22 15:07:29.326	5000000	1	PL000	\N	\N	\N	2020-07-22 08:04:19	2020-07-22 08:04:19
5e86b590-0b21-4a88-af58-cad9ca13bf3d1	\N	\N	2020-07-22 15:15:11.953	5000000	1	PL000	\N	\N	\N	2020-07-22 08:12:01	2020-07-22 08:12:01
64bb8e57-bae7-42d5-bee0-58b1d9082f8c2	\N	\N	2020-08-12 09:50:09.246	1615000	31774	PL001	0	0	\N	2020-08-11 22:21:16	2020-08-11 22:21:16
05f04fe5-46db-450d-a518-ad76f206c7a72	\N	\N	2020-08-12 10:49:18.269	380000	31778	PL001	0	0	\N	2020-08-11 22:21:16	2020-08-11 22:21:16
87efdc2a-88cb-42cb-91cc-7f235234f3ba2	\N	\N	2020-08-12 11:21:58.83	665000	31781	PL001	0	0	\N	2020-08-11 22:21:16	2020-08-11 22:21:16
94e8138e-9f61-4ed5-af53-991b8e7faa3c1	\N	\N	2020-07-22 15:28:09.709	5000000	1	PL000	\N	\N	\N	2020-07-22 08:24:59	2020-07-22 08:24:59
8533a52e-fb3e-4350-a56b-790c472da3be3	\N	\N	2020-08-12 13:11:42.747	31920	31795	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
cb2e0d5b-a578-4be6-9882-d843f07a43163	\N	\N	2020-08-12 13:19:32.255	31920	31796	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
f4d33f7f-eb52-4222-8334-fe2d335727221	\N	\N	2020-07-22 16:50:17.283	5000000	1	PL000	\N	\N	\N	2020-07-22 09:47:07	2020-07-22 09:47:07
51ae2a45-e733-4dd8-b061-75d6d190f79d1	\N	\N	2020-07-23 14:37:19.832	5000000	1	PL000	0	0	\N	2020-07-23 07:34:06	2020-07-23 07:34:06
2b5de79a-1a4b-49a5-a020-c1a7414663ce1	\N	\N	2020-08-04 08:29:05.965	5000000	1	PL000	0	0	\N	2020-08-04 01:25:48	2020-08-04 01:25:48
9d16f90b-0dd2-4cf2-8b0c-3443bf1c37611	\N	\N	2020-08-06 10:48:41.026	5000000	1	PL000	0	0	\N	2019-09-08 14:00:00	2019-10-07 14:00:00
06074d4f-623b-458b-acc1-9b2d86fbb9233	\N	\N	2020-08-12 15:04:32.001	30210	31803	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
db92b31d-f4bc-491c-99ce-c7a3c77b9a463	\N	\N	2020-08-12 15:57:21.627	30210	31817	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
84271bdb-5f44-41a1-8991-2142d049414d3	\N	\N	2020-08-12 16:28:00.323	60420	31841	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
56638e9f-2f11-42ee-a75f-88115500a0903	\N	\N	2020-08-09 13:28:26.319	46833456	31572	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
f553423a-81fa-4530-8695-2c7efc54dcf73	\N	\N	2020-08-09 15:25:15.201	46833456	31599	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
41cae28e-a429-42c9-b59a-1cb8fd5746903	\N	\N	2020-08-09 15:25:21.311	46833456	31600	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
24402bb6-5b54-42c3-a3fb-8baa0069ac383	\N	\N	2020-08-09 16:15:38.692	46833456	31608	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
a47de61f-a4b3-45f7-bdbd-35bed40a2bb93	\N	\N	2020-08-09 16:17:38.236	46833456	31609	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
d2563483-dae8-49c7-ae97-a8f3d636cdf13	\N	\N	2020-08-09 16:18:28.274	46833456	31610	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
94507e4e-1200-442c-8ac1-5f5caa619ba03	\N	\N	2020-08-09 16:18:53.953	46833456	31611	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
e161a4bc-d584-4e48-a124-71e2e1cd6db63	\N	\N	2020-08-09 16:22:12.671	46833456	31612	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
c376b99d-42fb-4d0e-a278-db74a8332aec3	\N	\N	2020-08-09 16:23:58.768	46833456	31613	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
469e76d8-456f-42f7-8086-f9b30d1ebadb3	\N	\N	2020-08-09 16:55:24.027	46833456	31627	PL001	0	0	\N	2020-08-09 06:21:10	2020-08-09 06:21:10
b4c7b959-182a-4635-872f-4f1cd84c0ce72	\N	\N	2020-08-09 21:25:28.295	380000	31634	PL001	0	0	\N	2020-08-09 14:21:11	2020-08-09 14:21:11
d1ca6c91-6434-48b8-968a-6d937c0903002	\N	\N	2020-08-10 09:50:00.446	261250	31638	PL001	0	0	\N	2020-08-09 22:21:13	2020-08-09 22:21:13
149603f2-19ae-4c19-8d5b-da6acce03bcd3	\N	\N	2020-08-10 13:47:40.801	4000000	1	PL004	0	0	\N	\N	2020-08-10 13:47:08
04b40949-9316-41ca-a83b-910f56d5e7ad3	\N	\N	2020-08-10 17:35:44.196	21470247	31692	PL001	0	0	\N	2020-08-10 06:21:14	2020-08-10 06:21:14
5f94f87c-e765-4bea-b1e7-86e906c4c4e03	\N	\N	2020-08-10 17:39:29.483	21470247	31693	PL001	0	0	\N	2020-08-10 06:21:14	2020-08-10 06:21:14
68efd02f-4fa9-4acd-966e-b7de7c3a04ac3	\N	\N	2020-08-10 19:48:40.634	21470247	31697	PL001	0	0	\N	2020-08-10 06:21:14	2020-08-10 06:21:14
3b67fbad-f1f0-4e08-a5d1-fd33fb2966e33	\N	\N	2020-08-10 21:30:58.642	21470247	31705	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
6cd3b31e-1b2d-4909-93c6-fa76462b243d3	\N	\N	2020-08-10 21:31:28.32	21470247	31706	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
8fab43d6-d4f7-4e2f-b041-1ffbed0421dd3	\N	\N	2020-08-10 21:34:02.338	21470247	31708	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
51c9d4f8-833a-4b87-8dd9-976657ec9bc83	\N	\N	2020-08-10 21:42:05.851	21470247	31712	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
7ae4ce09-ac66-424f-a0dd-a4f36b06fb973	\N	\N	2020-08-10 21:43:22.537	4000000	1	PL004	0	0	\N	\N	2020-08-10 21:42:48
888ef37f-ade8-42a9-b208-b84eb24b17003	\N	\N	2020-08-10 21:48:19.296	21470247	31715	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
9785ec94-14df-4056-9aff-5b6e17fff1ea3	\N	\N	2020-08-10 21:53:55.569	21470247	31718	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
a8029e57-888c-4c32-bfc6-96e9b1f4f8f53	\N	\N	2020-08-10 22:26:30.948	21470247	31727	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
c818d18a-551c-48ce-9865-a3afc202b54c3	\N	\N	2020-08-10 22:27:05.626	21470247	31728	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
350898ac-e770-40e8-bb7b-16549a9d7b333	\N	\N	2020-08-10 22:27:29.524	21470247	31729	PL001	0	0	\N	2020-08-10 14:21:14	2020-08-10 14:21:14
8dce64c0-1c56-478e-8e85-6f302c0943593	\N	\N	2020-08-12 16:35:16.821	60420	31844	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
b3b45d95-d498-41e5-8a59-7f8dac49eaa02	2020-08-12 00:46:52.471		2020-08-12 00:45:59.064	386008750	31768	PL001	1	1	Menunggu konfirmasi provider	2020-08-11 14:21:15	2020-08-11 14:21:15
728d7421-fe97-4d42-8794-0bd8848f217c3	\N	\N	2020-08-13 10:21:51.869	27787	31919	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
1de3ae9e-8f8c-4c26-b198-031c04760b6e1	\N	\N	2020-08-11 13:57:26.357	272887	31737	PL001	0	0	\N	2020-08-11 06:21:15	2020-08-11 06:21:15
e0324ebe-959a-4507-aa3f-b3259b54a2d53	\N	\N	2020-08-11 15:12:08.421	4000000	1	PL004	0	0	\N	\N	2020-08-11 15:11:30
73074801-2a98-470f-99fd-9ee43379d7c73	\N	\N	2020-08-11 16:02:03.958	1509075	31759	PL001	0	0	\N	2020-08-11 06:21:15	2020-08-11 06:21:15
51506b6a-ab3c-41d3-940b-ba5c5ae7bb913	\N	\N	2020-08-11 21:13:28.18	60420	31765	PL001	0	0	\N	2020-08-11 14:21:15	2020-08-11 14:21:15
3299b656-2ca5-483f-89a7-1feeb567ae593	\N	\N	2020-08-12 16:52:29.542	60420	31857	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
6bc3fd96-267c-46f7-953d-85b068c3d6292	\N	\N	2020-08-13 10:50:58.313	1045000	31921	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
946d6c34-b9d1-49f7-a6be-6ebc2248fe413	\N	\N	2020-08-12 16:58:09.858	29355	31863	PL001	0	0	\N	2020-08-12 06:21:16	2020-08-12 06:21:16
d78b2959-b4ee-43b2-b040-d51e200316bc2	\N	\N	2020-08-12 20:23:47.045	356250	31897	PL001	0	0	\N	2020-08-12 14:21:16	2020-08-12 14:21:16
130271c3-e6b8-4b5d-b7cc-22e112030afa2	\N	\N	2020-08-12 21:31:39.635	427500	31900	PL001	0	0	\N	2020-08-12 14:21:16	2020-08-12 14:21:16
9c50f350-9086-4c0a-bf8d-49d3e0ef9d022	\N	\N	2020-08-12 22:26:58.098	1045000	31905	PL001	0	0	\N	2020-08-12 14:21:16	2020-08-12 14:21:16
ce9fe201-df10-494e-97e7-9e03e5d1df683	\N	\N	2020-08-13 10:16:35.788	33498213	31912	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
f5916ee3-92ef-444c-9796-1fd14c35ba953	\N	\N	2020-08-13 10:17:07.859	32522537	31914	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
1502c5b6-d8d6-43fe-b99e-979e67f819ed3	\N	\N	2020-08-13 10:21:22.161	32522537	31917	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
393deb31-8169-4cf2-8ba9-ec90f20dcdc12	\N	\N	2020-08-13 11:02:08.793	1045000	31925	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
e6be14e2-ead1-4b9a-8a25-b0140fbb2afd3	\N	\N	2020-08-13 11:13:48.172	61750	31931	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
3bb5f3cf-f3df-4bc1-b885-3bcd03f113502	\N	\N	2020-08-13 11:25:07.403	1805000	31933	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
c9f35733-4f19-438f-8a6c-988f196aa9663	\N	\N	2020-08-13 11:27:16.546	61750	31934	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
1da70f64-5b87-415b-9fe4-bdbd54b1b0ed3	\N	\N	2020-08-13 11:43:39.416	61750	31936	PL001	0	0	\N	2020-08-12 22:21:17	2020-08-12 22:21:17
13a77a62-81c5-4aef-a0cc-f9fe4da444132	2020-08-13 12:58:29.661		2020-08-13 12:57:29.076	2256250	31938	PL001	1	1	Menunggu konfirmasi provider	2020-08-13 06:21:17	2020-08-13 06:21:17
77bc2a83-6c59-4368-89e3-a07414b247493	\N	\N	2020-08-13 13:49:43.73	16649989	31941	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
358f08e0-304b-4ab1-bba9-8266140dd3ee3	\N	\N	2020-08-13 13:53:57.805	30210	31943	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
31db3546-d72b-4b12-8f06-228fdf5787ab2	\N	\N	2020-08-13 14:17:50.54	1092500	31944	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
c41d9cfe-d43b-4684-8e47-f024cf38745a2	\N	\N	2020-08-13 14:18:35.594	1092500	31945	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
5d4d1702-cea0-48a2-806a-e49dece6fcc22	\N	\N	2020-08-13 14:18:40.69	1092500	31946	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
b1ee4420-1ec8-4632-bc23-f7884f4a01552	\N	\N	2020-08-13 14:34:40.916	4750000	31947	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
376fc438-db01-4a74-a8df-f5bdb475db123	\N	\N	2020-08-14 10:22:28.803	164644	32003	PL001	0	0	\N	2020-08-13 22:21:18	2020-08-13 22:21:18
1f5e6e70-d88d-4c77-8aaf-7fcf969f0f892	2020-08-13 14:36:13.224	test	2020-08-13 14:35:52.793	4750000	31948	PL001	1	0	Menunggu konfirmasi provider	2020-08-13 06:21:17	2020-08-13 06:21:17
376fc438-db01-4a74-a8df-f5bdb475db122	\N	\N	2020-08-14 10:22:30.771	200000	1	PL002	0	0	\N	2020-08-14 03:22:30	2020-08-14 03:22:30
11731f42-a238-4d5a-98e4-ea2bce56a4512	2020-08-13 14:39:46.378		2020-08-13 14:38:53.63	3610000	31949	PL001	1	0	Menunggu konfirmasi provider	2020-08-13 06:21:17	2020-08-13 06:21:17
17a88261-c390-4efa-bf6f-2148a24cb78e3	\N	\N	2020-08-14 10:26:09.664	164644	32004	PL001	0	0	\N	2020-08-13 22:21:18	2020-08-13 22:21:18
2b8e75a5-7fdd-461b-96df-ecf9236f7b7e2	2020-08-13 14:57:06.615		2020-08-13 14:56:39.931	3847500	31950	PL001	1	0	Menunggu konfirmasi provider	2020-08-13 06:21:17	2020-08-13 06:21:17
7bcc39e9-8c35-4c07-b5f5-fa4d6156cb173	\N	\N	2020-08-13 15:59:08.708	44650	31962	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
a7cb03a4-6df2-40b7-aaff-2bbddefcf9f53	\N	\N	2020-08-13 15:59:51.113	44650	31963	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
2cf2ca2c-86a5-482a-b719-2fa14745463b3	\N	\N	2020-08-13 16:00:44.99	44650	31964	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
c51b0744-5bdf-4367-8059-f4f7179c1b553	\N	\N	2020-08-13 16:01:29.897	44650	31965	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
9e56be05-2e0c-4295-ad36-102e02ea1f0a3	\N	\N	2020-08-13 16:02:13.103	44650	31966	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
f118a576-b1e3-4b2d-9837-a5273fd5a1e33	\N	\N	2020-08-13 16:02:49.75	44650	31967	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
6945177c-21e0-4aa1-a595-e4937364580d3	\N	\N	2020-08-13 16:12:17.17	15707537	31968	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
ca673fd8-e9bf-4fe0-b36e-07fb323083e93	\N	\N	2020-08-13 16:12:47.833	9585262	31969	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
a8ee1c44-c516-4df1-a972-336050450a113	\N	\N	2020-08-13 16:13:37.723	44650	31970	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
5c62f97a-e63f-4403-b302-3c0cb56283f73	\N	\N	2020-08-13 16:34:20.485	63840	31971	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
eeb5c761-1f78-4fe5-acb8-be00b51bd9c83	\N	\N	2020-08-13 17:31:43.252	50008	31972	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
bf729d59-045c-42e4-9132-def645d11a7a3	\N	\N	2020-08-13 17:32:15.997	50008	31973	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
802f2517-d155-4314-8649-213d660d3ff83	\N	\N	2020-08-13 20:10:23.118	30210	31974	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
f9f5939d-a6fa-49b9-a7ac-d307204418b93	\N	\N	2020-08-13 20:11:17.332	30210	31975	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
4001752e-c2d0-4014-98c0-e5e380bbcead3	\N	\N	2020-08-13 20:13:16.635	30210	31976	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
aa87edce-f827-41d2-bbe6-46653d4c355d3	\N	\N	2020-08-13 20:16:54.128	30210	31977	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
07fe62f9-642f-4ec4-9037-f4f3bcc1ee303	\N	\N	2020-08-13 20:17:19.909	30210	31978	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
fca9a802-f8b5-4409-a1ab-916a5cfb26193	\N	\N	2020-08-13 20:18:39.509	30210	31979	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
8c5d9ded-5bc7-4665-9d4f-2e63ec975c273	\N	\N	2020-08-13 20:19:05.525	30210	31980	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
1a03e7d0-6a76-49c5-b483-aa1eb1cee1043	\N	\N	2020-08-13 20:19:33.922	30210	31981	PL001	0	0	\N	2020-08-13 06:21:17	2020-08-13 06:21:17
a46a3dca-7948-421e-9193-7392db3d4fee3	\N	\N	2020-08-13 20:20:51.852	30210	31982	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
4e46e328-d1bc-41c5-88bd-4ab4ecbca7fc3	\N	\N	2020-08-13 20:22:03.844	30210	31983	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
c9214503-bada-4a24-ab1c-0e63c05296f43	\N	\N	2020-08-13 20:23:14.593	30210	31984	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
dbae5cec-9eb9-413a-8fe1-70ac482b24c73	\N	\N	2020-08-13 20:23:18.655	30210	31985	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
a00d0a6e-89b1-4fd6-9a0a-2508583656043	\N	\N	2020-08-13 20:24:14.456	30210	31986	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
e4f6d78c-6d48-436b-a387-6406950aee9b2	\N	\N	2020-08-13 20:34:40.794	176795000	31987	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
03a1f255-8904-448c-a71b-aa588b3211582	\N	\N	2020-08-13 20:38:32.255	176795000	31988	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
58299ef2-5dad-4f03-bd1c-c685280369de3	\N	\N	2020-08-13 20:48:29.456	30210	31989	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
13eec555-5a30-4bed-ad39-568e3d05173b3	\N	\N	2020-08-13 20:49:47.721	30210	31990	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
28303db8-f0ae-4e91-941d-6dfbfc5e2dfe3	\N	\N	2020-08-13 20:49:58.687	30210	31991	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
2c88c936-2807-44ef-b376-8adbda70ab0a3	\N	\N	2020-08-13 22:02:07.627	30210	31992	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
9b487a97-1992-4e78-bffc-8ab8279632833	\N	\N	2020-08-13 22:02:53.358	30210	31993	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
0e139ddf-5281-446c-ad83-67d323e35b233	\N	\N	2020-08-13 22:03:42.259	30210	31994	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
efd886c0-005d-4811-bff5-07f8215c9c233	\N	\N	2020-08-13 22:14:09.502	30210	31995	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
fe3719e1-080b-4bd8-a872-11a853d593ff3	\N	\N	2020-08-13 22:14:38.255	30210	31996	PL001	0	0	\N	2020-08-13 14:21:18	2020-08-13 14:21:18
a3efbc31-a9d2-4a3d-8328-9ae89d0460992	\N	\N	2020-08-14 07:41:17.033	308750	31997	PL001	0	0	\N	2020-08-13 22:21:18	2020-08-13 22:21:18
d6791664-e59a-45e5-b5ee-5a1f5eb6b5112	\N	\N	2020-08-14 10:01:51.976	1045000	31998	PL001	0	0	\N	2020-08-13 22:21:18	2020-08-13 22:21:18
d6791664-e59a-45e5-b5ee-5a1f5eb6b5113	\N	\N	2020-08-14 10:01:57.885	200000	1	PL002	0	0	\N	2020-08-14 03:01:57	2020-08-14 03:01:57
b3c1ee6f-0db6-4d44-a177-ecbd60650cbf3	\N	\N	2020-08-14 10:13:49.11	164644	31999	PL001	0	0	\N	2020-08-13 22:21:18	2020-08-13 22:21:18
b3c1ee6f-0db6-4d44-a177-ecbd60650cbf2	\N	\N	2020-08-14 10:13:51.308	200000	1	PL002	0	0	\N	2020-08-14 03:13:51	2020-08-14 03:13:51
a1a6b198-1cbb-4ca0-8a01-532acd4b97473	\N	\N	2020-08-14 10:16:40.181	164644	32000	PL001	0	0	\N	2020-08-13 22:21:18	2020-08-13 22:21:18
a1a6b198-1cbb-4ca0-8a01-532acd4b97472	\N	\N	2020-08-14 10:16:41.708	200000	1	PL002	0	0	\N	2020-08-14 03:16:41	2020-08-14 03:16:41
368cd932-6943-4e04-b2be-f3ca5e06bffe3	\N	\N	2020-08-14 10:17:58.903	164644	32001	PL001	0	0	\N	2020-08-13 22:21:18	2020-08-13 22:21:18
368cd932-6943-4e04-b2be-f3ca5e06bffe2	\N	\N	2020-08-14 10:18:00.716	200000	1	PL002	0	0	\N	2020-08-14 03:18:00	2020-08-14 03:18:00
a52582cb-9b3b-43e4-a2dd-c6795d575a6b3	\N	\N	2020-08-14 10:19:49.852	164644	32002	PL001	0	0	\N	2020-08-13 22:21:18	2020-08-13 22:21:18
a52582cb-9b3b-43e4-a2dd-c6795d575a6b2	\N	\N	2020-08-14 10:19:53.368	200000	1	PL002	0	0	\N	2020-08-14 03:19:53	2020-08-14 03:19:53
17a88261-c390-4efa-bf6f-2148a24cb78e2	\N	\N	2020-08-14 10:26:11.874	200000	1	PL002	0	0	\N	2020-08-14 03:26:11	2020-08-14 03:26:11
fd79d7ee-fe24-47ea-b81c-f41953d398b92	\N	\N	2020-08-14 10:37:32.838	1330000	32005	PL001	0	0	\N	2020-08-13 22:21:18	2020-08-13 22:21:18
6a982189-2dd7-4db5-a029-b11d0e928f5f3	\N	\N	2020-08-14 13:24:50.073	19274075	32006	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
fd79d7ee-fe24-47ea-b81c-f41953d398b93	2020-08-14 10:38:37.025	Booking Confirmed	2020-08-14 10:37:39.958	200000	1	PL002	1	0	Menunggu konfirmasi provider	2020-08-14 03:37:39	2020-08-14 03:37:39
6a982189-2dd7-4db5-a029-b11d0e928f5f2	\N	\N	2020-08-14 13:24:56.806	2544000	1	PL002	0	0	\N	2020-08-14 06:24:54	2020-08-14 06:24:54
f1590cfe-5205-4eb9-9bb2-fd46df6ebcef3	\N	\N	2020-08-14 13:25:44.854	19274075	32007	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
f1590cfe-5205-4eb9-9bb2-fd46df6ebcef2	\N	\N	2020-08-14 13:25:46.935	2544000	1	PL002	0	0	\N	2020-08-14 06:25:46	2020-08-14 06:25:46
fc105bb4-a344-4a5b-a207-9045af47d5373	\N	\N	2020-08-14 13:26:39.464	19274075	32008	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
fc105bb4-a344-4a5b-a207-9045af47d5372	\N	\N	2020-08-14 13:26:43.743	2544000	1	PL002	0	0	\N	2020-08-14 06:26:41	2020-08-14 06:26:41
06fa9267-0e32-4e76-a43a-bda916b9bb403	\N	\N	2020-08-14 13:27:24.409	19274075	32009	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
06fa9267-0e32-4e76-a43a-bda916b9bb402	\N	\N	2020-08-14 13:27:26.145	2544000	1	PL002	0	0	\N	2020-08-14 06:27:25	2020-08-14 06:27:25
e7dd4a74-ec93-4b6b-b8e5-ed3c7d0f5ff73	\N	\N	2020-08-14 13:28:28.165	3026035	32010	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
e7dd4a74-ec93-4b6b-b8e5-ed3c7d0f5ff72	\N	\N	2020-08-14 13:28:32.308	2666772	1	PL002	0	0	\N	2020-08-14 06:28:29	2020-08-14 06:28:29
d17ed8f6-7ca0-4fb8-9f77-72f91aaffeb03	\N	\N	2020-08-14 13:32:41.271	19088958	32011	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
c081762e-5d23-4922-bac7-bf69ee76caee3	\N	\N	2020-08-14 13:34:39.15	19088958	32012	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
938efdc5-e0dd-4d05-9815-d8cbde7433783	\N	\N	2020-08-14 13:44:27.608	12044223	32013	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
be1532b2-ac75-4fa0-9c12-6de62ec47d2f3	\N	\N	2020-08-14 13:54:43.139	6394640	32014	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
be1532b2-ac75-4fa0-9c12-6de62ec47d2f2	\N	\N	2020-08-14 13:54:45.914	5333544	1	PL002	0	0	\N	2020-08-14 06:54:45	2020-08-14 06:54:45
95b348fe-522e-4542-8555-e38a9045d7163	\N	\N	2020-08-14 14:17:27.802	3026035	32015	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
95b348fe-522e-4542-8555-e38a9045d7162	\N	\N	2020-08-14 14:17:35.762	2666772	1	PL002	0	0	\N	2020-08-14 07:17:29	2020-08-14 07:17:29
2c807148-d49d-493f-8aff-6d550797ca572	\N	\N	2020-08-14 14:30:36.822	31397500	32019	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
e64169f7-5a28-4596-963a-c82f7518ef823	\N	\N	2020-08-14 15:41:44.311	3875508	1	PL002	0	0	\N	2020-08-14 08:41:43	2020-08-14 08:41:43
bdaa653c-bf1c-4cc6-8fc1-758112cea4282	\N	\N	2020-08-14 15:41:44.515	1377500	32044	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
2cf0770f-cdb9-4b6c-a7a9-79e75089578a3	\N	\N	2020-08-14 15:51:47.545	859750	32051	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
e34ba684-8663-41ed-9f80-a1d5bea85f0f2	\N	\N	2020-08-14 15:54:06.662	3420000	32053	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
2c807148-d49d-493f-8aff-6d550797ca573	2020-08-14 14:37:01.15	Booking Confirmed	2020-08-14 14:30:39.819	2544000	1	PL002	1	0	Menunggu konfirmasi provider	2020-08-14 07:30:39	2020-08-14 07:30:39
d050ea8f-13f6-4677-863c-0b2b5bad0dab2	\N	\N	2020-08-14 14:44:52.067	1377500	32022	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
ed880f83-a3e2-4505-a043-401dbaf33c3e3	2020-08-14 15:45:49.589	Booking Confirmed	2020-08-14 15:45:16.921	2460000	1	PL002	1	0	Menunggu konfirmasi provider	2020-08-14 08:45:15	2020-08-14 08:45:15
b099e47b-2c40-4280-b3b9-d3679d2f8c622	\N	\N	2020-08-14 15:48:22.373	1377500	32050	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
d050ea8f-13f6-4677-863c-0b2b5bad0dab3	2020-08-14 14:47:01.072	Booking Confirmed	2020-08-14 14:44:54.698	1226666	1	PL002	1	0	Menunggu konfirmasi provider	2020-08-14 07:44:54	2020-08-14 07:44:54
1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd2	\N	\N	2020-08-14 14:59:20.938	451250	32023	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
bdaa653c-bf1c-4cc6-8fc1-758112cea4283	2020-08-14 15:42:20.948	Booking Confirmed	2020-08-14 15:41:47.355	1253333	1	PL002	1	0	Menunggu konfirmasi provider	2020-08-14 08:41:46	2020-08-14 08:41:46
1ad59cc8-f87c-4bdd-81b2-f5bc85be88bd3	2020-08-14 14:59:47.415		2020-08-14 14:59:23.741	1237323	1	PL002	1	0	Menunggu konfirmasi provider	2020-08-14 07:59:23	2020-08-14 07:59:23
5a941264-b263-4911-9ea8-26f61cb6f26b3	\N	\N	2020-08-14 15:02:28.783	9078105	32024	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
b766e83d-fa8e-45a6-9109-f35541b71b6b3	\N	\N	2020-08-14 15:02:47.356	90630	32025	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
ff44acf6-f27f-4324-8909-a426452304503	\N	\N	2020-08-14 15:03:05.045	90630	32026	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
ea482b12-8923-4dd7-8ce1-3bc4fa0922973	\N	\N	2020-08-14 15:03:11.99	90630	32027	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
f00436bc-328a-4bc8-9e76-1ee7eb6c80233	\N	\N	2020-08-14 15:04:57.378	90630	32028	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
8c689fea-dc94-4e79-add1-a100863461992	\N	\N	2020-08-14 15:07:03.545	1258750	32029	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
6e2cf3e8-9c58-4fc1-9524-06cbc5a407602	\N	\N	2020-08-14 15:12:58.108	1258750	32030	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
ed880f83-a3e2-4505-a043-401dbaf33c3e2	\N	\N	2020-08-14 15:45:13.511	2755000	32047	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
968dc9a3-ab15-4bcb-bb6e-9ef040e587c23	\N	\N	2020-08-14 16:50:39.365	90630	32065	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
6e2cf3e8-9c58-4fc1-9524-06cbc5a407603	2020-08-14 15:13:28.668	Booking Confirmed	2020-08-14 15:13:01.291	1205052	1	PL002	1	0	Menunggu konfirmasi provider	2020-08-14 08:13:01	2020-08-14 08:13:01
7f4e72c4-0116-45e6-88e6-18213396204b3	\N	\N	2020-08-14 15:15:11.105	90630	32031	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
260a6742-e6ed-4918-8ad1-e788b62bffb23	\N	\N	2020-08-14 15:17:01.087	90630	32032	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
0df1b390-8ce7-4501-9fd8-cd98e9c45d223	\N	\N	2020-08-14 15:17:08.644	90630	32033	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
8fdb6cab-5218-4331-94e7-e801929f5e3f3	\N	\N	2020-08-14 15:19:28.591	90630	32034	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
f79fbe9a-bc71-4d68-83e3-406261d485093	\N	\N	2020-08-14 15:20:25	90630	32035	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
35029a16-f1b4-47db-b088-79597e76629b3	\N	\N	2020-08-14 15:25:06.873	90630	32036	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
cdf5546c-fb3d-4daf-b863-70b222a686bb3	\N	\N	2020-08-14 15:26:55.492	90630	32037	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
dba3b7b5-e52a-4274-b346-9c56325a5bd43	\N	\N	2020-08-14 15:37:05.82	90630	32039	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
844c67b7-c2e4-4dd8-a369-86fd26970f533	\N	\N	2020-08-14 15:38:53.899	90630	32040	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
f87ea8e3-e3f0-404e-bbec-6b200034c1043	\N	\N	2020-08-14 15:40:14.168	90630	32041	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
e64169f7-5a28-4596-963a-c82f7518ef822	\N	\N	2020-08-14 15:41:40.578	3515000	32043	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
d00708c9-c77a-42a9-b15b-abd76e2495412	2020-08-14 15:57:00.153		2020-08-14 15:56:03.585	3420000	32055	PL001	1	0	Menunggu konfirmasi provider	2020-08-14 06:21:18	2020-08-14 06:21:18
60b0c35d-a58b-4bc8-a2e3-249a820a3f242	\N	\N	2020-08-14 16:40:44.175	1377500	32059	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
d8beea02-0ae4-4bf3-bf90-089a2382b1e12	\N	\N	2020-08-14 16:41:13.234	1377500	32060	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
c69824d3-7a04-4dab-9243-a4663e3ee4782	\N	\N	2020-08-14 16:42:32.877	1282500	32061	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
b099e47b-2c40-4280-b3b9-d3679d2f8c623	2020-08-14 15:48:52.866	Booking Confirmed	2020-08-14 15:48:23.832	1230000	1	PL002	1	0	Menunggu konfirmasi provider	2020-08-14 08:48:23	2020-08-14 08:48:23
0ae69f0c-b262-4dd8-b8b4-fd150cf5d05e3	\N	\N	2020-08-14 16:42:52.918	90630	32062	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
e735b12d-2a56-4d4a-a5a3-cb4ba5f2ea843	\N	\N	2020-08-14 17:00:29.087	29355	32066	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
89a90d27-3410-4d0e-b990-32ea7f1aac032	\N	\N	2020-08-14 17:53:44.903	1805000	32068	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
c69824d3-7a04-4dab-9243-a4663e3ee4783	2020-08-14 16:43:59.357	Booking Confirmed	2020-08-14 16:42:35.822	1277907	1	PL002	1	0	Menunggu konfirmasi provider	2020-08-14 09:42:35	2020-08-14 09:42:35
47d8f018-eb32-4bf1-ba56-93e32c2406993	\N	\N	2020-08-14 18:06:10.159	29355	32069	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
1193a2e3-1e59-41b6-b301-e020ad6c819b3	\N	\N	2020-08-14 18:08:15.597	29355	32070	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
8aaecd2c-1b6f-41de-9e87-aa330c9fb92f3	\N	\N	2020-08-14 18:08:55.746	29355	32071	PL001	0	0	\N	2020-08-14 06:21:18	2020-08-14 06:21:18
1ec0c1fe-6b84-44fe-924e-6d416f35f04d2	\N	\N	2020-08-14 21:36:58.647	2565000	32072	PL001	0	0	\N	2020-08-14 14:21:19	2020-08-14 14:21:19
20200816_1	\N	\N	2020-08-16 13:59:56.527	1442779	257	PL001	0	0	\N	2020-08-16 08:00:14	2020-08-16 08:00:14
1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	2020-08-14 21:38:19.989	Booking Confirmed	2020-08-14 21:37:01.796	2555814	1	PL002	1	1	Menunggu konfirmasi provider	2020-08-14 14:37:02	2020-08-14 14:37:02
a1e95462-8647-4546-b4c7-1fc9fe9dc0403	\N	\N	2020-08-15 16:49:40.793	60420	32075	PL001	0	0	\N	2020-08-15 06:21:20	2020-08-15 06:21:20
d42f6e79-9d75-43ff-9b09-799361d6bb363	\N	\N	2020-08-15 16:55:25.68	60420	32076	PL001	0	0	\N	2020-08-15 06:21:20	2020-08-15 06:21:20
35c10c1f-055f-4433-9559-d42ced19ca733	\N	\N	2020-08-15 21:48:09.589	638689	32077	PL001	0	0	\N	2020-08-15 14:21:20	2020-08-15 14:21:20
ed80bde9-65bc-47cd-9225-62e0917c6e6a3	\N	\N	2020-08-15 21:48:30.501	638689	32078	PL001	0	0	\N	2020-08-15 14:21:20	2020-08-15 14:21:20
437ebef7-53dd-4738-84dc-263aa239e6c72	\N	\N	2020-08-16 13:34:22.187	1306250	32079	PL001	0	0	\N	2020-08-16 06:21:21	2020-08-16 06:21:21
50090798-dfbd-4518-8a84-78a10397747b3	2020-08-17 20:27:10.966	Booking Confirmed	2020-08-16 15:44:54.666	2711532	1	PL002	1	1	Menunggu konfirmasi provider	2020-08-16 21:00:00	2020-08-16 21:00:00
ad119148-6875-4a87-8ec2-e044f2e3913e2	\N	\N	2020-08-17 20:30:21.637	1282500	32083	PL001	0	0	\N	2020-08-17 14:21:22	2020-08-17 14:21:22
ad119148-6875-4a87-8ec2-e044f2e3913e3	\N	\N	2020-08-17 20:30:23.961	2682817	1	PL002	0	0	\N	2020-08-17 13:30:24	2020-08-17 13:30:24
5fe18cfa-b431-465b-8555-7880de1d66e72	\N	\N	2020-08-17 23:02:33.55	1282500	32084	PL001	0	0	\N	2020-08-17 14:21:22	2020-08-17 14:21:22
47676dff-207c-46af-bc29-a7fb93afaf802	\N	\N	2020-08-17 23:03:19.608	1282500	32085	PL001	0	0	\N	2020-08-17 14:21:22	2020-08-17 14:21:22
04fc413c-3a4a-43d3-9fd3-3c5966ff50502	\N	\N	2020-08-17 23:04:03.274	1282500	32086	PL001	0	0	\N	2020-08-17 14:21:22	2020-08-17 14:21:22
d0db336c-d39c-467d-86ce-0fae0180bb6e2	\N	\N	2020-08-17 23:04:20.876	1282500	32087	PL001	0	0	\N	2020-08-17 14:21:22	2020-08-17 14:21:22
f65452ac-5fe1-45c9-b14f-f27da4d788942	\N	\N	2020-08-16 14:23:50.508	2612500	32080	PL001	0	0	\N	2020-08-16 06:21:21	2020-08-16 06:21:21
f65452ac-5fe1-45c9-b14f-f27da4d788943	\N	\N	2020-08-16 14:23:58.665	5423065	1	PL002	0	0	\N	2020-08-16 07:23:59	2020-08-16 07:23:59
437ebef7-53dd-4738-84dc-263aa239e6c73	2020-08-16 14:23:59.456	Booking Confirmed	2020-08-16 13:34:25.041	2732043	1	PL002	1	1	Menunggu konfirmasi provider	2020-08-16 06:34:26	2020-08-16 06:34:26
50090798-dfbd-4518-8a84-78a10397747b2	\N	\N	2020-08-16 14:48:30.876	1306250	32081	PL001	0	0	\N	2020-08-16 06:21:21	2020-08-16 06:21:21
fbf72b10-f25c-4751-bccd-80f8cdee66ac3	\N	\N	2020-08-16 16:12:40.365	60420	32082	PL001	0	0	\N	2020-08-16 06:21:21	2020-08-16 06:21:21
\.


--
-- Data for Name: domestic_vessel_booking; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.domestic_vessel_booking (id_request, alamat_penerima, alamat_pengirim, booking_date, deskripsi_umum_barang, hp_penerima, hp_pengirim, id_kawasan_asal, id_kawasan_tujuan, id_platform, id_port_asal, id_port_tujuan, nama_penerima, nama_pengirim, order_id, paid_date, price, qty, status, tanggal_stuffing, container_type, partner, service_type, ship_name, pelayaran, email, npwp, partner_rating) FROM stdin;
cba	surabaya	jakarta	2020-06-10 13:00:00	\N	0897	0899	3	8	PL001	2	4	rohman	raden	l	2020-06-10 13:00:00	45000.00	90	set	2020-07-10	\N	\N	\N	\N	\N	\N	321	\N
qwe	surabaya	Jakarta	2020-08-06 09:36:23.836	ooooo	0897	0899	3	8	PL002	2	4	rohman	raden	11	2020-07-12 13:00:00	27000.00	90	change	2020-07-08	\N	ow	kl	lw	\N	mail@.com	123	\N
asd	surabaya	Jakarta	2020-08-06 09:40:07.787	ooooo	0897	0899	3	8	PL001	2	4	rohman	raden	11	2020-07-12 13:00:00	27000.00	90	change	2020-07-08	\N	ow	kl	lw	\N	mail@.com	123	\N
652820cc-1617-4a33-819b-3101d89431e9	Medan	Jakarta	2020-08-14 12:08:10.717	deskripsi	0877	021-57900391	165	309	PL011	48	24	consigne	ARI ARIYANTO	805	2020-06-10 13:00:00	8500000.00	1	Menunggu Pembayaran	2020-08-14	DRY	PT. PARTNER CEK	CTC	KM. MERATUS KAPUAS	\N	ari@nagase.co.id	018245894059000	\N
652820cc-1617-4a33-819b-3101d89431e1	Medan	Wisma Keiai, Jakarta	2020-08-14 12:27:56.506	deskripsi	0877	021-57900391	165	309	PL011	48	24	consigne	ARI ARIYANTO	805	\N	8500000.00	1	Menunggu Pembayaran	2020-08-14	DRY	PT. PARTNER CEK	CTC	KM. MERATUS KAPUAS	\N	ari@nagase.co.id	018245894059000	\N
652820cc-1617-4a33-819b-3101d89431e2	Medan	Wisma Keiai jalan sudirman	2020-08-14 12:29:27.812	deskripsi	0877	021-57900391	165	309	PL011	48	24	consigne	ARI ARIYANTO	805	\N	8500000.00	1	Menunggu Pembayaran	2020-08-14	DRY	PT. PARTNER CEK	CTC	KM. MERATUS KAPUAS	\N	ari@nagase.co.id	018245894059000	\N
6427f3d8-573b-4dcb-bf85-f3165e492ee5	Makasar	WISMA KEIAI LT.12 	2020-08-14 16:09:14.118	deskripsi	0877	021-57900391	165	309	PL011	48	24	consigne	ARI ARIYANTO	806	\N	8500000.00	1	Menunggu Pembayaran	2020-08-14	DRY	PT. PARTNER CEK	CTC	KM. MERATUS KAPUAS	\N	ari@nagase.co.id	018245894059000	\N
3a8d47bc-eec1-47e7-b773-da9f15e8eaeb	Makasar	WISMA KEIAI JL. JEND. SUDIRMAN	2020-08-14 16:33:17.325	deskripsi umum	0877	021-57900391	165	309	PL011	48	24	consigne	ARI ARIYANTO	807	\N	8500000.00	1	Menunggu Pembayaran	2020-08-14	DRY	PT. PARTNER CEK	CTC	KM. MERATUS KAPUAS	\N	ari@nagase.co.id	018245894059000	\N
sjak8-93nada-jvnjns-bmmasji	surabaya	jakarta	2020-08-04 10:52:36.091	Tidak boleh mengandung bahan kimia atau bahan berbahaya lainnya.	0897	0899	3	8	PL011	2	4	rohman	raden	lll	2020-07-30 12:30:14	27000.00	90	Pesanan Di Proses	2020-07-09	20	PT Abadi Jaya	PAC	Kapal Fery	\N	abadisejahtera@gmail.com	018245894059000	\N
sjak8-93nada-jvnjns-bmmabcd	surabaya	jakarta	2020-08-05 13:41:43.56	Tidak boleh mengandung bahan kimia atau bahan berbahaya lainnya.	0897	0899	3	8	PL011	2	4	rohman	raden	lll	2020-06-10 13:00:00	27000.00	90	Pesanan Di Proses	2020-07-09	20	PT Abadi Jaya	PAC	Kapal Fery	\N	abadisejahtera@gmail.com	018245894059000	\N
abc	surabaya	jakarta	2020-06-10 13:00:00	\N	0897	0899	3	8	PL002	2	4	rohman	raden	lll	2020-07-30 12:30:14	27000.00	90	Pesanan Di Proses	2020-07-10	\N	\N	\N	\N	\N	\N	123	\N
fed	surabaya	jakarta	2020-07-28 16:05:57.374	\N	0897	0899	3	8	PL002	2	4	rohman	raden	lll	2020-07-30 12:30:14	27000.00	90	Pesanan Di Proses	2020-07-09	\N	\N	\N	\N	\N	\N	123	\N
def	surabaya	jakarta	2020-07-28 15:58:56.648	\N	0897	0899	3	8	PL002	2	4	rohman	raden	lll	2020-07-30 12:30:14	27000.00	90	Pesanan Di Proses	2020-07-09	\N	\N	\N	\N	\N	\N	123	\N
ua9s9-93nada-jvnjns-bmmasji	surabaya	jakarta	2020-07-28 16:02:30.832	\N	0897	0899	3	8	PL011	2	4	rohman	raden	lll	2020-07-30 12:30:14	27000.00	90	Pesanan Di Proses	2020-07-09	\N	\N	\N	\N	\N	\N	123	\N
\.


--
-- Data for Name: domestic_vessel_log_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.domestic_vessel_log_status (id_log_status, created_date, id_request, status) FROM stdin;
1	2020-07-21 11:06:04.634	abc	set
2	2020-07-21 11:10:12.059	abc	post
3	2020-07-22 09:33:42.979	cba	set
4	2020-07-28 15:58:56.648	def	Menunggu Pembayaran
5	2020-07-28 16:02:30.832	ua9s9-93nada-jvnjns-bmmasji	Menunggu Pembayaran
6	2020-07-28 16:05:57.374	fed	Menunggu Pembayaran
7	2020-07-28 16:06:06.476	ua9s9-93nada-jvnjns-bmmasji	Bersandar Di Pelabuhan
8	2020-07-28 16:22:15.675	def	change
9	2020-07-28 16:26:32.66	def	geek
10	2020-07-30 21:25:10.006	ua9s9-93nada-jvnjns-bmmasji	Menunggu Pembayaran
11	2020-07-31 14:34:11.088	abc	Berangkat Menuju Pelabuhan Tujuan
12	2020-07-31 14:34:11.099	fed	Berangkat Menuju Pelabuhan Tujuan
13	2020-07-31 14:34:11.108	def	Berangkat Menuju Pelabuhan Tujuan
14	2020-07-31 14:34:11.115	ua9s9-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
15	2020-08-04 10:52:36.091	sjak8-93nada-jvnjns-bmmasji	Menunggu Pembayaran
16	2020-08-04 15:24:58.085	abc	Berangkat Menuju Pelabuhan Tujuan
17	2020-08-04 15:24:58.092	fed	Berangkat Menuju Pelabuhan Tujuan
18	2020-08-04 15:24:58.097	def	Berangkat Menuju Pelabuhan Tujuan
19	2020-08-04 15:24:58.102	ua9s9-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
20	2020-08-04 15:24:58.106	sjak8-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
21	2020-08-04 15:25:16.223	abc	Menunggu Pembayaran
22	2020-08-04 15:25:16.232	fed	Menunggu Pembayaran
23	2020-08-04 15:25:16.24	def	Menunggu Pembayaran
24	2020-08-04 15:25:16.246	ua9s9-93nada-jvnjns-bmmasji	Menunggu Pembayaran
25	2020-08-04 15:25:16.253	sjak8-93nada-jvnjns-bmmasji	Menunggu Pembayaran
26	2020-08-05 07:45:29.732	abc	Berangkat Menuju Pelabuhan Tujuan
27	2020-08-05 07:45:29.739	fed	Berangkat Menuju Pelabuhan Tujuan
28	2020-08-05 07:45:29.743	def	Berangkat Menuju Pelabuhan Tujuan
29	2020-08-05 07:45:29.749	ua9s9-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
30	2020-08-05 07:45:29.754	sjak8-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
31	2020-08-05 08:10:12.141	abc	Berangkat Menuju Pelabuhan Tujuan
32	2020-08-05 08:10:12.145	fed	Berangkat Menuju Pelabuhan Tujuan
33	2020-08-05 08:10:12.148	def	Berangkat Menuju Pelabuhan Tujuan
34	2020-08-05 08:10:12.151	ua9s9-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
35	2020-08-05 08:10:12.154	sjak8-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
36	2020-08-05 08:21:06.002	abc	Ambil Kontainer
37	2020-08-05 08:21:06.011	fed	Ambil Kontainer
38	2020-08-05 08:21:06.017	def	Ambil Kontainer
39	2020-08-05 08:21:06.023	ua9s9-93nada-jvnjns-bmmasji	Ambil Kontainer
40	2020-08-05 08:21:06.029	sjak8-93nada-jvnjns-bmmasji	Ambil Kontainer
41	2020-08-05 08:27:54.644	abc	Menunggu Pembayaran
42	2020-08-05 08:27:54.649	fed	Menunggu Pembayaran
43	2020-08-05 08:27:54.654	def	Menunggu Pembayaran
44	2020-08-05 08:27:54.66	ua9s9-93nada-jvnjns-bmmasji	Menunggu Pembayaran
45	2020-08-05 08:27:54.666	sjak8-93nada-jvnjns-bmmasji	Menunggu Pembayaran
46	2020-08-05 13:41:43.56	sjak8-93nada-jvnjns-bmmabcd	Menunggu Pembayaran
47	2020-08-05 16:30:16.301	sjak8-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
48	2020-08-05 16:30:16.313	sjak8-93nada-jvnjns-bmmabcd	Berangkat Menuju Pelabuhan Tujuan
49	2020-08-05 16:30:16.32	abc	Berangkat Menuju Pelabuhan Tujuan
50	2020-08-05 16:30:16.326	fed	Berangkat Menuju Pelabuhan Tujuan
51	2020-08-05 16:30:16.332	def	Berangkat Menuju Pelabuhan Tujuan
52	2020-08-05 16:30:16.34	ua9s9-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
53	2020-08-05 16:37:03.057	sjak8-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
54	2020-08-05 16:37:03.067	sjak8-93nada-jvnjns-bmmabcd	Berangkat Menuju Pelabuhan Tujuan
55	2020-08-05 16:37:03.074	abc	Berangkat Menuju Pelabuhan Tujuan
56	2020-08-05 16:37:03.081	fed	Berangkat Menuju Pelabuhan Tujuan
57	2020-08-05 16:37:03.085	def	Berangkat Menuju Pelabuhan Tujuan
58	2020-08-05 16:37:03.089	ua9s9-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
59	2020-08-05 16:39:35.495	sjak8-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
60	2020-08-05 16:39:35.503	sjak8-93nada-jvnjns-bmmabcd	Berangkat Menuju Pelabuhan Tujuan
61	2020-08-05 16:39:35.508	abc	Berangkat Menuju Pelabuhan Tujuan
62	2020-08-05 16:39:35.513	fed	Berangkat Menuju Pelabuhan Tujuan
63	2020-08-05 16:39:35.517	def	Berangkat Menuju Pelabuhan Tujuan
64	2020-08-05 16:39:35.521	ua9s9-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
65	2020-08-05 16:40:22.78	sjak8-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
66	2020-08-05 16:40:22.787	sjak8-93nada-jvnjns-bmmabcd	Berangkat Menuju Pelabuhan Tujuan
67	2020-08-05 16:40:22.793	abc	Berangkat Menuju Pelabuhan Tujuan
68	2020-08-05 16:40:22.796	fed	Berangkat Menuju Pelabuhan Tujuan
69	2020-08-05 16:40:22.799	def	Berangkat Menuju Pelabuhan Tujuan
70	2020-08-05 16:40:22.802	ua9s9-93nada-jvnjns-bmmasji	Berangkat Menuju Pelabuhan Tujuan
71	2020-08-05 16:41:51.235	sjak8-93nada-jvnjns-bmmasji	Ambil Kontainer
72	2020-08-05 16:41:51.244	sjak8-93nada-jvnjns-bmmabcd	Ambil Kontainer
73	2020-08-05 16:41:51.252	abc	Ambil Kontainer
74	2020-08-05 16:41:51.257	fed	Ambil Kontainer
75	2020-08-05 16:41:51.262	def	Ambil Kontainer
76	2020-08-05 16:41:51.267	ua9s9-93nada-jvnjns-bmmasji	Ambil Kontainer
77	2020-08-05 16:46:05.171	sjak8-93nada-jvnjns-bmmasji	Ambil Kontainer
78	2020-08-05 16:46:05.177	sjak8-93nada-jvnjns-bmmabcd	Ambil Kontainer
79	2020-08-05 16:46:05.182	abc	Ambil Kontainer
80	2020-08-05 16:46:05.187	fed	Ambil Kontainer
81	2020-08-05 16:46:05.19	def	Ambil Kontainer
82	2020-08-05 16:46:05.194	ua9s9-93nada-jvnjns-bmmasji	Ambil Kontainer
83	2020-08-05 16:49:36.425	sjak8-93nada-jvnjns-bmmasji	Pesanan
84	2020-08-05 16:49:36.435	sjak8-93nada-jvnjns-bmmabcd	Pesanan
85	2020-08-05 16:49:36.442	abc	Pesanan
86	2020-08-05 16:49:36.45	fed	Pesanan
87	2020-08-05 16:49:36.457	def	Pesanan
88	2020-08-05 16:49:36.465	ua9s9-93nada-jvnjns-bmmasji	Pesanan
89	2020-08-05 16:55:19.954	sjak8-93nada-jvnjns-bmmasji	Pesanan
90	2020-08-05 16:55:19.961	sjak8-93nada-jvnjns-bmmabcd	Pesanan
91	2020-08-05 16:55:19.966	abc	Pesanan
92	2020-08-05 16:55:19.97	fed	Pesanan
93	2020-08-05 16:55:19.975	def	Pesanan
94	2020-08-05 16:55:19.98	ua9s9-93nada-jvnjns-bmmasji	Pesanan
95	2020-08-05 16:55:36.235	sjak8-93nada-jvnjns-bmmasji	Pesanan Di Proses
96	2020-08-05 16:55:36.239	sjak8-93nada-jvnjns-bmmabcd	Pesanan Di Proses
97	2020-08-05 16:55:36.243	abc	Pesanan Di Proses
98	2020-08-05 16:55:36.247	fed	Pesanan Di Proses
99	2020-08-05 16:55:36.252	def	Pesanan Di Proses
100	2020-08-05 16:55:36.257	ua9s9-93nada-jvnjns-bmmasji	Pesanan Di Proses
101	2020-08-06 09:36:23.836	qwe	Menunggu Pembayaran
102	2020-08-06 09:40:07.787	asd	Menunggu Pembayaran
103	2020-08-06 09:41:16.028	qwe	Menunggu Pembayaran
104	2020-08-06 09:43:50.041	qwe	change
105	2020-08-06 09:44:05.224	asd	change
106	2020-08-06 09:46:24.745	qwe	Menunggu Pelunasan
107	2020-08-06 09:46:24.751	asd	Menunggu Pelunasan
108	2020-08-06 09:52:00.659	qwe	change
109	2020-08-06 09:52:00.719	asd	change
110	2020-08-14 12:08:10.717	652820cc-1617-4a33-819b-3101d89431e9	Menunggu Pembayaran
111	2020-08-14 12:27:56.506	652820cc-1617-4a33-819b-3101d89431e1	Menunggu Pembayaran
112	2020-08-14 12:29:27.812	652820cc-1617-4a33-819b-3101d89431e2	Menunggu Pembayaran
113	2020-08-14 16:09:14.118	6427f3d8-573b-4dcb-bf85-f3165e492ee5	Menunggu Pembayaran
114	2020-08-14 16:33:17.325	3a8d47bc-eec1-47e7-b773-da9f15e8eaeb	Menunggu Pembayaran
\.


--
-- Data for Name: domestic_vessel_packing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.domestic_vessel_packing (id_packing, berat_muatan, deskripsi, jenis_muatan, jumlah_barang, id_request, kemasan) FROM stdin;
1	2	ee	er	9	abc	\N
2	2	uei	opop	15	cba	\N
3	2	ee	er	9	def	\N
4	2	ee	er	9	ua9s9-93nada-jvnjns-bmmasji	Kardus
5	2	ee	er	9	fed	\N
6	2	ee	er	9	sjak8-93nada-jvnjns-bmmasji	Kardus
7	2	er	er	9	sjak8-93nada-jvnjns-bmmabcd	Kardus
8	2	ee	er	9	qwe	3
9	2	ee	er	9	asd	3
10	8	deskripsi	jenis	8	652820cc-1617-4a33-819b-3101d89431e9	kg
11	8	deskripsi	jenis	8	652820cc-1617-4a33-819b-3101d89431e1	kg
12	8	deskripsi	jenis	8	652820cc-1617-4a33-819b-3101d89431e2	kg
13	10	deskripsi	jenis	10	6427f3d8-573b-4dcb-bf85-f3165e492ee5	LITER
14	10	deskripsi	jenis	10	3a8d47bc-eec1-47e7-b773-da9f15e8eaeb	TON
\.


--
-- Data for Name: mst_container_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_container_type (id, related_fields, type) FROM stdin;
1		General/ Dry Cargo
2		Tunne Type
3	[\n {\n "label":"Over Height",\n "column_name":"over_height",\n "type":"text",\n "maxlength":"255",\n "required":false\n},\n {\n "label":"Over Weight",\n "column_name":"over_weight",\n "type":"text",\n "maxlength":"255",\n "required":false\n}\n]	Open Top Steel
4	[\n{\n "label":"Over Height",\n "column_name":"over_height",\n "type":"text",\n "maxlength":"255",\n "required":false\n},\n {\n "label":"Over Width",\n "column_name":"over_width",\n "type":"text",\n "maxlength":"255",\n "required":false\n },\n {\n "label":"Over Length",\n "column_name":"over_length",\n "type":"text",\n "maxlength":"255",\n "required":false\n},\n {\n "label":"Over Weight",\n "column_name":"over_weight",\n "type":"text",\n "maxlength":"255",\n "required":false\n}\n]	Flat Rack
5	[\n{\n "label":"Temperatur",\n "column_name":"temperatur",\n "type":"text",\n "maxlength":"255",\n "required":false\n}\n]	Reefer/ Refigerator
6		Barge Container
7		Bulk Container
8		ISO Tank
9		Lain-lain
\.


--
-- Data for Name: mst_dangerous_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_dangerous_type (id, related_fields, type) FROM stdin;
1	[\n{\n "label": "Dangerous Material",\n "column_name": "dangerous_material",\n "type": "combobox",\n "options":\n [ \n {\n "label":"Class 1: Explosive materials",\n "value":"Class 1: Explosive materials" \n},\n {\n "label":"Class 2: Gases",\n "value":"Class 2: Gases"\n},\n {\n "label":"Class 3: Flammable Liquids",\n "value":"Class 3: Flammable Liquids" \n},\n {"label":"Class 4: Flammable Solids",\n "value":"Class 4: Flammable Solids" \n},\n {\n "label":"Class 5: Oxidizers and Organic Peroxides",\n "value":"Class 5: Oxidizers and Organic Peroxides"},\n {\n "label":"Class 6: Toxic and Infectious Substances",\n "value":"Class 6: Toxic and Infectious Substances" \n},\n {\n "label":"Class 7: Radioactive Materials",\n "value":"Class 7: Radioactive Materials" \n},\n {\n "label":"Class 8: Corrosives Materials",\n "value":"Class 8: Corrosives Materials" \n},\n {\n "label":"Class 9: Miscellaneous Dangerous Goods",\n "value":"Class 9: Miscellaneous Dangerous Goods" \n} \n],\n "required":false\n} \n]	Dangerous
2		Non Dangerous
\.


--
-- Data for Name: mst_document_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_document_type (kd_document_type, description, document_name, kd_process_type) FROM stdin;
1	Impor	BC 2.0	1
2	PLB	BC 2.3	1
3	TPB	BC 1.6	1
\.


--
-- Data for Name: mst_kawasan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_kawasan (id_kawasan, nama_kawasan, nama_kota, id_port) FROM stdin;
3	Bacan	Ternate	1
4	Galay Dubu	Dobo 	1
5	Jailolo Kota	Jailolo	1
6	Jl. Dr. J. Leimena Hative Besar	Ambon	1
7	Jl. DR. Setiabudi	Ambon	1
8	Kalumpang	Ternate	1
9	Labetawi	Tual	1
10	Labuha Kota	Labuha	1
11	Laksdya Wattimena	Ambon	1
12	Maba Kota	Maba	1
13	Masohi Kota	Masohi	1
14	Muna	Ambon	1
15	Namlea Kota	Namlea	1
16	Nusaniwe	Ambon	1
17	Oba Utara	Ternate	1
18	Obi	Obi	1
19	Seram Barat	Ambon	1
21	Tanjung Buli	Ternate	1
22	Teluk Kao	Halmahera Utara	1
23	Ternate Kota	Ternate	1
24	Way Apu	Namlea	1
25	Yos Sudarso	Tual	1
26	A.A Maramis	Bitung	94
27	Arif Rachman	Makassar	94
28	Bahodopi	Morowali	94
29	Bandara Hasanuddin	Makassar	94
30	Bandara Kendari	Kendari	94
31	Banggai Laut Kota	Banggai Laut	94
32	Bantaeng	Makassar	94
33	Bantimurung	Makassar	94
35	Batalaiworu	Raha	94
37	Belopa	Makassar	94
38	Binamu	Makassar	94
39	Biringkanaya	Makassar	94
40	Bone	Makassar	94
41	Botonompo	Makassar	94
42	Bua	Luwu	94
43	Bulukumba	Makassar	94
44	Bulutuba	Mamuju Utara	94
45	Bungoro	Makassar	94
46	Bunta	Banggai	94
47	Dampal Selatan	Tolitoli	94
48	Danone	Bitung	94
49	Desa Pakowa Bunta	Luwuk	94
50	Desa Puusanggula	Konawe Selatan	94
51	Dungingi	Gorontalo	94
52	Enrekang	Makassar	94
53	Galangan Kapal 	Makassar	94
54	Girian	Bitung	94
55	Gowa	Makassar	94
56	Ir Sutami	Makassar	94
57	Jalan Daeng tompo	Makassar	94
60	Jl. Bunggasi	Kendari	94
61	Jl. Dewi Sartika	Palu	94
62	Jl. Gunung Merapi	Luwuk	94
63	Jl. Laute	Kendari	94
64	Jl. Masjid Timur	Kendari	94
65	Jl. Pattimura	Kendari	94
67	Jl. Puebonggo	Palu	94
68	Jl. R. Suprapto	Kendari	94
69	Jl. Saranani	Kendari	94
71	Jl. Suprapto	Palu	94
72	Jl. Tolitoli	Palu	94
74	Kadoodan	Bitung	94
75	Kalawat	Bitung	94
76	Kapasa Raya	Makassar	94
77	Kasipute	Sikeli	94
78	Kasipute	Kendari	94
80	Kima	Makassar	94
81	Kintom	Luwuk	94
82	Korumba	Kendari	94
83	Kota Selatan	Gorontalo	94
84	Lalabata	Makassar	94
85	Lampoko	Barru	94
87	Layana-Dupa	Palu	94
88	Lolak	Bitung	94
89	Luwu Timur	Makassar	94
90	Maarang	Pangkep (Pangkajene)	94
91	Maccopa	Maros	94
73	Ternate Kota	Ternate	46
93	Malalayang	Bitung	94
95	Malili	Makassar	94
96	Mamosalato	Morowali	94
97	Mamuju Utara	Palu	94
98	Manado Ilir	Bitung	94
99	Manado Kota	Manado	94
100	Manado Mapanget	Bitung	94
101	Mangkutana	Makassar	94
102	Manimbunimbu	Bitung	94
103	Mapanget	Bitung	94
104	Maros	Makassar	94
105	Masamba	Makassar	94
107	Minahasa Selatan	Bitung	94
108	Minahasa Utara	Bitung	94
109	MT Haryono	Gorontalo	94
110	Nuha	Luwu Timur	94
111	Nuhon	Banggai	94
112	Onewila	Konawe Selatan	94
113	Pacceda	Bitung	94
114	Palopo	Makassar	94
116	Panakkukang	Makassar	94
117	Panciro	Makassar	94
118	Pangkajene (Pangkep)	Makassar	94
119	Pantoloan	Palu	94
120	Parangloe	Makassar	94
121	Pare-Pare Kota	Parepare	94
122	Pattallassang	Gowa	94
115	Timbang Langkat	Binjai	53
66	Samarinda Kota	Samarinda	45
86	Makassar Kota	Makassar	78
79	Dawuan	Majalengka	119
92	Panjunan	Pati	119
106	Medan Kota	Medan	53
36	Sidoarjo Kota	Sidoarjo	119
2	Surabaya Kota	Surabaya	119
1	Trawas	Mojokerto	119
70	Ambon Kota	Ambon	46
58	Banjarmasin Kota	Banjarmasin	45
20	Jetis	Mojokerto	119
34	Kapuk	Jakarta 	119
123	Pattene	Makassar	94
124	Pattene Raya	Makassar	94
125	Pekkabata	Makassar	94
126	Pergudangan Lantebung	Makassar	94
127	Perintis Kemerdekaan	Makassar	94
128	Pinrang Kota	Pinrang	94
129	Pohara	Konawe	94
130	Polewali Mandar	Makassar	94
131	Polmas	Makassar	94
132	Poros Barru	Makassar	94
133	Poros Malino	Makassar	94
134	Pulubala	Gorontalo	94
135	Pusat Pergudangan Palu	Palu	94
136	Puuwatu	Kendari	94
137	Rahandouna	Kendari	94
138	Rantepao	Makassar	94
139	Sampara/Morosi	Konawe	94
140	Sengkang	Makassar	94
141	Siawung	Barru	94
142	Sidenreng Rappang	Makassar	94
144	Silae	Palu	94
146	Sipatana	Gorontalo	94
153	Tahuna Timur	Bitung	94
164	Tempe	Makassar	94
196	Jl. DR. Setiabudi	Ambon	67
225	Bati-Bati	Banjarmasin	2
226	Benua Kayong	Pontianak	2
227	Bontang	Samarinda	2
229	Bundaran Monyet Kumai	Kumai	2
231	Djok Mentaya	Banjarmasin	2
232	Gunung Panjang	Samarinda	2
233	Harm Ayoeb	Berau	2
234	Jl. A. Wahab	Samarinda	2
235	Jl. Arteri Supandio	Pontianak	2
241	Jl. Kadrie Oening	Samarinda	2
94	Laksdya Wattimena	Ambon	46
238	Sebakis	Tarakan	45
236	Sesayap	Tarakan	45
166	Asem Jaya	Surabaya	119
168	Bendul Merisi	Surabaya	119
237	Sembakung	Tarakan	45
239	Nusaniwe	Ambon	46
240	Seram Barat	Ambon	46
169	Benowo	Surabaya	119
170	Benteng/Sarwajala	Surabaya	119
147	Berbek	Sidoarjo	119
171	Berlian	Surabaya	119
148	Buduran	Sidoarjo	119
149	By Pass Krian	Sidoarjo	119
172	Dana Karya	Surabaya	119
173	Dapuan	Surabaya	119
174	Demak	Surabaya	119
175	Demak Timur	Surabaya	119
176	Dupak	Surabaya	119
177	Dupak Rukun	Surabaya	119
150	Gedangan	Sidoarjo	119
151	Gempol	Sidoarjo	119
178	Hangtuah	Surabaya	119
179	Ikan	Surabaya	119
181	Jamrud	Surabaya	119
182	Jemur	Surabaya	119
183	Jepara	Surabaya	119
223	Juanda	Surabaya	119
184	Kalianak	Surabaya	119
185	Kalianak 55	Surabaya	119
186	Kalianak Gudang Dolok	Surabaya	119
189	Kalimas Barat	Surabaya	119
190	Kalimas Timur	Surabaya	119
191	Karangpilang	Surabaya	119
192	Kebalen	Surabaya	119
193	Kedinding	Surabaya	119
195	Kedurus	Surabaya	119
224	Kenjeran	Surabaya	119
194	Keputih	Surabaya	119
152	Kletek	Sidoarjo	119
154	Krian	Sidoarjo	119
197	Kutisari	Surabaya	119
198	Margomulyo	Surabaya	119
199	Mayjend Sungkono	Surabaya	119
200	Mirah	Surabaya	119
201	Moroseneng	Surabaya	119
202	Mulyorejo	Surabaya	119
203	Nilam	Surabaya	119
205	Panjangjiwo	Surabaya	119
206	Pertokoan Nam Permai Plaza	Surabaya	119
207	Pertokoan Semut Indah	Surabaya	119
209	Perumahan Kris Kencana	Surabaya	119
210	Petemon	Surabaya	119
163	Porong	Sidoarjo	119
143	Purwosari	Pasuruan	119
211	Rajawali	Surabaya	119
212	Raya Rungkut Industri	Surabaya	119
155	Sepanjang	Sidoarjo	119
213	Sidotopo	Surabaya	119
214	Sukomanunggal	Surabaya	119
156	Taman	Sidoarjo	119
215	Tambak langon	Surabaya	119
216	Tandes	Surabaya	119
157	Tanggulangin	Sidoarjo	119
218	Tanjung Sadari	Surabaya	119
217	Tanjungsari	Surabaya	119
219	Teluk	Surabaya	119
145	Tretes	Pasuruan	119
158	Trosobo	Sidoarjo	119
160	Waru	Sidoarjo	119
161	Waru Gunung	Sidoarjo	119
162	Wonoayu	Sidoarjo	119
222	Wonocolo	Surabaya	119
221	Wonokromo	Surabaya	119
220	Wonokusumo	Surabaya	119
242	Galay Dubu	Dobo 	46
230	Kubu Raya	Pontianak	45
165	Asem Rowo	Surabaya	119
167	Banyu Urip	Surabaya	119
243	Jl. Letkol. Pol. HM. Asnawi Arbain	Balikpapan	2
245	Jl. Marsma R Iswahyudi	Balikpapan	2
246	Jl. Mulawarman	Balikpapan	2
248	Jl. P. Suryanata	Samarinda	2
249	Jl. Padat Karya	Banjarmasin	2
250	Jl. Pangeran Hidayatullah	Banjarmasin	2
251	Jl. Pramuka	Banjarmasin	2
252	Jl. Raya Wajk Hulu	Pontianak	2
254	Jl. Teuku Umar	Samarinda	2
260	Kp. Melayu Barat	Banjarmasin	2
269	Mentawa Baru Ketapang	Sampit	2
277	Pergudangan Liang Anggang Industri Kecil (LIK)	Banjarmasin	2
294	Sungai Kunyit	Pontianak	2
302	Yos Sudarso	Pontianak	2
303	A.A Maramis	Bitung	102
304	Arif Rachman	Makassar	102
305	Bahodopi	Morowali	102
307	Bandara Kendari	Kendari	102
308	Banggai Laut Kota	Banggai Laut	102
271	Lapadde	Makassar	78
301	Maccopa	Maros	78
310	Batalaiworu	Raha	102
272	Malili	Makassar	78
273	Mamuju Utara	Palu	78
275	Maros	Makassar	78
276	Masamba	Makassar	78
312	Pacceda	Bitung	78
314	Bulutuba	Mamuju Utara	102
316	Bunta	Banggai	102
318	Danone	Bitung	102
278	Palopo	Makassar	78
297	Panciro	Makassar	78
298	Pangkajene (Pangkep)	Makassar	78
338	Jl. Suprapto	Palu	102
339	Jl. Tolitoli	Palu	102
340	Jl. Trans Bahomakmur	Kendari	102
341	Kadoodan	Bitung	102
342	Kalawat	Bitung	102
343	Kapasa Raya	Makassar	102
344	Kasipute	Sikeli	102
279	Parangloe	Makassar	78
258	Pare-Pare Kota	Parepare	78
280	Pinrang Kota	Pinrang	78
351	Layana-Dupa	Palu	102
281	Polewali Mandar	Makassar	78
282	Polmas	Makassar	78
299	Poros Malino	Makassar	78
283	Rantepao	Makassar	78
284	Sengkang	Makassar	78
285	Sidrap	Makassar	78
286	Soppeng	Makassar	78
287	Sultan Abdullah Raya	Makassar	78
311	Tahuna Timur	Bitung	78
300	Takalar	Makassar	78
288	Tanah Toraja	Makassar	78
289	Tanete Riatang	Makassar	78
290	Tapoyo	Makassar	78
291	Tempe	Makassar	78
262	Topoyo	Makassar	78
292	Wara	Makassar	78
293	Watang Sawito	Makassar	78
313	Wenang Selatan	Makassar	78
295	Wonomulyo	Makassar	78
296	Wotu	Makassar	78
309	A.A Maramis	Bitung	78
306	Muna	Ambon	46
317	Amplas	Medan	53
356	Bandengan 	Jakarta 	119
319	Barat	Medan	53
264	Bone	Makassar	78
253	Nabarua	Nabire	66
265	Bulukumba	Makassar	78
247	Tanjung Buli	Ternate	46
259	Bantaeng	Makassar	78
267	Jeneponto	Makassar	78
257	Ir Sutami	Makassar	78
322	Deli	Medan	53
315	Deli Serdang	Medan	53
323	Denai	Medan	53
324	Helvetia	Medan	53
325	Johor	Medan	53
326	Labuhan	Medan	53
327	Maimun	Medan	53
328	Marelan	Medan	53
329	Perjuangan	Medan	53
330	Petisah	Medan	53
331	Polonia	Medan	53
332	Selayang	Medan	53
337	Siantar Martoba	Pematang Siantar	53
333	Sunggal	Medan	53
334	Tembung	Medan	53
335	Timur	Medan	53
336	Tuntungan	Medan	53
255	Bantimurung	Makassar	78
263	Binamu	Makassar	78
268	Kima	Makassar	78
354	Ancol	Jakarta 	119
321	Belawan Kota	Medan	53
320	Baru	Medan	53
270	Lalabata	Makassar	78
261	Belopa	Makassar	78
256	Botonompo	Makassar	78
266	Enrekang	Makassar	78
357	Bantar Gebang 	Jakarta 	119
345	Banten Kota	Banten	119
349	Cikupa	Tangerang	119
350	Curug	Tangerang	119
353	Dawarblandong	Mojokerto	119
346	Kragilan	Banten	119
347	Merak	Banten	119
352	Serpong Purpitex	Tangerang	119
244	Bacan	Ternate	46
228	Ketapang Kota	Pontianak	45
348	Balaraja 	Tangerang	119
355	Bandara Cengkareng 	Jakarta 	119
370	Onewila	Konawe Selatan	102
380	Pohara	Konawe	102
392	Solog	Bolaang Mongondow	102
393	Somba Opu	Gowa	102
394	Stadion Klabat	Bitung	102
401	Telaga Jaya	Gorontalo	102
404	Tikala	Manado	102
405	Tinabite	Sikeli	102
407	Toili	Banggai	102
408	Tojo Una Una	Palu	102
412	TPM	Makassar	102
413	Tuminting	Bitung	102
415	Turikale	Maros	102
416	Ujung Tanah	Makassar	102
420	Watampone	Bone	102
421	Wenang	Bitung	102
422	Adi Sucipto	Pontianak	4
425	Bati-Bati	Banjarmasin	4
428	Bumi Harjo	Kumai	4
431	Djok Mentaya	Banjarmasin	4
434	Jl. A. Wahab	Samarinda	4
437	Jl. Gerbang Dayaku	Samarinda	4
439	Jl. Jenderal Sudirman	Balikpapan	4
440	Jl. Jendral Sudirman	Samarinda	4
441	Jl. Kadrie Oening	Samarinda	4
444	Jl. M Tohir Ketapang	Pontianak	4
448	Jl. P. Suryanata	Samarinda	4
454	Jl. Teuku Umar	Samarinda	4
455	Jl. Yos Sudarso	Balikpapan	4
458	Karang Joang	Balikpapan	4
459	Kariangau	Balikpapan	4
462	Kutai Kartanegara	Samarinda	4
463	Loa Buah	Samarinda	4
464	Loa Janan Ilir	Samarinda	4
465	Loh Sumber	Samarinda	4
443	Jl. Trans Bahomakmur	Kendari	78
359	Bukit Indah 	Jakarta 	119
453	Desa Pakowa Bunta	Luwuk	78
274	Mangkutana	Makassar	78
360	Cakung 	Jakarta 	119
417	Cibadak 	Sukabumi	119
395	Cibitung 	Bekasi	119
361	Cibubur 	Jakarta 	119
418	Cicirug 	Sukabumi	119
409	Cikampek 	Karawang	119
396	Cikarang 	Bekasi	119
397	Cileungsi 	Bogor	119
406	Cimanggis 	Depok	119
362	Ciracas 	Jakarta 	119
419	Cisaat 	Sukabumi	119
398	Citeureup 	Bogor	119
363	Daan Mogot 	Jakarta 	119
364	Dadap Perancis 	Jakarta 	119
430	GF Pati 	Semarang	119
426	Gondang	Mojokerto	119
442	Grati	Pasuruan	119
435	Grogol 	Sukoharjo	119
473	Gunung Gangsir 	Pasuruan	119
399	Gunung Putri 	Bogor	119
400	Gunung Sindur 	Bogor	119
467	Jabon 	Mojokerto	119
377	Jaya Karta 	Jakarta 	119
365	Jembatan II	Jakarta 	119
366	Jembatan III 	Jakarta 	119
367	Juru Mudi 	Jakarta 	119
423	Juwono 	Pati	119
368	Kalibata 	Jakarta 	119
433	Karanganyar 	Solo 	119
410	Karawang Barat 	Karawang	119
411	Karawang Timur 	Karawang	119
468	Kebomas	Gresik	119
369	Kebun Jeruk 	Jakarta 	119
460	Kertas 	Kudus	119
466	Lawang 	Malang	119
391	Lebak Bulus Ciputat 	Jakarta 	119
371	Mangga Besar 	Jakarta 	119
372	Mangga Dua 	Jakarta 	119
445	Manyar 	Gresik	119
373	Marunda	Jakarta 	119
374	Menceng 	Jakarta 	119
446	Menganti 	Gresik	119
456	Mojoagung 	Jombang	119
469	Mojosari 	Mojokerto	119
375	Muara Baru 	Jakarta 	119
471	Nganjuk Kota 	Nganjuk	119
470	Ngoro 	Mojokerto	119
385	Pacet	Mojokerto	119
447	Panceng 	Gresik	119
414	Pangandaran	Pangandaran	119
402	Parung 	Bogor	119
378	Pasar Ikan 	Jakarta 	119
379	Pasar Kemis 	Jakarta 	119
381	Pasar Rebo 	Jakarta 	119
382	Pegansaan 	Jakarta 	119
383	Penggilingan 	Jakarta 	119
384	Pluit 	Jakarta 	119
376	Pungging	Mojokerto	119
386	Sawah Besar 	Jakarta 	119
450	Sedayu 	Gresik	119
387	Semper 	Jakarta 	119
403	Semplak 	Bogor	119
388	Senayan 	Jakarta 	119
438	Sidomuncul 	Ungaran	119
427	Sumberwangi 	Pati	119
389	Sunter 	Jakarta 	119
451	Tambak Sawah 	Sidoarjo	119
390	Telaga Alur 	Jakarta 	119
432	Tugu 	Semarang	119
452	Wadeng 	Gresik	119
457	Wates	Kediri	119
449	Jl. Salak	Timika	66
461	Babat	Lamongan	119
436	Bina Guna Kimia 	Ungaran	119
358	Bintara 	Jakarta 	119
476	Pergudangan Basirih	Banjarmasin	4
481	Pulau Balang	Balikpapan	4
537	Sibolga	Medan	115
513	Segah	Berau	45
546	Tj. Pinggir Sekupang	Batam	115
497	Sungai Kunyit	Pontianak	45
548	Bagan Batu	Pekanbaru	49
549	Bakaran Batu	Banda Aceh	49
550	Banda Aceh Kota	Banda Aceh	49
551	Batam Kota	Batam	49
500	Dampal Selatan	Tolitoli	78
515	Gowa	Makassar	78
571	Langsa	Banda Aceh	49
521	Tamalan Rea	Makassar	78
572	Angsana Raya	Cikarang	119
483	Balaraja Barat	Tangerang	119
512	Manokwari Kota	Manokwari	66
496	Ahmad Yani	Pontianak	45
478	Maarang	Pangkep (Pangkajene)	78
498	Manimbunimbu	Bitung	78
493	Kasipute	Kendari	78
519	Tanjung Prapat	Medan	53
524	Tanete Bone	Makassar	78
502	Telaga Biru	Gorontalo	78
494	Tolinggula	Gorontalo	78
503	Tondo	Palu	78
542	Veteran Lingkaran	Bitung	78
488	Maumere	Sikka	112
492	Biringkanaya	Makassar	78
514	Harm Ayoeb	Berau	45
541	Binjai Timur	Binjai	53
485	Galangan Kapal 	Makassar	78
472	Jalan Raya Padang Kemiling	Bengkulu	53
544	Kapasa Raya	Makassar	78
486	Motaain	Belu	112
487	Luwu Timur	Makassar	78
543	Bunta	Banggai	78
509	Pergudangan Tenau Indah	Kupang	112
536	Pattene	Makassar	78
480	Sokoria	Ende	112
505	Yos Sudarso	Kupang	112
508	Montongbaan	Lombok Timur	103
538	Bambe	Gresik	119
552	Bandara Kertajati	Majalengka	119
479	Bangil	Pasuruan	119
553	Banyuwangi Kota	Banyuwangi	119
554	Batu Ceper	Tangerang	119
528	Beji	Pasuruan	119
555	Bekasi Kota	Bekasi	119
556	Blitar Kota	Blitar	119
557	Bojonegoro Kota	Bojonegoro	119
558	Bukaka, Cileungsi	Bekasi	119
535	Bungah	Gresik	119
477	Cerme	Gresik	119
559	Cibuluh	Bogor	119
560	Cigading	Banten	119
574	Cikande Rangkas, Serang	Banten	119
561	Cikuda	Bogor	119
562	Cilegon Kota	Cilegon	119
495	Ciputat	Tangerang	119
563	Ciwandan	Banten	119
491	Dlanggu	Mojokerto	119
525	Driyorejo	Gresik	119
534	Dukun	Gresik	119
564	Dunex Sunter	Jakarta 	119
565	Gresik Kota	Gresik	119
566	Gresik Petrokimia	Gresik	119
568	Hwa Hok, Kawasan Industri Modern	Banten	119
570	Jemursari	Surabaya	119
577	Jl. MercedesBenz Wanaherang	Bogor	119
578	Jl. Milenium 4, Panongan	Tangerang	119
581	Jl. Rambutan	Jakarta 	119
583	Jl. Raya Serang, Kragilan	Banten	119
584	Jl. Selayar, Cikarang Barat	Bekasi	119
588	Jombang Kota	Jombang	119
589	Jombang Mojoagung	Jombang	119
569	Junti Jawilan, Serang	Banten	119
506	Kawasan Industri Delta Mas	Jakarta 	119
522	Kawasan Industri Maspion Lot	Gresik	119
474	Kedamean	Gresik	119
529	Kediri Kota	Kediri	119
533	Kejayan	Pasuruan	119
575	Kemlagi	Mojokerto	119
540	Kutorejo	Mojokerto	119
517	Lekok	Pasuruan	119
523	Lingkar Timur	Sidoarjo	119
567	Lumbang	Pasuruan	119
489	Mojoanyar	Mojokerto	119
580	Narogong	Bekasi	119
579	Narogong Cileungsi	Bogor	119
475	Pandaan 	Pasuruan	119
518	Puri	Mojokerto	119
527	Raya Pasuruan	Pasuruan	119
539	Rejoso	Pasuruan	119
490	Romokalisari	Surabaya	119
511	Sedati	Sidoarjo	119
585	Serua Raya Ciputat tanggerang	Jakarta 	119
545	Sitobondo Kota	Situbondo	119
499	Sooko	Mojokerto	119
531	Sugondo	Gresik	119
530	Sukoharjo Kota	Sukoharjo	119
526	Sumput Driyorejo	Gresik	119
516	Tambun Selatan	Bekasi	119
501	Ujung Pangkah	Gresik	119
532	Wringinanom	Gresik	119
587	Yogyakarta Kota	Yogyakarta	119
507	Jl. Budi Utomo	Timika	66
510	Ahmad Yani	Banjarmasin	45
547	Balaraja Pulo Gadung	Tangerang	119
624	Karang Asam Ulu	Samarinda	51
626	Kariangau	Balikpapan	51
628	Kp. Satu Skip	Tarakan	51
640	Paringin	Banjarmasin	51
642	Sepaku	Balikpapan	45
605	Balongpanggang	Gresik	119
681	Kampung Maruni	Manokwari	66
672	Jl. Merpati, Udin Hilir	Manokwari	79
689	Jl. Sandubaya	Lombok	19
695	Sambinae	Bima	19
649	Jl. Kerapu	Batam	53
650	Jl. Laksamana Bintan	Batam	53
635	Oku timur	Palembang	53
648	Tiban Sekupang	Batam	53
645	Tj. Pinggir Sekupang	Batam	53
482	Way Kanan	Bandar Lampung	53
621	Desa Puusanggula	Konawe Selatan	78
686	Jl. Puebonggo	Palu	78
684	Layana-Dupa	Palu	78
668	Lolak	Bitung	78
653	Maesa	Bitung	78
666	Malalayang	Bitung	78
660	Manado Ilir	Bitung	78
651	Manado Kota	Manado	78
654	Manado Mapanget	Bitung	78
657	Minahasa	Bitung	78
665	Minahasa Selatan	Bitung	78
659	Minahasa Utara	Bitung	78
682	Palu Kota	Palu	78
678	Panakkukang	Makassar	78
685	Pantoloan	Palu	78
670	Pattene Raya	Makassar	78
677	Pekkabata	Makassar	78
676	Pergudangan Lantebung	Makassar	78
674	Perintis Kemerdekaan	Makassar	78
671	Poros Barru	Makassar	78
655	Stadion Klabat	Bitung	78
692	Suwelana Poso	Palu	78
683	Tojo Una Una	Palu	78
664	Tumpaan	Bitung	78
673	Arif Rachman	Makassar	78
644	Jl. Yos Sudarso	Balikpapan	45
646	Batam Kota	Batam	53
675	Batu Putih	Makassar	78
647	Jl. Brigjend Katamso	Batam	53
690	Jl. Soekarno Hatta	Palu	78
691	Jl. Suprapto	Palu	78
687	Jl. Tolitoli	Palu	78
630	Duduk Sampeyan	Gresik	119
688	Jl. Dewi Sartika	Palu	78
662	Kauditan	Bitung	78
484	Perum Sabrina	Sorong	66
652	Gedeg	Mojokerto	119
667	Kadoodan	Bitung	78
663	Kalawat	Bitung	78
639	Karang Joang	Balikpapan	45
636	Kariangau	Balikpapan	45
641	Manggar Batakan	Balikpapan	45
694	Manismata Ketapang	Pontianak	45
643	Pertamina Mulawarman	Balikpapan	45
693	Pontianak Kota	Pontianak	45
637	Pulau Balang	Balikpapan	45
696	Sungai Raya	Pontianak	45
638	Jl. Mulawarman	Balikpapan	45
669	Gondang Wetan	Pasuruan	119
661	Jatirejo	Mojokerto	119
590	Kalimalang	Bekasi	119
591	Karawaci	Tangerang	119
592	Karawang	Jakarta 	119
596	Kawasan Industri	Tangerang	119
593	Kawasan Industri Cikarang	Cikarang	119
594	Kawasan Industri Jatake	Tangerang	119
595	Kawasan Industri Loppo	Cikarang	119
597	Kediri Wates	Kediri	119
656	Kraton	Pasuruan	119
599	Kudus Kota	Kudus	119
600	Lamongan Babat	Lamongan	119
601	Lamongan Kota	Lamongan	119
602	Madiun Kota	Madiun 	119
603	Malang Kota	Malang	119
604	Malang Lawang	Malang	119
607	Maspion Manyar	Gresik	119
608	Mauk	Tangerang	119
609	Mojokerto Kota	Mojokerto	119
610	Pabrik Besi Pulo Gadung	Jakarta 	119
611	Pabrik Nippon Paint Gresik	Gresik	119
612	Pasuruan Kota	Pasuruan	119
613	Pati Kota	Pati	119
614	Pergudangan Cikarang	Cikarang	119
615	Pluit Raya	Jakarta 	119
616	Ponorogo Kota	Ponorogo	119
617	Pulogadung	Jakarta 	119
618	Pulomas	Jakarta 	119
619	Purwodadi Kota	Purwodadi 	119
620	Rangel	Tuban	119
623	Salembaran Kosambi	Tangerang	119
622	Segoromadu	Gresik	119
625	Semarang Kota	Semarang	119
627	Setia Mekar	Bekasi	119
629	Solo Kota	Solo 	119
631	Tangerang Kota	Tangerang	119
606	Trowulan	Mojokerto	119
632	Tuban Kota	Tuban	119
633	Tulungagung Kota	Tulungagung	119
634	Ungaran Kota	Ungaran	119
680	Jl. Lembah Hijau	Manokwari	66
59	Balikpapan Kota	Balikpapan	45
658	Bangsal	Mojokerto	119
598	Benjeng	Gresik	119
699	Banda Aceh Kota	Banda Aceh	132
710	Jl. Bawean	Medan	132
716	Langsa	Banda Aceh	132
739	Batalaiworu	Raha	90
758	Bandara Hasanuddin	Makassar	78
742	Danone	Bitung	90
749	Jl. Laute	Kendari	90
751	Jl. Pattimura	Kendari	90
746	Bakaran Batu	Banda Aceh	53
794	Banda Aceh Kota	Banda Aceh	53
504	Pusat Pergudangan Palu	Palu	78
792	Site IWIP	Halmahera Tengah	46
743	Singkil	Bitung	78
730	Sorong Kota	Sorong	66
750	Jl. Kemiri	Jayapura	66
753	TPM	Makassar	78
757	Wajo	Makassar	78
735	Ruteng	Labuan Bajo	112
799	Jalan Daeng tompo	Makassar	109
800	Jl. Badak	Kendari	109
801	Jl. Bunggasi	Kendari	109
802	Jl. Gunung Merapi	Luwuk	109
701	Benua Kayong	Pontianak	45
734	Pabrik Sagu Kais Sorong	Sorong	66
732	Jl. Klamalu	Sorong	66
748	Dungingi	Gorontalo	78
756	Sidenreng Rappang	Makassar	78
738	Bulutuba	Mamuju Utara	78
769	Cemengkalang	Sidoarjo	119
791	Kalumpang	Ternate	46
733	Jl. Sapta Taruna	Sorong	66
728	Jl. Sungai Maruni	Sorong	66
754	Tamalate	Makassar	78
747	Telaga Jaya	Gorontalo	78
815	Belian	Batam	53
793	Bireuen	Banda Aceh	53
737	Ilir Barat	Palembang	53
789	Jalan Indrapura	Banda Aceh	53
745	Jaya Baru	Banda Aceh	53
787	JL Indrapura, Seout Baroh , Indrapuri	Banda Aceh	53
759	Jl. Duri	Dumai	53
816	Jl. Jawa	Medan	53
796	Langsa	Banda Aceh	53
744	Tj. Morawa	Banda Aceh	53
755	Bungoro	Makassar	78
752	Kintom	Luwuk	78
714	Bontang	Samarinda	45
697	Delta Pawan	Pontianak	45
806	Djok Mentaya	Banjarmasin	45
724	Jl. A. Wahab	Samarinda	45
700	Jl. Arteri Supandio	Pontianak	45
718	Jl. Brigjend Katamso Bontang	Samarinda	45
726	Jl. Gerbang Dayaku	Samarinda	45
803	Jl. Jenderal Sudirman	Balikpapan	45
729	Jl. Jendral Sudirman	Samarinda	45
715	Jl. Kadrie Oening	Samarinda	45
708	Jl. M Tohir Ketapang	Pontianak	45
741	Jl. Marsma R Iswahyudi	Balikpapan	45
723	Jl. P. Antasari	Samarinda	45
810	Jl. Pangeran Hidayatullah	Banjarmasin	45
808	Jl. Pramuka	Banjarmasin	45
702	Jl. Raya Wajk Hulu	Pontianak	45
717	Jl. Teuku Umar	Samarinda	45
809	Karang Anyar	Banjarbaru	45
811	Kp. Melayu Barat	Banjarmasin	45
721	Kutai Kartanegara	Samarinda	45
719	Loa Buah	Samarinda	45
707	Loh Sumber	Samarinda	45
703	Matan Hilir, Ketapang	Pontianak	45
712	Palaran	Samarinda	45
812	Paringin	Banjarmasin	45
704	Pelabuhan Ketapang	Pontianak	45
727	Penajam Paser Utara	Samarinda	45
720	Sangatta	Samarinda	45
711	Sanggau	Pontianak	45
706	Siantan Mempawah	Pontianak	45
698	Singkawang	Pontianak	45
813	Sungai Cuka	Banjarmasin	45
709	Sungai Jawi Luar	Pontianak	45
772	Bangkalan Madura	Surabaya	119
797	Jl. Dr. J. Leimena Hative Besar	Ambon	46
814	Bati-Bati	Banjarmasin	45
760	Depo MSC	Surabaya	119
761	Greges Jaya	Surabaya	119
765	Jemundo	Sidoarjo	119
782	Jl. HOS Cokroaminoto	Bojonegoro	119
788	Jl. Kemang Pratama	Bekasi	119
768	Kapten Darmo Sugondo	Gresik	119
783	Kedung	Jepara	119
764	Krikilan	Gresik	119
762	Mayjen Sungkono	Gresik	119
767	Mojokerto Mill	Mojokerto	119
781	Ngadiluwih	Kediri	119
790	Nyompok, Serang	Banten	119
736	Osowilangon	Surabaya	119
775	Paciran	Lamongan	119
779	Pare	Kediri	119
771	Rembang	Pasuruan	119
766	Semen Gresik	Gresik	119
770	Sinar Gedangan	Sidoarjo	119
777	Singosari	Malang	119
784	Sleman	Yogyakarta	119
776	Sumbergondang	Jombang	119
763	Suramadu	Surabaya	119
740	Jailolo Kota	Jailolo	46
705	Adi Sucipto	Pontianak	45
785	Bantul	Yogyakarta	119
778	Blimbing	Malang	119
827	Silae	Palu	109
866	Jl. Laute	Kendari	78
839	Turikale	Maros	109
852	Teluk Kao	Halmahera Utara	60
842	Kelimutu	Ende	112
876	Kupang Timur	Kupang	112
882	Jl. Sandubaya	Lombok	103
893	Nuha	Luwu Timur	78
857	Onewila	Konawe Selatan	78
888	Pattallassang	Gowa	78
868	Sampara/Morosi	Konawe	78
910	Silae	Palu	78
844	Sipatana	Gorontalo	78
879	Solog	Bolaang Mongondow	78
897	Somba Opu	Gowa	78
890	Tallo	Makassar	78
838	Tikala	Bitung	78
855	Tinabite	Sikeli	78
896	Ujung Tanah	Makassar	78
867	Unaaha	Konawe	78
887	Watampone	Bone	78
918	Jl. P. Suryanata	Samarinda	45
912	Bagan Batu	Pekanbaru	53
901	Nabire Barat	Nabire	66
821	Binjai	Medan	53
860	Jl. Bunggasi	Kendari	78
861	Jl. Pattimura	Kendari	78
911	Jl. Prof. M. Yamin	Palu	78
858	Jl. R. Suprapto	Kendari	78
862	Jl. Saranani	Kendari	78
851	Padang Bulan	Jayapura	66
731	Pulau Gag Distrik Waigeo Barat	Sorong	66
849	Sentani	Jayapura	66
843	Yos Sudarso	Fakfak	66
853	Bandara Kendari	Kendari	78
919	Jabon 	Sidoarjo	119
900	Mandala Muli	Merauke	66
878	Korumba	Kendari	78
845	Kota Selatan	Gorontalo	78
895	Lampoko	Barru	78
891	Malili	Luwu Timur	78
904	Girian	Bitung	18
894	Mamosalato	Morowali	78
837	Mapanget	Bitung	78
847	MT Haryono	Gorontalo	78
854	Pohara	Konawe	78
846	Pulubala	Gorontalo	78
869	Puuwatu	Kendari	78
871	Tinanggea	Konawe Selatan	78
886	Toili	Banggai	78
835	Tuminting	Bitung	78
889	Turikale	Maros	78
840	Ende Port	Ende	112
850	Entrop	Jayapura	66
834	Danone	Bitung	78
864	Kasipute	Sikeli	78
870	Rahandouna	Kendari	78
836	Tenga	Bitung	78
863	Bahodopi	Morowali	78
928	Tarau	Kepulauan Yapen	66
927	Masohi Kota	Masohi	136
874	Fatululi	Kupang	112
875	Kuanino	Kupang	112
841	Mautapaga	Ende	112
883	Labuapi	Lombok Barat	103
881	Lombok Kota	Lombok	103
832	Marada Hu u	Dompu	103
833	Mpunda	Bima	103
831	Pekat	Dompu	103
830	Sambinae	Bima	103
885	Jl. Gunung Merapi	Luwuk	78
818	Bunut Barat	Medan	53
909	Ilir Timur	Palembang	53
826	Karang Tinggi	Bengkulu	53
907	Kemuning	Palembang	53
825	Langkat	Medan	53
899	Martubung	Medan	53
898	Padang Sidempuan	Medan	53
819	Pematang Siantar	Medan	53
820	Rantau Prapat	Medan	53
915	Siak Hulu	Pekanbaru	53
823	Sibolga	Medan	53
931	Sidikalang	Medan	53
824	Simalungun	Medan	53
906	Sukarame	Palembang	53
817	Tanjung Morawa	Medan	53
908	Tebat Agung	Muara Enim	53
822	Tebing Tinggi	Medan	53
913	Ujung Batu	Pekanbaru	53
856	Batalaiworu	Raha	78
859	Jl. Badak	Kendari	78
902	Teluk Kimi	Nabire	66
848	Abepura	Jayapura	66
880	Gurabesi	Jayapura	66
923	Karang Asam Ulu	Samarinda	45
920	Loa Janan Ilir	Samarinda	45
926	Mentawa Baru Ketapang	Sampit	45
925	Sampit Kota	Sampit	45
877	Seradang	Tabalong	45
905	Simanggaris	Nunukan	45
828	Tanjung Redeb	Berau	45
829	Tanjung Selor	Bulungan	45
934	Yos Sudarso	Tarakan	45
929	Banjarsari	Surakarta	119
916	Desa Kandang	Blitar	119
903	Way Apu	Namlea	46
872	Bundaran Monyet Kumai	Kumai	45
884	Jl Raya Boja, Kaliwungu	Kendal	119
892	Kertosari	Kendal	119
922	Kota Batu Indah	Jepara	119
932	Ngawi Kota	Ngawi	119
798	Jl. DR. Setiabudi	Ambon	46
873	Bumi Harjo	Kumai	45
917	Grobogan	Semarang	119
586	Jl. STM Walang Jaya, Plumpang Semper	Jakarta 	119
187	Kalianak Gudang Panadia	Surabaya	119
949	Siawung	Barru	24
188	Kalimas Baru	Surabaya	119
930	Kawasan Industri Gresik	Gresik	119
424	Kembang Joyo 	Pati	119
576	Kopo Maja, Serang	Banten	119
780	Lengkong	Nganjuk	119
204	Osowilangun	Surabaya	119
722	Gunung Panjang	Samarinda	45
807	Jl. Gubernur Soebardjo	Banjarmasin	45
921	Jl. Kyai H. Wahab Syahranie	Samarinda	45
804	Jl. Letkol. Pol. HM. Asnawi Arbain	Balikpapan	45
208	Perumahan Darmo Indah	Surabaya	119
774	Peterongan	Jombang	119
977	Banggai Laut Kota	Banggai Laut	78
941	Baolan	Tolitoli	78
974	Bua	Luwu	78
962	Girian	Bitung	78
954	Jalan Daeng tompo	Makassar	78
865	Jl. Masjid Timur	Kendari	78
963	Dumai	Pekanbaru	53
955	Jl. Bawean	Medan	53
795	Lhokseumawe	Banda Aceh	53
953	Mabar	Medan	53
914	Perawang	Pekanbaru	53
975	Tempeh	Lumajang 	119
159	Tropodo	Sidoarjo	119
945	Tulung	Klaten	119
773	Tunggorono	Jombang	119
937	Ahmad Yani	Ternate	46
942	Labetawi	Tual	46
935	Labuha Kota	Labuha	46
940	Maba Kota	Maba	46
936	Masohi Kota	Masohi	46
966	Namlea Kota	Namlea	46
939	Oba Utara	Ternate	46
976	Obi	Obi	46
938	Teluk Kao	Halmahera Utara	46
948	Nuhon	Banggai	78
964	Siawung	Barru	78
961	Tikala	Manado	78
943	Yos Sudarso	Tual	46
950	Aimas	Sorong	66
972	Jl. Jend. Ahmad Yani, Kwamki	Timika	66
679	Jl. Merpati, Udin Hilir	Manokwari	66
933	Jl. Padat Karya	Banjarmasin	45
713	Jl. Ring Road 1	Samarinda	45
958	Kp. Satu Skip	Tarakan	45
725	Padat Karya	Samarinda	45
957	Pantai Amal	Tarakan	45
805	Pergudangan Basirih	Banjarmasin	45
520	Pergudangan Liang Anggang Industri Kecil (LIK)	Banjarmasin	45
947	Perumahan Citraland Cluster voila	Banjarmasin	45
924	Seruyan Hilir	Sampit	45
951	Klamono	Sorong	66
965	Nabire Kota	Nabire	66
967	Topoyo	Mamuju	78
960	Wenang	Bitung	78
944	Motaain	Atambua	112
959	Tanjung Selor	Berau	45
956	Tarakan Kota	Tarakan	45
969	Teuku Umar	Pontianak	45
968	Yos Sudarso	Pontianak	45
573	Bobos Duku Puntang	Cirebon	119
946	Dampit	Malang	119
429	GF K.joyo 	Semarang	119
786	Gunung Putri Lembang	Bandung	119
180	Indrapura	Surabaya	119
952	Jalan kemang soka raya Bekasi	Jakarta 	119
973	Jalan Raya Kosambi Curug	Cimahi	119
971	Jember Kota	Jember	119
582	Jl. Raya Bekasi	Bekasi	119
970	Serui	Kepulauan Yapen	66
\.


--
-- Data for Name: mst_kemasan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_kemasan (id_kemasan, nama_kemasan) FROM stdin;
1	TON
2	KG
3	LITER
4	DUS
5	UNIT
10	M3
11	SAK
12	LEMBAR
14	KOLI
16	KARTON
\.


--
-- Data for Name: mst_payment_services; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_payment_services (id_payment_services, id_platform, payment_service) FROM stdin;
PL009VA	PL009	Mandiri Virtual Account
PL010VA	PL010	BRI Virtual Account
\.


--
-- Data for Name: mst_platform; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_platform (id_platform, company, live_tracking_url, nama_platform, payment_url, tc_url, no_rekening) FROM stdin;
PL001	PT Toms Daya Integra	https://itruck.id/toms_server/api/get_container_tracking	iTruck.ID	https://itruck.id/itruck-dev1/view/doku_checkout.php?enc=[ENC_CODE]&id=[ID_REQUEST_BOOKING]	https://itruck.id/itruck_front/term_condition.php	\N
PL003	Lontar		Lontar			\N
PL005	RADAR		RADAR			\N
PL006	Digico		Digico			\N
PL007	Logisly	\N	Logisly	\N	\N	\N
PL004	Logol	https://logol.co.id:8443/LogisticOnline/OrderTracker	Logol	https://smartpay.edi-indonesia.co.id/checkout/?enc=[ENC_CODE]&id=[ID_REQUEST_BOOKING]	https://logol.co.id/marketplace/termsandconditions	\N
PL008	Waresix	\N	Waresix	\N	\N	\N
PL000	IKC	https://itruck.id/toms_server/api/get_container_tracking	IKC	https://itruck.id/itruck-dev1/view/doku_checkout.php?enc=[ENC_CODE]&id=[ID_REQUEST_BOOKING]	https://itruck.id/itruck_front/term_condition.php	\N
PL009	Bank Mandiri	\N	Bank Mandiri	\N	\N	\N
PL011	PraHu Hub	\N	PraHu Hub	\N	\N	\N
PL010	Bank BRI	\N	Bank BRI	\N	\N	\N
PL002	Clickargo		Clickargo	https://elogistic.ms.demo-qs.xyz/clickpay/checkout/[ID_REQUEST_BOOKING]		\N
\.


--
-- Data for Name: mst_platform_configuration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_platform_configuration (id, category, key, value, id_platform) FROM stdin;
\.


--
-- Data for Name: mst_platform_services; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_platform_services (id_platform_services, id_platform, id_service) FROM stdin;
1	PL009	9
2	PL010	9
\.


--
-- Data for Name: mst_port; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_port (id_port, is_asal, is_tujuan, kota, port, singkatan) FROM stdin;
1	1	1	Ambon	Ambon	AMQ
94	1	1	Tojo Una-una	Ampana	APN
67	\N	1	Halmahera Selatan	Bacan	BCN
104	\N	1	Sumbawa	Badas	
2	\N	1	Balikpapan	Balikpapan	BPN
102	\N	1	Banggai Laut	Banggai	BGG
4	\N	1	Banjarmasin	Banjarmasin	BDJ
115	\N	1	Batam	Batam	BTM
49	\N	1	Medan	Belawan	BLW
51	1	1	Berau	Berau	BEJ
79	\N	1	Biak Numfor	Biak	BIK
19	\N	1	Bima	Bima	BMU
132	\N	1	Binjai	Binjai	
90	\N	1	Bitung	Bitung	BIT
109	\N	1	Buol	Buol	UOL
60	\N	1	Dobo 	Dobo	DOB
56	\N	1	Ende	Ende	ENE
63	\N	1	Fakfak	Fakfak	FKQ
17	\N	1	Gorontalo	Gorontalo	GTO
62	\N	1	Jayapura	Jayapura	DJJ
18	\N	1	Kendari	Kendari	KDI
107	\N	1	Kolaka	Kolaka	KOL
136	1	1	Labuha	Kupal Bacan	\N
20	\N	1	Kupang	Kupang	KOE
103	\N	1	Lombok Barat	Lembar	
23	\N	1	Luwuk	Luwuk	LUW
24	\N	1	Makassar	Makassar	MAK
108	\N	1	Aceh	Malahayati	
137	1	1	Mamuju	Mamuju	MJU
64	\N	1	Manokwari	Manokwari	MKW
55	\N	1	Merauke	Merauke	MKQ
65	\N	1	Nabire	Nabire	
69	\N	1	Namlea	Namlea	
139	1	1	Raha	Nusantara Raha	RAQ
53	\N	1	Palembang	Palembang	PLM
37	\N	1	Palu	Pantoloan	PTL
54	\N	1	Pontianak	Pontianak	PNK
39	\N	1	Samarinda	Samarinda	SRI
59	\N	1	Maluku Tenggara Barat	Saumlaki	\N
44	\N	1	Sorong	Sorong	SOQ
135	1	1	Semarang	Tanjung Emas	SRG
48	1	1	Surabaya	Tanjung Perak	SUB1
57	1	1	Jakarta 	Tanjung Priok	JKT
45	\N	1	Tarakan	Tarakan	TRK
119	1	1	Surabaya	Teluk Lamong	SUB2
46	\N	1	Ternate	Ternate	TTE
66	\N	1	Timika	Timika	TMK
111	\N	1	Tolitoli	Tolitoli	TLI
78	\N	1	Wakatobi	Wanci	\N
112	\N	1	Timor Tengah Utara	Wini	\N
\.


--
-- Data for Name: mst_process_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_process_type (kd_process_type, process_name) FROM stdin;
1	Inbound
2	Outbound
3	Domestic
\.


--
-- Data for Name: mst_province; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_province (code, name) FROM stdin;
11	Aceh
12	Sumatera Utara
13	Sumatera Barat
14	Riau
15	Jambi
16	Sumatera Selatan
17	Bengkulu
18	Lampung
19	Kepulauan Bangka Belitung
21	Kepulauan Riau
31	Dki Jakarta
32	Jawa Barat
33	Jawa Tengah
34	Daerah Istimewa Yogyakarta
35	Jawa Timur
36	Banten
51	Bali
52	Nusa Tenggara Barat
53	Nusa Tenggara Timur
61	Kalimantan Barat
62	Kalimantan Tengah
63	Kalimantan Selatan
64	Kalimantan Timur
65	Kalimantan Utara
71	Sulawesi Utara
72	Sulawesi Tengah
73	Sulawesi Selatan
74	Sulawesi Tenggara
75	Gorontalo
76	Sulawesi Barat
81	Maluku
82	Maluku Utara
91	Papua
92	Papua Barat
\.


--
-- Data for Name: mst_regency; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_regency (code, name, province_code) FROM stdin;
1101	Kab. Aceh Selatan	11
1102	Kab. Aceh Tenggara	11
1103	Kab. Aceh Timur	11
1104	Kab. Aceh Tengah	11
1105	Kab. Aceh Barat	11
1106	Kab. Aceh Besar	11
1107	Kab. Pidie	11
1108	Kab. Aceh Utara	11
1109	Kab. Simeulue	11
1110	Kab. Aceh Singkil	11
1111	Kab. Bireuen	11
1112	Kab. Aceh Barat Daya	11
1113	Kab. Gayo Lues	11
1114	Kab. Aceh Jaya	11
1115	Kab. Nagan Raya	11
1116	Kab. Aceh Tamiang	11
1117	Kab. Bener Meriah	11
1118	Kab. Pidie Jaya	11
1171	Kota Banda Aceh	11
1172	Kota Sabang	11
1173	Kota Lhokseumawe	11
1174	Kota Langsa	11
1175	Kota Subulussalam	11
1201	Kab. Tapanuli Tengah	12
1202	Kab. Tapanuli Utara	12
1203	Kab. Tapanuli Selatan	12
1204	Kab. Nias	12
1205	Kab. Langkat	12
1206	Kab. Karo	12
1207	Kab. Deli Serdang	12
1208	Kab. Simalungun	12
1209	Kab. Asahan	12
1210	Kab. Labuhanbatu	12
1211	Kab. Dairi	12
1212	Kab. Toba Samosir	12
1213	Kab. Mandailing Natal	12
1214	Kab. Nias Selatan	12
1215	Kab. Pakpak Bharat	12
1216	Kab. Humbang Hasundutan	12
1217	Kab. Samosir	12
1218	Kab. Serdang Bedagai	12
1219	Kab. Batu Bara	12
1220	Kab. Padang Lawas Utara	12
1221	Kab. Padang Lawas	12
1222	Kab. Labuhanbatu Selatan	12
1223	Kab. Labuhanbatu Utara	12
1224	Kab. Nias Utara	12
1225	Kab. Nias Barat	12
1271	Kota Medan	12
1272	Kota Pematangsiantar	12
1273	Kota Sibolga	12
1274	Kota Tanjung Balai	12
1275	Kota Binjai	12
1276	Kota Tebing Tinggi	12
1277	Kota Padangsidimpuan	12
1278	Kota Gunungsitoli	12
1301	Kab. Pesisir Selatan	13
1302	Kab. Solok	13
1303	Kab. Sijunjung	13
1304	Kab. Tanah Datar	13
1305	Kab. Padang Pariaman	13
1306	Kab. Agam	13
1307	Kab. Lima Puluh Kota	13
1308	Kab. Pasaman	13
1309	Kab. Kepulauan Mentawai	13
1310	Kab. Dharmasraya	13
1311	Kab. Solok Selatan	13
1312	Kab. Pasaman Barat	13
1371	Kota Padang	13
1372	Kota Solok	13
1373	Kota Sawahlunto	13
1374	Kota Padang Panjang	13
1375	Kota Bukittinggi	13
1376	Kota Payakumbuh	13
1377	Kota Pariaman	13
1401	Kab. Kampar	14
1402	Kab. Indragiri Hulu	14
1403	Kab. Bengkalis	14
1404	Kab. Indragiri Hilir	14
1405	Kab. Pelalawan	14
1406	Kab. Rokan Hulu	14
1407	Kab. Rokan Hilir	14
1408	Kab. Siak	14
1409	Kab. Kuantan Singingi	14
1410	Kab. Kepulauan Meranti	14
1471	Kota Pekanbaru	14
1472	Kota Dumai	14
1501	Kab. Kerinci	15
1502	Kab. Merangin	15
1503	Kab. Sarolangun	15
1504	Kab. Batanghari	15
1505	Kab. Muaro Jambi	15
1506	Kab. Tanjung Jabung Barat	15
1507	Kab. Tanjung Jabung Timur	15
1508	Kab. Bungo	15
1509	Kab. Tebo	15
1571	Kota Jambi	15
1572	Kota Sungai Penuh	15
1601	Kab. Ogan Komering Ulu	16
1602	Kab. Ogan Komering Ilir	16
1603	Kab. Muara Enim	16
1604	Kab. Lahat	16
1605	Kab. Musi Rawas	16
1606	Kab. Musi Banyuasin	16
1607	Kab. Banyuasin	16
1608	Kab. Ogan Komering Ulu Timur	16
1609	Kab. Ogan Komering Ulu Selatan	16
1610	Kab. Ogan Ilir	16
1611	Kab. Empat Lawang	16
1612	Kab. Penukal Abab Lematang Ilir	16
1613	Kab. Musi Rawas Utara	16
1671	Kota Palembang	16
1672	Kota Pagar Alam	16
1673	Kota Lubuk Linggau	16
1674	Kota Prabumulih	16
1701	Kab. Bengkulu Selatan	17
1702	Kab. Rejang Lebong	17
1703	Kab. Bengkulu Utara	17
1704	Kab. Kaur	17
1705	Kab. Seluma	17
1706	Kab. Muko Muko	17
1707	Kab. Lebong	17
1708	Kab. Kepahiang	17
1709	Kab. Bengkulu Tengah	17
1771	Kota Bengkulu	17
1801	Kab. Lampung Selatan	18
1802	Kab. Lampung Tengah	18
1803	Kab. Lampung Utara	18
1804	Kab. Lampung Barat	18
1805	Kab. Tulang Bawang	18
1806	Kab. Tanggamus	18
1807	Kab. Lampung Timur	18
1808	Kab. Way Kanan	18
1809	Kab. Pesawaran	18
1810	Kab. Pringsewu	18
1811	Kab. Mesuji	18
1812	Kab. Tulang Bawang Barat	18
1813	Kab. Pesisir Barat	18
1871	Kota Bandar Lampung	18
1872	Kota Metro	18
1901	Kab. Bangka	19
1902	Kab. Belitung	19
1903	Kab. Bangka Selatan	19
1904	Kab. Bangka Tengah	19
1905	Kab. Bangka Barat	19
1906	Kab. Belitung Timur	19
1971	Kota Pangkal Pinang	19
2101	Kab. Bintan	21
2102	Kab. Karimun	21
2103	Kab. Natuna	21
2104	Kab. Lingga	21
2105	Kab. Kepulauan Anambas	21
2171	Kota Batam	21
2172	Kota Tanjung Pinang	21
3101	Kab. Adm. Kep. Seribu	31
3171	Kota Adm. Jakarta Pusat	31
3172	Kota Adm. Jakarta Utara	31
3173	Kota Adm. Jakarta Barat	31
3174	Kota Adm. Jakarta Selatan	31
3175	Kota Adm. Jakarta Timur	31
3201	Kab. Bogor	32
3202	Kab. Sukabumi	32
3203	Kab. Cianjur	32
3204	Kab. Bandung	32
3205	Kab. Garut	32
3206	Kab. Tasikmalaya	32
3207	Kab. Ciamis	32
3208	Kab. Kuningan	32
3209	Kab. Cirebon	32
3210	Kab. Majalengka	32
3211	Kab. Sumedang	32
3212	Kab. Indramayu	32
3213	Kab. Subang	32
3214	Kab. Purwakarta	32
3215	Kab. Karawang	32
3216	Kab. Bekasi	32
3217	Kab. Bandung Barat	32
3218	Kab. Pangandaran	32
3271	Kota Bogor	32
3272	Kota Sukabumi	32
3273	Kota Bandung	32
3274	Kota Cirebon	32
3275	Kota Bekasi	32
3276	Kota Depok	32
3277	Kota Cimahi	32
3278	Kota Tasikmalaya	32
3279	Kota Banjar	32
3301	Kab. Cilacap	33
3302	Kab. Banyumas	33
3303	Kab. Purbalingga	33
3304	Kab. Banjarnegara	33
3305	Kab. Kebumen	33
3306	Kab. Purworejo	33
3307	Kab. Wonosobo	33
3308	Kab. Magelang	33
3309	Kab. Boyolali	33
3310	Kab. Klaten	33
3311	Kab. Sukoharjo	33
3312	Kab. Wonogiri	33
3313	Kab. Karanganyar	33
3314	Kab. Sragen	33
3315	Kab. Grobogan	33
3316	Kab. Blora	33
3317	Kab. Rembang	33
3318	Kab. Pati	33
3319	Kab. Kudus	33
3320	Kab. Jepara	33
3321	Kab. Demak	33
3322	Kab. Semarang	33
3323	Kab. Temanggung	33
3324	Kab. Kendal	33
3325	Kab. Batang	33
3326	Kab. Pekalongan	33
3327	Kab. Pemalang	33
3328	Kab. Tegal	33
3329	Kab. Brebes	33
3371	Kota Magelang	33
3372	Kota Surakarta	33
3373	Kota Salatiga	33
3374	Kota Semarang	33
3375	Kota Pekalongan	33
3376	Kota Tegal	33
3401	Kab. Kulon Progo	34
3402	Kab. Bantul	34
3403	Kab. Gunungkidul	34
3404	Kab. Sleman	34
3471	Kota Yogyakarta	34
3501	Kab. Pacitan	35
3502	Kab. Ponorogo	35
3503	Kab. Trenggalek	35
3504	Kab. Tulungagung	35
3505	Kab. Blitar	35
3506	Kab. Kediri	35
3507	Kab. Malang	35
3508	Kab. Lumajang	35
3509	Kab. Jember	35
3510	Kab. Banyuwangi	35
3511	Kab. Bondowoso	35
3512	Kab. Situbondo	35
3513	Kab. Probolinggo	35
3514	Kab. Pasuruan	35
3515	Kab. Sidoarjo	35
3516	Kab. Mojokerto	35
3517	Kab. Jombang	35
3518	Kab. Nganjuk	35
3519	Kab. Madiun	35
3520	Kab. Magetan	35
3521	Kab. Ngawi	35
3522	Kab. Bojonegoro	35
3523	Kab. Tuban	35
3524	Kab. Lamongan	35
3525	Kab. Gresik	35
3526	Kab. Bangkalan	35
3527	Kab. Sampang	35
3528	Kab. Pamekasan	35
3529	Kab. Sumenep	35
3571	Kota Kediri	35
3572	Kota Blitar	35
3573	Kota Malang	35
3574	Kota Probolinggo	35
3575	Kota Pasuruan	35
3576	Kota Mojokerto	35
3577	Kota Madiun	35
3578	Kota Surabaya	35
3579	Kota Batu	35
3601	Kab. Pandeglang	36
3602	Kab. Lebak	36
3603	Kab. Tangerang	36
3604	Kab. Serang	36
3671	Kota Tangerang	36
3672	Kota Cilegon	36
3673	Kota Serang	36
3674	Kota Tangerang Selatan	36
5101	Kab. Jembrana	51
5102	Kab. Tabanan	51
5103	Kab. Badung	51
5104	Kab. Gianyar	51
5105	Kab. Klungkung	51
5106	Kab. Bangli	51
5107	Kab. Karangasem	51
5108	Kab. Buleleng	51
5171	Kota Denpasar	51
5201	Kab. Lombok Barat	52
5202	Kab. Lombok Tengah	52
5203	Kab. Lombok Timur	52
5204	Kab. Sumbawa	52
5205	Kab. Dompu	52
5206	Kab. Bima	52
5207	Kab. Sumbawa Barat	52
5208	Kab. Lombok Utara	52
5271	Kota Mataram	52
5272	Kota Bima	52
5301	Kab. Kupang	53
5302	Kab Timor Tengah Selatan	53
5303	Kab. Timor Tengah Utara	53
5304	Kab. Belu	53
5305	Kab. Alor	53
5306	Kab. Flores Timur	53
5307	Kab. Sikka	53
5308	Kab. Ende	53
5309	Kab. Ngada	53
5310	Kab. Manggarai	53
5311	Kab. Sumba Timur	53
5312	Kab. Sumba Barat	53
5313	Kab. Lembata	53
5314	Kab. Rote Ndao	53
5315	Kab. Manggarai Barat	53
5316	Kab. Nagekeo	53
5317	Kab. Sumba Tengah	53
5318	Kab. Sumba Barat Daya	53
5319	Kab. Manggarai Timur	53
5320	Kab. Sabu Raijua	53
5321	Kab. Malaka	53
5371	Kota Kupang	53
6101	Kab. Sambas	61
6102	Kab. Mempawah	61
6103	Kab. Sanggau	61
6104	Kab. Ketapang	61
6105	Kab. Sintang	61
6106	Kab. Kapuas Hulu	61
6107	Kab. Bengkayang	61
6108	Kab. Landak	61
6109	Kab. Sekadau	61
6110	Kab. Melawi	61
6111	Kab. Kayong Utara	61
6112	Kab. Kubu Raya	61
6171	Kota Pontianak	61
6172	Kota Singkawang	61
6201	Kab. Kotawaringin Barat	62
6202	Kab. Kotawaringin Timur	62
6203	Kab. Kapuas	62
6204	Kab. Barito Selatan	62
6205	Kab. Barito Utara	62
6206	Kab. Katingan	62
6207	Kab. Seruyan	62
6208	Kab. Sukamara	62
6209	Kab. Lamandau	62
6210	Kab. Gunung Mas	62
6211	Kab. Pulang Pisau	62
6212	Kab. Murung Raya	62
6213	Kab. Barito Timur	62
6271	Kota Palangkaraya	62
6301	Kab. Tanah Laut	63
6302	Kab. Kotabaru	63
6303	Kab. Banjar	63
6304	Kab. Barito Kuala	63
6305	Kab. Tapin	63
6306	Kab. Hulu Sungai Selatan	63
6307	Kab. Hulu Sungai Tengah	63
6308	Kab. Hulu Sungai Utara	63
6309	Kab. Tabalong	63
6310	Kab. Tanah Bumbu	63
6311	Kab. Balangan	63
6371	Kota Banjarmasin	63
6372	Kota Banjarbaru	63
6401	Kab. Paser	64
6402	Kab. Kutai Kartanegara	64
6403	Kab. Berau	64
6407	Kab. Kutai Barat	64
6408	Kab. Kutai Timur	64
6409	Kab. Penajam Paser Utara	64
6411	Kab. Mahakam Ulu	64
6471	Kota Balikpapan	64
6472	Kota Samarinda	64
6474	Kota Bontang	64
6501	Kab. Bulungan	65
6502	Kab. Malinau	65
6503	Kab. Nunukan	65
6504	Kab. Tana Tidung	65
6571	Kota Tarakan	65
7101	Kab. Bolaang Mongondow	71
7102	Kab. Minahasa	71
7103	Kab. Kepulauan Sangihe	71
7104	Kab. Kepulauan Talaud	71
7105	Kab. Minahasa Selatan	71
7106	Kab. Minahasa Utara	71
7107	Kab. Minahasa Tenggara	71
7108	Kab. Bolaang Mongondow Utara	71
7109	Kab. Kep. Siau Tagulandang Biaro	71
7110	Kab. Bolaang Mongondow Timur	71
7111	Kab. Bolaang Mongondow Selatan	71
7171	Kota Manado	71
7172	Kota Bitung	71
7173	Kota Tomohon	71
7174	Kota Kotamobagu	71
7201	Kab. Banggai	72
7202	Kab. Poso	72
7203	Kab. Donggala	72
7204	Kab. Toli Toli	72
7205	Kab. Buol	72
7206	Kab. Morowali	72
7207	Kab. Banggai Kepulauan	72
7208	Kab. Parigi Moutong	72
7209	Kab. Tojo Una Una	72
7210	Kab. Sigi	72
7211	Kab. Banggai Laut	72
7212	Kab. Morowali Utara	72
7271	Kota Palu	72
7301	Kab. Kepulauan Selayar	73
7302	Kab. Bulukumba	73
7303	Kab. Bantaeng	73
7304	Kab. Jeneponto	73
7305	Kab. Takalar	73
7306	Kab. Gowa	73
7307	Kab. Sinjai	73
7308	Kab. Bone	73
7309	Kab. Maros	73
7310	Kab. Pangkajene Kepulauan	73
7311	Kab. Barru	73
7312	Kab. Soppeng	73
7313	Kab. Wajo	73
7314	Kab. Sidenreng Rappang	73
7315	Kab. Pinrang	73
7316	Kab. Enrekang	73
7317	Kab. Luwu	73
7318	Kab. Tana Toraja	73
7322	Kab. Luwu Utara	73
7324	Kab. Luwu Timur	73
7326	Kab. Toraja Utara	73
7371	Kota Makassar	73
7372	Kota Pare Pare	73
7373	Kota Palopo	73
7401	Kab. Kolaka	74
7402	Kab. Konawe	74
7403	Kab. Muna	74
7404	Kab. Buton	74
7405	Kab. Konawe Selatan	74
7406	Kab. Bombana	74
7407	Kab. Wakatobi	74
7408	Kab. Kolaka Utara	74
7409	Kab. Konawe Utara	74
7410	Kab. Buton Utara	74
7411	Kab. Kolaka Timur	74
7412	Kab. Konawe Kepulauan	74
7413	Kab. Muna Barat	74
7414	Kab. Buton Tengah	74
7415	Kab. Buton Selatan	74
7471	Kota Kendari	74
7472	Kota Bau Bau	74
7501	Kab. Gorontalo	75
7502	Kab. Boalemo	75
7503	Kab. Bone Bolango	75
7504	Kab. Pahuwato	75
7505	Kab. Gorontalo Utara	75
7571	Kota Gorontalo	75
7601	Kab. Mamuju Utara	76
7602	Kab. Mamuju	76
7603	Kab. Mamasa	76
7604	Kab. Polewali Mandar	76
7605	Kab. Majene	76
7606	Kab. Mamuju Tengah	76
8101	Kab. Maluku Tengah	81
8102	Kab. Maluku Tenggara	81
8103	Kab. Maluku Tenggara Barat	81
8104	Kab. Buru	81
8105	Kab. Seram Bagian Timur	81
8106	Kab. Seram Bagian Barat	81
8107	Kab. Kepulauan Aru	81
8108	Kab. Maluku Barat Daya	81
8109	Kab. Buru Selatan	81
8171	Kota Ambon	81
8172	Kota Tual	81
8201	Kab. Halmahera Barat	82
8202	Kab. Halmahera Tengah	82
8203	Kab. Halmahera Utara	82
8204	Kab. Halmahera Selatan	82
8205	Kab. Kepulauan Sula	82
8206	Kab. Halmahera Timur	82
8207	Kab. Pulau Morotai	82
8208	Kab. Pulau Taliabu	82
8271	Kota Ternate	82
8272	Kota Tidore Kepulauan	82
9101	Kab. Merauke	91
9102	Kab. Jayawijaya	91
9103	Kab. Jayapura	91
9104	Kab. Nabire	91
9105	Kab. Kepulauan Yapen	91
9106	Kab. Biak Numfor	91
9107	Kab. Puncak Jaya	91
9108	Kab. Paniai	91
9109	Kab. Mimika	91
9110	Kab. Sarmi	91
9111	Kab. Keerom	91
9112	Kab. Pegunungan Bintang	91
9113	Kab. Yahukimo	91
9114	Kab. Tolikara	91
9115	Kab. Waropen	91
9116	Kab. Boven Digoel	91
9117	Kab. Mappi	91
9118	Kab. Asmat	91
9119	Kab. Supiori	91
9120	Kab. Mamberamo Raya	91
9121	Kab. Mamberamo Tengah	91
9122	Kab. Yalimo	91
9123	Kab. Lanny Jaya	91
9124	Kab. Nduga	91
9125	Kab. Puncak	91
9126	Kab. Dogiyai	91
9127	Kab. Intan Jaya	91
9128	Kab. Deiyai	91
9171	Kota Jayapura	91
9201	Kab. Sorong	92
9202	Kab. Manokwari	92
9203	Kab. Fak Fak	92
9204	Kab. Sorong Selatan	92
9205	Kab. Raja Ampat	92
9206	Kab. Teluk Bintuni	92
9207	Kab. Teluk Wondama	92
9208	Kab. Kaimana	92
9209	Kab. Tambrauw	92
9210	Kab. Maybrat	92
9211	Kab. Manokwari Selatan	92
9212	Kab. Pegunungan Arfak	92
9271	Kota Sorong	92
\.


--
-- Data for Name: mst_services; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mst_services (id_service, service_name) FROM stdin;
1	DO
2	SP2
3	Trucking
4	Domestic Trucking
5	Warehouse
6	International Vessel
7	Domestic Vessel
8	Depo
9	Payment
\.


--
-- Data for Name: payment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payment (virtual_account, paid_date, price, status) FROM stdin;
\.


--
-- Data for Name: payment_billing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payment_billing (billing_id, amount, created_date, id_service, item_info, module, module_unique_id, paid_date, payment_via, service_unique_id, id_platform) FROM stdin;
16	7000000.00	2020-08-07 13:09:19.674	1	Container 	DO	3asdj83ad020as0kal	\N	\N	3asdj83ad020as0kal	PL002
17	4000000.00	2020-08-07 13:11:47.123	1	Container 	DO	3asdj83ad020as0kal	\N	\N	3asdj83ad020as0kal	PL002
18	4000000.00	2020-08-07 13:14:18.499	1	Container 	DO	3asdj83ad020as0kal	\N	\N	3asdj83ad020as0kal	PL002
19	4000000.00	2020-08-07 13:15:28.648	1	Container 	DO	3asdj83ad020as0kal	\N	\N	3asdj83ad020as0kal	PL002
20	3000000.00	2020-08-07 13:16:09.357	1	Container 	DO	3asdj83ad020as0kal	\N	\N	3asdj83ad020as0kal	PL002
21	7000000.00	2020-08-07 13:17:27.781	1	Container 1234567	DO	jsjs	\N	\N	jsjs	PL002
22	7000000.00	2020-08-07 13:18:26.519	1	Container 1234567	DO	jsjs	\N	\N	jsjs	PL002
23	8000000.00	2020-08-07 13:37:08.082	2	Container BL001X	Documents	16	\N	\N	2aisjd92u091djkas	PL002
24	7000000.00	2020-08-07 13:37:22.831	1	Container 1234567	DO	ksiwu288a83ad020as0kal	\N	\N	ksiwu288a83ad020as0kal	PL002
26	1353750.00	2020-08-08 15:20:44.881	3	Container FSCU4855805	Documents	64	\N	\N	62a84586-ebda-44d8-bb19-70adfcae2b3e2	PL001
34	2555814.00	2020-08-14 21:38:19.984	3	Container EISU2222223,EGHU3290837	Documents	82	\N	\N	1ec0c1fe-6b84-44fe-924e-6d416f35f04d3	PL002
28	1638987.00	2020-08-10 21:54:58.071	3	Container WHSU5363638	Documents	67	\N	\N	7ae4ce09-ac66-424f-a0dd-a4f36b06fb972	PL001
29	783750.00	2020-08-11 11:08:30.912	3	Container EISU2222223	Documents	75	\N	\N	4be5798d-5de5-4422-ad0b-a5081a5d75082	PL001
27	1400000.00	2020-08-11 15:14:01.808	3	Container ISMU3006513	Documents	65	\N	\N	7b60cd9f-bb76-4469-a0f0-2bd090349edd3	PL004
31	7000000.00	2020-08-11 17:52:10.254	1	Container 1234567	DO	ka9289sadjisiud8as0kalRN	\N	\N	ka9289sadjisiud8as0kalRN	PL002
25	8000000.00	2020-08-11 19:33:52.916	2	Container SUKA2365485	Documents	58	\N	\N	cb2be39c-3410-44ea-a34f-424aff83b6ab	PL002
32	7000000.00	2020-08-11 20:39:53.118	1	Container 1234567	DO	aus83adjisiud8as0kal	\N	\N	aus83adjisiud8as0kal	PL002
30	783750.00	2020-08-11 23:01:30.91	3	Container CAIU4439082	Documents	77	\N	\N	074008a8-73b0-4f10-a9af-2f79a8f7cf392	PL001
33	2256250.00	2020-08-13 12:59:07.351	3	Container NYKU3462589	Documents	79	\N	\N	13a77a62-81c5-4aef-a0cc-f9fe4da444132	PL001
35	2711532.00	2020-08-17 20:27:10.96	3	Container TCNU5064786	Documents	81	\N	\N	50090798-dfbd-4518-8a84-78a10397747b3	PL002
\.


--
-- Data for Name: payment_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payment_detail (id_payment_detail, price) FROM stdin;
\.


--
-- Data for Name: payment_invoice; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payment_invoice (order_number, close_period, costumer_name, costumer_number, created_date, currency_code, id_payment_services, invoice_account_number, module, module_unique_id, open_period, order_amount, status, unique_id, updated_date, va_created) FROM stdin;
\.


--
-- Data for Name: payment_invoice_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payment_invoice_item (invoice_item_id, amount, id_platform, id_service, item_info, quantity, service_unique_id, order_number) FROM stdin;
\.


--
-- Data for Name: td_document; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.td_document (id_document, bl_date, bl_no, document_date, document_no, document_status, kd_document_type, npwp_cargo_owner, finish_depo, finish_do, finish_sp2, finish_trucking, finish_warehouse, nm_cargoowner, no_sppb, tanggal_sppb) FROM stdin;
4	2020-06-30	12345678	2020-06-20	123456	DO	1	123456789	0	0	0	0	0	\N	\N	\N
27	2020-06-27	234567	\N			2	018245894059000	0	0	0	0	0	\N	\N	\N
33	2020-06-30	12345678	2020-06-20	123456	DO	1	123456789	0	0	0	0	0	\N	\N	\N
58	2020-01-06	SAB139732JKR	2020-08-08	555555	SPPB	1	018245894059000	0	0	0	0	0	PT. NAGASE IMPOR-EKSPOR INDONESIA	\N	\N
76	2020-07-17	CKCOKAN0008792	2020-07-28	335290	Gate Out	1	018245894059000	0	0	0	0	0	PT. NAGASE IMPOR EKSPOR INDONESIA	335665/KPU.01/2020	2020-07-28
3	2020-06-30	12345678	2020-06-20	123456	DO	1	123456789	1	1	0	0	0	\N	\N	\N
36	2020-07-09	123	\N			1	018245894059000	0	0	0	0	0	\N	\N	\N
37	2020-06-03	HJBI-022-033-20JP	2020-06-20	654321	DO	1	1234567890	0	0	0	0	0	\N	\N	\N
39	2020-06-13	HDMUSGWB0585372	\N			1	018245894059000	0	0	0	0	0	\N	\N	\N
40	2020-06-30	123456789	\N	123456	DO	1	122	0	0	0	0	0	\N	\N	\N
41	2020-07-01	5555	2020-07-01	080808	DO	1	122	0	0	0	0	0	\N	\N	\N
42	\N	\N	\N	\N	\N	1	018245894059000	0	0	0	0	0	\N	\N	\N
43	\N	\N	\N	\N	\N	1	018245894059000	0	0	0	0	0	\N	\N	\N
44	\N	\N	\N	\N	\N	1	018245894059000	0	0	0	0	0	\N	\N	\N
67	2020-07-05	JKTB4224102	2020-07-15	316750	SPPB	1	318194040413000	0	0	0	0	0	PT MICS STEEL INDONESIA	317142/KPU.01/2020	2020-07-15
46	2020-07-08	test	\N			1	018245894059000	0	0	0	0	0	\N	\N	\N
49	2020-06-27	HDMUSGWB0586694	\N			1	018245894059000	0	0	0	0	0	\N	\N	\N
68	2020-07-30	771081120684	2020-08-05	208928	SPPB	1	018245894059000	0	0	0	0	0	PT. NAGASE IMPOR-EKSPOR INDONESIA	204768/KPU.03/2020	2020-08-05
53	2020-06-03	KBJKT-022-033-20JP	2020-06-20	123456	DO	1	1234567890	0	0	0	0	0	PT. NAGASE IMPOR-EKSPOR INDONESIA	\N	\N
50	2020-07-02	ONEYTY8AM2768800	\N			1	018245894059000	0	0	0	0	0	\N	\N	\N
52	2020-08-03	PLNS-023-044-20PQ	2020-08-20	987261	DO	1	1234567890	0	0	0	0	0	\N	\N	\N
78	2020-07-05	CSJKT20070046/04	2020-07-28	335277	SPPB	1	018245894059000	0	0	0	0	0	PT. NAGASE IMPOR EKSPOR INDONESIA	335652/KPU.01/2020	2020-07-28
65	2020-07-26	02PENJKT2007016	2020-08-06	348230	SPPB	1	018245894059000	0	0	0	0	0	PT. NAGASE IMPOR-EKSPOR INDONESIA	348410/KPU.01/2020	2020-08-06
79	2020-07-28	ONEYTYOA66092900	\N			1	010711521052000	0	0	0	0	0	\N	\N	\N
80	2020-08-09	ONEYMNLA19207400	\N			1	020053708056000	0	0	0	0	0	\N	\N	\N
81	2020-08-03	HDMUSGWB0589660	2020-08-05	346602	Gate Out	1	018245894059000	0	0	0	0	0	PT. NAGASE IMPOR-EKSPOR INDONESIA	346772/KPU.01/2020	2020-08-05
82	2020-07-19	EGLV003090915504	2020-07-28	335291	SPPB	1	018245894059000	0	0	0	0	0	PT. NAGASE IMPOR EKSPOR INDONESIA	335666/KPU.01/2020	2020-07-28
83	2020-07-27	CKCOSHA2038436	2020-08-03	340661	Gate Out	1	013363882415000	0	0	0	0	0	PT. ALKINDO MITRARAYA	341106/KPU.01/2020	2020-08-03
84	2020-08-10	KLSGN200846	2020-08-13	216831	SPPB	1	032779621063000	0	0	0	0	0	PT. MISUMI INDONESIA	212483/KPU.03/2020	2020-08-13
85	2020-05-23	MFS00298950	2020-06-30	068148	Gate Out	1	022525133906000	0	0	0	0	0	PT. MERAH TEKNIK INDONESIA	069221/WBC.11/KPP.MP.01/2020	2020-07-03
86	2020-05-11	PKG0199436/001	2020-05-18	239627	SPPB	1	020419602062000	0	0	0	0	0	PT. TRAVIRA AIR	240183/KPU.01/2020	2020-05-18
87	2020-08-07	ONEYLCBA14752600	2020-08-12	358694	Gate Out	1	010716553052000	0	0	0	0	0	PT. AVERY DENNISON INDONESIA	358857/KPU.01/2020	2020-08-12
88	2020-08-09	MNLAST64160	\N			1	020053708056000	0	0	0	0	0	\N	\N	\N
89	2020-07-29	597823337	2020-08-13	084649	SPPB	1	012515995941000	0	0	0	0	0	PT. GIDION JAYA	084642/WBC.11/KPP.MP.01/2020	2020-08-13
90	2020-08-01	aaa	\N			1	012345678912	0	0	0	0	0	\N	\N	\N
91	2020-07-24	581816338	2020-08-03	341980	Gate Out	1	828219105047000	0	0	0	0	0	PT. CITRA PERKAKAS INDONESIA	342309/KPU.01/2020	2020-08-03
92	2020-08-03	GXSAG20076277	2020-08-13	085317	Gate Out	1	022525133906000	0	0	0	0	0	PT. MERAH TEKNIK INDONESIA	085499/WBC.11/KPP.MP.01/2020	2020-08-14
\.


--
-- Data for Name: td_user_document; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.td_user_document (id_user_document, id_document, npwp, access_depo, access_do, access_domestic, access_sp2, access_trucking, access_warehouse, is_ppjk, name) FROM stdin;
115	67	318194040413000	1	1	1	1	1	1	0	PT MICS STEEL INDONESIA
116	67	019926575043000	1	1	1	1	1	1	1	PT. GEMILANG LAJU SEJAHTERA
117	68	029142445012000	1	1	1	1	1	1	1	PT. FEDEX EXPRESS INTERNATIONAL
118	68	018245894059000	1	1	1	1	1	1	0	PT. NAGASE IMPOR-EKSPOR INDONESIA
2	3	123456789	1	1	1	1	1	1	0	\N
3	4	12345678	1	1	1	1	1	1	0	\N
133	76	013166038046000	1	1	1	1	1	1	1	PT.AGUNG RAYA
134	76	018245894059000	1	1	1	1	1	1	0	PT. NAGASE IMPOR EKSPOR INDONESIA
137	78	018245894059000	1	1	1	1	1	1	0	PT. NAGASE IMPOR EKSPOR INDONESIA
138	78	018770719042000	1	1	1	1	1	1	1	PT ANUGERAH AGUNG ABADI LOGISTICS
139	79	019926575043000	1	1	1	1	1	1	1	\N
140	79	010711521052000	1	1	1	1	1	1	0	\N
141	80	020053708056000	1	1	1	1	1	1	0	\N
142	80	020404224435000	1	1	1	1	1	1	1	\N
143	81	018245894059000	1	1	1	1	1	1	0	PT. NAGASE IMPOR-EKSPOR INDONESIA
144	81	025046657042000	1	1	1	1	1	1	1	PT. ANUGRAH VISTA PRATAMA
145	82	013166038046000	1	1	1	1	1	1	1	PT.AGUNG RAYA
146	82	018245894059000	1	1	1	1	1	1	0	PT. NAGASE IMPOR EKSPOR INDONESIA
147	83	013363882415000	1	1	1	1	1	1	0	PT. ALKINDO MITRARAYA
148	83	null	1	1	1	1	1	1	1	null
149	84	016700916402000	1	1	1	1	1	1	1	PT. KARGO LINTAS ANGKASA
150	84	032779621063000	1	1	1	1	1	1	0	PT. MISUMI INDONESIA
151	85	022525133906000	1	1	1	1	1	1	0	PT. MERAH TEKNIK INDONESIA
152	85	021138359614000	1	1	1	1	1	1	1	PT.WIDARTA PRATAMA MANDIRI
153	86	null	1	1	1	1	1	1	1	null
154	86	020419602062000	1	1	1	1	1	1	0	PT. TRAVIRA AIR
155	87	015425481041000	1	1	1	1	1	1	1	PT. GREEN AIR PACIFIC
156	87	010716553052000	1	1	1	1	1	1	0	PT. AVERY DENNISON INDONESIA
157	88	020404224435000	1	1	1	1	1	1	1	\N
158	88	020053708056000	1	1	1	1	1	1	0	\N
159	89	012515995941000	1	1	1	1	1	1	0	PT. GIDION JAYA
160	89	015714116618001	1	1	1	1	1	1	1	PT. AGILITY
48	27	1	1	1	1	1	1	1	1	\N
161	90	FF Demo	1	1	1	1	1	1	1	\N
60	33	123456789	1	1	1	1	1	1	0	\N
66	37	1234567890	1	1	1	1	1	1	0	\N
71	41	122	1	1	1	1	1	1	0	\N
72	41	123	1	1	1	1	1	1	0	\N
162	90	012345678912	1	1	1	1	1	1	0	\N
163	91	828219105047000	1	1	1	1	1	1	0	PT. CITRA PERKAKAS INDONESIA
164	91	027477611048000	1	1	1	1	1	1	1	PT. PARAMAS PERMATA
165	92	021138359614000	1	1	1	1	1	1	1	PT.WIDARTA PRATAMA MANDIRI
166	92	022525133906000	1	1	1	1	1	1	0	PT. MERAH TEKNIK INDONESIA
97	53	1234567890	1	1	1	1	1	1	0	cek
111	65	025046657042000	1	1	1	1	1	1	1	PT. ANUGRAH VISTA PRATAMA
112	65	018245894059000	1	1	1	1	1	1	0	PT. NAGASE IMPOR-EKSPOR INDONESIA
\.


--
-- Name: seq_container_type; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_container_type', 9, true);


--
-- Name: seq_detil_data_pelimpahan; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_detil_data_pelimpahan', 31, true);


--
-- Name: seq_document_do_container; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_document_do_container', 12, true);


--
-- Name: seq_document_do_log; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_document_do_log', 43, true);


--
-- Name: seq_document_sp2_container; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_document_sp2_container', 19, true);


--
-- Name: seq_document_sp2_log; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_document_sp2_log', 67, true);


--
-- Name: seq_document_trucking_container; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_document_trucking_container', 972, true);


--
-- Name: seq_document_trucking_destination; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_document_trucking_destination', 705, true);


--
-- Name: seq_domestic_vessel_log_status; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_domestic_vessel_log_status', 114, true);


--
-- Name: seq_domestic_vessel_packing; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_domestic_vessel_packing', 14, true);


--
-- Name: seq_dtdetil; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_dtdetil', 134, true);


--
-- Name: seq_logtruck; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_logtruck', 92, true);


--
-- Name: seq_mst_dangerous_type; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_mst_dangerous_type', 2, true);


--
-- Name: seq_mst_document_type; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_mst_document_type', 3, true);


--
-- Name: seq_mst_kawasan; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_mst_kawasan', 977, true);


--
-- Name: seq_mst_kemasan; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_mst_kemasan', 16, true);


--
-- Name: seq_mst_platform_configuration; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_mst_platform_configuration', 1, false);


--
-- Name: seq_mst_platform_services; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_mst_platform_services', 2, true);


--
-- Name: seq_mst_port; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_mst_port', 139, true);


--
-- Name: seq_mst_process_type; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_mst_process_type', 3, true);


--
-- Name: seq_mst_services; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_mst_services', 9, true);


--
-- Name: seq_payment_billing; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_payment_billing', 35, true);


--
-- Name: seq_payment_detail; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_payment_detail', 1, false);


--
-- Name: seq_payment_invoice; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_payment_invoice', 1, false);


--
-- Name: seq_payment_invoice_item; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_payment_invoice_item', 1, false);


--
-- Name: seq_td_document; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_td_document', 92, true);


--
-- Name: seq_td_user_document; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_td_user_document', 166, true);


--
-- Name: data_pelimpahan data_pelimpahan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_pelimpahan
    ADD CONSTRAINT data_pelimpahan_pkey PRIMARY KEY (id_surat_kuasa);


--
-- Name: data_transporter_detil data_transporter_detil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_transporter_detil
    ADD CONSTRAINT data_transporter_detil_pkey PRIMARY KEY (id_data_transporter_detil);


--
-- Name: data_transporter data_transporter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_transporter
    ADD CONSTRAINT data_transporter_pkey PRIMARY KEY (id_request_booking);


--
-- Name: detil_data_pelimpahan detil_data_pelimpahan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detil_data_pelimpahan
    ADD CONSTRAINT detil_data_pelimpahan_pkey PRIMARY KEY (id);


--
-- Name: document_do_container document_do_container_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_do_container
    ADD CONSTRAINT document_do_container_pkey PRIMARY KEY (id_do_container);


--
-- Name: document_do_log document_do_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_do_log
    ADD CONSTRAINT document_do_log_pkey PRIMARY KEY (id_do_log);


--
-- Name: document_do document_do_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_do
    ADD CONSTRAINT document_do_pkey PRIMARY KEY (id_document_do);


--
-- Name: document_log_truck document_log_truck_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_log_truck
    ADD CONSTRAINT document_log_truck_pkey PRIMARY KEY (id_log_truck);


--
-- Name: document_sp2_container document_sp2_container_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_sp2_container
    ADD CONSTRAINT document_sp2_container_pkey PRIMARY KEY (id_sp2_container);


--
-- Name: document_sp2_log document_sp2_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_sp2_log
    ADD CONSTRAINT document_sp2_log_pkey PRIMARY KEY (id_sp2_log);


--
-- Name: document_sp2 document_sp2_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_sp2
    ADD CONSTRAINT document_sp2_pkey PRIMARY KEY (id_document_sp2);


--
-- Name: document_trucking_container document_trucking_container_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_trucking_container
    ADD CONSTRAINT document_trucking_container_pkey PRIMARY KEY (id_container);


--
-- Name: document_trucking_destination document_trucking_destination_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_trucking_destination
    ADD CONSTRAINT document_trucking_destination_pkey PRIMARY KEY (id);


--
-- Name: document_trucking_detil document_trucking_detil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_trucking_detil
    ADD CONSTRAINT document_trucking_detil_pkey PRIMARY KEY (id_request_booking);


--
-- Name: document_trucking document_trucking_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_trucking
    ADD CONSTRAINT document_trucking_pkey PRIMARY KEY (id_request_booking);


--
-- Name: domestic_vessel_booking domestic_vessel_booking_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domestic_vessel_booking
    ADD CONSTRAINT domestic_vessel_booking_pkey PRIMARY KEY (id_request);


--
-- Name: domestic_vessel_log_status domestic_vessel_log_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domestic_vessel_log_status
    ADD CONSTRAINT domestic_vessel_log_status_pkey PRIMARY KEY (id_log_status);


--
-- Name: domestic_vessel_packing domestic_vessel_packing_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domestic_vessel_packing
    ADD CONSTRAINT domestic_vessel_packing_pkey PRIMARY KEY (id_packing);


--
-- Name: payment_invoice invoice_module_index; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_invoice
    ADD CONSTRAINT invoice_module_index UNIQUE (module);


--
-- Name: payment_invoice invoice_module_unique_id_index; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_invoice
    ADD CONSTRAINT invoice_module_unique_id_index UNIQUE (module_unique_id);


--
-- Name: mst_container_type mst_container_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_container_type
    ADD CONSTRAINT mst_container_type_pkey PRIMARY KEY (id);


--
-- Name: mst_dangerous_type mst_dangerous_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_dangerous_type
    ADD CONSTRAINT mst_dangerous_type_pkey PRIMARY KEY (id);


--
-- Name: mst_document_type mst_document_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_document_type
    ADD CONSTRAINT mst_document_type_pkey PRIMARY KEY (kd_document_type);


--
-- Name: mst_kawasan mst_kawasan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_kawasan
    ADD CONSTRAINT mst_kawasan_pkey PRIMARY KEY (id_kawasan);


--
-- Name: mst_kemasan mst_kemasan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_kemasan
    ADD CONSTRAINT mst_kemasan_pkey PRIMARY KEY (id_kemasan);


--
-- Name: mst_payment_services mst_payment_services_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_payment_services
    ADD CONSTRAINT mst_payment_services_pkey PRIMARY KEY (id_payment_services);


--
-- Name: mst_platform_configuration mst_platform_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_platform_configuration
    ADD CONSTRAINT mst_platform_configuration_pkey PRIMARY KEY (id);


--
-- Name: mst_platform mst_platform_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_platform
    ADD CONSTRAINT mst_platform_pkey PRIMARY KEY (id_platform);


--
-- Name: mst_platform_services mst_platform_services_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_platform_services
    ADD CONSTRAINT mst_platform_services_pkey PRIMARY KEY (id_platform_services);


--
-- Name: mst_port mst_port_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_port
    ADD CONSTRAINT mst_port_pkey PRIMARY KEY (id_port);


--
-- Name: mst_process_type mst_process_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_process_type
    ADD CONSTRAINT mst_process_type_pkey PRIMARY KEY (kd_process_type);


--
-- Name: mst_province mst_province_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_province
    ADD CONSTRAINT mst_province_pkey PRIMARY KEY (code);


--
-- Name: mst_regency mst_regency_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_regency
    ADD CONSTRAINT mst_regency_pkey PRIMARY KEY (code);


--
-- Name: mst_services mst_services_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_services
    ADD CONSTRAINT mst_services_pkey PRIMARY KEY (id_service);


--
-- Name: payment_billing payment_billing_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_billing
    ADD CONSTRAINT payment_billing_pkey PRIMARY KEY (billing_id);


--
-- Name: payment_detail payment_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_detail
    ADD CONSTRAINT payment_detail_pkey PRIMARY KEY (id_payment_detail);


--
-- Name: payment_invoice_item payment_invoice_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_invoice_item
    ADD CONSTRAINT payment_invoice_item_pkey PRIMARY KEY (invoice_item_id);


--
-- Name: payment_invoice payment_invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_invoice
    ADD CONSTRAINT payment_invoice_pkey PRIMARY KEY (order_number);


--
-- Name: payment payment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (virtual_account);


--
-- Name: td_document td_document_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.td_document
    ADD CONSTRAINT td_document_pkey PRIMARY KEY (id_document);


--
-- Name: td_user_document td_user_document_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.td_user_document
    ADD CONSTRAINT td_user_document_pkey PRIMARY KEY (id_user_document);


--
-- Name: module_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX module_index ON public.payment_billing USING btree (module);


--
-- Name: module_unique_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX module_unique_id_index ON public.payment_billing USING btree (module_unique_id);


--
-- Name: document_log_truck fk5mgicc0pg8pyj2vuav33gkp0f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_log_truck
    ADD CONSTRAINT fk5mgicc0pg8pyj2vuav33gkp0f FOREIGN KEY (id_data_transporter_detil) REFERENCES public.data_transporter_detil(id_data_transporter_detil);


--
-- Name: mst_kawasan fk5phq0pdqt6g7e5hvhj3mtenj1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_kawasan
    ADD CONSTRAINT fk5phq0pdqt6g7e5hvhj3mtenj1 FOREIGN KEY (id_port) REFERENCES public.mst_port(id_port);


--
-- Name: document_trucking_destination fk7lkspa1669rlf1gf2ph646q7f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_trucking_destination
    ADD CONSTRAINT fk7lkspa1669rlf1gf2ph646q7f FOREIGN KEY (id_request_booking) REFERENCES public.document_trucking(id_request_booking);


--
-- Name: detil_data_pelimpahan fk_data_pelimpahan; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detil_data_pelimpahan
    ADD CONSTRAINT fk_data_pelimpahan FOREIGN KEY (id_surat_kuasa) REFERENCES public.data_pelimpahan(id_surat_kuasa);


--
-- Name: detil_data_pelimpahan fk_document_type; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detil_data_pelimpahan
    ADD CONSTRAINT fk_document_type FOREIGN KEY (kd_document_type) REFERENCES public.mst_document_type(kd_document_type);


--
-- Name: document_sp2 fk_id_document; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_sp2
    ADD CONSTRAINT fk_id_document FOREIGN KEY (id_document) REFERENCES public.td_document(id_document);


--
-- Name: document_trucking fk_id_document; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_trucking
    ADD CONSTRAINT fk_id_document FOREIGN KEY (id_document) REFERENCES public.td_document(id_document);


--
-- Name: td_user_document fk_id_document; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.td_user_document
    ADD CONSTRAINT fk_id_document FOREIGN KEY (id_document) REFERENCES public.td_document(id_document);


--
-- Name: document_do_container fk_id_document_do; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_do_container
    ADD CONSTRAINT fk_id_document_do FOREIGN KEY (id_document_do) REFERENCES public.document_do(id_document_do);


--
-- Name: document_do_log fk_id_document_do; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_do_log
    ADD CONSTRAINT fk_id_document_do FOREIGN KEY (id_document_do) REFERENCES public.document_do(id_document_do);


--
-- Name: document_sp2_container fk_id_document_sp2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_sp2_container
    ADD CONSTRAINT fk_id_document_sp2 FOREIGN KEY (id_document_sp2) REFERENCES public.document_sp2(id_document_sp2);


--
-- Name: document_sp2_log fk_id_document_sp2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_sp2_log
    ADD CONSTRAINT fk_id_document_sp2 FOREIGN KEY (id_document_sp2) REFERENCES public.document_sp2(id_document_sp2);


--
-- Name: domestic_vessel_booking fk_id_kawasan_asal; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domestic_vessel_booking
    ADD CONSTRAINT fk_id_kawasan_asal FOREIGN KEY (id_kawasan_asal) REFERENCES public.mst_kawasan(id_kawasan);


--
-- Name: domestic_vessel_booking fk_id_kawasan_tujuan; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domestic_vessel_booking
    ADD CONSTRAINT fk_id_kawasan_tujuan FOREIGN KEY (id_kawasan_tujuan) REFERENCES public.mst_kawasan(id_kawasan);


--
-- Name: document_sp2 fk_id_platform; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_sp2
    ADD CONSTRAINT fk_id_platform FOREIGN KEY (id_platform) REFERENCES public.mst_platform(id_platform);


--
-- Name: document_trucking fk_id_platform; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_trucking
    ADD CONSTRAINT fk_id_platform FOREIGN KEY (id_platform) REFERENCES public.mst_platform(id_platform);


--
-- Name: document_do fk_id_platform; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_do
    ADD CONSTRAINT fk_id_platform FOREIGN KEY (id_platform) REFERENCES public.mst_platform(id_platform);


--
-- Name: domestic_vessel_booking fk_id_platform; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domestic_vessel_booking
    ADD CONSTRAINT fk_id_platform FOREIGN KEY (id_platform) REFERENCES public.mst_platform(id_platform);


--
-- Name: mst_payment_services fk_id_platform; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_payment_services
    ADD CONSTRAINT fk_id_platform FOREIGN KEY (id_platform) REFERENCES public.mst_platform(id_platform);


--
-- Name: mst_platform_services fk_id_platform; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_platform_services
    ADD CONSTRAINT fk_id_platform FOREIGN KEY (id_platform) REFERENCES public.mst_platform(id_platform);


--
-- Name: payment_invoice_item fk_id_platform; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_invoice_item
    ADD CONSTRAINT fk_id_platform FOREIGN KEY (id_platform) REFERENCES public.mst_platform(id_platform);


--
-- Name: payment_billing fk_id_platform; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_billing
    ADD CONSTRAINT fk_id_platform FOREIGN KEY (id_platform) REFERENCES public.mst_platform(id_platform);


--
-- Name: domestic_vessel_booking fk_id_port_asal; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domestic_vessel_booking
    ADD CONSTRAINT fk_id_port_asal FOREIGN KEY (id_port_asal) REFERENCES public.mst_port(id_port);


--
-- Name: domestic_vessel_booking fk_id_port_tujuan; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domestic_vessel_booking
    ADD CONSTRAINT fk_id_port_tujuan FOREIGN KEY (id_port_tujuan) REFERENCES public.mst_port(id_port);


--
-- Name: data_transporter_detil fk_id_request_booking; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_transporter_detil
    ADD CONSTRAINT fk_id_request_booking FOREIGN KEY (id_request_booking) REFERENCES public.document_trucking(id_request_booking);


--
-- Name: mst_platform_services fk_id_service; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_platform_services
    ADD CONSTRAINT fk_id_service FOREIGN KEY (id_service) REFERENCES public.mst_services(id_service);


--
-- Name: payment_invoice_item fk_id_service; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_invoice_item
    ADD CONSTRAINT fk_id_service FOREIGN KEY (id_service) REFERENCES public.mst_services(id_service);


--
-- Name: payment_billing fk_id_service; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_billing
    ADD CONSTRAINT fk_id_service FOREIGN KEY (id_service) REFERENCES public.mst_services(id_service);


--
-- Name: td_document fk_kd_document_type; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.td_document
    ADD CONSTRAINT fk_kd_document_type FOREIGN KEY (kd_document_type) REFERENCES public.mst_document_type(kd_document_type);


--
-- Name: mst_document_type fk_kd_process_type; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_document_type
    ADD CONSTRAINT fk_kd_process_type FOREIGN KEY (kd_process_type) REFERENCES public.mst_process_type(kd_process_type);


--
-- Name: document_log_truck fkmytnvgwdvybk8qe8kgry5r1we; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_log_truck
    ADD CONSTRAINT fkmytnvgwdvybk8qe8kgry5r1we FOREIGN KEY (id_data_transporter_detil_id_data_transporter_detil) REFERENCES public.data_transporter_detil(id_data_transporter_detil);


--
-- Name: domestic_vessel_packing fkn17wmgcr0paniqu2m65w6vh8f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domestic_vessel_packing
    ADD CONSTRAINT fkn17wmgcr0paniqu2m65w6vh8f FOREIGN KEY (id_request) REFERENCES public.domestic_vessel_booking(id_request);


--
-- Name: domestic_vessel_log_status fkodoadu9rjtk9yjorirvqmsnya; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domestic_vessel_log_status
    ADD CONSTRAINT fkodoadu9rjtk9yjorirvqmsnya FOREIGN KEY (id_request) REFERENCES public.domestic_vessel_booking(id_request);


--
-- Name: document_trucking_container fkrmf3n0xgpkesgv8jqd5ah45wn; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_trucking_container
    ADD CONSTRAINT fkrmf3n0xgpkesgv8jqd5ah45wn FOREIGN KEY (id_request_booking) REFERENCES public.document_trucking(id_request_booking);


--
-- Name: mst_platform_configuration fkrom7vitn0c0053x5ftdb48j23; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mst_platform_configuration
    ADD CONSTRAINT fkrom7vitn0c0053x5ftdb48j23 FOREIGN KEY (id_platform) REFERENCES public.mst_platform(id_platform);


--
-- Name: payment_invoice_item fktoa0cojqygnltajbdd947tee1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_invoice_item
    ADD CONSTRAINT fktoa0cojqygnltajbdd947tee1 FOREIGN KEY (order_number) REFERENCES public.payment_invoice(order_number);


--
-- PostgreSQL database dump complete
--

