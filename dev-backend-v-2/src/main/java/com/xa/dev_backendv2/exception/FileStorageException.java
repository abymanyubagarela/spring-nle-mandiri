package com.xa.dev_backendv2.exception;

public class FileStorageException extends RuntimeException{
	public FileStorageException(String message) {
		super(message);
	}
	
	public FileStorageException(String message, Throwable cause) {
		super(message, cause);
	}
}
