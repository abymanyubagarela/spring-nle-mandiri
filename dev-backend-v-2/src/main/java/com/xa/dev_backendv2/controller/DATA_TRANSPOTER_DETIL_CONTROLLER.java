package com.xa.dev_backendv2.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xa.dev_backendv2.model.DATA_TRANSPORTER_DETIL;
import com.xa.dev_backendv2.model.DOCUMENT_LOG_TRUCK;
import com.xa.dev_backendv2.repository.DATA_TRANSPOTER_DETIL_REPO;
import com.xa.dev_backendv2.repository.DOCUMENT_LOG_TRUCK_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/DataTransporterDetil")
public class DATA_TRANSPOTER_DETIL_CONTROLLER {
	@Autowired
	private DATA_TRANSPOTER_DETIL_REPO transporter_detil_repo;
	
	@Autowired
	private DOCUMENT_LOG_TRUCK_REPO LogTruckRepo;
	
	@Autowired
	private KafkaTemplate<String,String> kafkaTemplate;
	
	@PostMapping(value="/",consumes = {"application/json"},produces = {"application/json"})
	private String save(@RequestBody DATA_TRANSPORTER_DETIL item)throws JsonProcessingException{
		String hasil = null;
		try {
			this.transporter_detil_repo.save(item);
			hasil = "Success.";
		} catch (Exception e) {
			hasil = "Failed. "+e;
		}
		return hasil;
	}
	
	@PutMapping(value="/",consumes = {"application/json"},produces = {"application/json"})
	private String edit(@RequestParam String idRequestBooking,
			@RequestParam String containerNo,
			@RequestBody DATA_TRANSPORTER_DETIL item)throws JsonProcessingException{
		String hasil = null;
		String Topic = "dev-truckStatus";
		if(this.transporter_detil_repo.findByIdDTDetil(idRequestBooking, containerNo) != null) {
			Integer getId_Detil = this.transporter_detil_repo.findByIdDTDetil(idRequestBooking, containerNo);
			
			if(this.transporter_detil_repo.findById(getId_Detil).isPresent()) {
				DATA_TRANSPORTER_DETIL getID = this.transporter_detil_repo.findById(getId_Detil).get();
				
				getID.setStatus(item.getStatus());
				getID.setIsFinished(item.getIsFinished());
				
				this.transporter_detil_repo.save(getID);
				
				Date dt = new Date();
				Long time = dt.getTime();
				Timestamp ts = new Timestamp(time);
				
//				TRIGGER LOG TRUCK
				DOCUMENT_LOG_TRUCK log_truck = new DOCUMENT_LOG_TRUCK();
				log_truck.setCreate_date(ts);
				log_truck.setStatus(item.getStatus());
				log_truck.setIdDataTransporterDetil(getId_Detil);
				this.LogTruckRepo.save(log_truck);
				
				ObjectMapper mapper = new ObjectMapper();
				String key = this.transporter_detil_repo.findbyIdUser(idRequestBooking);
				kafkaTemplate.send(Topic,key,mapper.writeValueAsString(getID));
				hasil = "Success.";
			}
		} else {
			hasil = "Failed. ";
		}
		return hasil;
	}
	
	@PutMapping(value="/container",consumes = {"application/json"},produces = {"application/json"})
	private String editContainer(@RequestParam String idRequestBooking,
			@RequestParam String containerNo,
			@RequestBody DATA_TRANSPORTER_DETIL item)throws JsonProcessingException {
		String hasil = null;
		String Topic = "dev-containerUpdate";
		if(this.transporter_detil_repo.findByDTDetil(idRequestBooking, containerNo) != null) {
			Integer idDataTransporterDetil = this.transporter_detil_repo.findByDTDetil(idRequestBooking, containerNo);
			if(this.transporter_detil_repo.findById(idDataTransporterDetil).isPresent()) {
				DATA_TRANSPORTER_DETIL getID = this.transporter_detil_repo.findById(idDataTransporterDetil).get();
				
				getID.setNamaDriver(item.getNamaDriver());
				getID.setTruckPlateNo(item.getTruckPlateNo());
				getID.setHpDriver(item.getHpDriver());
				
				this.transporter_detil_repo.save(getID);
				ObjectMapper mapper = new ObjectMapper();
				kafkaTemplate.send(Topic,idRequestBooking,mapper.writeValueAsString(getID));
				hasil = "Success.";
			}
		} else {
			hasil = "Failed.";
		}
		return hasil;
	}
	
	@DeleteMapping(value="/")
	private String delete(@RequestParam Integer idDataTransporterDetil) {
		String hasil = null;
		try {
			this.transporter_detil_repo.deleteById(idDataTransporterDetil);
			hasil = "Success.";
		} catch (Exception e) {
			hasil = "Failed. "+ e;
		}
		return hasil;
	}
	
	@GetMapping(value="/")
	private List<DATA_TRANSPORTER_DETIL> getAllTransporterDetil(){
		return this.transporter_detil_repo.findAll();
	}
	
	@GetMapping(value="/Limit/")
	private List<DATA_TRANSPORTER_DETIL> getLimit(@RequestParam Integer value){
		return this.transporter_detil_repo.findLimit(value);
	}
	
	@GetMapping(value="/createdDate/")
	private List<DATA_TRANSPORTER_DETIL> getCreatedDate(@RequestParam Timestamp createdDate){
		return this.transporter_detil_repo.findByCreatedDate(createdDate);
	}
	
	@GetMapping(value="/idDataTransporterDetil/")
	private List<DATA_TRANSPORTER_DETIL> getID(@RequestParam String value){
		return this.transporter_detil_repo.findByIdRequestBooking(value);
	}
	
	@GetMapping(value="/ListActiveTrucks/")
	private Page<DATA_TRANSPORTER_DETIL> searchActiveTrucks(@RequestParam (defaultValue = "", required = false)String bookedDate,
			@RequestParam(defaultValue = "", required = false) String idRequestBooking ,
			@RequestParam String npwp,@RequestParam (required = false) Integer documentID, @RequestParam (defaultValue = "", required = false) String blNo, Pageable pageable){
		documentID = documentID==null?0:documentID;
		return this.transporter_detil_repo.findListActiveTruck(bookedDate, idRequestBooking, npwp,documentID, blNo, pageable);
	}
	
	@GetMapping(value="/ListHistoryTrucks/")
	private Page<DATA_TRANSPORTER_DETIL> searchHistoryTrucks(@RequestParam (defaultValue = "", required = false)String bookedDate,
			@RequestParam(defaultValue = "", required = false) String idRequestBooking ,
			@RequestParam String npwp,@RequestParam (required = false) Integer documentID,@RequestParam (defaultValue = "", required = false) String blNo, Pageable pageable){
		documentID = documentID==null?0:documentID;
		return this.transporter_detil_repo.findListHistoryTruck(bookedDate, idRequestBooking, npwp,documentID, blNo, pageable);
	}
}
	
