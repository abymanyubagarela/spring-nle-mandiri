package com.xa.dev_backendv2.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xa.dev_backendv2.model.DOCUMENT_DO_LOG;
import com.xa.dev_backendv2.repository.DOCUMENT_DO_LOG_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/document_do_log")
public class DOCUMENT_DO_LOG_CONTROLLER {
	@Autowired
	private DOCUMENT_DO_LOG_REPO document_do_log_repo;
	
	// simpan data
	@PostMapping(value = "/")
	private String save(@RequestBody DOCUMENT_DO_LOG item)throws JsonProcessingException{
		String hasil = null;
		ResponseEntity<DOCUMENT_DO_LOG> result = null;
		try {
			Date dt = new Date();
			Long time = dt.getTime();
			Timestamp ts = new Timestamp(time);
			item.setCreated_date(ts);
			
			this.document_do_log_repo.save(item);
			result = new ResponseEntity<DOCUMENT_DO_LOG>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			result = new ResponseEntity<DOCUMENT_DO_LOG>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
		}
		return hasil;
	}
	
	//find all
	@GetMapping(value = "/")
	private List<DOCUMENT_DO_LOG> getDocumentDoLog(){
		return this.document_do_log_repo.findAll();
	}
}
