package com.xa.dev_backendv2.controller;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

import com.xa.dev_backendv2.model.*;
import com.xa.dev_backendv2.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/document_do")
public class DOCUMENT_DO_CONTROLLER {
	@Autowired
	private DOCUMENT_DO_REPO document_do_repo;
	
	@Autowired
	private DOCUMENT_DO_CONTAINER_REPO doc_container_repo;
	
//	@Autowired
//	private DOCUMENT_BL_HOST_REPO doc_bl_host_repo;

	@Autowired
	private TD_DOCUMENT_REPO td_document_repo;
	
	@Autowired
	private DOCUMENT_DO_LOG_REPO doc_do_log_repo;
	@Autowired
	PAYMENT_BILLING_REPO payment_billing_repo;
	@Autowired
	MST_PLATFORM_REPO mst_platform_repo;
	
	// simpan data
	@PostMapping(value = "/")
	private String save(@RequestBody DOCUMENT_DO item) throws JsonProcessingException{
		String hasil = null;
		
		ResponseEntity<DOCUMENT_DO> result = null;
		try {
			Date dt = new Date();
			Long time = dt.getTime();
			Timestamp ts = new Timestamp(time);
			item.setRequest_date(ts);
			
			Set<DOCUMENT_DO_CONTAINER> containers = item.getContainer();
//			Set<DOCUMENT_BL_HOST> document_bl = item.getDocument_bl_host();
			item.setContainer(new HashSet<>());
//			item.setDocument_bl_host(new HashSet<>());
			Optional<DOCUMENT_DO> document = document_do_repo.findById(item.getId_document_do());
			if(!document.isPresent()) {
				final DOCUMENT_DO do_document = this.document_do_repo.save(item);				
				containers = containers.stream().peek(m-> m.setId_document_do(do_document)).collect(Collectors.toSet());
//				document_bl = document_bl.stream().peek(m-> m.setId_document_do(do_document)).collect(Collectors.toSet());
				doc_container_repo.saveAll(containers);

//				Trigger DOCUMENT DO LOG						
				DOCUMENT_DO_LOG doc_do_log = new DOCUMENT_DO_LOG();
				doc_do_log.setId_document_do(do_document.getId_document_do());
				doc_do_log.setStatus(do_document.getStatus());
				doc_do_log.setCreated_date(ts);
				this.doc_do_log_repo.save(doc_do_log);
//				doc_bl_host_repo.saveAll(document_bl);
			}

//			List<String> blHosts = document_bl.stream().map(DOCUMENT_BL_HOST::getBl_host_no).collect(Collectors.toList());
//			List<TD_DOCUMENT> tdDocuments = td_document_repo.findAllByBlNoInAndNpwpCargoOwner(blHosts,item.getNpwpCargoOwner());
//
//			for (TD_DOCUMENT tdDocument : tdDocuments){
//				tdDocument.setId_document_do(item.getId_document_do());
//				td_document_repo.save(tdDocument);
//			}

			result = new ResponseEntity<DOCUMENT_DO>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			result = new ResponseEntity<DOCUMENT_DO>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
			e.printStackTrace();
		}
		return hasil;
	}
	
	@PutMapping(value="/status/")
	private String editStatus(@RequestParam String doID, @RequestBody DOCUMENT_DO item) {
		String hasil = null;
		ResponseEntity<DOCUMENT_DO> result = null;
		if(this.document_do_repo.findById(doID).isPresent()) {
			DOCUMENT_DO getID = this.document_do_repo.findById(doID).get();
			
			getID.setStatus(item.getStatus());
			getID.setDoNumber(item.getDoNumber());
			getID.setDo_date_number(item.getDo_date_number());
			getID.setBlType(item.getBlType());
			
			this.document_do_repo.save(getID);
			
			
			Date dt = new Date();
			Long time = dt.getTime();
			Timestamp ts = new Timestamp(time);

//			Trigger DOCUMENT DO LOG			
			DOCUMENT_DO_LOG doc_do_log = new DOCUMENT_DO_LOG();
			doc_do_log.setCreated_date(ts);
			doc_do_log.setId_document_do(doID);
			doc_do_log.setStatus(item.getStatus());
		
			doc_do_log_repo.save(doc_do_log);
			
			result = new ResponseEntity<DOCUMENT_DO>(HttpStatus.OK);
			hasil = "success.";
		} else {
			result = new ResponseEntity<DOCUMENT_DO>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed.";
		}
		return hasil;
	}
	
	@PutMapping(value="/price/")
	private String editPrice(@RequestParam String doID, @RequestBody DOCUMENT_DO item) {
		String hasil = null;
		ResponseEntity<DOCUMENT_DO> result = null;
		if(this.document_do_repo.findById(doID).isPresent()) {
			DOCUMENT_DO getID = this.document_do_repo.findById(doID).get();
			
			getID.setPrice(item.getPrice());
			getID.setStatus(item.getStatus());
			getID.setPayment_url(item.getPayment_url());
			
			this.document_do_repo.save(getID);

			MST_PLATFORM platform = mst_platform_repo.findById(getID.getId_platform()).orElse(null);

			Date dt = new Date();
			long time = dt.getTime();
			Timestamp ts = new Timestamp(time);

			try {
				String moduleUniqueId = getID.getId_document_do();
				String module = "DO";
				PAYMENT_BILLING payment_billing = payment_billing_repo.findByModuleUniqueIdAndModule(moduleUniqueId, module).orElse(new PAYMENT_BILLING());
				payment_billing.setModule(module);
				payment_billing.setModuleUniqueId(moduleUniqueId);
				payment_billing.setId_service(1);
				payment_billing.setService_unique_id(moduleUniqueId);
				String sisInfo = getID.getContainer().stream().map(DOCUMENT_DO_CONTAINER::getContainer_no).collect(Collectors.joining(","));
				payment_billing.setItem_info(String.format("Container %s", sisInfo));
				BigDecimal ammount = getID.getPrice()==null?new BigDecimal(0):getID.getPrice();
				payment_billing.setAmount( ammount );
				payment_billing.setCreated_date(ts);
				payment_billing.setIdPlatform(platform);
				payment_billing_repo.save(payment_billing);
			} catch (Exception e) {
				result = new ResponseEntity<DOCUMENT_DO>(HttpStatus.INTERNAL_SERVER_ERROR);
				hasil = "Failed.";
			}
//			Trigger DOCUMENT DO LOG			
			DOCUMENT_DO_LOG doc_do_log = new DOCUMENT_DO_LOG();
			doc_do_log.setCreated_date(ts);
			doc_do_log.setId_document_do(doID);
			doc_do_log.setStatus(item.getStatus());
			
			doc_do_log_repo.save(doc_do_log);
			
			result = new ResponseEntity<DOCUMENT_DO>(HttpStatus.OK);
			hasil = "success.";
		} else {
			result = new ResponseEntity<DOCUMENT_DO>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed.";
		}
		return hasil;
	}
	
	@PutMapping(value="/paidDate/")
	private String editPaidDate(@RequestParam String doID, @RequestBody DOCUMENT_DO item) {
		String hasil = null;
		ResponseEntity<DOCUMENT_DO> result = null;
		if(this.document_do_repo.findById(doID).isPresent()) {
			DOCUMENT_DO getID = this.document_do_repo.findById(doID).get();
			
			getID.setPaid_date(item.getPaid_date());
			getID.setStatus(item.getStatus());
			
			this.document_do_repo.save(getID);
			
			Date dt = new Date();
			Long time = dt.getTime();
			Timestamp ts = new Timestamp(time);
			
//			Trigger DOCUMENT DO LOG			
			DOCUMENT_DO_LOG doc_do_log = new DOCUMENT_DO_LOG();
			doc_do_log.setCreated_date(ts);
			doc_do_log.setId_document_do(doID);
			doc_do_log.setStatus(item.getStatus());
			
			doc_do_log_repo.save(doc_do_log);
			
			result = new ResponseEntity<DOCUMENT_DO>(HttpStatus.OK);
			hasil = "success.";
		} else {
			result = new ResponseEntity<DOCUMENT_DO>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed.";
		}
		return hasil;
	}
	
	//find all
	@GetMapping(value = "/")
	private Page<DOCUMENT_DO> getDocumentDo(@RequestParam String npwp,
			@RequestParam(required = false) String bl_no,Pageable pageable){
		if(null==bl_no){
			return this.document_do_repo.findAllByNpwpCargoOwnerOrIdFfPpjk(npwp,npwp,pageable);
		}else {
			return this.document_do_repo.findAllByQuery(npwp,npwp,bl_no,pageable);
		}
	}
	@GetMapping(value = "/findbl")
	private DOCUMENT_DO getDocumentDo(@RequestParam String npwp,
									  @RequestParam String bl_no,
									  @RequestParam String bl_type){
		return this.document_do_repo.findOneByQuery(npwp,npwp,bl_no,bl_type);
	}
	
	@GetMapping(value = "/activeDOByBL")
	private List<DOCUMENT_DO> getDocumentDoByBl(@RequestParam String npwp,
									  @RequestParam String bl_no,
									  @RequestParam String bl_type){
		return this.document_do_repo.findOneByDocumentDo(npwp,npwp,bl_no,bl_type);
	}
	
	@GetMapping(value = "/list")
	private List<DOCUMENT_DO> getDocumentDoList(@RequestParam String npwp,
									  @RequestParam String bl_no,
									  @RequestParam String bl_type){
		return this.document_do_repo.findOneByQueryList(npwp,npwp,bl_no,bl_type);
	}
	
	//find all
	@GetMapping(value = "/findByBlNo")
	private List<DOCUMENT_DO> getDocumentDoByBlNo(@RequestParam String blNo, @RequestParam Integer page, @RequestParam Integer size){
		Pageable pageable = PageRequest.of(page, size);
		return this.document_do_repo.findAllByBlNo(blNo,pageable);
	}

	@GetMapping(value = "/activeDO")
	private List<DOCUMENT_DO> documentDoList(@RequestParam String npwp){
		return document_do_repo.findAllByQueryNpwpCargoOwner(npwp);
	}

	//find by blhost npwp
//	@GetMapping(value = "/finddata")
//	private List<DOCUMENT_DO> findByBlhostNpwp(@RequestParam String bl_host, @RequestParam String npwp ){
//		return this.document_do_repo.findByBlhostNpwp(bl_host,npwp);
//	}
}
