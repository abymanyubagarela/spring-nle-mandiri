package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.dev_backendv2.model.MST_CONTAINER_TYPE;
import com.xa.dev_backendv2.repository.MST_CONTAINER_TYPE_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/ContainerType")
public class MST_CONTAINER_TYPE_CONTROLLER {
	@Autowired
	private MST_CONTAINER_TYPE_REPO containerTypeRepo;
	
	@GetMapping(value="/")
	private List<MST_CONTAINER_TYPE> getListData(){
		return this.containerTypeRepo.findAll();
	}
	
}
