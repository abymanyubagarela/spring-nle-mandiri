package com.xa.dev_backendv2.controller;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xa.dev_backendv2.model.TD_DOCUMENT;
import com.xa.dev_backendv2.model.TD_USER_DOCUMENT;
import com.xa.dev_backendv2.repository.TD_DOCUMENT_REPO;
import com.xa.dev_backendv2.repository.TD_USER_DOCUMENT_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/document")
public class TD_DOCUMENT_CONTROLLER {
	@Autowired
	private TD_DOCUMENT_REPO td_document_repo;
	
	@Autowired
	private TD_USER_DOCUMENT_REPO td_user_document_repo;
	
	// simpan data
	@PostMapping(value = "/")
	private String save(@RequestBody TD_DOCUMENT item)throws JsonProcessingException{
		String hasil = null;
//		TD_USER_DOCUMENT item2 = new TD_USER_DOCUMENT();
		ResponseEntity<TD_DOCUMENT> result = null;
		try {
			Set<TD_USER_DOCUMENT> td_user_documents = item.getUser_document()==null?new HashSet<>():item.getUser_document();
			List<TD_DOCUMENT> td_documents = td_document_repo.findTdDocument(item.getNpwpCargoOwner(),
					item.getDocument_no(),
					item.getDocument_date(),
					item.getBlNo(),
					item.getBl_date());
			TD_DOCUMENT td_document;
			
			if(td_documents.size()>0){
				// result of td_documents supposed to be 1
				// if somehow it return many rows
				// use only the first element
				td_document = td_documents.get(0);
				TD_DOCUMENT td_document1 = td_document_repo.getOne(td_document.getId_document());
				td_document1.setBl_date(item.getBl_date());
				td_document1.setBlNo(item.getBlNo());
				td_document1.setDocument_date(item.getDocument_date());
				td_document1.setDocument_no(item.getDocument_no());
				td_document1.setDocument_status(item.getDocument_status());
				td_document1.setKd_document_type(item.getKd_document_type());
				td_document1.setNpwpCargoOwner(item.getNpwpCargoOwner());
				td_document1.setNm_cargoowner(item.getNm_cargoowner());
				td_document1.setNo_sppb(item.getNo_sppb());
				td_document1.setTanggal_sppb(item.getTanggal_sppb());
				td_document_repo.save(td_document1);
//				hasil = "Data already exists and has been updated.";
				hasil = String.valueOf(td_document1.getId_document());
			}else {
				item.setUser_document(new HashSet<>());
				td_document=this.td_document_repo.save(item);
				hasil = "success.";
			}
			for(TD_USER_DOCUMENT row: td_user_documents){
				updateUserDocument(row, td_document);
			}

			result = new ResponseEntity<>(HttpStatus.OK);
//			hasil = "success.";
		} catch (Exception e) {
			result = new ResponseEntity<TD_DOCUMENT>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
			e.printStackTrace();
		}
		return hasil;
	}
	
	@PutMapping(value="/")
	private String edit(@RequestParam Integer id_document,@RequestBody TD_DOCUMENT item) {
		String hasil = null;
		ResponseEntity<TD_DOCUMENT> result = null;
		if(this.td_document_repo.findById(id_document).isPresent()) {
			TD_DOCUMENT getId = this.td_document_repo.findById(id_document).get();
			
			getId.setDocument_no(item.getDocument_no());
			getId.setDocument_date(item.getDocument_date());
			getId.setDocument_status(item.getDocument_status());
			getId.setBlNo(item.getBlNo());
			getId.setBl_date(item.getBl_date());
			getId.setFinish_do(item.getFinish_do());
			getId.setFinish_trucking(item.getFinish_trucking());
			getId.setFinish_sp2(item.getFinish_sp2());
			getId.setFinish_warehouse(item.getFinish_warehouse());
			getId.setFinish_depo(item.getFinish_depo());
			
			this.td_document_repo.save(getId);
			result = new ResponseEntity<TD_DOCUMENT>(HttpStatus.OK);
			hasil = "success.";
		} else {
			result = new ResponseEntity<TD_DOCUMENT>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed.";
		}
		return hasil;
	}
	
	@DeleteMapping(value="/")
	private String delete(@RequestParam Integer id_document) {
		String hasil = null;
		ResponseEntity<TD_DOCUMENT> result = null;
		try {
			this.td_document_repo.deleteById(id_document);
			result = new ResponseEntity<TD_DOCUMENT>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			result = new ResponseEntity<TD_DOCUMENT>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed.";
		}
		return hasil;
	}
	
//	@GetMapping(value = "/")
//	private List<TD_DOCUMENT> getDocument(){
//		return this.td_document_repo.findAll();
//	}
	
	//find by npwp
	@GetMapping(value = "/finddata/")
	private Page<Map> getByNPWP(@RequestParam String npwp,@RequestParam(defaultValue = "",required = false) String no, Pageable pageable){
		return this.td_document_repo.findByNPWP(npwp,no,pageable);
	}

	private void updateUserDocument(TD_USER_DOCUMENT new_user_document, TD_DOCUMENT td_document){
		List<TD_USER_DOCUMENT> td_user_documents1 = td_user_document_repo.findByNpwpAndIdDocument(new_user_document.getNpwp(), td_document.getId_document()) ;
		if(td_user_documents1.size()>0){
			//if found user_document, update
			TD_USER_DOCUMENT td_user_document = td_user_documents1.get(0);
			td_user_document_repo.save(td_user_document);
		}else{
			//if new user_document, save
			new_user_document.setId_document(td_document);
			td_user_document_repo.save(new_user_document);
		}
	}
}
