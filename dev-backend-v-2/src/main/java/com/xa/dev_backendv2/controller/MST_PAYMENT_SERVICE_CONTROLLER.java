package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.dev_backendv2.model.MST_PAYMENT_SERVICES;
import com.xa.dev_backendv2.repository.MST_PAYMENT_SERVICES_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/PaymentService")
public class MST_PAYMENT_SERVICE_CONTROLLER {
	@Autowired
	private MST_PAYMENT_SERVICES_REPO paymentServiceRepo;
	
	@GetMapping(value="/")
	private List<MST_PAYMENT_SERVICES> getAll(){
		return this.paymentServiceRepo.findAll();
	}
}
