package com.xa.dev_backendv2.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xa.dev_backendv2.model.DOCUMENT_TRUCKING;
import com.xa.dev_backendv2.model.DOCUMENT_TRUCKING_CONTAINER;
import com.xa.dev_backendv2.repository.DOCUMENT_TRUCKING_CONTAINER_REPO;
import com.xa.dev_backendv2.repository.DOCUMENT_TRUCKING_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/Booking")
public class DOCUMENT_TRUCKING_CONTROLLER {
	@Autowired
	private DOCUMENT_TRUCKING_REPO document_trucking_repo;
	
	@Autowired
	private DOCUMENT_TRUCKING_CONTAINER_REPO document_trucking_container_repo;
	
	@Autowired
	private KafkaTemplate<String,String> kafkaTemplate;
	
	// simpan data
	@PostMapping(value = "/")
	private String save(@RequestBody DOCUMENT_TRUCKING item)throws JsonProcessingException{
		String hasil = null;
		String Topic = "dev-booking";
		ResponseEntity<DOCUMENT_TRUCKING> result = null;
		try {
			Optional<DOCUMENT_TRUCKING> booking = document_trucking_repo.findById(item.getIdRequestBooking());
			if(!booking.isPresent()){
				this.document_trucking_repo.save(item);
			}
			
			result = new ResponseEntity<DOCUMENT_TRUCKING>(HttpStatus.OK);
			hasil = "success.";
			
			ObjectMapper mapper = new ObjectMapper();
			kafkaTemplate.send(Topic,item.getId_platform(),mapper.writeValueAsString(item));
		} catch (Exception e) {
			result = new ResponseEntity<DOCUMENT_TRUCKING>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
		}
		return hasil;
	}
	
	@PostMapping(value = "/TruckingPlatform")
	public String saveBookingTruckingPlatform(@RequestBody List<DOCUMENT_TRUCKING> item) {
		String hasil = null;
		ResponseEntity<DOCUMENT_TRUCKING> result = null;
		try {	
			ObjectMapper mapper = new ObjectMapper();
			this.document_trucking_repo.saveAll(item);
			for (DOCUMENT_TRUCKING entity : item) {
				System.out.println(mapper.writeValueAsString(entity));
				kafkaTemplate.send("dev-booking",entity.getId_platform(),mapper.writeValueAsString(entity));
			}
			result = new ResponseEntity<DOCUMENT_TRUCKING>(HttpStatus.OK);
			hasil = "success.";	
		} catch(Exception e) {
			result = new ResponseEntity<DOCUMENT_TRUCKING>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
		}
		return hasil;
	}
	
	// Edit data
	@PutMapping(value="/",consumes = {"application/json"},produces = {"application/json"})
	private String edit(@RequestParam String idRequestBooking, @RequestBody DOCUMENT_TRUCKING item)
			throws JsonProcessingException {
		String hasil = null;
		String Topic = "nle-bookingUpdate";
		ResponseEntity<DOCUMENT_TRUCKING> result = null;
		if (this.document_trucking_repo.findById(idRequestBooking).isPresent()) {
			DOCUMENT_TRUCKING getID = this.document_trucking_repo.findById(idRequestBooking).get();
			
			getID.setBooking_date(item.getBooking_date());
			getID.setPod(item.getPod());
			getID.setPlan_date(item.getPlan_date());
			getID.setBl_no(item.getBl_no());
			getID.setBl_date(item.getBl_date());
			getID.setSp2valid_date(item.getSp2valid_date());
			getID.setSpcvalid_date(item.getSpcvalid_date());
			getID.setId_User(item.getId_User());
			getID.setId_platform(item.getId_platform());
			getID.setPod_lat(item.getPod_lat());
			getID.setPod_lon(item.getPod_lon());
			getID.setTotal_distance(item.getTotal_distance());
			getID.setParty(item.getParty());
			getID.setId_User(item.getId_User());
			getID.setNpwp(item.getNpwp());
			getID.setCompany(item.getCompany());
			getID.setAddress(item.getAddress());
			getID.setEmail(item.getEmail());
			getID.setMobile(item.getEmail());
			getID.setTerm_of_payment(item.getTerm_of_payment());			
			
			this.document_trucking_repo.save(getID);
			ObjectMapper mapper = new ObjectMapper();
			kafkaTemplate.send(Topic,idRequestBooking,mapper.writeValueAsString(getID));
			result = new ResponseEntity<DOCUMENT_TRUCKING>(HttpStatus.OK);
			hasil = "Success.";
		} else {
			result = new ResponseEntity<DOCUMENT_TRUCKING>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed.";
		}
		return hasil;
	}
	
	//find all
	@GetMapping(value = "/")
	private Page<DOCUMENT_TRUCKING> getDocumentTrucking(Pageable pageable){
		return this.document_trucking_repo.findAll(pageable);
	}
	
	//find ID 
	@GetMapping(value="/idRequestBooking/")
	private DOCUMENT_TRUCKING searchID(@RequestParam String value){
		return this.document_trucking_repo.findById(value).orElse(null);
	}
	
	//=============================================================
	//find data trucking Container Booking by bl_no and paidStatus 
	@GetMapping(value="/Container/")
	private List<DOCUMENT_TRUCKING_CONTAINER> searchContainerBooking(
			@RequestParam String bl_no, 
			@RequestParam Integer paidStatus){
		return this.document_trucking_container_repo.findByContainerBooking(bl_no, paidStatus);
	}
}
