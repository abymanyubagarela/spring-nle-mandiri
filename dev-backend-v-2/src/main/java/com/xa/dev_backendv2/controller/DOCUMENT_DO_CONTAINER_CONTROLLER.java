package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xa.dev_backendv2.model.DOCUMENT_DO_CONTAINER;
import com.xa.dev_backendv2.repository.DOCUMENT_DO_CONTAINER_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/document_do_container")
public class DOCUMENT_DO_CONTAINER_CONTROLLER {
	@Autowired
	private DOCUMENT_DO_CONTAINER_REPO document_do_container_repo;
	
	// simpan data
	@PostMapping(value = "/")
	private String save(@RequestBody DOCUMENT_DO_CONTAINER item)throws JsonProcessingException{
		String hasil = null;
		ResponseEntity<DOCUMENT_DO_CONTAINER> result = null;
		try {
			this.document_do_container_repo.save(item);
			result = new ResponseEntity<DOCUMENT_DO_CONTAINER>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			result = new ResponseEntity<DOCUMENT_DO_CONTAINER>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
		}
		return hasil;
	}
	
	//find all
	@GetMapping(value = "/")
	private List<DOCUMENT_DO_CONTAINER> getDocumentDoContainer(){
		return this.document_do_container_repo.findAll();
	}
}
