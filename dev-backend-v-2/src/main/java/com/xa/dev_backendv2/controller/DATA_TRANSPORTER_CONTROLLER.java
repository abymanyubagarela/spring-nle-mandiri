package com.xa.dev_backendv2.controller;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

import com.xa.dev_backendv2.model.DATA_TRANSPORTER_DETIL;
import com.xa.dev_backendv2.model.MST_PLATFORM;
import com.xa.dev_backendv2.model.PAYMENT_BILLING;
import com.xa.dev_backendv2.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xa.dev_backendv2.model.DATA_TRANSPORTER;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/DTransporter")
public class DATA_TRANSPORTER_CONTROLLER {
	@Autowired DATA_TRANSPORTER_REPO data_transpoter_repo;
	@Autowired
	DATA_TRANSPOTER_DETIL_REPO data_transpoter_detil_repo;
	@Autowired
	PAYMENT_BILLING_REPO payment_billing_repo;
	@Autowired
	DOCUMENT_TRUCKING_DETIL_REPO document_trucking_detil_repo;
	@Autowired
	MST_PLATFORM_REPO mst_platform_repo;

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	@PostMapping(value="/",consumes = {"application/json"},produces = {"application/json"})
	private String save(@RequestBody DATA_TRANSPORTER item) {
		String hasil = null;
		String Topic = "dev-truck";
		ResponseEntity<DATA_TRANSPORTER> result = null;

		Date dt = new Date();
		Long time = dt.getTime();
		Timestamp ts = new Timestamp(time);

		try {
			Set<DATA_TRANSPORTER_DETIL> data_transporter_detils = item.getTransporterDetil();
			System.out.println();
			item.setTransporterDetil(new HashSet<>());
			item.setCreated_date(ts);
			this.data_transpoter_repo.save(item);

			for (DATA_TRANSPORTER_DETIL row : data_transporter_detils){
				DATA_TRANSPORTER_DETIL data_transporter_detil = data_transpoter_detil_repo.findByIdRequestBookingAndContainerNo(row.getIdRequestBooking(), row.getContainerNo()).orElse(row);
				data_transpoter_detil_repo.save(data_transporter_detil);
			}

			result = new ResponseEntity<DATA_TRANSPORTER>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			e.printStackTrace();
			result = new ResponseEntity<DATA_TRANSPORTER>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "failed." +e;
		}

		//payment
		try{
			Map map = data_transpoter_repo.findByQueryIdRequestBooking(item.getIdRequestBooking());
			String moduleUniqueId=map.get("id_document").toString();
			String module = "Documents";
			MST_PLATFORM platform = mst_platform_repo.findById(String.valueOf(map.get("id_platform"))).orElse(null);
			PAYMENT_BILLING payment_billing = payment_billing_repo.findByModuleUniqueIdAndModule(moduleUniqueId,module).orElse(new PAYMENT_BILLING());
			payment_billing.setModule(module);
			payment_billing.setModuleUniqueId(moduleUniqueId);
			payment_billing.setId_service(3);
			payment_billing.setService_unique_id(item.getIdRequestBooking());
			String sisInfo = item.getTransporterDetil().stream().map(DATA_TRANSPORTER_DETIL::getContainerNo).collect(Collectors.joining(","));
			payment_billing.setItem_info(String.format("Container %s", sisInfo));
			BigDecimal amount = new BigDecimal(Integer.parseInt( map.get("harga_penawaran").toString()));
			payment_billing.setAmount(amount );
			payment_billing.setCreated_date(ts);
			payment_billing.setIdPlatform(platform);
			payment_billing_repo.save(payment_billing);
		}catch (Exception e){
			e.printStackTrace();
		}

		//kafka
		try {
			ObjectMapper mapper = new ObjectMapper();
			String key = this.data_transpoter_repo.findbyIdUser(item.getIdRequestBooking());
			kafkaTemplate.send(Topic,key,mapper.writeValueAsString(item));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return hasil;
	}
	
	@GetMapping(value="/")
	private List<DATA_TRANSPORTER> getDataTranspoter(){
		return this.data_transpoter_repo.findAll();
	}
	
	@GetMapping(value="/idRequestBooking/")
	private DATA_TRANSPORTER getIdDTransporter(@RequestParam String value){
		return this.data_transpoter_repo.findById(value).orElse(null);
	}
}
