package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xa.dev_backendv2.model.DOCUMENT_SP2_LOG;
import com.xa.dev_backendv2.repository.DOCUMENT_SP2_LOG_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/document_sp2_log")
public class DOCUMENT_SP2_LOG_CONTROLLER {
	@Autowired
	private DOCUMENT_SP2_LOG_REPO document_sp2_log_repo;
	
	// simpan data
	@PostMapping(value = "/")
	private String save(@RequestBody DOCUMENT_SP2_LOG item)throws JsonProcessingException{
		String hasil = null;
		ResponseEntity<DOCUMENT_SP2_LOG> result = null;
		try {
			this.document_sp2_log_repo.save(item);
			result = new ResponseEntity<DOCUMENT_SP2_LOG>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			result = new ResponseEntity<DOCUMENT_SP2_LOG>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
		}
		return hasil;
	}
	
	//find all
	@GetMapping(value = "/")
	private List<DOCUMENT_SP2_LOG> getDocumentSp2Log(){
		return this.document_sp2_log_repo.findAll();
	}
}
