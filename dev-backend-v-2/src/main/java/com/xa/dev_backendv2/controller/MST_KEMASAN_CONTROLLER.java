package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xa.dev_backendv2.model.MST_KEMASAN;
import com.xa.dev_backendv2.repository.MST_KEMASAN_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/Kemasan")
public class MST_KEMASAN_CONTROLLER {
	@Autowired
	private MST_KEMASAN_REPO mst_kemasan_repo;
	
	@GetMapping(value = "/")
	private List<MST_KEMASAN> getAllData(){
		return this.mst_kemasan_repo.findAll();
	}
	
	@PostMapping(value = "/")
	private String save(@RequestBody MST_KEMASAN item) {
		String hasil = null;
		ResponseEntity<MST_KEMASAN> result = null;
		try {
			this.mst_kemasan_repo.save(item);
			result = new ResponseEntity<MST_KEMASAN>(HttpStatus.OK);
			hasil = "Success";
		} catch (Exception e) {
			result = new ResponseEntity<MST_KEMASAN>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed " + e;
		}
		return hasil;
	}
	
	@PutMapping(value ="/")
	private String edit(@RequestParam Integer id_kemasan, @RequestBody MST_KEMASAN item) {
		String hasil = null;
		ResponseEntity<MST_KEMASAN> result = null;
		if(this.mst_kemasan_repo.findById(id_kemasan).isPresent()) {
			MST_KEMASAN getId = this.mst_kemasan_repo.findById(id_kemasan).get();
			
			getId.setNama_kemasan(item.getNama_kemasan());
			this.mst_kemasan_repo.save(getId);
			result = new ResponseEntity<MST_KEMASAN>(HttpStatus.OK);
			hasil = "Success";
		} else {
			result = new ResponseEntity<MST_KEMASAN>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed";
		}
		return hasil;
	}
	
	@DeleteMapping(value = "/")
	private String delete(@RequestParam Integer id_kemasan, @RequestBody MST_KEMASAN item) {
		String hasil = null;
		ResponseEntity<MST_KEMASAN> result = null;
		try {
			this.mst_kemasan_repo.deleteById(id_kemasan);
			result = new ResponseEntity<MST_KEMASAN>(HttpStatus.OK);
			hasil = "Success";
		} catch (Exception e) {
			result = new ResponseEntity<MST_KEMASAN>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed " + e;
		}
		return hasil;
	}
}
