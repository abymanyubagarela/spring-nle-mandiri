package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xa.dev_backendv2.model.MST_REGENCY;
import com.xa.dev_backendv2.repository.MST_REGENCY_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/Regency")
public class MST_REGENCY_CONTROLLER {
	@Autowired
	private MST_REGENCY_REPO mst_regency_repo;
	
	@GetMapping(value = "/")
	private List<MST_REGENCY> getProvinceCode(@RequestParam String province_code){
		return this.mst_regency_repo.findByProvinceCode(province_code);
	}
	
	@GetMapping(value = "/findAll/")
	private List<MST_REGENCY> getAll(){
		return this.mst_regency_repo.findAll();
	}

}
