package com.xa.dev_backendv2.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xa.dev_backendv2.DTO.DTOStatusPaidDate;
import com.xa.dev_backendv2.model.DOMESTIC_VESSEL_BOOKING;
import com.xa.dev_backendv2.model.DOMESTIC_VESSEL_LOG_STATUS;
import com.xa.dev_backendv2.model.DOMESTIC_VESSEL_PACKING;
import com.xa.dev_backendv2.model.DTOCariRute;
import com.xa.dev_backendv2.model.MST_KAWASAN;
import com.xa.dev_backendv2.model.MST_PLATFORM;
import com.xa.dev_backendv2.model.MST_PORT;
import com.xa.dev_backendv2.repository.DOMESTIC_VESSEL_BOOKING_REPO;
import com.xa.dev_backendv2.repository.DOMESTIC_VESSEL_LOG_STATUS_REPO;
import com.xa.dev_backendv2.repository.DOMESTIC_VESSEL_PACKING_REPO;
import com.xa.dev_backendv2.repository.MST_KAWASAN_REPO;
import com.xa.dev_backendv2.repository.MST_PLATFORM_REPO;
import com.xa.dev_backendv2.repository.MST_PORT_REPO;
import com.xa.dev_backendv2.service.DomesticVesselKafka;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/DomesticVesselBooking")
public class DOMESTIC_VESSEL_BOOKING_CONTROLLER {
	@Autowired
	private DOMESTIC_VESSEL_BOOKING_REPO domestic_vessel_booking_repo;
	
	@Autowired
	private DOMESTIC_VESSEL_PACKING_REPO domestic_vessel_packing_repo;
	
	@Autowired
	private DOMESTIC_VESSEL_LOG_STATUS_REPO domestic_vessel_log_status_repo;
	
	@Autowired MST_PORT_REPO mst_port_repo; 
	
	@Autowired MST_KAWASAN_REPO mst_kawasan_repo;
	
	@Autowired MST_PLATFORM_REPO mst_platform_repo;
	
	@Autowired
	private KafkaTemplate<String,String> kafkaTemplate;
	
	//save data booking
	@PostMapping(value = "/")
	private String save(@RequestBody DOMESTIC_VESSEL_BOOKING item) throws JsonProcessingException{
		String hasil = null;
		String Topic = "dev-domestikBookingShipper";
		ResponseEntity<DOMESTIC_VESSEL_BOOKING> result = null;
		try {
			Set<DOMESTIC_VESSEL_PACKING> domestic_vessel_packingSet = item.getVessel_packing();
			Date dt = new Date();
			Long time = dt.getTime();
			item.setVessel_packing(new HashSet<>());
			Timestamp ts = new Timestamp(time);
			
			Optional<DOMESTIC_VESSEL_BOOKING> vesselBooking = domestic_vessel_booking_repo.findById(item.getId_request());
			if(!vesselBooking.isPresent()) {
				item.setStatus("Menunggu Pembayaran");
				item.setBooking_date(ts);
				DOMESTIC_VESSEL_LOG_STATUS log_status = new DOMESTIC_VESSEL_LOG_STATUS();
				final DOMESTIC_VESSEL_BOOKING v_booking = this.domestic_vessel_booking_repo.save(item);
				domestic_vessel_packingSet = domestic_vessel_packingSet
						.stream()
						.peek(m-> m.setId_request(v_booking))
						.collect(Collectors.toSet());
				domestic_vessel_packing_repo.saveAll(domestic_vessel_packingSet);
				
				log_status.setId_request(v_booking.getId_request());
				log_status.setStatus(v_booking.getStatus());
				log_status.setCreated_date(ts);
				this.domestic_vessel_log_status_repo.save(log_status);
				
				ObjectMapper mapper = new ObjectMapper();
				kafkaTemplate.send(Topic,mapper.writeValueAsString(item));
			}
			result = new ResponseEntity<DOMESTIC_VESSEL_BOOKING>(HttpStatus.OK);
			hasil = "Success.";
		} catch (Exception e) {
			// TODO: handle exception
			result = new ResponseEntity<DOMESTIC_VESSEL_BOOKING>(HttpStatus.BAD_REQUEST);
			hasil = "Failed."+e;
		}
		return hasil;
	}
	
	@PostMapping(value = "/kafkasend", consumes = {"application/json"}, produces = {"application/json"})
	private String saveKafka(@RequestBody DTOCariRute item, Pageable pageable) throws JsonProcessingException{
		String hasil = null;
		String Topic = "dev-domestikPesanRoute";
		ResponseEntity<DOMESTIC_VESSEL_BOOKING> result = null;
		try {
			item.setId_request(item.getId_request());
			item.setPortAsalId(item.getPortAsalId());
			item.setPortAsalName(item.getPortAsalName());
			item.setPortTujuanId(item.getPortTujuanId());
			item.setPortTujuanName(item.getPortTujuanName());
			item.setKawasanAsalId(item.getKawasanAsalId());
			item.setKawasanTujuanId(item.getKawasanTujuanId());
			item.setKawasanAsalName(item.getKawasanAsalName());
			item.setKawasanTujuanName(item.getKawasanTujuanName());
			item.setNpwp(item.getNpwp());
			
//			Set<DOMESTIC_VESSEL_PACKING> domestic_vessel_packingSet = item.getVessel_packing();
//			Optional<MST_PORT> port_asal = this.mst_port_repo.findById(item.getId_port_asal());
//			Optional<MST_PORT> port_tujuan = this.mst_port_repo.findById(item.getId_port_tujuan());
//			Optional<MST_KAWASAN> kawasan_asal = this.mst_kawasan_repo.findById(item.getId_kawasan_asal());
//			Optional<MST_KAWASAN> kawasan_tujuan = this.mst_kawasan_repo.findById(item.getId_kawasan_tujuan());
			
//			Integer idPortAsal = port_asal.get().getId_port();
//			Integer idPortTujuan = port_tujuan.get().getId_port();
//			String namaPortAsal = port_asal.get().getPort();
//			String namaPortTujuan = port_tujuan.get().getPort();
//			Integer idKawasanAsal = kawasan_asal.get().getId_kawasan();
//			Integer idKawasanTujuan = kawasan_tujuan.get().getId_kawasan();
//			String namaKawasanAsal = kawasan_asal.get().getNama_kawasan();
//			String namaKAwasanTujuan = kawasan_tujuan.get().getNama_kawasan();
			
//			port_asal.get().setId_port(item.getId_port_asal());
//			item.setPort_asal(port_asal.get());
//			item.setPort_tujuan(port_tujuan.get());
//			item.setKawasan_asal(kawasan_asal.get());
//			item.setKawasan_tujuan(kawasan_tujuan.get());
			
			ObjectMapper mapper = new ObjectMapper();
			kafkaTemplate.send(Topic,mapper.writeValueAsString(item));
//			System.out.println("check message kafka = "+mapper.writeValueAsString(item));
			result = new ResponseEntity<DOMESTIC_VESSEL_BOOKING>(HttpStatus.OK);
			hasil = "Success.";
		} catch (Exception e) {
			// TODO: handle exception
			result = new ResponseEntity<DOMESTIC_VESSEL_BOOKING>(HttpStatus.OK);
			hasil = "Failed."+e;
		}
		return hasil;
	}
	
	@PostMapping(value = "/Offer", consumes = {"application/json"}, produces = {"application/json"})
	private String offerKafka(@RequestBody DomesticVesselKafka item)throws JsonProcessingException{
		String hasil = null;
		String Topic = "dev-domestikOffer";
		try {
			item.setId_request(item.getId_request());
			item.setId_platform(item.getId_platform());
			item.setShip_name(item.getShip_name());
			item.setContainer_type(item.getContainer_type());
			item.setPartner(item.getPartner());
			item.setPartner_rating(item.getPartner_rating());
			item.setService_type(item.getService_type());
			item.setPrice(item.getPrice());
			item.setPrice_description(item.getPrice_description());
			item.setClosing_time(item.getClosing_time());
			item.setProvision(item.getProvision());
			
			ObjectMapper mapper = new ObjectMapper();
			kafkaTemplate.send(Topic,item.getId_request(),mapper.writeValueAsString(item));
			hasil = "Success.";
		} catch (Exception e) {
			hasil = "Failed.";
		}
		return hasil;
	}
	
	//edit column status & paid_date findby id_request
	@PutMapping(value = "/paidStatus/")
	private String editPaidStatus(@RequestParam String id_request, 
			@Valid @RequestBody DTOStatusPaidDate item) 
			throws JsonProcessingException{
		String hasil = null;
		DOMESTIC_VESSEL_LOG_STATUS vessel_log_status = new DOMESTIC_VESSEL_LOG_STATUS();
		ResponseEntity<DOMESTIC_VESSEL_BOOKING> result = null;
		try {			
			List<DOMESTIC_VESSEL_BOOKING> domestic_vessel_bookingList = this.domestic_vessel_booking_repo.findByIdRequest(id_request);
			if(domestic_vessel_bookingList.size() > 0) {
				for(DOMESTIC_VESSEL_BOOKING vessel_booking : domestic_vessel_bookingList) {
					Date dt = new Date();
					Long time = dt.getTime();
					Timestamp ts = new Timestamp(time);
					if(!item.getStatus().isEmpty() && !item.getPaid_date().equals(null)) {
						vessel_booking.setStatus(item.getStatus());
						vessel_booking.setPaid_date(item.getPaid_date());
						this.domestic_vessel_booking_repo.save(vessel_booking);
						
						vessel_log_status.setId_request(id_request);
						vessel_log_status.setCreated_date(ts);
						vessel_log_status.setStatus(item.getStatus());
						this.domestic_vessel_log_status_repo.save(vessel_log_status);
						
						result = new ResponseEntity<DOMESTIC_VESSEL_BOOKING>(HttpStatus.OK);
						hasil = "Success.";					
					}
					
				}
			} else {
				result = new ResponseEntity<DOMESTIC_VESSEL_BOOKING>(HttpStatus.NOT_FOUND);
				hasil = "Data not found.";
			}
		} catch (Exception e) {
			// TODO: handle exception
			result = new ResponseEntity<DOMESTIC_VESSEL_BOOKING>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
		}
		return hasil;
	}
	
	//edit column status findby orderID
	@PutMapping(value = "/statusByOrderID/")
	private String edit(
			@RequestParam String orderID, 
			@Valid @RequestBody DTOStatusPaidDate item) 
			throws JsonProcessingException{
		String hasil = null;
		ResponseEntity<DOMESTIC_VESSEL_BOOKING> result = null;
		try {
			List<DOMESTIC_VESSEL_BOOKING> domestic_vessel_bookingList = this.domestic_vessel_booking_repo.findByOrderID(orderID);
			if (domestic_vessel_bookingList.size() > 0) {				
				for (DOMESTIC_VESSEL_BOOKING vessel_booking : domestic_vessel_bookingList) {
					DOMESTIC_VESSEL_LOG_STATUS vessel_log_status = new DOMESTIC_VESSEL_LOG_STATUS();
//					DOMESTIC_VESSEL_BOOKING getID = this.domestic_vessel_booking_repo.findById(vessel_booking.getId_request()).get();
					Date dt = new Date();
					Long time = dt.getTime();
					Timestamp ts = new Timestamp(time);
					if (!item.getStatus().isEmpty()) {
						vessel_booking.setStatus(item.getStatus());
						this.domestic_vessel_booking_repo.save(vessel_booking);
						
						vessel_log_status.setId_request(vessel_booking.getId_request());
						vessel_log_status.setCreated_date(ts);
						vessel_log_status.setStatus(item.getStatus());
						this.domestic_vessel_log_status_repo.save(vessel_log_status);
						
						result = new ResponseEntity<DOMESTIC_VESSEL_BOOKING>(HttpStatus.OK);
						hasil = "Success.";						
					}
				}
			} else {
				result = new ResponseEntity<DOMESTIC_VESSEL_BOOKING>(HttpStatus.NOT_FOUND);
				hasil = "Data not found."; 
			}
		} catch (Exception e) {
			// TODO: handle exception
			result = new ResponseEntity<DOMESTIC_VESSEL_BOOKING>(HttpStatus.BAD_REQUEST);
			hasil = "failed."+e;
		}
		return hasil;
	}

	//edit column status & paid_date findby orderID
	@PutMapping(value = "/paidStatusByOrderID/")
	private String editPaidStatusOrder(@RequestParam String orderID, 
			@Valid @RequestBody DTOStatusPaidDate item) 
			throws JsonProcessingException{
		String hasil = null;
		ResponseEntity<DOMESTIC_VESSEL_BOOKING> result = null;
		try {
			List<DOMESTIC_VESSEL_BOOKING> domestic_vessel_bookingList = this.domestic_vessel_booking_repo.findByOrderID(orderID);
			if (domestic_vessel_bookingList.size() > 0) {
				for (DOMESTIC_VESSEL_BOOKING vessel_booking : domestic_vessel_bookingList) {
					DOMESTIC_VESSEL_LOG_STATUS vessel_log_status = new DOMESTIC_VESSEL_LOG_STATUS();
//					DOMESTIC_VESSEL_BOOKING getID = this.domestic_vessel_booking_repo.findById(vessel_booking.getId_request()).get();
					Date dt = new Date();
					Long time = dt.getTime();
					Timestamp ts = new Timestamp(time);
					
					if(!item.getStatus().isEmpty() && !item.getPaid_date().equals(null)) {
						vessel_booking.setStatus(item.getStatus());
						vessel_booking.setPaid_date(item.getPaid_date());
						this.domestic_vessel_booking_repo.save(vessel_booking);
						
						vessel_log_status.setId_request(vessel_booking.getId_request());
						vessel_log_status.setCreated_date(ts);
						vessel_log_status.setStatus(item.getStatus());
						this.domestic_vessel_log_status_repo.save(vessel_log_status);
						result = new ResponseEntity<DOMESTIC_VESSEL_BOOKING>(HttpStatus.OK);
						hasil = "Success.";
					}
				}
			} else {
				result = new ResponseEntity<DOMESTIC_VESSEL_BOOKING>(HttpStatus.NOT_FOUND);
				hasil = "Data not found."; 
			}
		} catch (Exception e) {
			result = new ResponseEntity<DOMESTIC_VESSEL_BOOKING>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "failed."+e;
		}
		return hasil;
	} 
	
	@GetMapping(value = "/")
	private List<DOMESTIC_VESSEL_BOOKING> getvesselbooking(
			@RequestParam(defaultValue = "", required = false) String id_request,
			@RequestParam(defaultValue = "", required = false) String npwp){
		List<DOMESTIC_VESSEL_BOOKING> hasil = null;
		if(id_request.isEmpty() && npwp.isEmpty()) {
			hasil = this.domestic_vessel_booking_repo.findAll();
		} else if(!id_request.isEmpty() && npwp.isEmpty()){
			hasil = this.domestic_vessel_booking_repo.findByIdRequest(id_request);
		} else {
			hasil = this.domestic_vessel_booking_repo.findByNPWP(npwp);
		}
		return hasil;
	}
}
