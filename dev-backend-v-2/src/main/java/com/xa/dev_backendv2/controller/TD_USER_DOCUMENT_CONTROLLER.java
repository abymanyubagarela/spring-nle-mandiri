package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xa.dev_backendv2.model.TD_DOCUMENT;
import com.xa.dev_backendv2.model.TD_USER_DOCUMENT;
import com.xa.dev_backendv2.repository.TD_USER_DOCUMENT_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/user_document")
public class TD_USER_DOCUMENT_CONTROLLER {
	@Autowired
	private TD_USER_DOCUMENT_REPO td_user_document_repo;
	
	// simpan data
	@PostMapping(value = "/")
	private String save(@RequestBody TD_USER_DOCUMENT item)throws JsonProcessingException{
		String hasil = null;
		ResponseEntity<TD_USER_DOCUMENT> result = null;
		try {
			this.td_user_document_repo.save(item);
			result = new ResponseEntity<TD_USER_DOCUMENT>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			result = new ResponseEntity<TD_USER_DOCUMENT>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
		}
		return hasil;
	}
	
	@PutMapping(value="/")
	private String edit(@RequestParam Integer id_user_document, @RequestBody TD_USER_DOCUMENT item) {
		String hasil = null;
		ResponseEntity<TD_USER_DOCUMENT> result = null;
		if(this.td_user_document_repo.findById(id_user_document).isPresent()) {
			TD_USER_DOCUMENT getId = this.td_user_document_repo.findById(id_user_document).get();
			
			getId.setNpwp(item.getNpwp());
			getId.setId_document(item.getId_document());
			
			this.td_user_document_repo.save(getId);
			result = new ResponseEntity<TD_USER_DOCUMENT>(HttpStatus.OK);
			hasil = "success.";
		} else {
			result = new ResponseEntity<TD_USER_DOCUMENT>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed.";
		}
		return hasil;
	}
	
	@DeleteMapping(value="/")
	private String delete(@RequestParam Integer id_user_document) {
		String hasil = null;
		ResponseEntity<TD_USER_DOCUMENT> result = null;
		try {
			this.td_user_document_repo.deleteById(id_user_document);
			result = new ResponseEntity<TD_USER_DOCUMENT>(HttpStatus.OK);
			hasil = "success.";
		}catch(Exception e) {
			result = new ResponseEntity<TD_USER_DOCUMENT>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failder";
		}
		return hasil;
	}
	
	//find all
	@GetMapping(value = "/")
	private List<TD_USER_DOCUMENT> getUserDocument(){
		return this.td_user_document_repo.findAll();
	}
}
