package com.xa.dev_backendv2.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xa.dev_backendv2.model.DOCUMENT_TRUCKING_DETIL;
import com.xa.dev_backendv2.repository.DOCUMENT_TRUCKING_DETIL_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/DetilBooking")
public class DOCUMENT_TRUCKING_DETIL_CONTROLLER {
	@Autowired
	private DOCUMENT_TRUCKING_DETIL_REPO trucking_detil_repo;
	
	@Autowired
	private KafkaTemplate<String,String> kafkaTemplate;
	
	// simpan data
	@PostMapping(value = "/",consumes = {"application/json"},produces = {"application/json"})
	private String save(@RequestBody DOCUMENT_TRUCKING_DETIL item)throws JsonProcessingException{
		String hasil = null;
		String Topic = "dev-offer";
		ResponseEntity<DOCUMENT_TRUCKING_DETIL> result = null;
		try {
			Date dt = new Date();
			Long time = dt.getTime();
			Timestamp ts = new Timestamp(time);
			item.setCreated_date(ts);
			DOCUMENT_TRUCKING_DETIL resut = this.trucking_detil_repo.save(item);
			this.trucking_detil_repo.refresh(resut);
			
			DOCUMENT_TRUCKING_DETIL getId = this.trucking_detil_repo.findByIdBooking(item.getIdRequestBooking());
			ObjectMapper mapper = new ObjectMapper();
			String key = getId.getBooking().getId_User();
			kafkaTemplate.send(Topic,key,mapper.writeValueAsString(getId));
			result = new ResponseEntity<DOCUMENT_TRUCKING_DETIL>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			result = new ResponseEntity<DOCUMENT_TRUCKING_DETIL>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
		}
		return hasil;
	}
	
	// Edit data-all
	@PutMapping(value="/",consumes = {"application/json"},produces = {"application/json"})
	private String edit(@RequestParam String idRequestBooking, @RequestBody DOCUMENT_TRUCKING_DETIL item)
			throws JsonProcessingException{
		String hasil = null;
		String Topic = "nle-offerUpdate";
		ResponseEntity<DOCUMENT_TRUCKING_DETIL> result = null;
		if(this.trucking_detil_repo.findById(idRequestBooking).isPresent()) {
			DOCUMENT_TRUCKING_DETIL getID = this.trucking_detil_repo.findById(idRequestBooking).get();
			
			getID.setIdServiceOrder(item.getIdServiceOrder());
			getID.setHargaPenawaran(item.getHargaPenawaran());
			getID.setWaktuPenawaran(item.getWaktuPenawaran());
			getID.setTimestamp(item.getTimestamp());
			getID.setStatus(item.getStatus());
			getID.setPaidStatus(item.getPaidStatus());
			getID.setIdPlatform(item.getIdPlatform());
			getID.setIsBooked(item.getIsBooked());
			getID.setCreated_date(item.getCreated_date());
			
			this.trucking_detil_repo.save(getID);
			ObjectMapper mapper = new ObjectMapper();
			kafkaTemplate.send(Topic,idRequestBooking,mapper.writeValueAsString(getID));
			result = new ResponseEntity<DOCUMENT_TRUCKING_DETIL>(HttpStatus.OK);
			hasil = "Success.";
		} else {
			result = new ResponseEntity<DOCUMENT_TRUCKING_DETIL>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed.";
		}
		return hasil;
	}
	
	// Edit data status
	@PutMapping(value="/status/",consumes = {"application/json"},produces = {"application/json"})
	private String editStatus(@RequestParam String idRequestBooking, @RequestBody DOCUMENT_TRUCKING_DETIL item)
			throws JsonProcessingException{
		String hasil = null;
		String Topic = "dev-bookingStatus";
		ResponseEntity<DOCUMENT_TRUCKING_DETIL> result = null;
		if(this.trucking_detil_repo.findById(idRequestBooking).isPresent()) {
			DOCUMENT_TRUCKING_DETIL getID = this.trucking_detil_repo.findById(idRequestBooking).get();
			
			getID.setStatus(item.getStatus());
			this.trucking_detil_repo.save(getID);
			
			ObjectMapper mapper = new ObjectMapper();
			String key = getID.getBooking().getId_User();
			kafkaTemplate.send(Topic,key,mapper.writeValueAsString(getID));
			hasil = "Success.";
		} else {
			hasil = "Failed.";
		}
		return hasil;
	}
	
	// Edit data paidStatus
	@PutMapping(value="/paidStatus/",consumes = {"application/json"},produces = {"application/json"})
	private String editPaidStatus(@RequestParam String idRequestBooking, @RequestBody DOCUMENT_TRUCKING_DETIL item)
			throws JsonProcessingException{
		String hasil = null;
		String Topic = "dev-payment";
		ResponseEntity<DOCUMENT_TRUCKING_DETIL> result = null;
		if(this.trucking_detil_repo.findById(idRequestBooking).isPresent()) {
			DOCUMENT_TRUCKING_DETIL getID = this.trucking_detil_repo.findById(idRequestBooking).get();
			
			getID.setPaidStatus(item.getPaidStatus());
			
			this.trucking_detil_repo.save(getID);
			ObjectMapper mapper = new ObjectMapper();
			kafkaTemplate.send(Topic,idRequestBooking,mapper.writeValueAsString(getID));
			result = new ResponseEntity<DOCUMENT_TRUCKING_DETIL>(HttpStatus.OK);
			hasil = "Success.";
		} else {
			result = new ResponseEntity<DOCUMENT_TRUCKING_DETIL>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed.";
		}
		return hasil;
	}
	
	// Edit data isBooked
	@PutMapping(value="/isBooked/",consumes = {"application/json"},produces = {"application/json"})
	private String editIsBooked(@RequestParam String idRequestBooking, @RequestBody DOCUMENT_TRUCKING_DETIL item)
			throws JsonProcessingException{
		String hasil = null;
		String Topic = "dev-selectedOffer";
		ResponseEntity<DOCUMENT_TRUCKING_DETIL> result = null;
		if(this.trucking_detil_repo.findById(idRequestBooking).isPresent()) {
			DOCUMENT_TRUCKING_DETIL getID = this.trucking_detil_repo.findById(idRequestBooking).get();
			
			getID.setIsBooked(item.getIsBooked());
			getID.setBooking_note(item.getBooking_note());
			this.trucking_detil_repo.save(getID);
			
			Date dt = new Date();
			Long time = dt.getTime();
			Timestamp ts = new Timestamp(time);
			
			if(item.getIsBooked() == 1) {
				getID.setBooked_date(ts);
				getID.setStatus("Menunggu konfirmasi provider");
				String key = getID.getIdPlatform();
				this.trucking_detil_repo.save(getID);
				ObjectMapper mapper = new ObjectMapper();
				kafkaTemplate.send(Topic,key,mapper.writeValueAsString(getID));
			}
			hasil = "Success.";
		} else {
			hasil = "Failed.";
		}
		return hasil;
	}
	
	//find all
	@GetMapping(value="/")
	private Page<DOCUMENT_TRUCKING_DETIL> getTruckingDetil(Pageable pageable){
		return this.trucking_detil_repo.findAll(pageable);
	}
	
	//find requestbooking 
	@GetMapping(value="/idRequestBooking/")
	private List<DOCUMENT_TRUCKING_DETIL> searchidRequestBooking(@RequestParam String value){
		return this.trucking_detil_repo.findByIdRequestBooking(value);
	}
	
	//find id_user 
	@GetMapping(value="/idUser/")
	private List<DOCUMENT_TRUCKING_DETIL> searchIdUser(@RequestParam String value){
		return this.trucking_detil_repo.findByIdUser(value);
	}
	
	//find limit data 
	@GetMapping(value="/Limit/")
	private List<DOCUMENT_TRUCKING_DETIL> getLimit(@RequestParam Integer value){
		return this.trucking_detil_repo.findByLimit(value);
	}
	
	//find limit page 
	@GetMapping(value="/LimitPage/")
	private Page<DOCUMENT_TRUCKING_DETIL> getLimitPage(@RequestParam(defaultValue = "", required = false) String bookedDate,
			@RequestParam(defaultValue = "", required = false) String idRequestBooking,
			@RequestParam String npwp, @RequestParam (required = false) Integer documentID ,
			Pageable pageable){
		documentID = documentID==null?0:documentID;
		return this.trucking_detil_repo.findByLimitPageAndBookedDate(bookedDate, idRequestBooking,npwp,documentID, pageable);
	}
}
