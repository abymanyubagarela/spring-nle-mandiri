package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xa.dev_backendv2.model.PAYMENT_BILLING;
import com.xa.dev_backendv2.repository.PAYMENT_BILLING_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/PaymentBilling")
public class PAYMENT_BILLING_CONTROLLER {
	@Autowired
	private PAYMENT_BILLING_REPO paymentBillingRepo;
	
	@GetMapping(value="/")
	private List<PAYMENT_BILLING> getModule(@RequestParam String module,@RequestParam String moduleID){
		return this.paymentBillingRepo.findByModule(module,moduleID);
	}
	
}
