package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xa.dev_backendv2.model.MST_PLATFORM;
import com.xa.dev_backendv2.repository.MST_PLATFORM_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/platform")
public class MST_PLATFORM_CONTROLLER {
	@Autowired
	private MST_PLATFORM_REPO mst_platform_repo;
	
	// simpan data
	@PostMapping(value = "/")
	private String save(@RequestBody MST_PLATFORM item)throws JsonProcessingException{
		String hasil = null;
		ResponseEntity<MST_PLATFORM> result = null;
		try {
			this.mst_platform_repo.save(item);
			result = new ResponseEntity<MST_PLATFORM>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			result = new ResponseEntity<MST_PLATFORM>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
		}
		return hasil;
	}
	
	@PutMapping(value="/")
	private String edit(@RequestParam String id_platform, @RequestBody MST_PLATFORM item) {
		String hasil = null;
		ResponseEntity<MST_PLATFORM> result = null;
		if(this.mst_platform_repo.findById(id_platform).isPresent()) {
			MST_PLATFORM getId = this.mst_platform_repo.findById(id_platform).get();
			
			getId.setNama_platform(item.getNama_platform());
			getId.setCompany(item.getCompany());
			getId.setTc_url(item.getTc_url());
			getId.setPayment_url(item.getPayment_url());
			getId.setLive_tracking_url(item.getLive_tracking_url());
			
			this.mst_platform_repo.save(getId);
			result = new ResponseEntity<MST_PLATFORM>(HttpStatus.OK);
			hasil = "success.";
		} else {
			result = new ResponseEntity<MST_PLATFORM>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed.";
		}
		return hasil;
	}
	
	@DeleteMapping(value="/")
	private String delete(@RequestParam String id_platform) {
		String hasil = null;
		ResponseEntity<MST_PLATFORM> result = null;
		try {
			this.mst_platform_repo.deleteById(id_platform);
			result = new ResponseEntity<MST_PLATFORM>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			result = new ResponseEntity<MST_PLATFORM>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed.";
		}
		return hasil;
	}
	//find all
	@GetMapping(value = "/")
	private List<MST_PLATFORM> getPlatformType(){
		return this.mst_platform_repo.findAll();
	}
}
