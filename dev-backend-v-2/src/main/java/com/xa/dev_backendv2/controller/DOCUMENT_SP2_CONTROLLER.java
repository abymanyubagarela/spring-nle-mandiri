package com.xa.dev_backendv2.controller;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.xa.dev_backendv2.model.*;
import com.xa.dev_backendv2.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/document_sp2")
public class DOCUMENT_SP2_CONTROLLER {
	@Autowired
	private DOCUMENT_SP2_REPO document_sp2_repo;
	@Autowired
	private DOCUMENT_SP2_CONTAINER_REPO container_repo;
	@Autowired
	private DOCUMENT_SP2_LOG_REPO doc_sp2_log_repo;
	@Autowired
	PAYMENT_BILLING_REPO payment_billing_repo;
	@Autowired
	MST_PLATFORM_REPO mst_platform_repo;

	// simpan data
	@PostMapping(value = "/")
	private String save(@RequestBody DOCUMENT_SP2 item)throws JsonProcessingException{
		String hasil = null;
		ResponseEntity<DOCUMENT_SP2> result = null;
		try {
			Date dt = new Date();
			Long time = dt.getTime();
			Timestamp ts = new Timestamp(time);
			item.setRequest_date(ts);
			Set<DOCUMENT_SP2_CONTAINER> document_sp2_containerSet = item.getContainer();
			item.setContainer(new HashSet<>());
			DOCUMENT_SP2 document_sp2 = this.document_sp2_repo.save(item);

			for (DOCUMENT_SP2_CONTAINER container : document_sp2_containerSet){
				DOCUMENT_SP2_CONTAINER container1 = container_repo.findByIdDocumentSp2(document_sp2).orElse(container);
				container.setIdDocumentSp2(document_sp2);
				container_repo.save(container1);
			}

//			Trigger DOCUMENT SP2 LOG
			DOCUMENT_SP2_LOG sp2_log = new DOCUMENT_SP2_LOG();
			sp2_log.setCreate_date(ts);
			sp2_log.setId_document_sp2(document_sp2.getId_document_sp2());
			sp2_log.setStatus(document_sp2.getStatus());
			this.doc_sp2_log_repo.save(sp2_log);

			result = new ResponseEntity<DOCUMENT_SP2>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			result = new ResponseEntity<DOCUMENT_SP2>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
		}
		return hasil;
	}

	//find all
//	@GetMapping(value = "/")
//	private List<DOCUMENT_SP2> getDocumentSp2(){
//		return this.document_sp2_repo.findAll();
//	}
//	find by npwp
	@GetMapping(value = "/")
	private Page<DOCUMENT_SP2> getBynpwp(@RequestParam String npwp ,@RequestParam(defaultValue = "0",required = false) Integer isFinished, Pageable pageable){
		return this.document_sp2_repo.findByNpwp(npwp,isFinished, pageable);
	}
	//find by id_document
	@GetMapping(value = "/finddata")
	private List<DOCUMENT_SP2> getByDocumentID(@RequestParam Integer documentID){
		return this.document_sp2_repo.findAllByIdDocument(documentID);
	}
//	Find by id_document_sp2
	@GetMapping(value = "/detail/{id_document_sp2}")
	private List<DOCUMENT_SP2> getByIdDocumentSp2(@PathVariable String id_document_sp2){
		return this.document_sp2_repo.findByIdDocumentSp2(id_document_sp2);
	}

	@PutMapping(value="/price")
	private String editPrice(@RequestParam String documentSp2ID, @RequestBody DOCUMENT_SP2 item) {
		String hasil;
		if(this.document_sp2_repo.findById(documentSp2ID).isPresent()) {
			DOCUMENT_SP2 getID = this.document_sp2_repo.findById(documentSp2ID).get();
			getID.setPrice(item.getPrice());
			getID.setStatus(item.getStatus());
			getID.setPayment_url(item.getPayment_url());
			this.document_sp2_repo.save(getID);

			MST_PLATFORM platform = mst_platform_repo.findById(getID.getId_platform()).orElse(null);

			Date dt = new Date();
			long time = dt.getTime();
			Timestamp ts = new Timestamp(time);
			try {
				String moduleUniqueId = getID.getIdDocument().toString();
				String module = "Documents";
				PAYMENT_BILLING payment_billing = payment_billing_repo.findByModuleUniqueIdAndModule(moduleUniqueId, module).orElse( new PAYMENT_BILLING() );
				payment_billing.setModule(module);
				payment_billing.setModuleUniqueId(moduleUniqueId);
				payment_billing.setId_service(2);
				payment_billing.setService_unique_id(documentSp2ID);
				String sisInfo = getID.getContainer().stream().map(DOCUMENT_SP2_CONTAINER::getContainer_no).collect(Collectors.joining(","));
				payment_billing.setItem_info(String.format("Container %s", sisInfo));
				BigDecimal amount = getID.getPrice()==null?new BigDecimal(0):getID.getPrice();
				payment_billing.setAmount( amount );
				payment_billing.setCreated_date(ts);
				payment_billing.setIdPlatform(platform);
				payment_billing_repo.save(payment_billing);
			} catch (Exception e) {
				hasil = "Failed.";
			}
//			Trigger DOCUMENT SP2 LOG
			DOCUMENT_SP2_LOG sp2_log = new DOCUMENT_SP2_LOG();
			sp2_log.setCreate_date(ts);
			sp2_log.setId_document_sp2(documentSp2ID);
			sp2_log.setStatus(item.getStatus());

			this.doc_sp2_log_repo.save(sp2_log);

			hasil = "success.";
		} else {
			hasil = "Failed.";
		}
		return hasil;
	}
	@PutMapping(value="/paidThrudDate")
	private String paidTrud(@RequestParam String documentSp2ID, @RequestBody DOCUMENT_SP2 item) {
		String hasil;
		if(this.document_sp2_repo.findById(documentSp2ID).isPresent()) {
			DOCUMENT_SP2 getID = this.document_sp2_repo.findById(documentSp2ID).get();
			getID.setPaid_thrud_date(item.getPaid_thrud_date());
			getID.setStatus(item.getStatus());
			this.document_sp2_repo.save(getID);

			Date dt = new Date();
			Long time = dt.getTime();
			Timestamp ts = new Timestamp(time);

//			Trigger DOCUMENT SP2 LOG
			DOCUMENT_SP2_LOG sp2_log = new DOCUMENT_SP2_LOG();
			sp2_log.setCreate_date(ts);
			sp2_log.setId_document_sp2(documentSp2ID);
			sp2_log.setStatus(item.getStatus());

			this.doc_sp2_log_repo.save(sp2_log);

			hasil = "success.";
		} else {
			hasil = "Failed.";
		}
		return hasil;
	}
	@PutMapping(value="/status")
	private String setStatus(@RequestParam String documentSp2ID, @RequestBody DOCUMENT_SP2 item) {
		String hasil;
		if(this.document_sp2_repo.findById(documentSp2ID).isPresent()) {
			DOCUMENT_SP2 getID = this.document_sp2_repo.findById(documentSp2ID).get();
			getID.setStatus(item.getStatus());
			getID.setIs_finished(item.getIs_finished());
			getID.setGate_pass(item.getGate_pass());
			this.document_sp2_repo.save(getID);

			Date dt = new Date();
			Long time = dt.getTime();
			Timestamp ts = new Timestamp(time);

//			Trigger DOCUMENT SP2 LOG
			DOCUMENT_SP2_LOG sp2_log = new DOCUMENT_SP2_LOG();
			sp2_log.setCreate_date(ts);
			sp2_log.setId_document_sp2(documentSp2ID);
			sp2_log.setStatus(item.getStatus());
			this.doc_sp2_log_repo.save(sp2_log);

			hasil = "success.";
		} else {
			hasil = "Failed.";
		}
		return hasil;
	}
	@GetMapping(value = "/getContainers")
	private List<DOCUMENT_SP2_CONTAINER> document_sp2_containers(@RequestParam Integer documentID, @RequestParam String paidThrudDate){
		Date date = java.sql.Date.valueOf(paidThrudDate);
		return this.container_repo.findAllContainerByQuery(documentID , date);
	}
}
