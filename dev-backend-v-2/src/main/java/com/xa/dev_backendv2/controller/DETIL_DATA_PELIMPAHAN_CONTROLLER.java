package com.xa.dev_backendv2.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xa.dev_backendv2.model.DETIL_DATA_PELIMPAHAN;
import com.xa.dev_backendv2.repository.DATA_PELIMPAHAN_REPO;
import com.xa.dev_backendv2.repository.DETIL_DATA_PELIMPAHAN_REPO;
import com.xa.dev_backendv2.service.FileStorageService;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/detil_data_pelimpahan")
public class DETIL_DATA_PELIMPAHAN_CONTROLLER {
	@Autowired
	private DETIL_DATA_PELIMPAHAN_REPO document_detil_data_pelimpahan_repo;
	
	@Autowired
	private DATA_PELIMPAHAN_REPO data_pelimpahan_repo;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	// simpan data
	@PostMapping(value = "/")
	private String save(@RequestBody DETIL_DATA_PELIMPAHAN item)throws JsonProcessingException{
		String hasil = null;
		ResponseEntity<DETIL_DATA_PELIMPAHAN> result = null;
		try {
			this.document_detil_data_pelimpahan_repo.save(item);
			result = new ResponseEntity<DETIL_DATA_PELIMPAHAN>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			result = new ResponseEntity<DETIL_DATA_PELIMPAHAN>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
		}
		return hasil;
	}
	
	//find all
	@GetMapping(value = "/")
	private Page<DETIL_DATA_PELIMPAHAN> getDocumentDetilDataPelimpahan(@RequestParam (defaultValue = "", required = false) String createdNpwp,
			@RequestParam (defaultValue = "", required = false) String ppjk,
			@RequestParam (defaultValue = "", required = false) String doctype,
			Pageable page){
		return this.document_detil_data_pelimpahan_repo.findByPage(createdNpwp, ppjk, doctype, page);
	}
	
	@GetMapping(value="/doctype/")
	private List<DETIL_DATA_PELIMPAHAN> getPpjkDoctype(@RequestParam String ppjk,
			@RequestParam String doctype){
		return this.document_detil_data_pelimpahan_repo.findByPPJKdoctype(ppjk, doctype);
	}
	@PutMapping(value="/",consumes = {"application/json"},produces = {"application/json"})
	private String edit(@RequestParam Integer id, @RequestBody DETIL_DATA_PELIMPAHAN item ) throws JsonProcessingException{
		String hasil = null;
		ResponseEntity<DETIL_DATA_PELIMPAHAN> result = null;
		if(this.document_detil_data_pelimpahan_repo.findById(id).isPresent()) {
			DETIL_DATA_PELIMPAHAN getId = this.document_detil_data_pelimpahan_repo.findById(id).get();
			
			getId.setBl_no(item.getBl_no());
			getId.setBl_date(item.getBl_date());
			getId.setNama_ppjk(item.getNama_ppjk());
			getId.setPpjk(item.getPpjk());
			getId.setCreated_npwp(item.getCreated_npwp());
			getId.setKd_document_type(item.getKd_document_type());
			getId.setDelivery_order(item.getDelivery_order());
			getId.setWarehouse(item.getWarehouse());
			getId.setSp2(item.getSp2());
			getId.setDepo(item.getDepo());
			getId.setTrucking(item.getTrucking());
			getId.setDomestic(item.getDomestic());
			
			this.document_detil_data_pelimpahan_repo.save(getId);
			result = new ResponseEntity<DETIL_DATA_PELIMPAHAN>(HttpStatus.OK);
			hasil = "Update Success";
		} else {
			result = new ResponseEntity<DETIL_DATA_PELIMPAHAN>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Update Failed";
		}
		return hasil;
	}
	
	@DeleteMapping(value="/")
	private String delete(@RequestParam Integer id) {
		String hasil = null;
		String idSuratKuasa = this.document_detil_data_pelimpahan_repo.findByIdSuratKuasa(id);
		this.document_detil_data_pelimpahan_repo.deleteById(id);
		try {
			Date dt = this.document_detil_data_pelimpahan_repo.findFileDate(idSuratKuasa);
			final java.sql.Date dt1 = new java.sql.Date(dt.getTime());
			if(this.document_detil_data_pelimpahan_repo.findToGetListSuratKuasa(idSuratKuasa).isEmpty()) {
				//delete file
				String fileName = this.document_detil_data_pelimpahan_repo.findByFilename(idSuratKuasa);
				fileStorageService.deleteFile(fileName, dt1);
				
				//delete data
				this.data_pelimpahan_repo.deleteById(idSuratKuasa);
				hasil = "Delete Data Detil and File Success.";
			} else {
				hasil = "Delete Data Detil Success.";
			}
		} catch (Exception e) {
			hasil = "Failed. "+e;
		}
		return hasil;
	}
}
