package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xa.dev_backendv2.model.DOCUMENT_LOG_TRUCK;
import com.xa.dev_backendv2.repository.DOCUMENT_LOG_TRUCK_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value = "/LogTruck")
public class DOCUMENT_LOG_TRUCK_CONTROLLER {
	@Autowired
	private DOCUMENT_LOG_TRUCK_REPO log_truck_repo;
	
	@Autowired
	private KafkaTemplate<String,String> kafkaTemplate;
	
	@PostMapping(value="/",consumes = {"application/json"},produces = {"application/json"})
	private String save(@RequestBody DOCUMENT_LOG_TRUCK item)throws JsonProcessingException {
		String hasil = "";
		try {
			this.log_truck_repo.save(item);
			return hasil = "Success";
		} catch (Exception e) {
			return hasil = "Failed "+e;
		}
	}
	
	@GetMapping(value="/")
	private List<DOCUMENT_LOG_TRUCK> getLogTruck(){
		return this.log_truck_repo.findAll();
	}
}
