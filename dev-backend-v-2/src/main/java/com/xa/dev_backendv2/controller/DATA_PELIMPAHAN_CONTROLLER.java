package com.xa.dev_backendv2.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xa.dev_backendv2.exception.MyFileNotFoundException;
import com.xa.dev_backendv2.model.DATA_PELIMPAHAN;
import com.xa.dev_backendv2.repository.DATA_PELIMPAHAN_REPO;
import com.xa.dev_backendv2.service.FileStorageService;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/data_pelimpahan")
public class DATA_PELIMPAHAN_CONTROLLER {
	private static final Logger logger = LoggerFactory.getLogger(DATA_PELIMPAHAN.class);
	
	@Autowired
	private DATA_PELIMPAHAN_REPO document_data_pelimpahan_repo;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	// simpan data
	@PostMapping(value = "/")
	private ResponseEntity<DATA_PELIMPAHAN> save(@RequestBody DATA_PELIMPAHAN item)throws JsonProcessingException{
		String hasil = null;
		ResponseEntity<DATA_PELIMPAHAN> result = null;
		try {
			this.document_data_pelimpahan_repo.save(item);
			result = new ResponseEntity<DATA_PELIMPAHAN>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			result = new ResponseEntity<DATA_PELIMPAHAN>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed."+e;
		}
		return result;
	}
	
	@PostMapping(value="/file")
	private DATA_PELIMPAHAN saveFile(@RequestParam("file") MultipartFile file) {
		UUID uuid = UUID.randomUUID();
		String idSuratKuasa = uuid.toString();
		Date dt = new Date();
		String fileName = fileStorageService.storeFile(file, idSuratKuasa, dt);
		final java.sql.Date dt1 = new java.sql.Date(dt.getTime());
		
//		String pathFile = (fileProperties.getUploadDir()+"/").replace(".","");
		String fileViewUri = (ServletUriComponentsBuilder.fromCurrentRequestUri()
    			.scheme("https")
    			.host("esbbcext01.beacukai.go.id").port(8088).replacePath(null)
    			.path("/data_pelimpahan/").path("viewFile/").path(fileName).build()
    			.toUri()).toString();
		return new DATA_PELIMPAHAN(idSuratKuasa, fileName, dt1, fileViewUri);
	}
	
	@PutMapping(value = "/")
	private ResponseEntity<DATA_PELIMPAHAN> edit(@RequestParam String idSuratKuasa, 
			@RequestBody DATA_PELIMPAHAN item) {
		String hasil = null;
		ResponseEntity<DATA_PELIMPAHAN> result = null;
		if(this.document_data_pelimpahan_repo.findById(idSuratKuasa).isPresent()) {
			DATA_PELIMPAHAN getID = this.document_data_pelimpahan_repo.findById(idSuratKuasa).get();
			
			getID.setFilename(item.getFilename());
			getID.setFile_date(item.getFile_date());
			getID.setFile(item.getFile());
			
			this.document_data_pelimpahan_repo.save(getID);
			hasil = "Success.";
			result =  new ResponseEntity<DATA_PELIMPAHAN>(HttpStatus.OK);
		} else {
			hasil = "Failed.";
			result =  new ResponseEntity<DATA_PELIMPAHAN>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@PutMapping(value = "/file")
	public DATA_PELIMPAHAN editFile(@RequestParam String filename, 
			@RequestParam("file") MultipartFile file) {
		String fileName = null;
		String fileViewUri = null;
		String idSuratKuasa = null;
		Date dt = this.document_data_pelimpahan_repo.findByTgl(filename);
		
		final java.sql.Date dt1 = new java.sql.Date(dt.getTime());
		if (filename != null) {
			fileStorageService.deleteFile(filename, dt1);
			
			idSuratKuasa = this.document_data_pelimpahan_repo.findByidSuratKuasa(filename);
			fileName = fileStorageService.storeFile(file, idSuratKuasa, dt1);
			fileViewUri = (ServletUriComponentsBuilder.fromCurrentRequestUri()
	    			.scheme("https")
	    			.host("esbbcext01.beacukai.go.id").port(8088).replacePath(null)
	    			.path("/data_pelimpahan/").path("viewFile/").path(fileName).build()
	    			.toUri()).toString();
		}
		return new DATA_PELIMPAHAN(idSuratKuasa, fileName, dt1, fileViewUri);
	}
	
	//find all
	@GetMapping(value = "/")
	private Page<DATA_PELIMPAHAN> getDocumentDataPelimpahan(Pageable pageable){
		return this.document_data_pelimpahan_repo.findAll(pageable);
	}
	
	private static final List<String> contentTypes = Arrays.asList("image/png", "image/jpeg");
	
	@GetMapping(value = "/viewFile/{fileName:.+}")
	public ResponseEntity<Resource> viewFile(
			@PathVariable String fileName,
			HttpServletRequest request) throws Exception{
		// load file as resource
		Date dt = this.document_data_pelimpahan_repo.findByTgl(fileName);
		Resource resource = fileStorageService.loadFileAsResource(fileName,dt);
		ResponseEntity<Resource> result = null;
		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (Exception e) {
			logger.info("Could not determine file type.");
		}
		
		// Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
        
        if(contentTypes.contains(contentType)) {
        	result = ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .body(resource); 
        } else {
        	result = ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
        }
        
        return result;
    }
}
