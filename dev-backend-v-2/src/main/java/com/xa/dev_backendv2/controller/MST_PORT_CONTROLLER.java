package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xa.dev_backendv2.model.MST_PORT;
import com.xa.dev_backendv2.repository.MST_PORT_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/Port")
public class MST_PORT_CONTROLLER {
	@Autowired
	private MST_PORT_REPO mst_port_repo;
	
	@GetMapping(value = "/")
	private List<MST_PORT> getAllData(){
		return this.mst_port_repo.findAll();
	}
	
	@GetMapping(value = "/Asal/")
	private List<MST_PORT> getAsal(){
		return this.mst_port_repo.findByAsal(1);
	}
	
	@GetMapping(value ="/Tujuan/")
	private List<MST_PORT> getTujuan(){
		return this.mst_port_repo.findByTujuan(1);
	}
	
//	@PostMapping(value = "/")
//	private String save(@RequestBody MST_PORT item)throws JsonProcessingException{
//		String hasil = null;
//		ResponseEntity<MST_PORT> result = null;
//		try {
//			this.mst_port_repo.save(item);
//			result = new ResponseEntity<MST_PORT>(HttpStatus.OK);
//			hasil = "Success";
//		} catch (Exception e) {
//			result = new ResponseEntity<MST_PORT>(HttpStatus.INTERNAL_SERVER_ERROR);
//			hasil = "Failed " + e;
//		}
//		return hasil;
//	}
	
}
