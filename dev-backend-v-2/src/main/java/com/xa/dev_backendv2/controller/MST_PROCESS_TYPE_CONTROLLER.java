package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xa.dev_backendv2.model.MST_PROCESS_TYPE;
import com.xa.dev_backendv2.repository.MST_PROCESS_TYPE_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/process_type")
public class MST_PROCESS_TYPE_CONTROLLER {
	@Autowired
	private MST_PROCESS_TYPE_REPO mst_process_type_repo;
	
//	// simpan data
//	@PostMapping(value = "/")
//	private String save(@RequestBody MST_PROCESS_TYPE item)throws JsonProcessingException{
//		String hasil = null;
//		ResponseEntity<MST_PROCESS_TYPE> result = null;
//		try {
//			this.mst_process_type_repo.save(item);
//			result = new ResponseEntity<MST_PROCESS_TYPE>(HttpStatus.OK);
//			hasil = "success.";
//		} catch (Exception e) {
//			result = new ResponseEntity<MST_PROCESS_TYPE>(HttpStatus.INTERNAL_SERVER_ERROR);
//			hasil = "Failed."+e;
//		}
//		return hasil;
//	}
	
	//find all
	@GetMapping(value = "/")
	private List<MST_PROCESS_TYPE> getProcessType(){
		return this.mst_process_type_repo.findAll();
	}
}
