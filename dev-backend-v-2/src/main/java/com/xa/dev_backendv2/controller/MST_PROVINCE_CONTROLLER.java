package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xa.dev_backendv2.model.MST_PROVINCE;
import com.xa.dev_backendv2.repository.MST_PROVINCE_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/Province")
public class MST_PROVINCE_CONTROLLER {
	@Autowired
	private MST_PROVINCE_REPO mst_prov_repo;
	
	@GetMapping(value="/")
	private List<MST_PROVINCE> getAll(){
		return this.mst_prov_repo.findAll();
	}
}
