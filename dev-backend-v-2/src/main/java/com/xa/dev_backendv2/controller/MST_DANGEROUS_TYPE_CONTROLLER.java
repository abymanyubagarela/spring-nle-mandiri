package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.dev_backendv2.model.MST_DANGEROUS_TYPE;
import com.xa.dev_backendv2.repository.MST_DANGEROUS_TYPE_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/DangerousType")
public class MST_DANGEROUS_TYPE_CONTROLLER {
	@Autowired
	private MST_DANGEROUS_TYPE_REPO dangerousTypeRepo;
	
	@GetMapping(value = "/")
	private List<MST_DANGEROUS_TYPE> getListData(){
		return this.dangerousTypeRepo.findAll();
	}
}
