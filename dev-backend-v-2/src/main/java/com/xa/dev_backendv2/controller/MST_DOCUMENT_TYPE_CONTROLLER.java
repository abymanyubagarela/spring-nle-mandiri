package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xa.dev_backendv2.model.MST_DOCUMENT_TYPE;
import com.xa.dev_backendv2.repository.MST_DOCUMENT_TYPE_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/document_type")
public class MST_DOCUMENT_TYPE_CONTROLLER {
	@Autowired
	private MST_DOCUMENT_TYPE_REPO mst_document_type_repo;
	
	//find all
	@GetMapping(value = "/")
	private List<MST_DOCUMENT_TYPE> getDocumentType(){
		return this.mst_document_type_repo.findAll();
	}
}
