package com.xa.dev_backendv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xa.dev_backendv2.model.MST_KAWASAN;
import com.xa.dev_backendv2.repository.MST_KAWASAN_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value="/Kawasan")
public class MST_KAWASAN_CONTROLLER {
	@Autowired
	private MST_KAWASAN_REPO mst_kawasan_repo;
	
	@GetMapping(value = "/")
	private List<MST_KAWASAN> getIdPort(@RequestParam Integer id_port){
		return this.mst_kawasan_repo.findByIdPort(id_port);
	}
	
	@GetMapping(value = "/findAll/")
	private List<MST_KAWASAN> getAllData(){
		return this.mst_kawasan_repo.findAll();
	}
	
	@PostMapping(value = "/")
	private String save(@RequestBody MST_KAWASAN item)throws JsonProcessingException{
		String hasil = null;
		ResponseEntity<MST_KAWASAN> result = null;
		try {
			this.mst_kawasan_repo.save(item);
			result = new ResponseEntity<MST_KAWASAN>(HttpStatus.OK);
			hasil = "Success";
		} catch (Exception e) {
			result = new ResponseEntity<MST_KAWASAN>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed " + e;
		}
		return hasil;
	}
	
	@PutMapping(value="/")
	private String edit(@RequestParam Integer id_kawasan, @RequestBody MST_KAWASAN item) {
		String hasil = null;
		ResponseEntity<MST_KAWASAN> result = null;
		if(this.mst_kawasan_repo.findById(id_kawasan).isPresent()) {
			MST_KAWASAN getId = this.mst_kawasan_repo.findById(id_kawasan).get();
			
			getId.setNama_kawasan(item.getNama_kawasan());
			getId.setNama_kota(item.getNama_kota());
			getId.setId_port(item.getId_port());
			
			this.mst_kawasan_repo.save(getId);
			result = new ResponseEntity<MST_KAWASAN>(HttpStatus.OK);
			hasil = "Success";
		} else {
			result = new ResponseEntity<MST_KAWASAN>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed";
		}
		return hasil;
	}
	
	@DeleteMapping(value="/")
	private String delete(@RequestParam Integer id_kawasan, @RequestBody MST_KAWASAN item) {
		String hasil = null;
		ResponseEntity<MST_KAWASAN> result = null;
		try {
			this.mst_kawasan_repo.deleteById(id_kawasan);
			result = new ResponseEntity<MST_KAWASAN>(HttpStatus.OK);
			hasil = "Success";
		} catch (Exception e) {
			result = new ResponseEntity<MST_KAWASAN>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "Failed";
		}
		return hasil;
	}
}
