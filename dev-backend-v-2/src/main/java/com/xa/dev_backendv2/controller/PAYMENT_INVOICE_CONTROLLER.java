package com.xa.dev_backendv2.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xa.dev_backendv2.model.*;
import com.xa.dev_backendv2.repository.PAYMENT_INVOICE_ITEM_REPO;
import com.xa.dev_backendv2.repository.PAYMENT_INVOICE_REPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value = "/invoice")
public class PAYMENT_INVOICE_CONTROLLER {

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	@Autowired
	private PAYMENT_INVOICE_REPO payment_invoice_repo;
	@Autowired
	private PAYMENT_INVOICE_ITEM_REPO payment_invoice_item_repo;
	@Autowired
	private Environment env;

	@PostMapping(value = "/", consumes = { "application/json" }, produces = { "application/json" })
	private String save(@RequestBody PAYMENT_INVOICE item) {
		String hasil = null;
		String Topic = "dev-paymentBill";
		try {
			Date dt = new Date();
			long time = dt.getTime();
			Timestamp ts = new Timestamp(time);
			item.setCreated_date(ts);
			item.setVa_created(1);
			item.setUpdated_date(ts);
			BigDecimal orderAmount = item.getInvoice_item().stream().map(PAYMENT_INVOICE_ITEM::getAmount)
					.reduce(BigDecimal.ZERO, BigDecimal::add);
			item.setOrder_amount(orderAmount);
			Set<PAYMENT_INVOICE_ITEM> payment_invoice_itemSet = item.getInvoice_item();
			item.setInvoice_item(new HashSet<>());
			PAYMENT_INVOICE payment_invoice = this.payment_invoice_repo.save(item);
			payment_invoice_itemSet = payment_invoice_itemSet.stream().peek(m -> m.setOrder_number(payment_invoice))
					.collect(Collectors.toSet());
			this.payment_invoice_item_repo.saveAll(payment_invoice_itemSet);
			new ResponseEntity<DATA_TRANSPORTER>(HttpStatus.OK);
			hasil = "success.";
		} catch (Exception e) {
			e.printStackTrace();
			new ResponseEntity<DATA_TRANSPORTER>(HttpStatus.INTERNAL_SERVER_ERROR);
			hasil = "failed." + e;
			System.out.println(e);
		}

		// kafka
		try {
			ObjectMapper mapper = new ObjectMapper();
			String key = String.valueOf(item.getId_payment_services());
			// kafkaTemplate.send(Topic,key,mapper.writeValueAsString(item));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return hasil;
	}

	@GetMapping(value = "/")
	private List<PAYMENT_INVOICE> getModule(@RequestParam String module, @RequestParam String moduleID) {
		return this.payment_invoice_repo.findByModule(module, moduleID);
	}

	@GetMapping(value = "/findByOrderNumber")
	private Optional<PAYMENT_INVOICE> getInvoice(@RequestParam String orderNumber) {
		return this.payment_invoice_repo.findById(orderNumber);
	}

	@PutMapping(value = "/invoiceAccountNumber")
	private String editAccountNumber(@RequestParam String doID, @RequestBody PAYMENT_INVOICE item) {
		String hasil = null;
		if (this.payment_invoice_repo.findById(doID).isPresent()) {
			PAYMENT_INVOICE getID = this.payment_invoice_repo.findById(doID).get();
			getID.setInvoiceAccountNumber(item.getInvoiceAccountNumber());

			try {
				this.payment_invoice_repo.save(getID);
				new ResponseEntity<DATA_TRANSPORTER>(HttpStatus.OK);
				hasil = "success.";
			} catch (Exception e) {
				e.printStackTrace();
				new ResponseEntity<DATA_TRANSPORTER>(HttpStatus.INTERNAL_SERVER_ERROR);
				hasil = "failed." + e;
				System.out.println(e);
			}
		}
		return hasil;
	}

	@GetMapping(value = "/getVirtualAccount")
	private void getVirtualAccount(@RequestParam String orderNumber) {
		PAYMENT_BANKING_MANDIRI_CONTROLLER mandiri = new PAYMENT_BANKING_MANDIRI_CONTROLLER(this.payment_invoice_repo,
				this.payment_invoice_item_repo, this.env);
		mandiri.createVirtualAccountMandiri(orderNumber);
	}
}
