package com.xa.dev_backendv2.controller;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.HttpMethod;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.xa.dev_backendv2.DTO.DTOMandiriBillInfo;
import com.xa.dev_backendv2.DTO.DTOMandiriInquiry;
import com.xa.dev_backendv2.DTO.DTOMandiriNotification;
import com.xa.dev_backendv2.DTO.DTOMandiriProvider;
import com.xa.dev_backendv2.DTO.DTOMandiriProviderItem;
import com.xa.dev_backendv2.DTO.DTOMandiriResponseToken;
import com.xa.dev_backendv2.DTO.DTOMandiriVirtualAccountInfo;
import com.xa.dev_backendv2.model.DATA_TRANSPORTER;
import com.xa.dev_backendv2.model.PAYMENT_INVOICE;
import com.xa.dev_backendv2.model.PAYMENT_INVOICE_ITEM;
import com.xa.dev_backendv2.repository.PAYMENT_INVOICE_ITEM_REPO;
import com.xa.dev_backendv2.repository.PAYMENT_INVOICE_REPO;
import com.xa.dev_backendv2.service.mandiri.InvokeRestService;
import com.xa.dev_backendv2.service.mandiri.UtilDigitalSignature;

@CrossOrigin(allowedHeaders = "*", origins = "*", allowCredentials = "true")
@RestController
@RequestMapping(value = "/mandiri")
public class PAYMENT_BANKING_MANDIRI_CONTROLLER {
	@Autowired
	private Environment env;
	@Autowired
	private PAYMENT_INVOICE_REPO payment_invoice_repo;

	private InvokeRestService invoke_rest_service = new InvokeRestService();

	ObjectWriter object_mapper = new ObjectMapper().writer().withDefaultPrettyPrinter();

	public PAYMENT_BANKING_MANDIRI_CONTROLLER(PAYMENT_INVOICE_REPO payment_invoice_repo,
			PAYMENT_INVOICE_ITEM_REPO payment_invoice_item_repo, Environment env) {
		this.payment_invoice_repo = payment_invoice_repo;
		this.env = env;
	}
	
	public String createVirtualAccountMandiri(String OrderNumber) {

		PAYMENT_INVOICE invoice_detail = this.payment_invoice_repo.findById(OrderNumber).get();

		DTOMandiriBillInfo bill_info = new DTOMandiriBillInfo();
		bill_info.setOrderNumber(invoice_detail.getOrder_number());
		bill_info.setOrderAmount(invoice_detail.getOrder_amount().toString());
		bill_info.setCustomerNumber(invoice_detail.getCostumer_number());
		bill_info.setCompanyCode(invoice_detail.getCostumer_number());
		bill_info.setCurrencyCode(invoice_detail.getCurrency_code());
		bill_info.setPeriodeOpen(invoice_detail.getOpen_period());
		bill_info.setPeriodeClose(invoice_detail.getClose_period());

		List<DTOMandiriVirtualAccountInfo> virtual_account_info = Arrays
				.asList(new DTOMandiriVirtualAccountInfo("National Logistics Ecosystem", "NLE"));
		bill_info.setVaInfo(virtual_account_info);

		List<DTOMandiriProvider> providers = new ArrayList<DTOMandiriProvider>();
		List<DTOMandiriProviderItem> items = new ArrayList<DTOMandiriProviderItem>();

		for (PAYMENT_INVOICE_ITEM entity : invoice_detail.getInvoice_item()) {
			DTOMandiriProviderItem item = new DTOMandiriProviderItem(entity.getId_platform(),
					entity.getServices().getService_name(), entity.getAmount().toString(), entity.getQuantity(),
					entity.getAmount().toString(), entity.getItem_info());
			items.add(item);

			DTOMandiriProvider provider = new DTOMandiriProvider(entity.getPlatform().getId_platform(),
					entity.getPlatform().getNama_platform(), entity.getAmount().toString(), entity.getItem_info(),
					items);

			providers.add(provider);
		}

		bill_info.setProviders(providers);

		// call mandiri api here
		try {
			String request = object_mapper.writeValueAsString(bill_info);
			String access_token = this.authSignature();

			Date date = new Date();
			SimpleDateFormat timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");

			ObjectMapper object_reader = new ObjectMapper();
			JsonNode minify_request = object_reader.readValue(request, JsonNode.class);
			String message = HttpMethod.POST + ":" + env.getProperty("va.urlcreate") + ":" + access_token + ":"
					+ minify_request.toString() + ":" + timestamp.format(date).replace(" ", "T");

			String signature = UtilDigitalSignature.generateDigitalSignatureHMAC(env.getProperty("va.clientsecret"),
					message);
			System.out.println(minify_request.toString());
			SimpleDateFormat extIdFormat = new SimpleDateFormat("yyyyMMddHHmmssSS");
			String external_id = extIdFormat.format(date);

			HashMap<String, String> header = new HashMap<>();
			header.put("Accept", "application/json");
			header.put("Content-Type", "application/json");
			header.put("Authorization", "Bearer " + access_token);
			header.put("X-TIMESTAMP", timestamp.format(date).replace(" ", "T"));
			header.put("X-SIGNATURE", signature);
			header.put("X-PARTNER-ID", invoice_detail.getUnique_id());
			header.put("X-EXTERNAL-ID", external_id);

			String urlString = env.getProperty("va.url") + env.getProperty("va.urlCreate");

			String response = null;
			try {
				response = invoke_rest_service.invokeRestService(HttpMethod.POST, null, urlString, header,request);
			} catch (KeyManagementException | NoSuchAlgorithmException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("respon create va" + response);

			// done update VA di invoice

		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "ok";
	}

	private Boolean updateInvoice(DTOMandiriNotification notif) {
		Boolean status = false;

		if (this.payment_invoice_repo.findById(notif.getOrderNumber()).isPresent()) {
			PAYMENT_INVOICE invoice = this.payment_invoice_repo.findById(notif.getOrderNumber()).get();
			invoice.setVa_created(1);
			invoice.setInvoiceAccountNumber(notif.getMandiriVirtualAccount());
			invoice.setOrder_amount(notif.getPaidAmount());
			/*
			 * update paid date ada di billing
			 */
			try {
				this.payment_invoice_repo.save(invoice);
				System.out.println("update" + object_mapper.writeValueAsString(invoice));
				status = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return status;
	}

	@PostMapping(value = "/inquiry")
	private String inquiry(@RequestBody PAYMENT_INVOICE invoice) {
		DTOMandiriInquiry inquiry = new DTOMandiriInquiry(invoice.getOrder_number(), invoice.getCostumer_number(),
				invoice.getCostumer_number());

		// call mandiri api here
		try {
			String request = object_mapper.writeValueAsString(inquiry);

			String access_token = this.authSignature();
			Date date = new Date();
			SimpleDateFormat timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");

			ObjectMapper object_reader = new ObjectMapper();
			JsonNode minify_request = object_reader.readValue(request, JsonNode.class);
			String message = HttpMethod.POST + ":" + env.getProperty("va.urlGet") + ":" + access_token + ":"
					+ minify_request.toString() + ":" + timestamp.format(date).replace(" ", "T");

			String signature = UtilDigitalSignature.generateDigitalSignatureHMAC(env.getProperty("va.clientsecret"),
					message);

			SimpleDateFormat extIdFormat = new SimpleDateFormat("yyyyMMddHHmmssSS");
			String external_id = extIdFormat.format(date);

			HashMap<String, String> header = new HashMap<>();
//			header.put("Accept", "application/json");
			header.put("Content-Type", "application/json");
			header.put("Authorization", "Bearer " + access_token);
			header.put("X-TIMESTAMP", timestamp.format(date).replace(" ", "T"));
			header.put("X-SIGNATURE", signature);
			header.put("X-PARTNER-ID", invoice.getOrder_number());
			header.put("X-EXTERNAL-ID", external_id);
			System.out.println(header);
			System.out.println(request);
			String urlString = env.getProperty("va.url") + env.getProperty("va.urlGet");

			String response = invoke_rest_service.invokeRestService(HttpMethod.POST, null, urlString, header, request);
			DTOMandiriBillInfo response_inquiry = new ObjectMapper().readValue(response, DTOMandiriBillInfo.class);

			String jsonString = new JSONObject().put("responseCode", "91").put("responseMessage", "balala")
					.put("data", object_mapper.writeValueAsString(response_inquiry)).toString();

			System.out.println(jsonString);
			return jsonString;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "ok";
	}

	@GetMapping(value = "/notification")
	private String notification(@RequestBody DTOMandiriNotification notification) {
		Boolean updated = this.updateInvoice(notification);
		String hasil = "failed";
		try {
			if (updated) {
				new ResponseEntity<DATA_TRANSPORTER>(HttpStatus.OK);
//				JSONObject data = new JSONObject().put("xx", "balaxxxla");
				hasil = "sukses";
			} else {
				new ResponseEntity<DATA_TRANSPORTER>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			new ResponseEntity<DATA_TRANSPORTER>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return hasil;
	}

	private String generateSignature(String key) {
		URL url = this.getClass().getClassLoader().getResource("API_Portal.jks");
		File file = new File(url.getPath());

		String signature = null;
		try {
			signature = UtilDigitalSignature.generateDigitalSignature(file.getCanonicalPath(),
					env.getProperty("va.keystorepassword"), env.getProperty("va.keyalias"),
					env.getProperty("va.keypassword"), "SHA256withRSA", key);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return signature;
	}

	private String authSignature() {
		String key = env.getProperty("va.clientid");
		Date date = new Date();
		SimpleDateFormat timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
		String signature = this.generateSignature(key + "|" + timestamp.format(date).replace(" ", "T"));

		HashMap<String, String> header = new HashMap<>();
		HashMap<String, String> params = new HashMap<>();
		header.put("Accept", "application/json");
		header.put("Content-Type", "application/x-www-form-urlencoded");
		header.put("X-Mandiri-Key", "6d35494b-73e7-483e-9959-def17ba117c3");
		header.put("X-SIGNATURE", signature);
		header.put("X-TIMESTAMP", timestamp.format(date).replace(" ", "T"));
		params.put("grant_type", "client_credentials");

		String urlString = env.getProperty("va.url") + "/openapi/auth/token";
		String response = null;
		try {
			response = invoke_rest_service.invokeRestService(HttpMethod.POST, params, urlString, header, null);
			DTOMandiriResponseToken response_token = new ObjectMapper().readValue(response,
					DTOMandiriResponseToken.class);
			return response_token.getAccessToken();
		} catch (KeyManagementException | NoSuchAlgorithmException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HttpStatusCodeException e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return response;
	}
}
