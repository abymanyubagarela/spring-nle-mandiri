package com.xa.dev_backendv2.DTO;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class DTOMandiriBillInfo {
	private String orderNumber;
	private String orderAmount;
	private String customerNumber;
	private String companyCode;
	private String currencyCode;
	@DateTimeFormat(pattern = "yyyyMMdd")
	@JsonFormat(pattern = "yyyyMMdd")
	private Date periodeOpen;
	@DateTimeFormat(pattern = "yyyyMMdd")
	@JsonFormat(pattern = "yyyyMMdd")
	private Date periodeClose;
	private List<DTOMandiriVirtualAccountInfo> vaInfo;
	private List<DTOMandiriProvider> providers;
	
	public DTOMandiriBillInfo() {
		super();
	}

	public DTOMandiriBillInfo(String orderNumber, String orderAmount, String customerNumber, String companyCode,
			String currencyCode, Date periodeOpen, Date periodeClose, 
			List<DTOMandiriVirtualAccountInfo> vaInfo,
			List<DTOMandiriProvider> providers) {
		super();
		this.orderNumber = orderNumber;
		this.orderAmount = orderAmount;
		this.customerNumber = customerNumber;
		this.companyCode = companyCode;
		this.currencyCode = currencyCode;
		this.periodeOpen = periodeOpen;
		this.periodeClose = periodeClose;
		this.vaInfo = vaInfo;
		this.providers = providers;
	}
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(String orderAmount) {
		this.orderAmount = orderAmount;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public Date getPeriodeOpen() {
		return periodeOpen;
	}
	public void setPeriodeOpen(Date periodeOpen) {
		this.periodeOpen = periodeOpen;
	}
	public Date getPeriodeClose() {
		return periodeClose;
	}
	public void setPeriodeClose(Date periodeClose) {
		this.periodeClose = periodeClose;
	}
	public List<DTOMandiriVirtualAccountInfo> getVaInfo() {
		return vaInfo;
	}
	public void setVaInfo(List<DTOMandiriVirtualAccountInfo> vaInfo) {
		this.vaInfo = vaInfo;
	}
	public List<DTOMandiriProvider> getProviders() {
		return providers;
	}
	public void setProviders(List<DTOMandiriProvider> providers) {
		this.providers = providers;
	}
}
