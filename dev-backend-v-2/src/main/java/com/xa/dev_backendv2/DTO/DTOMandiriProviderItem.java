package com.xa.dev_backendv2.DTO;

import java.math.BigDecimal;

public class DTOMandiriProviderItem {
	private String code;
	private String name;
	private String price;
	private String quantity;
	private String amount;
	private String itemInfo1;
	
	
	public DTOMandiriProviderItem() {
		super();
	}
	public DTOMandiriProviderItem(String code, String name, String price, String quantity, String amount, String itemInfo1) {
		this.code = code;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.amount = amount;
		this.itemInfo1 = itemInfo1;
	}	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getItemInfo1() {
		return itemInfo1;
	}
	public void setItemInfo1(String itemInfo1) {
		this.itemInfo1 = itemInfo1;
	}
}
