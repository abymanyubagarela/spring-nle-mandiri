package com.xa.dev_backendv2.DTO;

import java.math.BigDecimal;

public class DTOMandiriInquiry {
	private String orderNumber;
	private String customerNumber;
	private String companyCode;
	
	public DTOMandiriInquiry(String orderNumber, String customerNumber, String companyCode) {
		this.orderNumber = orderNumber;
		this.customerNumber = customerNumber;
		this.companyCode = companyCode;
	}
	
	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
}

