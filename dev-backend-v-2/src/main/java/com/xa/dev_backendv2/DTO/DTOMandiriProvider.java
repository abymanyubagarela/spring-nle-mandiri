package com.xa.dev_backendv2.DTO;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DTOMandiriProvider {
	@JsonProperty("code")
	private String code;
	@JsonProperty("name")
	private String name;
	@JsonProperty("amount")
	private String amount;
	@JsonProperty("info1")
	private String info1;
	@JsonProperty("Items")
	private List<DTOMandiriProviderItem> Items;
	
	public DTOMandiriProvider(String code, String name, String amount, String info1) {
		super();
		this.code = code;
		this.name = name;
		this.amount = amount;
		this.info1 = info1;
	}
	
	public DTOMandiriProvider(String code, String name, String amount, String info1, List<DTOMandiriProviderItem> items) {
		super();
		this.code = code;
		this.name = name;
		this.amount = amount;
		this.info1 = info1;
		this.Items = items;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getItemInfo1() {
		return info1;
	}

	public void setItemInfo1(String info1) {
		this.info1 = info1;
	}

	public List<DTOMandiriProviderItem> getItems() {
		return Items;
	}

	public void setItems(List<DTOMandiriProviderItem> items) {
		this.Items = items;
	}
}
