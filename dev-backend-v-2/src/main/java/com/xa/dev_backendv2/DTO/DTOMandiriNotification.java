package com.xa.dev_backendv2.DTO;

import java.math.BigDecimal;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class DTOMandiriNotification {
	String orderNumber;
	String mandiriVirtualAccount;
	String paidByChannel;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	String paidDate;
	BigDecimal paidAmount;
	
	public DTOMandiriNotification(String orderNumber, String mandiriVirtualAccount, String paidByChannel,
			String paidDate, BigDecimal paidAmount) {
		super();
		this.orderNumber = orderNumber;
		this.mandiriVirtualAccount = mandiriVirtualAccount;
		this.paidByChannel = paidByChannel;
		this.paidDate = paidDate;
		this.paidAmount = paidAmount;
	}
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getMandiriVirtualAccount() {
		return mandiriVirtualAccount;
	}
	public void setMandiriVirtualAccount(String mandiriVirtualAccount) {
		this.mandiriVirtualAccount = mandiriVirtualAccount;
	}
	public String getPaidByChannel() {
		return paidByChannel;
	}
	public void setPaidByChannel(String paidByChannel) {
		this.paidByChannel = paidByChannel;
	}
	public String getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

}
