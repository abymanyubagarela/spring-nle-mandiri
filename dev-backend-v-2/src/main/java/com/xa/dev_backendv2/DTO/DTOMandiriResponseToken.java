package com.xa.dev_backendv2.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DTOMandiriResponseToken {
	@JsonProperty("accessToken")
	String accessToken;
	@JsonProperty("tokenType")
	String tokenType;
	@JsonProperty("expiresIn")
	String expiresIn;
	
	@JsonCreator
	public DTOMandiriResponseToken() {}
	
	public DTOMandiriResponseToken(String accessToken, String tokenType, String expiresIn) {
		super();
		this.accessToken = accessToken;
		this.tokenType = tokenType;
		this.expiresIn = expiresIn;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public String getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}
}
