package com.xa.dev_backendv2.DTO;

public class DTOMandiriVirtualAccountInfo {
	private String info;
	private String shortInfo;
	
	public DTOMandiriVirtualAccountInfo(String info, String shortInfo) {
		this.info = info;
		this.shortInfo = shortInfo;
	}
	
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getShortInfo() {
		return shortInfo;
	}

	public void setShortInfo(String shortInfo) {
		this.shortInfo = shortInfo;
	}

}
