package com.xa.dev_backendv2.DTO;

public class DTOMandiriDataVirtualAccount {
	String mandiriVirtualAccount;
	String orderNumber;
	
	public DTOMandiriDataVirtualAccount(String mandiriVirtualAccount, String orderNumber) {
		super();
		this.mandiriVirtualAccount = mandiriVirtualAccount;
		this.orderNumber = orderNumber;
	}
	
	public String getMandiriVirtualAccount() {
		return mandiriVirtualAccount;
	}
	public void setMandiriVirtualAccount(String mandiriVirtualAccount) {
		this.mandiriVirtualAccount = mandiriVirtualAccount;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
}
