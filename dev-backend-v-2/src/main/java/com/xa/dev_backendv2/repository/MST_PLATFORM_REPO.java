package com.xa.dev_backendv2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.MST_PLATFORM;

@Repository
public interface MST_PLATFORM_REPO extends JpaRepository<MST_PLATFORM, String>{

}
