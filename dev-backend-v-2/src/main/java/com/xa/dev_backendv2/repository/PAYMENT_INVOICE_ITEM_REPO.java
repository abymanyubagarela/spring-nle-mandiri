package com.xa.dev_backendv2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.PAYMENT_INVOICE_ITEM;

@Repository
public interface PAYMENT_INVOICE_ITEM_REPO extends JpaRepository<PAYMENT_INVOICE_ITEM, Integer>{
	@Query(value = "SELECT * FROM payment_invoice_item WHERE order_number =:OrderNumber ",nativeQuery = true)
	List<PAYMENT_INVOICE_ITEM> findByOrderNumber(@Param("OrderNumber") String OrderNumber);
}
