package com.xa.dev_backendv2.repository;

import com.xa.dev_backendv2.model.DOCUMENT_SP2;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DOCUMENT_SP2_CONTAINER;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface DOCUMENT_SP2_CONTAINER_REPO extends JpaRepository<DOCUMENT_SP2_CONTAINER, Integer>{
    Optional<DOCUMENT_SP2_CONTAINER> findByIdDocumentSp2(DOCUMENT_SP2 document_sp2);
    @Query("select A from DOCUMENT_SP2_CONTAINER A where A.idDocumentSp2.idDocument=:documentID and A.idDocumentSp2.paid_thrud_date=:paidThrudDate and A.idDocumentSp2.is_finished=1")
    List<DOCUMENT_SP2_CONTAINER> findAllContainerByQuery(Integer documentID, @DateTimeFormat(pattern="yyyy-MM-dd") Date paidThrudDate);
}
