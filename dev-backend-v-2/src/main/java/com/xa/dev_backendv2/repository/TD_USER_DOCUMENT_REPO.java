package com.xa.dev_backendv2.repository;

import java.util.List;

import com.xa.dev_backendv2.model.TD_DOCUMENT;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.TD_USER_DOCUMENT;

@Repository
public interface TD_USER_DOCUMENT_REPO extends JpaRepository<TD_USER_DOCUMENT, Integer>{
	@Query(value="SELECT A.id_user_document, A.npwp, A.access_do, A.access_sp2, A.access_trucking, " + 
			"A.access_warehouse, A.access_depo, A.access_domestic, A.is_ppjk, B.* " + 
			"FROM td_user_document A JOIN td_document B ON A.id_document = B.id_document " + 
			"WHERE A.npwp =:npwp",nativeQuery = true)
	Page<TD_USER_DOCUMENT> findByNPWP(@Param("npwp") String npwp, Pageable pageable);

	@Query("select u from TD_USER_DOCUMENT u where u.npwp=:npwp AND u.id_document.id_document = :idDocument")
	List<TD_USER_DOCUMENT> findByNpwpAndIdDocument( @Param("npwp") String npwp, @Param("idDocument") Integer idDocument);
}
