package com.xa.dev_backendv2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface DATA_TRANSPORTER_REPO extends JpaRepository<com.xa.dev_backendv2.model.DATA_TRANSPORTER, String>{
	@Query(value = "SELECT id_user FROM document_trucking WHERE id_request_booking =:idRequestBooking", nativeQuery = true)
	String findbyIdUser(@Param("idRequestBooking") String idRequestBooking);

	@Query(value = "SELECT A.id_request_booking, A.id_document, B.harga_penawaran, A.id_platform FROM document_trucking A LEFT JOIN document_trucking_detil B ON A.id_request_booking = B.id_request_booking WHERE A.id_request_booking = :idRequestBooking", nativeQuery = true)
	Map findByQueryIdRequestBooking(String idRequestBooking);
}
