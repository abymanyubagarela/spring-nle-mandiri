package com.xa.dev_backendv2.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DATA_PELIMPAHAN;

@Repository
public interface DATA_PELIMPAHAN_REPO extends JpaRepository<DATA_PELIMPAHAN, String>{
	@Query(value = "SELECT file_date FROM data_pelimpahan "
			+ "WHERE filename =:filename", nativeQuery = true)
	Date findByTgl(@Param("filename") String filename);
	
	@Query(value = "SELECT id_surat_kuasa FROM data_pelimpahan "
			+ "WHERE filename =:filename", nativeQuery = true)
	String findByidSuratKuasa(@Param("filename") String filename);
}
