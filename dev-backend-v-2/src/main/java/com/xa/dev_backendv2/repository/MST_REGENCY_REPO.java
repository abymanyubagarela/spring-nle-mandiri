package com.xa.dev_backendv2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.MST_REGENCY;

@Repository
public interface MST_REGENCY_REPO extends JpaRepository<MST_REGENCY, String>{

	@Query(value ="SELECT * FROM mst_regency WHERE province_code =:provinceCode", nativeQuery = true)
    List<MST_REGENCY> findByProvinceCode(@Param("provinceCode") String provinceCode);
    
}
