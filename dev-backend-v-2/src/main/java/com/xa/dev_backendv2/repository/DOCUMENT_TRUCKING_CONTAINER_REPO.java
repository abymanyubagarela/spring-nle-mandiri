package com.xa.dev_backendv2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DOCUMENT_TRUCKING_CONTAINER;

@Repository
public interface DOCUMENT_TRUCKING_CONTAINER_REPO extends JpaRepository<DOCUMENT_TRUCKING_CONTAINER,Integer>{
	@Query(value = "SELECT * FROM document_trucking_container AS c, document_trucking AS b, document_trucking_detil AS db "
			+ "WHERE c.id_request_booking = b.id_request_booking "
			+ "AND db.id_request_booking = b.id_request_booking "
			+ "AND db.id_request_booking = c.id_request_booking "
			+ "AND b.bl_no =:bl_no AND db.paid_status =:paidStatus", nativeQuery = true)
	List<DOCUMENT_TRUCKING_CONTAINER> findByContainerBooking(@Param("bl_no") String bl_no, 
				@Param("paidStatus") Integer paidStatus);
}
