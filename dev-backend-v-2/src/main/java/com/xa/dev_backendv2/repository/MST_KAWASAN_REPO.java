package com.xa.dev_backendv2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.MST_KAWASAN;

@Repository
public interface MST_KAWASAN_REPO extends JpaRepository<MST_KAWASAN, Integer>{
	
	@Query(value="SELECT * FROM mst_kawasan WHERE id_port =:idPort", nativeQuery= true)
	List<MST_KAWASAN> findByIdPort(@Param("idPort") Integer idPort);
}
