package com.xa.dev_backendv2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DOMESTIC_VESSEL_LOG_STATUS;

@Repository
public interface DOMESTIC_VESSEL_LOG_STATUS_REPO extends JpaRepository<DOMESTIC_VESSEL_LOG_STATUS, Integer>{

}
