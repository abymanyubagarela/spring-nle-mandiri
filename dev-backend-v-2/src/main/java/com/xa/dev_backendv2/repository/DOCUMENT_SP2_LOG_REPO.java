package com.xa.dev_backendv2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DOCUMENT_SP2_LOG;

@Repository
public interface DOCUMENT_SP2_LOG_REPO extends JpaRepository<DOCUMENT_SP2_LOG, Integer>{

}
