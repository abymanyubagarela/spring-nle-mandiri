package com.xa.dev_backendv2.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.TD_DOCUMENT;

import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.List;


@Repository
public interface TD_DOCUMENT_REPO extends JpaRepository<TD_DOCUMENT, Integer>{
	@Query(value="SELECT A.id_user_document, A.npwp, A.access_do, A.access_sp2, A.access_trucking, A.access_warehouse, "
			+ "A.access_depo, A.access_domestic, A.is_ppjk, A.name, B.id_document, B.kd_document_type, B.document_no, "
			+ "B.document_date, B.no_sppb, B.tanggal_sppb, B.document_status, B.bl_date, B.finish_do, B.finish_trucking, B.finish_sp2, B.finish_warehouse, "
			+ "B.finish_depo, B.npwp_cargo_owner, B.bl_no, B.nm_cargoowner, C.document_name, C.description FROM td_user_document A " +
			"JOIN td_document B ON A.id_document = B.id_document " +
			"LEFT JOIN mst_document_type C ON B.kd_document_type =  C.kd_document_type " +
			"WHERE A.npwp=:npwp  " + 
			"AND (:documentNo is null or :documentNo = '' or B.document_no LIKE %:documentNo%) " + 
			"ORDER BY B.id_document DESC",nativeQuery = true)
	Page<Map> findByNPWP(@Param("npwp") String npwp,@Param("documentNo") String documentNo, Pageable pageable);

	@Query("select u from TD_DOCUMENT u where u.npwpCargoOwner = :npwpCargoOwner AND ((u.document_no = :documentNo AND u.document_date = :documentDate) OR (u.blNo = :blNo AND u.bl_date = :blDate))")
	List<TD_DOCUMENT> findTdDocument(@Param("npwpCargoOwner") String npwpCargoOwner,
									 @Param("documentNo") String documentNo,
									 @Param("documentDate") Date documentDate,
									 @Param("blNo") String blNo,
									 @Param("blDate") Date blDate);

	List<TD_DOCUMENT> findAllByBlNoInAndNpwpCargoOwner(List<String> blNos, String npwpCargoOwner);
}
