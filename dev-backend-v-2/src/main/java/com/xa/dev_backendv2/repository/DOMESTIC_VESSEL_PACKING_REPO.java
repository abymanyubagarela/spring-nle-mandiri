package com.xa.dev_backendv2.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DOMESTIC_VESSEL_BOOKING;
import com.xa.dev_backendv2.model.DOMESTIC_VESSEL_PACKING;

@Repository
public interface DOMESTIC_VESSEL_PACKING_REPO extends JpaRepository<DOMESTIC_VESSEL_PACKING, Integer>{
		
}
