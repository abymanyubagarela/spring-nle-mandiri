package com.xa.dev_backendv2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DOCUMENT_LOG_TRUCK;

@Repository
public interface DOCUMENT_LOG_TRUCK_REPO extends JpaRepository<DOCUMENT_LOG_TRUCK, Integer>{

}
