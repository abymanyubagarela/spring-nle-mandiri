package com.xa.dev_backendv2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.MST_PORT;

@Repository
public interface MST_PORT_REPO extends JpaRepository<MST_PORT, Integer>{
	
	@Query(value="SELECT * FROM mst_port WHERE is_asal =:isAsal", nativeQuery = true)
	List<MST_PORT> findByAsal(@Param("isAsal") Integer isAsal);
	
	@Query(value="SELECT * FROM mst_port WHERE is_tujuan =:isTujuan", nativeQuery = true)
	List<MST_PORT> findByTujuan(@Param("isTujuan") Integer isTujuan);
}
