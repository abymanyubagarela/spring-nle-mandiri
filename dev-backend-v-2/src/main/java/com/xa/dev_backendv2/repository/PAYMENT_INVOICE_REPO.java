package com.xa.dev_backendv2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.PAYMENT_INVOICE;

@Repository
public interface PAYMENT_INVOICE_REPO extends JpaRepository<PAYMENT_INVOICE, String>{
	@Query(value = "SELECT * FROM payment_invoice WHERE module =:Module AND module_unique_id =:moduleId ",nativeQuery = true)
	List<PAYMENT_INVOICE> findByModule(@Param("Module") String Module , @Param("moduleId") String moduleId);
	
	@Query(value = "SELECT pi FROM payment_invoice pi "
			+ "JOIN payment_invoice_item pii "
			+ "ON pi.order_number = pii.order_number "
			+ "WHERE pi.order_number = '098766' and pii.order_number = '098766' ",nativeQuery = true)
	List<PAYMENT_INVOICE> findByOrderNumber(@Param("OrderNumber") String OrderNumber);
}

	