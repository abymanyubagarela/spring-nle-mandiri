package com.xa.dev_backendv2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DOCUMENT_TRUCKING_DESTINATION;

@Repository
public interface DOCUMENT_TRUCKING_DESTINATION_REPO extends JpaRepository<DOCUMENT_TRUCKING_DESTINATION, Integer>{

}
