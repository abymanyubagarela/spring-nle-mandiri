package com.xa.dev_backendv2.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DOCUMENT_DO;

import java.util.List;

@Repository
public interface DOCUMENT_DO_REPO extends PagingAndSortingRepository<DOCUMENT_DO, String> {
    Page<DOCUMENT_DO> findAllByNpwpCargoOwnerOrIdFfPpjk(String npwpCargoOwner, String idFfPpjk, Pageable pageable);
    
    @Query("select U from DOCUMENT_DO U where U.blNo=:blNo AND (U.npwpCargoOwner=:npwpCargoOwner OR U.idFfPpjk=:idFfPpjk)")
    Page<DOCUMENT_DO> findAllByQuery(String npwpCargoOwner, String idFfPpjk, String blNo, Pageable pageable);
    
    @Query("select U from DOCUMENT_DO U where U.blNo=:blNo AND (U.npwpCargoOwner=:npwpCargoOwner OR U.idFfPpjk=:idFfPpjk) AND U.blType=:blType")
    DOCUMENT_DO findOneByQuery(String npwpCargoOwner, String idFfPpjk, String blNo, String blType );
    
    @Query("select U from DOCUMENT_DO U where U.blNo=:blNo AND (U.npwpCargoOwner=:npwpCargoOwner OR U.idFfPpjk=:idFfPpjk) AND U.blType=:blType AND U.doNumber is not null ")
    List <DOCUMENT_DO> findOneByDocumentDo(String npwpCargoOwner, String idFfPpjk, String blNo, String blType );
    
    @Query("select U from DOCUMENT_DO U where U.blNo=:blNo AND (U.npwpCargoOwner=:npwpCargoOwner OR U.idFfPpjk=:idFfPpjk) AND U.blType=:blType")
    List<DOCUMENT_DO> findOneByQueryList(String npwpCargoOwner, String idFfPpjk, String blNo, String blType );
    
//    @Query("SELECT B FROM DOCUMENT_BL_HOST A JOIN DOCUMENT_DO B ON A.id_document_do = B.id_document_do WHERE A.bl_host_no = :bl_host AND B.npwpCargoOwner = :npwp")
//    List<DOCUMENT_DO> findByBlhostNpwp(String bl_host, String npwp);
    List<DOCUMENT_DO> findAllByBlNo(String blNo, Pageable pageable);

    @Query("select U from DOCUMENT_DO U where U.doNumber is not null AND U.npwpCargoOwner=:npwpCargoOwner ")
    List<DOCUMENT_DO> findAllByQueryNpwpCargoOwner(String npwpCargoOwner);
}
