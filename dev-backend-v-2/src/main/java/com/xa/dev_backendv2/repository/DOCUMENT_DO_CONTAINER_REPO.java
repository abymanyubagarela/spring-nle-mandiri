package com.xa.dev_backendv2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DOCUMENT_DO_CONTAINER;

@Repository
public interface DOCUMENT_DO_CONTAINER_REPO extends JpaRepository<DOCUMENT_DO_CONTAINER,Integer>{

}
