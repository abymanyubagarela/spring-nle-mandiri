package com.xa.dev_backendv2.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.PAYMENT_BILLING;

@Repository
public interface PAYMENT_BILLING_REPO extends JpaRepository<PAYMENT_BILLING, Integer>{
	@Query(value = "SELECT * FROM payment_billing WHERE module =:Module AND module_unique_id =:moduleId",nativeQuery = true)
	List<PAYMENT_BILLING> findByModule(@Param("Module") String Module , @Param("moduleId") String moduleId);

	Optional<PAYMENT_BILLING> findByModuleUniqueIdAndModule(String moduleUniqueId, String module);
}
