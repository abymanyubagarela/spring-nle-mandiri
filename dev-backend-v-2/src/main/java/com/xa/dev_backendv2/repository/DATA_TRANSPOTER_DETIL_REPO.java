package com.xa.dev_backendv2.repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DATA_TRANSPORTER_DETIL;

@Repository
public interface DATA_TRANSPOTER_DETIL_REPO  extends JpaRepository<DATA_TRANSPORTER_DETIL, Integer>{
	
	@Query(value = "SELECT id_user FROM document_trucking WHERE id_request_booking =:idRequestBooking ", nativeQuery = true)
	String findbyIdUser(@Param("idRequestBooking") String idRequestBooking);
	
	@Query(value="SELECT id_data_transporter_detil FROM data_transporter_detil " + 
			"WHERE id_request_booking =:idRequestBooking " + 
			"AND container_no =:containerNo", nativeQuery = true)
	Integer findByIdDTDetil(@Param("idRequestBooking") String idRequestBooking,
			@Param("containerNo") String containerNo);
	
	@Query(value="SELECT id_data_transporter_detil FROM data_transporter_detil "
			+ "WHERE id_request_booking =:idRequestBooking "
			+ "AND container_no =:containerNo", nativeQuery = true)
	Integer findByDTDetil(@Param("idRequestBooking") String idRequestBooking,
			@Param("containerNo") String containerNo);
	
	@Query(value="SELECT * FROM data_transporter_detil LIMIT :value", nativeQuery = true)
	List<DATA_TRANSPORTER_DETIL> findLimit(@Param("value") Integer limit);
	
	@Query(value="SELECT * FROM data_transporter_detil as A, data_transporter as B "
			+ "WHERE A.id_request_booking = B.id_request_booking "
			+ "AND B.created_date =:createdDate", nativeQuery = true)
	List<DATA_TRANSPORTER_DETIL> findByCreatedDate(@Param("createdDate") Timestamp createdDate);
	
	@Query(value="SELECT * FROM data_transporter_detil WHERE id_request_booking =:idRequestBooking", nativeQuery = true)
	List<DATA_TRANSPORTER_DETIL> findByIdRequestBooking(@Param("idRequestBooking") String idRequestBooking);
	
	@Query(value="SELECT A.id_data_transporter_detil, A.id_request_booking, " + 
			"A.container_no, F.container_size, F.container_type, F.over_height, " + 
			"F.over_width, F.over_length, F.over_weight, F.temperatur, " + 
			"F.dangerous_type, F.dangerous_material, A.id_eseal , " + 
			"A.truck_plate_no , A.nama_driver , A.hp_driver , A.url_tracking ,A.status , A.is_finished , " + 
			"B.bl_no , B.bl_date , " + 
			"B.sp2valid_date , B.spcvalid_date , " + 
			"B.pod , B.pod_lat , B.pod_lon ,B.total_distance , " + 
			"C.id_platform , C.nama_platform , C.company , " + 
			"D.billing_code , " + 
			"E.booked_date " + 
			"FROM data_transporter_detil as A " + 
			"JOIN document_trucking as B ON A.id_request_booking = B.id_request_booking " + 
			"LEFT JOIN mst_platform as C ON B.id_platform = C.id_platform " + 
			"JOIN data_transporter as D ON A.id_request_booking = D.id_request_booking " + 
			"JOIN document_trucking_detil as E ON A.id_request_booking = E.id_request_booking " + 
			"JOIN document_trucking_container as F ON A.id_request_booking = F.id_request_booking AND A.container_no = F.container_no " + 
			"WHERE E.is_booked = 1 AND (TO_CHAR(E.booked_date , 'yyyy-mm-dd') =:bookedDate or :bookedDate is null or :bookedDate ='' ) " + 
			"AND E.paid_status = 1 AND B.id_user =:npwp  "+ 
			"AND (:documentID = 0 or B.id_document =:documentID) " + 
			"AND A.is_finished = 0 AND (:idRequestBooking is null or :idRequestBooking = '' or A.id_request_booking LIKE %:idRequestBooking%) " + 
			"AND (:blNo is null or :blNo = '' or B.bl_no =:blNo) " + 
			"ORDER BY E.created_date DESC",nativeQuery = true)
	Page<DATA_TRANSPORTER_DETIL> findListActiveTruck( @Param("bookedDate") String bookedDate,
			@Param("idRequestBooking") String idRequestBooking,
			@Param("npwp") String npwp,@Param("documentID") Integer documentID,@Param("blNo") String blNo,
			Pageable pageable);
	
	@Query(value="SELECT A.id_data_transporter_detil, A.id_request_booking, " + 
			"A.container_no, F.container_size, F.container_type, F.over_height, " + 
			"F.over_width, F.over_length, F.over_weight, F.temperatur, " + 
			"F.dangerous_type, F.dangerous_material, A.id_eseal , " + 
			"A.truck_plate_no , A.nama_driver , A.hp_driver , A.url_tracking ,A.status , A.is_finished , " + 
			"B.bl_no , B.bl_date , " + 
			"B.sp2valid_date , B.spcvalid_date , " + 
			"B.pod , B.pod_lat , B.pod_lon ,B.total_distance , " + 
			"C.id_platform , C.nama_platform , C.company , " + 
			"D.billing_code , " + 
			"E.booked_date " + 
			"FROM data_transporter_detil as A " + 
			"JOIN document_trucking as B ON A.id_request_booking = B.id_request_booking " + 
			"LEFT JOIN mst_platform as C ON B.id_platform = C.id_platform " + 
			"JOIN data_transporter as D ON A.id_request_booking = D.id_request_booking " + 
			"JOIN document_trucking_detil as E ON A.id_request_booking = E.id_request_booking " + 
			"JOIN document_trucking_container as F ON A.id_request_booking = F.id_request_booking AND A.container_no = F.container_no " + 
			"WHERE E.is_booked = 1 AND (TO_CHAR(E.booked_date , 'yyyy-mm-dd') =:bookedDate or :bookedDate is null or :bookedDate ='' ) " + 
			"AND E.paid_status = 1 AND B.id_user =:npwp " + 
			"AND (:documentID = 0 or B.id_document =:documentID) " + 
			"AND A.is_finished = 1 AND (:idRequestBooking is null or :idRequestBooking = '' or A.id_request_booking LIKE %:idRequestBooking%) " + 
			"AND (:blNo is null or :blNo = '' or B.bl_no =:blNo) " + 
			"ORDER BY E.created_date DESC",nativeQuery = true)
	Page<DATA_TRANSPORTER_DETIL> findListHistoryTruck( @Param("bookedDate") String bookedDate,
			@Param("idRequestBooking") String idRequestBooking,
			@Param("npwp") String npwp,@Param("documentID") Integer documentID,@Param("blNo") String blNo,
			Pageable pageable);

	Optional<DATA_TRANSPORTER_DETIL> findByIdRequestBookingAndContainerNo(String idRequestBooking, String containerNo);
}
