package com.xa.dev_backendv2.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.xa.dev_backendv2.model.DETIL_DATA_PELIMPAHAN;


@Repository
public interface DETIL_DATA_PELIMPAHAN_REPO extends JpaRepository<DETIL_DATA_PELIMPAHAN,Integer>{
	@Query(value="SELECT * FROM detil_data_pelimpahan as A JOIN mst_document_type as B ON A.kd_document_type = B.kd_document_type\r\n" + 
			"WHERE (:createdNpwp is null or :createdNpwp = '' OR A.created_npwp ILIKE %:createdNpwp%) \r\n" + 
			"AND (:PPJK is null or :PPJK = '' OR A.ppjk ILIKE %:PPJK%)\r\n" + 
			"AND (:doctype is null or :doctype = '' OR B.document_name ILIKE %:doctype%) ORDER BY A.id ",nativeQuery = true)
	Page<DETIL_DATA_PELIMPAHAN> findByPage(@Param("createdNpwp") String createdNpwp,
			@Param("PPJK") String PPJK,@Param("doctype") String doctype,
			Pageable pageable);
	
	@Query(value="SELECT * FROM detil_data_pelimpahan as A JOIN mst_document_type as B ON A.kd_document_type = B.kd_document_type \r\n" + 
			"WHERE A.ppjk ILIKE :PPJK \r\n" + 
			"AND B.document_name ILIKE %:doctype% ORDER BY A.id ", nativeQuery = true)
	List<DETIL_DATA_PELIMPAHAN> findByPPJKdoctype(@Param("PPJK") String PPJK,
			@Param("doctype") String doctype);
	
	@Query(value="SELECT A.id_surat_kuasa FROM detil_data_pelimpahan as A WHERE A.id =:ID",nativeQuery= true)
	String findByIdSuratKuasa(@Param("ID") Integer ID);
	
	@Query(value="SELECT * FROM data_pelimpahan as A, detil_data_pelimpahan as B "
			+ "WHERE A.id_surat_kuasa = B.id_surat_kuasa AND A.id_surat_kuasa=:ID", nativeQuery = true)
	List<DETIL_DATA_PELIMPAHAN> findToGetListSuratKuasa(@Param("ID") String ID);
	
	@Query(value="SELECT A.filename FROM data_pelimpahan as A "
			+ "WHERE A.id_surat_kuasa =:ID", nativeQuery = true)
	String findByFilename(@Param("ID") String ID);
	
	@Query(value="SELECT file_date FROM data_pelimpahan WHERE id_surat_kuasa =:ID", nativeQuery = true)
	Date findFileDate(@Param("ID") String ID);
}
