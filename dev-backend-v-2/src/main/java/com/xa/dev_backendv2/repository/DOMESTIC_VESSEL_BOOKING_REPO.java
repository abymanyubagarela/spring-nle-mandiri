package com.xa.dev_backendv2.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DOMESTIC_VESSEL_BOOKING;

@Repository
public interface DOMESTIC_VESSEL_BOOKING_REPO extends JpaRepository<DOMESTIC_VESSEL_BOOKING, String>{
	@Query(value = "SELECT * FROM domestic_vessel_booking WHERE id_request =:id_request", nativeQuery = true)
	List<DOMESTIC_VESSEL_BOOKING> findByIdRequest(@Param("id_request") String id_request);
	
	@Query(value = "SELECT * FROM domestic_vessel_booking WHERE npwp =:npwp", nativeQuery = true)
	List<DOMESTIC_VESSEL_BOOKING> findByNPWP(@Param("npwp") String npwp);
	
	@Query(value = "SELECT * FROM domestic_vessel_booking WHERE order_id =:order_id", nativeQuery = true)
	List<DOMESTIC_VESSEL_BOOKING> findByOrderID(@Param("order_id") String order_id);
	
	@Query(value = "SELECT a.id_port_asal AS id_port_asal, a.id_port_tujuan AS id_port_tujuan, "
			+ "port_asal.port AS nama_port_asal, port_tujuan.port AS nama_port_tujuan, "
			+ "kawasan_asal.id_kawasan AS id_kawasan_asal, kawasan_tujuan.id_kawasan AS id_kawasan_tujuan,"
			+ "kawasan_asal.nama_kawasan AS nama_kawasan_asal, kawasan_tujuan.nama_kawasan AS nama_kawasan_tujuan "
			+ "FROM domestic_vessel_packing b "
			+ "JOIN domestic_vessel_booking a on a.id_request = b.id_request "
			+ "LEFT JOIN mst_platform ON mst_platform.id_platform = a.id_platform "
			+ "LEFT JOIN mst_port port_asal ON port_asal.id_port = a.id_port_asal "
			+ "LEFT JOIN mst_port port_tujuan ON port_tujuan.id_port = a.id_port_tujuan "
			+ "LEFT JOIN mst_kawasan kawasan_asal ON kawasan_asal.id_kawasan = a.id_kawasan_asal "
			+ "LEFT JOIN mst_kawasan kawasan_tujuan ON kawasan_tujuan.id_kawasan = a.id_kawasan_tujuan "
			+ "WHERE a.id_request =:id_request", nativeQuery = true)
	Page<Map> findDTORute(@Param("id_request") String id_request, Pageable pageable);
}
