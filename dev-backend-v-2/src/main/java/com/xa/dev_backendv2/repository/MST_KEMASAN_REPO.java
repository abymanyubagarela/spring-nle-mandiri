package com.xa.dev_backendv2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.MST_KEMASAN;

@Repository
public interface MST_KEMASAN_REPO extends JpaRepository<MST_KEMASAN, Integer>{

}
