package com.xa.dev_backendv2.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DOCUMENT_TRUCKING_DETIL;

@Repository
public interface DOCUMENT_TRUCKING_DETIL_REPO extends CustomRepository<DOCUMENT_TRUCKING_DETIL, String>{
	@Query(value="SELECT * FROM document_trucking_detil " 
				+ "WHERE id_request_booking=:idRequestBooking",nativeQuery = true)
	DOCUMENT_TRUCKING_DETIL findByIdBooking(@Param("idRequestBooking") String idRequestBooking);
	
	@Query(value="SELECT * FROM document_trucking_detil AS a, document_trucking AS b "
			+ "WHERE a.id_request_booking = b.id_request_booking "
			+ "AND a.is_booked = 1 "
			+ "AND b.id_user = :npwp AND (:documentID = null OR b.id_document = :documentID) "
			+ "AND (to_char(a.booked_date, 'YYYY-MM-DD') = :bookedDate "
			+ "OR :bookedDate IS NULL OR :bookedDate = '') "
			+ "AND (:idRequestBooking IS null "
			+ "OR :idRequestBooking = '' "
			+ "OR a.id_request_booking LIKE %:idRequestBooking%) "
			+ "ORDER BY a.booked_date DESC",nativeQuery = true)
	Page<DOCUMENT_TRUCKING_DETIL> findByLimitPageAndBookedDate(@Param("bookedDate") String bookedDate, 
			@Param("idRequestBooking") String idRequestBooking, 
			@Param("npwp") String npwp, @Param("documentID") Integer documentID,
			Pageable pageable);
	
	@Query(value="SELECT * FROM document_trucking_detil as detbook, document_trucking as book "
			+ "where detbook.id_request_booking=book.id_request_booking and "
			+ "book.id_user =:idUser order by detbook.created_date desc ",nativeQuery = true)
	List<DOCUMENT_TRUCKING_DETIL> findByIdUser(@Param("idUser") String idUser);
	
	@Query(value="SELECT * FROM document_trucking_detil WHERE "
			+ "id_request_booking=:idRequestBooking",nativeQuery = true)
	List<DOCUMENT_TRUCKING_DETIL> findByIdRequestBooking(@Param("idRequestBooking") String idRequestBooking);
	
	@Query(value="SELECT * FROM document_trucking_detil LIMIT :value",nativeQuery = true)
	List<DOCUMENT_TRUCKING_DETIL> findByLimit(@Param("value") Integer Limit);
}
