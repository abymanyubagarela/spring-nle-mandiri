package com.xa.dev_backendv2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.MST_DANGEROUS_TYPE;

@Repository
public interface MST_DANGEROUS_TYPE_REPO extends JpaRepository<MST_DANGEROUS_TYPE, Integer>{

}
