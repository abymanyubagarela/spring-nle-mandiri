package com.xa.dev_backendv2.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.dev_backendv2.model.DOCUMENT_SP2;

import java.util.Optional;
import java.util.List;

@Repository
public interface DOCUMENT_SP2_REPO extends JpaRepository<DOCUMENT_SP2, String>{
    List<DOCUMENT_SP2> findAllByIdDocument(Integer idDocument);
    
    @Query(value = "SELECT * FROM document_sp2 WHERE id_document_sp2 =:idDocumentSp2", nativeQuery = true)
    List<DOCUMENT_SP2> findByIdDocumentSp2(@Param("idDocumentSp2") String idDocumentSp2);
    
    @Query(value = "SELECT * FROM document_sp2 A JOIN td_document B ON A.id_document = B.id_document WHERE B.npwp_cargo_owner =:npwp AND A.is_finished =:isFinished", nativeQuery= true)
    Page<DOCUMENT_SP2> findByNpwp(@Param("npwp") String npwp,@Param("isFinished") Integer isFinished, Pageable pageable);
}
