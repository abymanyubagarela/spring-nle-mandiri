package com.xa.dev_backendv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.xa.dev_backendv2.property.FileStorageProperties;
import com.xa.dev_backendv2.service.CustomRepositoryImpl;

@SpringBootApplication
@EnableConfigurationProperties({
	FileStorageProperties.class
})
@EnableJpaRepositories (repositoryBaseClass = CustomRepositoryImpl.class)
@EnableScheduling
public class DevBackendV2Application {

	public static void main(String[] args) {
		SpringApplication.run(DevBackendV2Application.class, args);
	}

}
