package com.xa.dev_backendv2.service.mandiri;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.apache.logging.log4j.util.PropertiesUtil;
import org.springframework.boot.logging.LogFile;

public class UtilProperties {
	private Properties param_properties =  new Properties();
	
	private static UtilProperties instance;
	
	private UtilProperties() {
		loadProperties();
	}
	
	public static synchronized UtilProperties getInstance()
	{
		if(instance == null)
			instance = new UtilProperties();
		
		return instance;
	}
	
	private void loadProperties() {
		try {
			URL resource = this.getClass().getResource("parameter.properties");
			File file;
			try {
				System.out.println(resource.toURI().toString());
				file = new File(resource.toURI());
				param_properties.load(new FileInputStream(file));
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Properties getParam_properties() {
		return param_properties;
	}

	public void setParam_properties(Properties param_properties) {
		this.param_properties = param_properties;
	}
	
}
