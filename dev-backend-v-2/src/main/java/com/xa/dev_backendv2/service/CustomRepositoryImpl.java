package com.xa.dev_backendv2.service;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.xa.dev_backendv2.repository.CustomRepository;

import javax.persistence.EntityManager;
import java.io.Serializable;

public class CustomRepositoryImpl <T, ID extends Serializable> extends SimpleJpaRepository<T, ID>
implements CustomRepository<T, ID>{
	private final EntityManager entityManager;

	  public CustomRepositoryImpl(JpaEntityInformation entityInformation, EntityManager entityManager) {
	    super(entityInformation, entityManager);
	    this.entityManager = entityManager;
	  }

	  @Override
	  @Transactional
	  public void refresh(T t) {
	    entityManager.refresh(t);
	  }
}
