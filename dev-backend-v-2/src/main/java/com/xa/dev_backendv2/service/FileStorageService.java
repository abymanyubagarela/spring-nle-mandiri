package com.xa.dev_backendv2.service;

import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.xa.dev_backendv2.exception.FileStorageException;
import com.xa.dev_backendv2.exception.MyFileNotFoundException;
import com.xa.dev_backendv2.property.FileStorageProperties;

@Service
public class FileStorageService {
	private final Path fileStorageLocation;
	
	@Autowired
	public FileStorageService(FileStorageProperties fileStorageProperties) {
		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
				.toAbsolutePath().normalize();
		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception e) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", e);
		}
	}
	
	private static final List<String> contentTypes = Arrays.asList("image/png", "image/jpeg", "application/msword"
			, "application/vnd.ms-excel", "application/pdf"
			, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
	
	private static String getLocationDirectory = null;
	
	public String storeFile(MultipartFile file, String idSuratKuasa, Date dt) {
		// Normalize file name
		String fileName = idSuratKuasa+"."+StringUtils.cleanPath(file.getOriginalFilename()
				.substring(file.getOriginalFilename().lastIndexOf(".")+1));
		String contentType = file.getContentType();
		try {
			System.out.println("idSuratKuasa : "+idSuratKuasa);
			// Check if the file's name contains invalid characters
			if(contentTypes.contains(contentType)) {
				// Copy file to the target location (Replacing existing file with the same name)
				Path h1 = this.fileStorageLocation;
				Path h2 = Paths.get("assetsNLE/"+(dt.getYear()+1900)+"/"+(dt.getMonth()+1)+"/");
				Path targetLocation = h1.resolveSibling(h2).resolve(fileName);
				if (!Files.exists(h2)) {
					Files.createDirectories(h2);
				}
//				Path targetLocation = h1.relativize(h2).resolve(fileName);
				System.out.println("fileName = "+fileName);
				getLocationDirectory = targetLocation.toString();
				System.out.println(getLocationDirectory);
				Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);	
			} else {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}
			
			return fileName;
		} catch (Exception e) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", e);
		}
	}
	
	public Resource loadFileAsResource(String fileName, Date dt) throws Exception{
		try {
			Path h1 = this.fileStorageLocation;
			String h2 = "assetsNLE/"+(dt.getYear()+1900)+"/"+(dt.getMonth()+1)+"/";
			Path h = h1.resolveSibling(h2).resolve(fileName).normalize();
			
			Resource resource = new UrlResource(h.toUri());
			System.out.println("load file = "+this.fileStorageLocation+"\npath edit = "+h);
			if(resource.exists()) {
				return resource;
			} else {
				throw new MyFileNotFoundException("File not found " + fileName);
			}
		} catch (MalformedURLException e) {
			throw new MyFileNotFoundException("File not found " + fileName, e);
		}
	}
	
	public String deleteFile(String fileName, Date dt) {
		try {
			if(fileName == null) {
				throw new FileStorageException("Sorry! Filename contains null " + fileName);
			} else {
				// Copy file to the target location (Replacing existing file with the same name)
				Path h1 = this.fileStorageLocation;
				String h2 = "assetsNLE/"+(dt.getYear()+1900)+"/"+(dt.getMonth()+1)+"/";
				Path targetLocation = h1.resolveSibling(h2).resolve(fileName).normalize();
				
//				Path targetLocation = this.fileStorageLocation.resolve(fileName);
//				Resource resource = new UrlResource(targetLocation.toUri());
				Files.deleteIfExists(targetLocation);
//				return resource;
				return "Delete filename "+fileName+" is success.";
			}
		} catch (Exception e) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", e);
		}
	}
}
