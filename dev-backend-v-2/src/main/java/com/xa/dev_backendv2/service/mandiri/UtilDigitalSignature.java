package com.xa.dev_backendv2.service.mandiri;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.kafka.common.utils.Java;

public class UtilDigitalSignature {
	private static final String HMAC_SHA512 = "HmacSHA512";

	public static String generateDigitalSignature(String keystore, String keystorePassword, String keyAlias,
			String keyPassword, String algorithm, String message) {
		String signature = null;
		String keystoreFile = keystore;
		try {
			KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
			FileInputStream fis = new FileInputStream(keystoreFile);
			ks.load(fis, keystorePassword.toCharArray());
			KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry) ks.getEntry(keyAlias,
					new KeyStore.PasswordProtection(keyPassword.toCharArray()));
			RSAPrivateKey pvKey = (RSAPrivateKey) pkEntry.getPrivateKey();
			Signature sign = Signature.getInstance(algorithm);
			sign.initSign(pvKey);
			byte[] byteMessage = message.getBytes();

			sign.update(byteMessage, 0, byteMessage.length);
			byte[] byteSignature = sign.sign();

			signature = new String(Base64.encodeBase64(byteSignature));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return signature;
	}
	
	public static final String generateDigitalSignatureHMAC(String key, String message) {
		Mac sha512_HMAC = null;
		String result = null;

		try {
			byte[] byteKey = key.getBytes("UTF-8");
			final String HMAC_SHA512 = "HmacSHA512";
			sha512_HMAC = Mac.getInstance(HMAC_SHA512);
			SecretKeySpec keySpec = new SecretKeySpec(byteKey, HMAC_SHA512);
			sha512_HMAC.init(keySpec);
			byte[] mac_data = sha512_HMAC.doFinal(message.getBytes("UTF-8"));
			// result = Base64.encode(mac_data);
			result = bytesToHex(mac_data);

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}

	public static String bytesToHex(byte[] bytes) {
		final char[] hexArray = "0123456789ABCDEF".toCharArray();
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
}
