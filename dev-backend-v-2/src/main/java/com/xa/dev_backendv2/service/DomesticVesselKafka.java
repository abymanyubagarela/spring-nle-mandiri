package com.xa.dev_backendv2.service;

import java.math.BigDecimal;

public class DomesticVesselKafka {
	private String id_request;
	private String id_platform;
	private String ship_name;
	private String container_type;
	private String partner;
	private Integer partner_rating;
	private String service_type;
	private BigDecimal price;
	private String price_description;
	private String closing_time;
	private String provision;
	
	public String getId_request() {
		return id_request;
	}
	public void setId_request(String id_request) {
		this.id_request = id_request;
	}
	public String getId_platform() {
		return id_platform;
	}
	public void setId_platform(String id_platform) {
		this.id_platform = id_platform;
	}
	public String getShip_name() {
		return ship_name;
	}
	public void setShip_name(String ship_name) {
		this.ship_name = ship_name;
	}
	public String getContainer_type() {
		return container_type;
	}
	public void setContainer_type(String container_type) {
		this.container_type = container_type;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	
	public Integer getPartner_rating() {
		return partner_rating;
	}
	public void setPartner_rating(Integer partner_rating) {
		this.partner_rating = partner_rating;
	}
	public String getService_type() {
		return service_type;
	}
	public void setService_type(String service_type) {
		this.service_type = service_type;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getPrice_description() {
		return price_description;
	}
	public void setPrice_description(String price_description) {
		this.price_description = price_description;
	}
	public String getClosing_time() {
		return closing_time;
	}
	public void setClosing_time(String closing_time) {
		this.closing_time = closing_time;
	}
	public String getProvision() {
		return provision;
	}
	public void setProvision(String provision) {
		this.provision = provision;
	}
	
}
