package com.xa.dev_backendv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.NotNull;

@Entity
@Table(name="document_do_container")
public class DOCUMENT_DO_CONTAINER {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "document_do_container_generator")
	@SequenceGenerator(name = "document_do_container_generator", sequenceName = "seq_document_do_container", allocationSize = 1)
	@Column(name="id_do_container")
	private Integer id_do_container;
	
	@JsonBackReference
	@JoinColumn(name = "id_document_do", referencedColumnName = "id_document_do" )
	@ManyToOne(fetch = FetchType.EAGER)
	private DOCUMENT_DO id_document_do;
	
	@NotNull
	@Column(name="container_no", nullable = true)
	private String container_no;
	
	@Column(name="container_size")
	private String container_size;
	
	@Column(name="container_type")
	private String container_type;
	
	public Integer getId_do_container() {
		return id_do_container;
	}

	public void setId_do_container(Integer id_do_container) {
		this.id_do_container = id_do_container;
	}

	public DOCUMENT_DO getId_document_do() {
		return id_document_do;
	}

	public void setId_document_do(DOCUMENT_DO id_document_do) {
		this.id_document_do = id_document_do;
	}

	public String getContainer_no() {
		return container_no;
	}

	public void setContainer_no(String container_no) {
		this.container_no = container_no;
	}

	public String getContainer_size() {
		return container_size;
	}

	public void setContainer_size(String container_size) {
		this.container_size = container_size;
	}

	public String getContainer_type() {
		return container_type;
	}

	public void setContainer_type(String container_type) {
		this.container_type = container_type;
	}
}
