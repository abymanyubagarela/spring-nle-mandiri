package com.xa.dev_backendv2.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "data_transporter_detil")
public class DATA_TRANSPORTER_DETIL {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dtdetil_generator")
	@SequenceGenerator(name="dtdetil_generator", sequenceName = "seq_dtdetil",allocationSize = 1)
	@Column(name = "id_data_transporter_detil")
	private Integer idDataTransporterDetil;

	//TODO: @ManyToOne
	@Column(name = "id_request_booking")
	private String idRequestBooking;
	
	@Column(name = "Container_no")
	@JsonProperty( "container_no")
	private String containerNo;
	
	private String container_size;
	private String container_type;
	private Integer over_height;
	private Integer over_width;
	private Integer over_length;
	private Integer over_weight;
	private Integer temperatur;
	private String dangerous_type;
	private String dangerous_material;
	
	@ManyToOne
	@JoinColumn(name="id_request_booking", foreignKey = @ForeignKey(name = "fk_id_request_booking"),
	insertable = false, updatable = false)
	private DOCUMENT_TRUCKING booking;
	
	@Column(name = "id_eseal")
	private String idEseal;
	
	@Column(name = "truck_plate_no")
	private String truckPlateNo;
	
	@Column(name = "nama_driver")
	private String namaDriver;
	
	@Column(name = "hp_driver")
	private String hpDriver;
	
	@Column(name = "url_tracking")
	private String urlTracking;
	
	@Column(name = "Status", insertable = false, updatable = true)
	private String Status;
	
	@Column(name ="is_finished", insertable = false, updatable = true)
	private Integer isFinished;
	
	private String bl_no;
	private String bl_date;
	private String sp2valid_date;
	private String spcvalid_date;
	
	private String pod;
	private String pod_lat;
	private String pod_lon;
	
	private String total_distance;
	private String id_platform;
	private String nama_platform;
	private String company;
	private String billing_code;
	private String booked_date;
	
	@OneToMany(mappedBy = "idDataTransporterDetil", cascade = CascadeType.ALL)
	@OrderBy(value = "create_date ASC")
	private List<DOCUMENT_LOG_TRUCK> logtruck;

	public Integer getIdDataTransporterDetil() {
		return idDataTransporterDetil;
	}

	public void setIdDataTransporterDetil(Integer idDataTransporterDetil) {
		this.idDataTransporterDetil = idDataTransporterDetil;
	}

	public String getIdRequestBooking() {
		return idRequestBooking;
	}

	public void setIdRequestBooking(String idRequestBooking) {
		this.idRequestBooking = idRequestBooking;
	}

	public String getContainerNo() {
		return containerNo;
	}

	public void setContainerNo(String container_no) {
		this.containerNo = container_no;
	}

	public String getContainer_size() {
		return container_size;
	}

	public void setContainer_size(String container_size) {
		this.container_size = container_size;
	}

	public String getContainer_type() {
		return container_type;
	}

	public void setContainer_type(String container_type) {
		this.container_type = container_type;
	}

	public Integer getOver_height() {
		return over_height;
	}

	public void setOver_height(Integer over_height) {
		this.over_height = over_height;
	}

	public Integer getOver_width() {
		return over_width;
	}

	public void setOver_width(Integer over_width) {
		this.over_width = over_width;
	}

	public Integer getOver_length() {
		return over_length;
	}

	public void setOver_length(Integer over_length) {
		this.over_length = over_length;
	}

	public Integer getOver_weight() {
		return over_weight;
	}

	public void setOver_weight(Integer over_weight) {
		this.over_weight = over_weight;
	}

	public Integer getTemperatur() {
		return temperatur;
	}

	public void setTemperatur(Integer temperatur) {
		this.temperatur = temperatur;
	}

	public String getDangerous_type() {
		return dangerous_type;
	}

	public void setDangerous_type(String dangerous_type) {
		this.dangerous_type = dangerous_type;
	}

	public String getDangerous_material() {
		return dangerous_material;
	}

	public void setDangerous_material(String dangerous_material) {
		this.dangerous_material = dangerous_material;
	}

	public DOCUMENT_TRUCKING getBooking() {
		return booking;
	}

	public void setBooking(DOCUMENT_TRUCKING booking) {
		this.booking = booking;
	}

	public String getIdEseal() {
		return idEseal;
	}

	public void setIdEseal(String idEseal) {
		this.idEseal = idEseal;
	}

	public String getTruckPlateNo() {
		return truckPlateNo;
	}

	public void setTruckPlateNo(String truckPlateNo) {
		this.truckPlateNo = truckPlateNo;
	}

	public String getNamaDriver() {
		return namaDriver;
	}

	public void setNamaDriver(String namaDriver) {
		this.namaDriver = namaDriver;
	}

	public String getHpDriver() {
		return hpDriver;
	}

	public void setHpDriver(String hpDriver) {
		this.hpDriver = hpDriver;
	}

	public String getUrlTracking() {
		return urlTracking;
	}

	public void setUrlTracking(String urlTracking) {
		this.urlTracking = urlTracking;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public Integer getIsFinished() {
		return isFinished;
	}

	public void setIsFinished(Integer isFinished) {
		this.isFinished = isFinished;
	}

	public String getBl_no() {
		return bl_no;
	}

	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}

	public String getBl_date() {
		return bl_date;
	}

	public void setBl_date(String bl_date) {
		this.bl_date = bl_date;
	}

	public String getSp2valid_date() {
		return sp2valid_date;
	}

	public void setSp2valid_date(String sp2valid_date) {
		this.sp2valid_date = sp2valid_date;
	}

	public String getSpcvalid_date() {
		return spcvalid_date;
	}

	public void setSpcvalid_date(String spcvalid_date) {
		this.spcvalid_date = spcvalid_date;
	}

	public String getPod() {
		return pod;
	}

	public void setPod(String pod) {
		this.pod = pod;
	}

	public String getPod_lat() {
		return pod_lat;
	}

	public void setPod_lat(String pod_lat) {
		this.pod_lat = pod_lat;
	}

	public String getPod_lon() {
		return pod_lon;
	}

	public void setPod_lon(String pod_lon) {
		this.pod_lon = pod_lon;
	}

	public String getTotal_distance() {
		return total_distance;
	}

	public void setTotal_distance(String total_distance) {
		this.total_distance = total_distance;
	}

	public String getId_platform() {
		return id_platform;
	}

	public void setId_platform(String id_platform) {
		this.id_platform = id_platform;
	}

	public String getNama_platform() {
		return nama_platform;
	}

	public void setNama_platform(String nama_platform) {
		this.nama_platform = nama_platform;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getBilling_code() {
		return billing_code;
	}

	public void setBilling_code(String billing_code) {
		this.billing_code = billing_code;
	}

	public String getBooked_date() {
		return booked_date;
	}

	public void setBooked_date(String booked_date) {
		this.booked_date = booked_date;
	}

	public List<DOCUMENT_LOG_TRUCK> getLogtruck() {
		return logtruck;
	}

	public void setLogtruck(List<DOCUMENT_LOG_TRUCK> logtruck) {
		this.logtruck = logtruck;
	}
	
}
