package com.xa.dev_backendv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="mst_payment_services")
public class MST_PAYMENT_SERVICES {
	@Id
	@Column(name = "id_payment_services", length = 7)
	private String id_payment_services;
	
	@NotNull
	@Column(name = "id_platform", length = 5, nullable = true)
	private String id_platform;
	
	@NotNull
	@Column(name = "payment_service", length = 50, nullable = true)
	private String payment_service;
	
	@ManyToOne
	@JoinColumn(name = "id_platform", foreignKey = @ForeignKey(name = "fk_id_platform"),
	insertable = false, updatable = false)
	private MST_PLATFORM platform;

	public String getId_payment_services() {
		return id_payment_services;
	}

	public void setId_payment_services(String id_payment_services) {
		this.id_payment_services = id_payment_services;
	}

	public String getId_platform() {
		return id_platform;
	}

	public void setId_platform(String id_platform) {
		this.id_platform = id_platform;
	}

	public String getPayment_service() {
		return payment_service;
	}

	public void setPayment_service(String payment_service) {
		this.payment_service = payment_service;
	}

	public MST_PLATFORM getPlatform() {
		return platform;
	}

	public void setPlatform(MST_PLATFORM platform) {
		this.platform = platform;
	}
}
