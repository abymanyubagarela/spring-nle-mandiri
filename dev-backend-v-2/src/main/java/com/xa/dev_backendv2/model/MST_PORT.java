package com.xa.dev_backendv2.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="mst_port")
public class MST_PORT {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "mst_port_generator")
	@SequenceGenerator(name="mst_port_generator", sequenceName = "seq_mst_port", allocationSize = 1)
	@Column(name="id_port")
	private Integer id_port;
	
	@NotNull
	@Column(name="port", nullable = true)
	private String port;
	
	@Column(name="singkatan", length = 60)
	private String singkatan;
	
	@Column(name="kota")
	private String kota;
	
	@NotNull
	@Column(name="is_asal", nullable = true)
	private Integer is_asal;
	
	@NotNull
	@Column(name="is_tujuan", nullable = true)
	private Integer is_tujuan;

	public Integer getId_port() {
		return id_port;
	}

	public void setId_port(Integer id_port) {
		this.id_port = id_port;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getSingkatan() {
		return singkatan;
	}

	public void setSingkatan(String singkatan) {
		this.singkatan = singkatan;
	}

	public String getKota() {
		return kota;
	}

	public void setKota(String kota) {
		this.kota = kota;
	}

	public Integer getIs_asal() {
		return is_asal;
	}

	public void setIs_asal(Integer is_asal) {
		this.is_asal = is_asal;
	}

	public Integer getIs_tujuan() {
		return is_tujuan;
	}

	public void setIs_tujuan(Integer is_tujuan) {
		this.is_tujuan = is_tujuan;
	}
	
	@Override
	public String toString() {
		return "MST_PORT{" +
				"id_port=" + id_port +
				'}';
	}
}
