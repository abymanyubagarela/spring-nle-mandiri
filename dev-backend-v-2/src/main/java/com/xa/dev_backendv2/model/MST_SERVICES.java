package com.xa.dev_backendv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="mst_services")
public class MST_SERVICES {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_services_generator")
	@SequenceGenerator(name="mst_services_generator", sequenceName = "seq_mst_services",allocationSize = 1)
	@Column(name = "id_service")
	private Integer id_service;
	
	@NotNull
	@Column(name = "service_name", nullable = true)
	private String service_name;

	public Integer getId_service() {
		return id_service;
	}

	public void setId_service(Integer id_service) {
		this.id_service = id_service;
	}

	public String getService_name() {
		return service_name;
	}

	public void setService_name(String service_name) {
		this.service_name = service_name;
	}
}
