package com.xa.dev_backendv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="mst_dangerous_type")
public class MST_DANGEROUS_TYPE {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_dangerous_type_generator")
	@SequenceGenerator(name = "mst_dangerous_type_generator", sequenceName = "seq_mst_dangerous_type", allocationSize = 1)
	@Column(name="id")
	private Integer id;
	
	@NotNull
	@Column(name="type", length = 100, nullable = true)
	private String type;
	
	@Column(name="related_fields", columnDefinition = "TEXT")
	private String related_fields;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRelated_fields() {
		return related_fields;
	}

	public void setRelated_fields(String related_fields) {
		this.related_fields = related_fields;
	}
}
