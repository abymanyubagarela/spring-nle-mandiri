package com.xa.dev_backendv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="mst_document_type")
public class MST_DOCUMENT_TYPE {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_document_type_generator")
	@SequenceGenerator(name = "mst_document_type_generator", sequenceName = "seq_mst_document_type", allocationSize = 1)
	@Column(name="kd_document_type")
	private Integer kd_document_type;
	
	@NotNull
	@Column(name="kd_process_type", nullable = true)
	private Integer kd_process_type;
	
	@NotNull
	@Column(name="document_name", length = 20)
	private String document_name;
	
	@NotNull
	@Column(name="description", length = 20)
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "kd_process_type", foreignKey = @ForeignKey(name = "fk_kd_process_type"), 
	insertable = false, updatable = false)
	private MST_PROCESS_TYPE mst_process_type;

	public MST_PROCESS_TYPE getMst_process_type() {
		return mst_process_type;
	}

	public void setMst_process_type(MST_PROCESS_TYPE mst_process_type) {
		this.mst_process_type = mst_process_type;
	}

	public Integer getKd_document_type() {
		return kd_document_type;
	}

	public void setKd_document_type(Integer kd_document_type) {
		this.kd_document_type = kd_document_type;
	}

	public Integer getKd_process_type() {
		return kd_process_type;
	}

	public void setKd_process_type(Integer kd_process_type) {
		this.kd_process_type = kd_process_type;
	}

	public String getDocument_name() {
		return document_name;
	}

	public void setDocument_name(String document_name) {
		this.document_name = document_name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
