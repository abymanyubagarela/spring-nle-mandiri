package com.xa.dev_backendv2.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="payment_invoice_item")
public class PAYMENT_INVOICE_ITEM {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "payment_invoice_item_generator")
	@SequenceGenerator(name = "payment_invoice_item_generator", sequenceName = "seq_payment_invoice_item", allocationSize = 1)
	@Column(name = "invoice_item_id")
	private Integer invoice_item_id;
	
	@JsonBackReference
	@JoinColumn(name = "order_number", referencedColumnName = "order_number")
	@ManyToOne(fetch = FetchType.EAGER)
	private PAYMENT_INVOICE order_number;
	
	@NotNull
	@Column(name="id_platform", length = 5, nullable = true)
	private String id_platform;
	
	@ManyToOne
	@JoinColumn(name = "id_platform", foreignKey = @ForeignKey(name = "fk_id_platform"), 
	insertable = false, updatable = false)
	private MST_PLATFORM platform;
	
	@NotNull
	@Column(name="amount", precision = 14, scale = 2, nullable = true)
	private BigDecimal amount;
	
	@Column(name="quantity", length = 18)
	private String quantity;
	
	@NotNull
	@Column(name="id_service", nullable = true)
	private Integer id_service;
	
	@ManyToOne
	@JoinColumn(name = "id_service", foreignKey = @ForeignKey(name = "fk_id_service"),
	insertable = false, updatable = false)
	private MST_SERVICES services;
	
	@NotNull
	@Column(name="service_unique_id", nullable = true)
	private String service_unique_id;
	
	@Column(name="item_info", columnDefinition = "text")
	private String item_info;

	public Integer getInvoice_item_id() {
		return invoice_item_id;
	}

	public void setInvoice_item_id(Integer invoice_item_id) {
		this.invoice_item_id = invoice_item_id;
	}

	public PAYMENT_INVOICE getOrder_number() {
		return order_number;
	}

	public void setOrder_number(PAYMENT_INVOICE order_number) {
		this.order_number = order_number;
	}

	public String getId_platform() {
		return id_platform;
	}

	public void setId_platform(String id_platform) {
		this.id_platform = id_platform;
	}

	public MST_PLATFORM getPlatform() {
		return platform;
	}

	public void setPlatform(MST_PLATFORM platform) {
		this.platform = platform;
	}

	public MST_SERVICES getServices() {
		return services;
	}

	public void setServices(MST_SERVICES services) {
		this.services = services;
	}

	public void setId_service(Integer id_service) {
		this.id_service = id_service;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public Integer getId_service() {
		return id_service;
	}

	public String getService_unique_id() {
		return service_unique_id;
	}

	public void setService_unique_id(String service_unique_id) {
		this.service_unique_id = service_unique_id;
	}

	public String getItem_info() {
		return item_info;
	}

	public void setItem_info(String item_info) {
		this.item_info = item_info;
	}
}
