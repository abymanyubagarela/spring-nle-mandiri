package com.xa.dev_backendv2.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="document_sp2_log")
public class DOCUMENT_SP2_LOG {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "document_sp2_log_generator")
	@SequenceGenerator(name = "document_sp2_log_generator", sequenceName = "seq_document_sp2_log", allocationSize = 1)
	@Column(name="id_sp2_log")
	private Integer id_sp2_log;
	
	@Column(name="id_document_sp2")
	private String id_document_sp2;
	
	@Column(name="create_date")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp create_date;
	
	@Column(name="status", length = 255)
	private String status;

	public Integer getId_sp2_log() {
		return id_sp2_log;
	}

	public void setId_sp2_log(Integer id_sp2_log) {
		this.id_sp2_log = id_sp2_log;
	}

	public String getId_document_sp2() {
		return id_document_sp2;
	}

	public void setId_document_sp2(String id_document_sp2) {
		this.id_document_sp2 = id_document_sp2;
	}

	
	public Timestamp getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Timestamp create_date) {
		this.create_date = create_date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
