package com.xa.dev_backendv2.model;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="td_document")
//@SecondaryTable(name = "td_user_document", pkJoinColumns = @PrimaryKeyJoinColumn(name="id_document"))
public class TD_DOCUMENT {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "td_document_generator")
	@SequenceGenerator(name = "td_document_generator", sequenceName = "seq_td_document", allocationSize = 1)
	@Column(name="id_document")
	private Integer id_document;
	
	@NotNull
	@Column(name="kd_document_type", nullable = true)
	private Integer kd_document_type;
	
	@NotNull
	@Column(name="npwp_cargo_owner", length = 30, nullable = true)
	@JsonProperty("npwp_cargo_owner")
	private String npwpCargoOwner;
	
//	@Column(name="id_document_do", length = 255)
//	private String id_document_do;
	
	@Column(name="document_no", length = 255)
	private String document_no;
	
	@Column(name="document_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date document_date;
	
	@Column(name="document_status",length = 255)
	private String document_status;
	
	@Column(name="bl_no",length = 20)
	@JsonProperty("bl_no")
	private String blNo;
	
	@Column(name="bl_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date bl_date;
	
	@NotNull
	@Column(name="finish_do", nullable = true, columnDefinition = "integer default 0")
	private Integer finish_do;
	
	@NotNull
	@Column(name="finish_trucking", nullable = true, columnDefinition = "integer default 0")
	private Integer finish_trucking;
	
	@NotNull
	@Column(name="finish_sp2", nullable = true, columnDefinition = "integer default 0")
	private Integer finish_sp2;
	
	@NotNull
	@Column(name="finish_warehouse", nullable = true, columnDefinition = "integer default 0")
	private Integer finish_warehouse;
	
	@NotNull
	@Column(name="finish_depo", nullable = true, columnDefinition = "integer default 0")
	private Integer finish_depo;
	
	@Column(name="nm_cargoowner")
	private String nm_cargoowner;
	
	@Column(name="no_sppb")
	private String no_sppb;
	
	@Column(name="tanggal_sppb")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date tanggal_sppb;
	
	@ManyToOne
	@JoinColumn(name="kd_document_type", foreignKey = @ForeignKey(name="fk_kd_document_type"),
	insertable = false, updatable = false)
	private MST_DOCUMENT_TYPE mst_document_type;

	@JsonManagedReference
	@OneToMany(mappedBy = "id_document",cascade = CascadeType.ALL)
	@OrderBy(value = "id_user_document ASC")
	private Set<TD_USER_DOCUMENT> user_document;

//	public String getId_document_do() {
//		return id_document_do;
//	}
//
//	public void setId_document_do(String id_document_do) {
//		this.id_document_do = id_document_do;
//	}

	public Date getTanggal_sppb() {
		return tanggal_sppb;
	}

	public void setTanggal_sppb(Date tanggal_sppb) {
		this.tanggal_sppb = tanggal_sppb;
	}

	public String getNo_sppb() {
		return no_sppb;
	}

	public void setNo_sppb(String no_sppb) {
		this.no_sppb = no_sppb;
	}

	public String getNm_cargoowner() {
		return nm_cargoowner;
	}

	public void setNm_cargoowner(String nm_cargoowner) {
		this.nm_cargoowner = nm_cargoowner;
	}

	public Set<TD_USER_DOCUMENT> getUser_document() {
		return user_document;
	}

	public void setUser_document(Set<TD_USER_DOCUMENT> td_user_document) {
		this.user_document = td_user_document;
	}

	public Integer getFinish_do() {
		return finish_do;
	}

	public void setFinish_do(Integer finish_do) {
		this.finish_do = finish_do;
	}

	public Integer getFinish_trucking() {
		return finish_trucking;
	}

	public void setFinish_trucking(Integer finish_trucking) {
		this.finish_trucking = finish_trucking;
	}

	public Integer getFinish_sp2() {
		return finish_sp2;
	}

	public void setFinish_sp2(Integer finish_sp2) {
		this.finish_sp2 = finish_sp2;
	}

	public Integer getFinish_warehouse() {
		return finish_warehouse;
	}

	public void setFinish_warehouse(Integer finish_warehouse) {
		this.finish_warehouse = finish_warehouse;
	}

	public Integer getFinish_depo() {
		return finish_depo;
	}

	public void setFinish_depo(Integer finish_depo) {
		this.finish_depo = finish_depo;
	}

	public Integer getId_document() {
		return id_document;
	}

	public void setId_document(Integer id_document) {
		this.id_document = id_document;
	}
	
	public Integer getKd_document_type() {
		return kd_document_type;
	}

	public void setKd_document_type(Integer kd_document_type) {
		this.kd_document_type = kd_document_type;
	}

	public String getNpwpCargoOwner() {
		return npwpCargoOwner;
	}

	public void setNpwpCargoOwner(String npwp_cargo_owner) {
		this.npwpCargoOwner = npwp_cargo_owner;
	}

	public String getDocument_no() {
		return document_no;
	}

	public void setDocument_no(String document_no) {
		this.document_no = document_no;
	}

	public Date getDocument_date() {
		return document_date;
	}

	public void setDocument_date(Date document_date) {
		this.document_date = document_date;
	}

	public String getDocument_status() {
		return document_status;
	}

	public void setDocument_status(String document_status) {
		this.document_status = document_status;
	}

	public String getBlNo() {
		return blNo;
	}

	public void setBlNo(String bl_no) {
		this.blNo = bl_no;
	}

	public Date getBl_date() {
		return bl_date;
	}

	public void setBl_date(Date bl_date) {
		this.bl_date = bl_date;
	}

	public MST_DOCUMENT_TYPE getMst_document_type() {
		return mst_document_type;
	}

	public void setMst_document_type(MST_DOCUMENT_TYPE mst_document_type) {
		this.mst_document_type = mst_document_type;
	}

	@Override
	public String toString() {
		return "TD_DOCUMENT{" +
				"id_document=" + id_document +
				'}';
	}
}
