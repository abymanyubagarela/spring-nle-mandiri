package com.xa.dev_backendv2.model;

import java.sql.Timestamp;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "document_log_truck")
public class DOCUMENT_LOG_TRUCK {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "logtruck_generator")
	@SequenceGenerator(name="logtruck_generator", sequenceName = "seq_logtruck",allocationSize = 1)
	@Column(name = "id_log_truck")
	private Integer idLogTruck;

//	@ManyToOne
//	private DATA_TRANSPORTER_DETIL idDataTransporterDetil;
	
	@Column(name = "idDataTransporterDetil")
	private Integer idDataTransporterDetil;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "create_date")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp create_date;

	public Integer getIdLogTruck() {
		return idLogTruck;
	}

	public void setIdLogTruck(Integer idLogTruck) {
		this.idLogTruck = idLogTruck;
	}

//	public DATA_TRANSPORTER_DETIL getIdDataTransporterDetil() {
//		return idDataTransporterDetil;
//	}
//
//	public void setIdDataTransporterDetil(DATA_TRANSPORTER_DETIL idDataTransporterDetil) {
//		this.idDataTransporterDetil = idDataTransporterDetil;
//	}

	public Integer getIdDataTransporterDetil() {
		return idDataTransporterDetil;
	}

	public void setIdDataTransporterDetil(Integer idDataTransporterDetil) {
		this.idDataTransporterDetil = idDataTransporterDetil;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Timestamp create_date) {
		this.create_date = create_date;
	}
	
}
