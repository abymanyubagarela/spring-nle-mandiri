package com.xa.dev_backendv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="domestic_vessel_packing")
public class DOMESTIC_VESSEL_PACKING {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "domestic_vessel_packing_generator")
	@SequenceGenerator(name="domestic_vessel_packing_generator", sequenceName = "seq_domestic_vessel_packing",allocationSize = 1)
	@Column(name="id_packing")
	private Integer id_packing;
	
	//reference on table domestic_vessel_booking.id_request
	@JsonBackReference
	@JoinColumn(name = "id_request", referencedColumnName = "id_request", nullable = true)
	@ManyToOne(fetch = FetchType.EAGER)
	private DOMESTIC_VESSEL_BOOKING id_request;
	
	@NotNull
	@Column(name="jenis_muatan", nullable = true)
	private String jenis_muatan;
	
	@Column(name="deskripsi", columnDefinition = "text")
	private String deskripsi;
	
	@Column(name="kemasan", length = 100, nullable = true)
	private String kemasan;
	
	@NotNull
	@Column(name="jumlah_barang", nullable = true)
	private Integer jumlah_barang;
	
	@NotNull
	@Column(name="berat_muatan", length = 20, nullable = true)
	private String berat_muatan;
	
	//===================================================================================
	public DOMESTIC_VESSEL_BOOKING getId_request() {
		return id_request;
	}

	public void setId_request(DOMESTIC_VESSEL_BOOKING id_request) {
		this.id_request = id_request;
	}

	public Integer getId_packing() {
		return id_packing;
	}

	public void setId_packing(Integer id_packing) {
		this.id_packing = id_packing;
	}

	public String getJenis_muatan() {
		return jenis_muatan;
	}

	public void setJenis_muatan(String jenis_muatan) {
		this.jenis_muatan = jenis_muatan;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public String getKemasan() {
		return kemasan;
	}

	public void setKemasan(String kemasan) {
		this.kemasan = kemasan;
	}

	public Integer getJumlah_barang() {
		return jumlah_barang;
	}

	public void setJumlah_barang(Integer jumlah_barang) {
		this.jumlah_barang = jumlah_barang;
	}

	public String getBerat_muatan() {
		return berat_muatan;
	}

	public void setBerat_muatan(String berat_muatan) {
		this.berat_muatan = berat_muatan;
	}
}
