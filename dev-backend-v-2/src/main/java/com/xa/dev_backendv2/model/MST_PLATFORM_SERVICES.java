package com.xa.dev_backendv2.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="mst_platform_services")
public class MST_PLATFORM_SERVICES {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_platform_services_generator")
	@SequenceGenerator(name="mst_platform_services_generator", sequenceName = "seq_mst_platform_services",allocationSize = 1)
	@Column(name = "id_platform_services")
	private Integer id_platform_services;
	
	@NotNull
	@Column(name = "id_platform", nullable = true, length = 5)
	private String id_platform;
	
	@NotNull
	@Column(name = "id_service", nullable = true)
	private Integer id_service;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "id_platform", referencedColumnName = "id_platform", foreignKey = @ForeignKey(name = "fk_id_platform"),
	insertable = false, updatable = false)
	private MST_PLATFORM mst_platform;

	@ManyToOne
	@JoinColumn(name = "id_service", foreignKey = @ForeignKey(name = "fk_id_service"),
	insertable = false, updatable = false)
	private MST_SERVICES mst_services;

	public MST_PLATFORM getMst_platform() {
		return mst_platform;
	}

	public void setMst_platform(MST_PLATFORM mst_platform) {
		this.mst_platform = mst_platform;
	}

	public MST_SERVICES getMst_services() {
		return mst_services;
	}

	public void setMst_services(MST_SERVICES mst_services) {
		this.mst_services = mst_services;
	}

	public Integer getId_platform_services() {
		return id_platform_services;
	}

	public void setId_platform_services(Integer id_platform_services) {
		this.id_platform_services = id_platform_services;
	}

	public String getId_platform() {
		return id_platform;
	}

	public void setId_platform(String id_platform) {
		this.id_platform = id_platform;
	}

	public Integer getId_service() {
		return id_service;
	}

	public void setId_service(Integer id_service) {
		this.id_service = id_service;
	}
}
