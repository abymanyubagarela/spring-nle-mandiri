package com.xa.dev_backendv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="mst_container_type")
public class MST_CONTAINER_TYPE {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "container_type_generator")
	@SequenceGenerator(name = "container_type_generator", sequenceName = "seq_container_type", allocationSize = 1)
	@Column(name="id")
	private Integer id_container_type;
	
	@NotNull
	@Column(name="type", length = 100)
	private String type;
	
	@Column(name="related_fields")
	private String related_fields;

	public Integer getId_container_type() {
		return id_container_type;
	}

	public void setId_container_type(Integer id_container_type) {
		this.id_container_type = id_container_type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRelated_fields() {
		return related_fields;
	}

	public void setRelated_fields(String related_fields) {
		this.related_fields = related_fields;
	}
	
}
