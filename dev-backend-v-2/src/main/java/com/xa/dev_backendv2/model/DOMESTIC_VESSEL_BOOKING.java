package com.xa.dev_backendv2.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="domestic_vessel_booking")
public class DOMESTIC_VESSEL_BOOKING {
	@Id
	@Column(name="id_request")
	private String id_request;
	
	//reference on table mst_platform.id_platform
	@NotNull
	@Column(name="id_platform", length = 5, nullable = true)
	private String id_platform;
	
	@Column(name="ship_name")
	private String ship_name;
	
	@Column(name="container_type" , length = 50)
	private String container_type;
	
	@Column(name="partner" , length = 255)
	private String partner;
	
	@Column(name="partner_rating", columnDefinition = "SMALLINT")
	private Short partner_rating;
	
	@Column(name="service_type" , length = 50)
	private String service_type;
	
	@Column(name="order_id")
	private String order_id;
	
	//reference on table mst_port.id_port
	@NotNull
	@Column(name="id_port_asal", nullable = true)
	private Integer id_port_asal;
	
	//reference on table mst_port.id_port
	@NotNull
	@Column(name="id_port_tujuan", nullable = true)
	private Integer id_port_tujuan;
	
	//reference on table mst_kawasan.id_kawasan
	@Column(name="id_kawasan_asal")
	private Integer id_kawasan_asal;
	
	//reference on table mst_kawasan.id_kawasan
	@Column(name="id_kawasan_tujuan")
	private Integer id_kawasan_tujuan;
	
	@Column(name="price",precision = 14, scale = 2)
	private BigDecimal price;
	
	@Column(name="paid_date")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp paid_date;
	
	@NotNull
	@Column(name="nama_pengirim", nullable = true)
	private String nama_pengirim;
	
	@Column(name="hp_pengirim", length = 30)
	private String hp_pengirim;
	
	@NotNull
	@Column(name="alamat_pengirim", columnDefinition = "text", nullable = true)
	private String alamat_pengirim;
	
	@NotNull
	@Column(name="nama_penerima", nullable = true)
	private String nama_penerima;
	
	@Column(name="hp_penerima", length = 30)
	private String hp_penerima;
	
	@NotNull
	@Column(name="alamat_penerima", columnDefinition = "text", nullable = true)
	private String alamat_penerima;
	
	@NotNull
	@Column(name="qty", nullable = true, columnDefinition = "smallint")
	private Integer qty;
	
	@Column(name="deskripsi_umum_barang", columnDefinition = "text")
	private String deskripsi_umum_barang;
	
	@NotNull
	@Column(name="tanggal_stuffing", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date tanggal_stuffing;
	
	@Column(name="status")
	private String status;
	
	@Column(name="npwp", length = 30)
	private String npwp;
	
	@Column(name="email", length = 255)
	private String email;
	
	@NotNull
	@Column(name="booking_date", nullable = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp booking_date;
	
	//===================================================================================
	@ManyToOne
	@JoinColumn(name = "id_platform", foreignKey = @ForeignKey(name = "fk_id_platform"), 
	insertable = false, updatable = false)
	private MST_PLATFORM platform;
	
	@ManyToOne
	@JoinColumn(name = "id_port_asal", referencedColumnName = "id_port", foreignKey = @ForeignKey(name = "fk_id_port_asal"), 
	insertable = false, updatable = false)
	private MST_PORT port_asal;
	
	@ManyToOne
	@JoinColumn(name = "id_port_tujuan", referencedColumnName = "id_port", foreignKey = @ForeignKey(name = "fk_id_port_tujuan"), 
	insertable = false, updatable = false)
	private MST_PORT port_tujuan;
	
	@ManyToOne
	@JoinColumn(name = "id_kawasan_asal", referencedColumnName = "id_kawasan", foreignKey = @ForeignKey(name = "fk_id_kawasan_asal"), 
	insertable = false, updatable = false)
	private MST_KAWASAN kawasan_asal;
	
	@ManyToOne
	@JoinColumn(name = "id_kawasan_tujuan", referencedColumnName = "id_kawasan", foreignKey = @ForeignKey(name = "fk_id_kawasan_tujuan"), 
	insertable = false, updatable = false)
	private MST_KAWASAN kawasan_tujuan;
	
	@OneToMany(mappedBy = "id_request", cascade = CascadeType.ALL)
	@OrderBy(value = "id_packing ASC")
	private Set<DOMESTIC_VESSEL_PACKING> vessel_packing;
	
	@OneToMany(mappedBy = "id_request", cascade = CascadeType.ALL)
	@OrderBy(value = "id_log_status ASC")
	private Set<DOMESTIC_VESSEL_LOG_STATUS> vessel_log_status;

	public Set<DOMESTIC_VESSEL_LOG_STATUS> getVessel_log_status() {
		return vessel_log_status;
	}

	public void setVessel_log_status(Set<DOMESTIC_VESSEL_LOG_STATUS> vessel_log_status) {
		this.vessel_log_status = vessel_log_status;
	}

	public MST_PLATFORM getPlatform() {
		return platform;
	}

	public Set<DOMESTIC_VESSEL_PACKING> getVessel_packing() {
		return vessel_packing;
	}

	public void setVessel_packing(Set<DOMESTIC_VESSEL_PACKING> vessel_packing) {
		this.vessel_packing = vessel_packing;
	}

	public Integer getId_kawasan_asal() {
		return id_kawasan_asal;
	}

	public void setId_kawasan_asal(Integer id_kawasan_asal) {
		this.id_kawasan_asal = id_kawasan_asal;
	}

	public Integer getId_kawasan_tujuan() {
		return id_kawasan_tujuan;
	}

	public void setId_kawasan_tujuan(Integer id_kawasan_tujuan) {
		this.id_kawasan_tujuan = id_kawasan_tujuan;
	}

	public MST_KAWASAN getKawasan_asal() {
		return kawasan_asal;
	}

	public void setKawasan_asal(MST_KAWASAN kawasan_asal) {
		this.kawasan_asal = kawasan_asal;
	}

	public MST_KAWASAN getKawasan_tujuan() {
		return kawasan_tujuan;
	}

	public void setKawasan_tujuan(MST_KAWASAN kawasan_tujuan) {
		this.kawasan_tujuan = kawasan_tujuan;
	}

	public Integer getId_port_tujuan() {
		return id_port_tujuan;
	}

	public void setId_port_tujuan(Integer id_port_tujuan) {
		this.id_port_tujuan = id_port_tujuan;
	}

	public MST_PORT getPort_tujuan() {
		return port_tujuan;
	}

	public void setPort_tujuan(MST_PORT port_tujuan) {
		this.port_tujuan = port_tujuan;
	}

	public Integer getId_port_asal() {
		return id_port_asal;
	}

	public void setId_port_asal(Integer id_port_asal) {
		this.id_port_asal = id_port_asal;
	}

	public void setPlatform(MST_PLATFORM platform) {
		this.platform = platform;
	}

	public MST_PORT getPort_asal() {
		return port_asal;
	}

	public void setPort_asal(MST_PORT port_asal) {
		this.port_asal = port_asal;
	}

	public String getId_request() {
		return id_request;
	}

	public void setId_request(String id_request) {
		this.id_request = id_request;
	}

	public String getId_platform() {
		return id_platform;
	}

	public void setId_platform(String id_platform) {
		this.id_platform = id_platform;
	}
	
	public String getShip_name() {
		return ship_name;
	}

	public void setShip_name(String ship_name) {
		this.ship_name = ship_name;
	}

	public String getContainer_type() {
		return container_type;
	}

	public void setContainer_type(String container_type) {
		this.container_type = container_type;
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public Short getPartner_rating() {
		return partner_rating;
	}

	public void setPartner_rating(Short partner_rating) {
		this.partner_rating = partner_rating;
	}

	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Timestamp getPaid_date() {
		return paid_date;
	}

	public void setPaid_date(Timestamp paid_date) {
		this.paid_date = paid_date;
	}

	public String getNama_pengirim() {
		return nama_pengirim;
	}

	public void setNama_pengirim(String nama_pengirim) {
		this.nama_pengirim = nama_pengirim;
	}

	public String getHp_pengirim() {
		return hp_pengirim;
	}

	public void setHp_pengirim(String hp_pengirim) {
		this.hp_pengirim = hp_pengirim;
	}

	public String getAlamat_pengirim() {
		return alamat_pengirim;
	}

	public void setAlamat_pengirim(String alamat_pengirim) {
		this.alamat_pengirim = alamat_pengirim;
	}

	public String getNama_penerima() {
		return nama_penerima;
	}

	public void setNama_penerima(String nama_penerima) {
		this.nama_penerima = nama_penerima;
	}

	public String getHp_penerima() {
		return hp_penerima;
	}

	public void setHp_penerima(String hp_penerima) {
		this.hp_penerima = hp_penerima;
	}

	public String getAlamat_penerima() {
		return alamat_penerima;
	}

	public void setAlamat_penerima(String alamat_penerima) {
		this.alamat_penerima = alamat_penerima;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public String getDeskripsi_umum_barang() {
		return deskripsi_umum_barang;
	}

	public void setDeskripsi_umum_barang(String deskripsi_umum_barang) {
		this.deskripsi_umum_barang = deskripsi_umum_barang;
	}

	public Date getTanggal_stuffing() {
		return tanggal_stuffing;
	}

	public void setTanggal_stuffing(Date tanggal_stuffing) {
		this.tanggal_stuffing = tanggal_stuffing;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Timestamp getBooking_date() {
		return booking_date;
	}

	public void setBooking_date(Timestamp booking_date) {
		this.booking_date = booking_date;
	}
}
