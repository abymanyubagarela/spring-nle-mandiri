package com.xa.dev_backendv2.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.NotNull;

@Entity
@Table(name="document_do")
public class DOCUMENT_DO {
	@Id
	@Column(name="id_document_do", length = 255)
	private String id_document_do;
	
	@NotNull
	@Column(name="request_date", nullable = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp request_date;
	
	@NotNull
	@Column(name="npwp_cargo_owner", length = 30, nullable = true)
	@JsonProperty("npwp_cargo_owner")
	private String npwpCargoOwner;
	
	@NotNull
	@Column(name="id_platform", length = 5, nullable = true)
	private String id_platform;
	
	@Column(name="id_ff_ppjk", length = 30)
	@JsonProperty("id_ff_ppjk")
	private String idFfPpjk;
	
	@Column(name="shipping_name")
	private String shipping_name;
	
	@Column(name="forwarder_name")
	private String forwarder_name;
	
	@Column(name="party", columnDefinition = "integer default 0")
	private Integer party;

	@JsonProperty("bl_no")
	@Column(name="bl_no", length = 20)
	private String blNo;
	
	@Column(name="bl_type", length = 20)
	@JsonProperty("bl_type")
	private String blType;
	
	@Column(name="bl_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date bl_date;
	
	@Column(name="price",precision = 14, scale = 2)
	private BigDecimal price;
	
	@Column(name="paid_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date paid_date;
	
	@Column(name="status")
	private String status;
	
	@Column(name="do_number", length = 60)
	@JsonProperty("do_number")
	private String doNumber;

	@Column(name="do_date_number")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date do_date_number;
	
	@Column(name = "payment_url", columnDefinition = "TEXT")
	private String payment_url;
	
	@ManyToOne
	@JoinColumn(name = "id_platform", foreignKey = @ForeignKey(name = "fk_id_platform"), 
	insertable = false, updatable = false)
	private MST_PLATFORM mst_platform;
	
	@OneToMany(mappedBy = "id_document_do",cascade = CascadeType.ALL)
	@OrderBy(value = "id_do_container ASC")
	private Set<DOCUMENT_DO_CONTAINER> container = new HashSet<>();
	
	@OneToMany(mappedBy = "id_document_do",cascade = CascadeType.ALL)
	@OrderBy(value = "id ASC")
	private Set<DOCUMENT_DO_LOG> document_do_log = new HashSet<>();
	
//	@OneToMany(mappedBy = "id_document_do",cascade = CascadeType.ALL)
//	@OrderBy(value = "id ASC")
//	private Set<DOCUMENT_BL_HOST> document_bl_host ;
	
	public String getNpwpCargoOwner() {
		return npwpCargoOwner;
	}

	public String getBlType() {
		return blType;
	}

	public void setBlType(String bl_type) {
		this.blType = bl_type;
	}

	public void setNpwpCargoOwner(String npwp_cargo_owner) {
		this.npwpCargoOwner = npwp_cargo_owner;
	}

	public String getId_document_do() {
		return id_document_do;
	}

	public void setId_document_do(String id_document_do) {
		this.id_document_do = id_document_do;
	}

	public Timestamp getRequest_date() {
		return request_date;
	}

	public void setRequest_date(Timestamp request_date) {
		this.request_date = request_date;
	}

	public String getId_platform() {
		return id_platform;
	}

	public void setId_platform(String id_platform) {
		this.id_platform = id_platform;
	}

	public String getIdFfPpjk() {
		return idFfPpjk;
	}

	public void setIdFfPpjk(String id_ff_ppjk) {
		this.idFfPpjk = id_ff_ppjk;
	}

	public String getShipping_name() {
		return shipping_name;
	}

	public void setShipping_name(String shipping_name) {
		this.shipping_name = shipping_name;
	}

	public String getForwarder_name() {
		return forwarder_name;
	}

	public void setForwarder_name(String forwarder_name) {
		this.forwarder_name = forwarder_name;
	}

	public Integer getParty() {
		return party;
	}

	public void setParty(Integer party) {
		this.party = party;
	}

	public String getBlNo() {
		return blNo;
	}

	public void setBlNo(String bl_no) {
		this.blNo = bl_no;
	}

	public Date getBl_date() {
		return bl_date;
	}

	public void setBl_date(Date bl_date) {
		this.bl_date = bl_date;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getPaid_date() {
		return paid_date;
	}

	public void setPaid_date(Date paid_date) {
		this.paid_date = paid_date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDoNumber() {
		return doNumber;
	}

	public void setDoNumber(String do_number) {
		this.doNumber = do_number;
	}

	public Date getDo_date_number() {
		return do_date_number;
	}

	public void setDo_date_number(Date do_date_number) {
		this.do_date_number = do_date_number;
	}

	public String getPayment_url() {
		return payment_url;
	}

	public void setPayment_url(String payment_url) {
		this.payment_url = payment_url;
	}

	public MST_PLATFORM getMst_platform() {
		return mst_platform;
	}

	public void setMst_platform(MST_PLATFORM mst_platform) {
		this.mst_platform = mst_platform;
	}

	public Set<DOCUMENT_DO_CONTAINER> getContainer() {
		return container;
	}

	public void setContainer(Set<DOCUMENT_DO_CONTAINER> container) {
		this.container = container;
	}

	public Set<DOCUMENT_DO_LOG> getDocument_do_log() {
		return document_do_log;
	}

	public void setDocument_do_log(Set<DOCUMENT_DO_LOG> document_do_log) {
		this.document_do_log = document_do_log;
	}

//	public Set<DOCUMENT_BL_HOST> getDocument_bl_host() {
//		return document_bl_host;
//	}
//
//	public void setDocument_bl_host(Set<DOCUMENT_BL_HOST> document_bl_host) {
//		this.document_bl_host = document_bl_host;
//	}
	
}
