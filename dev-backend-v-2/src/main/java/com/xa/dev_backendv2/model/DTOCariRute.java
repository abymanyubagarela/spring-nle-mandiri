package com.xa.dev_backendv2.model;

public class DTOCariRute {
	private String id_request;
	private Integer portAsalId;
	private String portAsalName;
	private Integer portTujuanId;
	private String portTujuanName;
	private Integer kawasanAsalId;
	private String kawasanAsalName;
	private Integer kawasanTujuanId;
	private String kawasanTujuanName;
	private String npwp;
	
	public String getId_request() {
		return id_request;
	}
	public void setId_request(String id_request) {
		this.id_request = id_request;
	}
	public String getNpwp() {
		return npwp;
	}
	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}
	public Integer getPortAsalId() {
		return portAsalId;
	}
	public void setPortAsalId(Integer portAsalId) {
		this.portAsalId = portAsalId;
	}
	public String getPortAsalName() {
		return portAsalName;
	}
	public void setPortAsalName(String portAsalName) {
		this.portAsalName = portAsalName;
	}
	public Integer getPortTujuanId() {
		return portTujuanId;
	}
	public void setPortTujuanId(Integer portTujuanId) {
		this.portTujuanId = portTujuanId;
	}
	public String getPortTujuanName() {
		return portTujuanName;
	}
	public void setPortTujuanName(String portTujuanName) {
		this.portTujuanName = portTujuanName;
	}
	public Integer getKawasanAsalId() {
		return kawasanAsalId;
	}
	public void setKawasanAsalId(Integer kawasanAsalId) {
		this.kawasanAsalId = kawasanAsalId;
	}
	public String getKawasanAsalName() {
		return kawasanAsalName;
	}
	public void setKawasanAsalName(String kawasanAsalName) {
		this.kawasanAsalName = kawasanAsalName;
	}
	public Integer getKawasanTujuanId() {
		return kawasanTujuanId;
	}
	public void setKawasanTujuanId(Integer kawasanTujuanId) {
		this.kawasanTujuanId = kawasanTujuanId;
	}
	public String getKawasanTujuanName() {
		return kawasanTujuanName;
	}
	public void setKawasanTujuanName(String kawasanTujuanName) {
		this.kawasanTujuanName = kawasanTujuanName;
	}
}
