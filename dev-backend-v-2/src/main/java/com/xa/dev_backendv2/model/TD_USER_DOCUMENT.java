package com.xa.dev_backendv2.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="td_user_document")
public class TD_USER_DOCUMENT {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "td_user_document_generator")
	@SequenceGenerator(name = "td_user_document_generator", sequenceName = "seq_td_user_document", allocationSize = 1)
	@Column(name="id_user_document")
	private Integer id_user_document;
	
	@NotNull
	@Column(name="npwp", length = 30, nullable = true)
	private String npwp;

	@JsonBackReference
	@JoinColumn(name = "id_document", referencedColumnName = "id_document" )
	@ManyToOne(fetch = FetchType.EAGER)
	private TD_DOCUMENT id_document;
	
	@NotNull
	@Column(name="access_do", nullable = true, columnDefinition = "integer default 1")
	private Integer access_do;
	
	@NotNull
	@Column(name="access_sp2", nullable = true, columnDefinition = "integer default 1")
	private Integer access_sp2;
	
	@NotNull
	@Column(name="access_trucking", nullable = true, columnDefinition = "integer default 1")
	private Integer access_trucking;
	
	@NotNull
	@Column(name="access_warehouse", nullable = true, columnDefinition = "integer default 1")
	private Integer access_warehouse;
	
	@NotNull
	@Column(name="access_depo", nullable = true, columnDefinition = "integer default 1")
	private Integer access_depo;
	
	@NotNull
	@Column(name="access_domestic", nullable = true, columnDefinition = "integer default 1")
	private Integer access_domestic;
	
	@NotNull
	@Column(name="is_ppjk", nullable = true, columnDefinition = "integer default 0")
	private Integer is_ppjk;
	
	@Column(name="name")
	private String name;
	
//	@ManyToOne
//	@JoinColumn(name="id_document", foreignKey = @ForeignKey(name="fk_id_document"),
//	insertable = false, updatable = false)
//	private TD_DOCUMENT td_document;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAccess_do() {
		return access_do;
	}

	public void setAccess_do(Integer access_do) {
		this.access_do = access_do;
	}

	public Integer getAccess_sp2() {
		return access_sp2;
	}

	public void setAccess_sp2(Integer access_sp2) {
		this.access_sp2 = access_sp2;
	}

	public Integer getAccess_trucking() {
		return access_trucking;
	}

	public void setAccess_trucking(Integer access_trucking) {
		this.access_trucking = access_trucking;
	}

	public Integer getAccess_warehouse() {
		return access_warehouse;
	}

	public void setAccess_warehouse(Integer access_warehouse) {
		this.access_warehouse = access_warehouse;
	}

	public Integer getAccess_depo() {
		return access_depo;
	}

	public void setAccess_depo(Integer access_depo) {
		this.access_depo = access_depo;
	}

	public Integer getAccess_domestic() {
		return access_domestic;
	}

	public void setAccess_domestic(Integer access_domestic) {
		this.access_domestic = access_domestic;
	}

	public Integer getIs_ppjk() {
		return is_ppjk;
	}

	public void setIs_ppjk(Integer is_ppjk) {
		this.is_ppjk = is_ppjk;
	}

	public Integer getId_user_document() {
		return id_user_document;
	}

	public void setId_user_document(Integer id_user_document) {
		this.id_user_document = id_user_document;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public TD_DOCUMENT getId_document() {
		return id_document;
	}

	public void setId_document(TD_DOCUMENT id_document) {
		this.id_document = id_document;
	}

//	public TD_DOCUMENT getTd_document() {
//		return td_document;
//	}
//
//	public void setTd_document(TD_DOCUMENT td_document) {
//		this.td_document = td_document;
//	}
	
}
