package com.xa.dev_backendv2.model;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="payment")
public class PAYMENT {
	@Id
	@Column(name="virtual_account")
	private String virtual_account;
	
	@NotNull
	@Column(name="price",precision = 14, scale = 2, nullable = true)
	private BigDecimal price;
	
	@Column(name="paid_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date paid_date;
	
	@Column(name="status")
	private String status;

	public String getVirtual_account() {
		return virtual_account;
	}

	public void setVirtual_account(String virtual_account) {
		this.virtual_account = virtual_account;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getPaid_date() {
		return paid_date;
	}

	public void setPaid_date(Date paid_date) {
		this.paid_date = paid_date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
