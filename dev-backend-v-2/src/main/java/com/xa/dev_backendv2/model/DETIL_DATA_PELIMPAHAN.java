package com.xa.dev_backendv2.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="detil_data_pelimpahan")
public class DETIL_DATA_PELIMPAHAN {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "detil_data_pelimpahan_generator")
	@SequenceGenerator(name = "detil_data_pelimpahan_generator", sequenceName = "seq_detil_data_pelimpahan", allocationSize = 1)
	@Column(name="id")
	private Integer id;
	
	@Column(name="bl_no")
	private String bl_no;
	
	@Column(name="bl_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date bl_date;
	
	@Column(name="ppjk")
	private String ppjk;
	
	@Column(name="nama_ppjk")
	private String nama_ppjk;
	
	@Column(name="created_npwp")
	private String created_npwp;
	
	@Column(name="idSuratKuasa")
	private String id_surat_kuasa;
	
	@Column(name="kd_document_type")
	private Integer kd_document_type;
	
	@Column(name="delivery_order",columnDefinition = "integer default 0")
	private Integer delivery_order;
	
	@Column(name="warehouse",columnDefinition = "integer default 0")
	private Integer warehouse;
	
	@Column(name="sp2",columnDefinition = "integer default 0")
	private Integer sp2;
	
	@Column(name="depo",columnDefinition = "integer default 0")
	private Integer depo;
	
	@Column(name="trucking",columnDefinition = "integer default 0")
	private Integer trucking;
	
	@Column(name="domestic",columnDefinition = "integer default 0")
	private Integer domestic;
	
	@ManyToOne
	@JoinColumn(name="idSuratKuasa", foreignKey = @ForeignKey(name = "fk_data_pelimpahan"),
	insertable = false, updatable = false)
	private DATA_PELIMPAHAN data_pelimpahan;

	@ManyToOne
	@JoinColumn(name="kd_document_type", foreignKey = @ForeignKey(name ="fk_document_type"),
	insertable = false, updatable = false)
	private MST_DOCUMENT_TYPE mst_document;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBl_no() {
		return bl_no;
	}

	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}

	public Date getBl_date() {
		return bl_date;
	}

	public void setBl_date(Date bl_date) {
		this.bl_date = bl_date;
	}

	public String getPpjk() {
		return ppjk;
	}

	public void setPpjk(String ppjk) {
		this.ppjk = ppjk;
	}

	public String getNama_ppjk() {
		return nama_ppjk;
	}

	public void setNama_ppjk(String nama_ppjk) {
		this.nama_ppjk = nama_ppjk;
	}

	public String getCreated_npwp() {
		return created_npwp;
	}

	public void setCreated_npwp(String created_npwp) {
		this.created_npwp = created_npwp;
	}

	public String getId_surat_kuasa() {
		return id_surat_kuasa;
	}

	public void setId_surat_kuasa(String id_surat_kuasa) {
		this.id_surat_kuasa = id_surat_kuasa;
	}

	public Integer getKd_document_type() {
		return kd_document_type;
	}

	public void setKd_document_type(Integer kd_document_type) {
		this.kd_document_type = kd_document_type;
	}

	public Integer getDelivery_order() {
		return delivery_order;
	}

	public void setDelivery_order(Integer delivery_order) {
		this.delivery_order = delivery_order;
	}

	public Integer getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Integer warehouse) {
		this.warehouse = warehouse;
	}

	public Integer getSp2() {
		return sp2;
	}

	public void setSp2(Integer sp2) {
		this.sp2 = sp2;
	}

	public Integer getDepo() {
		return depo;
	}

	public void setDepo(Integer depo) {
		this.depo = depo;
	}

	public Integer getTrucking() {
		return trucking;
	}

	public void setTrucking(Integer trucking) {
		this.trucking = trucking;
	}

	public Integer getDomestic() {
		return domestic;
	}

	public void setDomestic(Integer domestic) {
		this.domestic = domestic;
	}

	public DATA_PELIMPAHAN getData_pelimpahan() {
		return data_pelimpahan;
	}

	public void setData_pelimpahan(DATA_PELIMPAHAN data_pelimpahan) {
		this.data_pelimpahan = data_pelimpahan;
	}

	public MST_DOCUMENT_TYPE getMst_document() {
		return mst_document;
	}

	public void setMst_document(MST_DOCUMENT_TYPE mst_document) {
		this.mst_document = mst_document;
	}
	
}
