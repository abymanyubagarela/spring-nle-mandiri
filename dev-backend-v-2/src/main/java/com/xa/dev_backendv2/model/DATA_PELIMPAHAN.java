package com.xa.dev_backendv2.model;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="data_pelimpahan")
public class DATA_PELIMPAHAN {
	@Id
	@Column(name="idSuratKuasa")
	private String idSuratKuasa;
	
	@NotNull
	@Column(name="filename")
	private String filename;
	
	@NotNull
	@Column(name="file_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date file_date;
	
	@NotNull
	@Column(name="file")
	private String file;
	
	public DATA_PELIMPAHAN() {}
	
	public DATA_PELIMPAHAN(String idSuratKuasa, String filename, Date file_date, String file) {
		this.idSuratKuasa = idSuratKuasa;
		this.filename = filename;
		this.file_date = file_date;
		this.file = file;
	}
	
	@OneToMany(mappedBy = "id_surat_kuasa",cascade = CascadeType.ALL)
	private Set<DETIL_DATA_PELIMPAHAN> detil_data_pelimpahan;

	public Date getFile_date() {
		return file_date;
	}

	public void setFile_date(Date file_date) {
		this.file_date = file_date;
	}

	public String getIdSuratKuasa() {
		return idSuratKuasa;
	}

	public void setIdSuratKuasa(String idSuratKuasa) {
		this.idSuratKuasa = idSuratKuasa;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

//	public Set<DETIL_DATA_PELIMPAHAN> getDetil_data_pelimpahan() {
//		return detil_data_pelimpahan;
//	}

	public void setDetil_data_pelimpahan(Set<DETIL_DATA_PELIMPAHAN> detil_data_pelimpahan) {
		this.detil_data_pelimpahan = detil_data_pelimpahan;
	}
	
}
