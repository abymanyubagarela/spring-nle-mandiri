package com.xa.dev_backendv2.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="domestic_vessel_log_status")
public class DOMESTIC_VESSEL_LOG_STATUS {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "domestic_vessel_log_status_generator")
	@SequenceGenerator(name="domestic_vessel_log_status_generator", sequenceName = "seq_domestic_vessel_log_status",allocationSize = 1)
	@Column(name="id_log_status")
	private Integer id_log_status;
	
	//reference on table domestic_vessel_booking.id_request 
	@NotNull
	@Column(name="id_request", nullable = true)
	private String id_request;
	
	@NotNull
	@Column(name="status", nullable = true)
	private String status;
	
	@NotNull
	@Column(name="created_date", nullable = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp created_date;
	
//	@ManyToOne
//	@JoinColumn(name = "id_request", foreignKey = @ForeignKey(name = "fk_id_request"), 
//	insertable = false, updatable = false)
//	private DOMESTIC_VESSEL_BOOKING domestic_vessel_booking;

//	public DOMESTIC_VESSEL_BOOKING getDomestic_vessel_booking() {
//		return domestic_vessel_booking;
//	}
//
//	public void setDomestic_vessel_booking(DOMESTIC_VESSEL_BOOKING domestic_vessel_booking) {
//		this.domestic_vessel_booking = domestic_vessel_booking;
//	}

	public Integer getId_log_status() {
		return id_log_status;
	}

	public void setId_log_status(Integer id_log_status) {
		this.id_log_status = id_log_status;
	}

	public String getId_request() {
		return id_request;
	}

	public void setId_request(String id_request) {
		this.id_request = id_request;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}
}
