package com.xa.dev_backendv2.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="payment_detail")
public class PAYMENT_DETAIL {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "payment_detail_generator")
	@SequenceGenerator(name = "payment_detail_generator", sequenceName = "seq_payment_detail", allocationSize = 1)
	@Column(name="id_payment_detail")
	private Integer id_payment_detail;
	
	@NotNull
	@Column(name="price", precision = 14, scale = 2, nullable = true)
	private BigDecimal price;
}
