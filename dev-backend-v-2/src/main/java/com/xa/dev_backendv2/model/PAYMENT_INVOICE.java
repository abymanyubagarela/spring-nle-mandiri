package com.xa.dev_backendv2.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="payment_invoice", 
	indexes = {
		@Index(name = "invoice_module_index", columnList = "module", unique = false), 
		@Index(name = "invoice_module_unique_id_index", columnList = "module_unique_id", unique = false)
	}
)
public class PAYMENT_INVOICE {
	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "payment_invoice_generator")
//	@SequenceGenerator(name = "payment_invoice_generator", sequenceName = "seq_payment_invoice", allocationSize = 1)
	@Column(name="order_number", length = 20)
	private String order_number;
	
	@NotNull
	@Column(name="unique_id", length = 20, nullable = true)
	private String unique_id;
	
	@NotNull
	@Column(name = "module", length = 20, nullable = true)
	private String module;
	
	@NotNull
	@Column(name = "module_unique_id", length = 60, nullable = true)
	private String module_unique_id;
	
	@NotNull
	@Column(name="id_payment_services", length = 7, nullable = true)
	private Integer id_payment_services;
	
	@Column(name="invoiceAccountNumber", length = 20)
	private String invoiceAccountNumber;
	
	@Column(name="order_amount", precision = 14, scale = 2)
	private BigDecimal order_amount;
	
	@Column(name="costumer_number", length = 20)
	private String costumer_number;
	
	@Column(name="costumer_name")
	private String costumer_name;
	
	@NotNull
	@Column(name="currency_code", columnDefinition = "varchar default 'IDR'", 
	length = 5, nullable = true)
	private String currency_code;
	
	@Column(name="open_period")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date open_period;
	
	@Column(name="close_period")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date close_period;
	
	@Column(name="status")
	private String status;
	
	@NotNull
	@Column(name="va_created", length = 1,
	columnDefinition = "smallint default 0", nullable = true)
	private Integer va_created;
	
	@NotNull
	@Column(name="created_date", nullable = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp created_date;
	
	@NotNull
	@Column(name="updated_date", nullable = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp updated_date;
	
	/////////////////////////////////////////////////////////////////////////
	@OneToMany(mappedBy = "order_number", cascade = CascadeType.ALL)
	@OrderBy(value = "invoice_item_id ASC")
	private Set<PAYMENT_INVOICE_ITEM> invoice_item;

	public Integer getId_payment_services() {
		return id_payment_services;
	}

	public void setId_payment_services(Integer id_payment_services) {
		this.id_payment_services = id_payment_services;
	}

	public Set<PAYMENT_INVOICE_ITEM> getInvoice_item() {
		return invoice_item;
	}

	public void setInvoice_item(Set<PAYMENT_INVOICE_ITEM> invoice_item) {
		this.invoice_item = invoice_item;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getModule_unique_id() {
		return module_unique_id;
	}

	public void setModule_unique_id(String module_unique_id) {
		this.module_unique_id = module_unique_id;
	}

	public String getOrder_number() {
		return order_number;
	}

	public void setOrder_number(String order_number) {
		this.order_number = order_number;
	}

	public String getUnique_id() {
		return unique_id;
	}

	public void setUnique_id(String unique_id) {
		this.unique_id = unique_id;
	}

	public String getInvoiceAccountNumber() {
		return invoiceAccountNumber;
	}

	public void setInvoiceAccountNumber(String invoiceAccountNumber) {
		this.invoiceAccountNumber = invoiceAccountNumber;
	}

	public BigDecimal getOrder_amount() {
		return order_amount;
	}

	public void setOrder_amount(BigDecimal order_amount) {
		this.order_amount = order_amount;
	}

	public String getCostumer_number() {
		return costumer_number;
	}

	public void setCostumer_number(String costumer_number) {
		this.costumer_number = costumer_number;
	}

	public String getCostumer_name() {
		return costumer_name;
	}

	public void setCostumer_name(String costumer_name) {
		this.costumer_name = costumer_name;
	}

	public String getCurrency_code() {
		return currency_code;
	}

	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}

	public Date getOpen_period() {
		return open_period;
	}

	public void setOpen_period(Date open_period) {
		this.open_period = open_period;
	}

	public Date getClose_period() {
		return close_period;
	}

	public void setClose_period(Date close_period) {
		this.close_period = close_period;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getVa_created() {
		return va_created;
	}

	public void setVa_created(Integer va_created) {
		this.va_created = va_created;
	}

	public Timestamp getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}

	public Timestamp getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}


}
