package com.xa.dev_backendv2.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="document_trucking")
public class DOCUMENT_TRUCKING {
	@Id
	@Column(name="id_request_booking")
	private String idRequestBooking;
	
	@NotNull
	@Column(name="id_document", nullable = true)
	private Integer id_document;
	
	@NotNull
	@Column(name="id_platform", length = 5, nullable = true)
	private String id_platform;
	
	@Column(name="id_ff_ppjk", length = 30)
	private String id_ff_ppjk;
	
	@NotNull
	@Column(name="request_date", nullable = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp booking_date;
	
	@NotNull
	@Column(name="pod", nullable = true)
	private String pod;
	
	@Column(name="pod_lat", length = 100)
	private String pod_lat;
	
	@Column(name="pod_lon", length = 100)
	private String pod_lon;
	
	@Column(name="total_distance", columnDefinition = "integer default 0")
	private Integer total_distance;
	
	@Column(name="party", columnDefinition = "integer default 0")
	private Integer party;
	
	@NotNull
	@Column(name="plan_date", nullable = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp plan_date;
	
	@Column(name="bl_no", length = 20)
	private String bl_no;
	
	@Column(name="bl_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date bl_date;
	
	@Column(name="sp2valid_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date sp2valid_date;
	
	@Column(name="spcvalid_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date spcvalid_date;
	
	@NotNull
	@Column(name="id_User", length = 60, nullable = true)
	private String id_User;
	
	@Column(name="npwp", length = 60)
	private String npwp;
	
	@Column(name="company")
	private String company;
	
	@Column(name="address")
	private String address;
	
	@Column(name="email")
	private String email;
	
	@Column(name="mobile")
	private String mobile;

	@Column(name="term_of_payment")
	private Integer term_of_payment;
	
	@ManyToOne
	@JoinColumn(name = "id_document", foreignKey = @ForeignKey(name = "fk_id_document"), 
	insertable = false, updatable = false)
	private TD_DOCUMENT td_document;
	
	@ManyToOne
	@JoinColumn(name = "id_platform", foreignKey = @ForeignKey(name = "fk_id_platform"), 
	insertable = false, updatable = false)
	private MST_PLATFORM platform;
	
	@OneToMany(mappedBy = "idRequestBooking",cascade = CascadeType.ALL)
	@OrderBy(value = "id ASC")
	private Set<DOCUMENT_TRUCKING_DESTINATION> other_destination;
	
	@OneToMany(mappedBy = "idRequestBooking",cascade = CascadeType.ALL)
	@OrderBy(value = "idContainer ASC")
	private Set<DOCUMENT_TRUCKING_CONTAINER> container;
	
	public String getIdRequestBooking() {
		return idRequestBooking;
	}

	public void setIdRequestBooking(String idRequestBooking) {
		this.idRequestBooking = idRequestBooking;
	}

	public Set<DOCUMENT_TRUCKING_DESTINATION> getOther_destination() {
		return other_destination;
	}

	public void setOther_destination(Set<DOCUMENT_TRUCKING_DESTINATION> other_destination) {
		this.other_destination = other_destination;
	}

	public Set<DOCUMENT_TRUCKING_CONTAINER> getContainer() {
		return container;
	}

	public void setContainer(Set<DOCUMENT_TRUCKING_CONTAINER> container) {
		this.container = container;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public Integer getTerm_of_payment() {
		return term_of_payment;
	}

	public void setTerm_of_payment(Integer term_of_payment) {
		this.term_of_payment = term_of_payment;
	}

	public TD_DOCUMENT getTd_document() {
		return td_document;
	}

	public void setTd_document(TD_DOCUMENT td_document) {
		this.td_document = td_document;
	}

	public MST_PLATFORM getPlatform() {
		return platform;
	}

	public void setPlatform(MST_PLATFORM platform) {
		this.platform = platform;
	}

	public Integer getId_document() {
		return id_document;
	}

	public void setId_document(Integer id_document) {
		this.id_document = id_document;
	}

	public String getId_platform() {
		return id_platform;
	}

	public void setId_platform(String id_platform) {
		this.id_platform = id_platform;
	}

	public String getId_ff_ppjk() {
		return id_ff_ppjk;
	}

	public void setId_ff_ppjk(String id_ff_ppjk) {
		this.id_ff_ppjk = id_ff_ppjk;
	}

	public Timestamp getBooking_date() {
		return booking_date;
	}

	public void setBooking_date(Timestamp booking_date) {
		this.booking_date = booking_date;
	}

	public String getPod() {
		return pod;
	}

	public void setPod(String pod) {
		this.pod = pod;
	}

	public String getPod_lat() {
		return pod_lat;
	}

	public void setPod_lat(String pod_lat) {
		this.pod_lat = pod_lat;
	}

	public String getPod_lon() {
		return pod_lon;
	}

	public void setPod_lon(String pod_lon) {
		this.pod_lon = pod_lon;
	}

	public Integer getTotal_distance() {
		return total_distance;
	}

	public void setTotal_distance(Integer total_distance) {
		this.total_distance = total_distance;
	}

	public Integer getParty() {
		return party;
	}

	public void setParty(Integer party) {
		this.party = party;
	}

	public Timestamp getPlan_date() {
		return plan_date;
	}

	public void setPlan_date(Timestamp plan_date) {
		this.plan_date = plan_date;
	}

	public String getBl_no() {
		return bl_no;
	}

	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}

	public Date getBl_date() {
		return bl_date;
	}

	public void setBl_date(Date bl_date) {
		this.bl_date = bl_date;
	}

	public Date getSp2valid_date() {
		return sp2valid_date;
	}

	public void setSp2valid_date(Date sp2valid_date) {
		this.sp2valid_date = sp2valid_date;
	}

	public Date getSpcvalid_date() {
		return spcvalid_date;
	}

	public void setSpcvalid_date(Date spcvalid_date) {
		this.spcvalid_date = spcvalid_date;
	}

	public String getId_User() {
		return id_User;
	}

	public void setId_User(String id_User) {
		this.id_User = id_User;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
}
