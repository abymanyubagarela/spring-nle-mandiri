package com.xa.dev_backendv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="mst_process_type")
public class MST_PROCESS_TYPE {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_process_type_generator")
	@SequenceGenerator(name="mst_process_type_generator", sequenceName = "seq_mst_process_type",allocationSize = 1)
	@Column(name = "kd_process_type")
	private Integer kd_process_type;
	
	@NotNull
	@Column(name = "process_name", length = 15, nullable = true)
	private String process_name;

	public Integer getKd_process_type() {
		return kd_process_type;
	}

	public void setKd_process_type(Integer kd_process_type) {
		this.kd_process_type = kd_process_type;
	}

	public String getProcess_name() {
		return process_name;
	}

	public void setProcess_name(String process_name) {
		this.process_name = process_name;
	}
}
