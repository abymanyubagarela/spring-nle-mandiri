package com.xa.dev_backendv2.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="document_sp2")
public class DOCUMENT_SP2 {
	@Id
	@Column(name="id_document_sp2", length = 255)
	private String id_document_sp2;
	
	@Column(name="request_date", nullable = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp request_date;
	
	@Column(name="id_document", nullable = true)
	@JsonProperty("id_document")
	private Integer idDocument;
	
	@Column(name="id_platform",length = 5, nullable = true)
	private String id_platform;
	
	@Column(name="id_ff_ppjk",length = 30)
	private String id_ff_ppjk;
	
	@Column(name="terminal", length = 255)
	private String terminal;
	
	@Column(name="paid_thrud_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date paid_thrud_date;
	
	@Column(name="proforma", length = 255)
	private String proforma;
	
	@Column(name="proforma_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date proforma_date;
	
	@Column(name="price",precision = 14, scale = 2)
	private BigDecimal price;
	
	@Column(name="status", length = 255)
	private String status;
	
	@Column(name="party",columnDefinition = "integer default 0")
	private Integer party;
	
	@Column(name="url", columnDefinition = "TEXT")
	private String url;
	
	@Column(name="is_finished", columnDefinition = "integer default 0",
			insertable = false,updatable = true)
	private Integer is_finished;
	
	@Column(name = "payment_url", columnDefinition = "TEXT")
	private String payment_url;
	
	@Column(name = "gate_pass", columnDefinition = "TEXT")
	private String gate_pass;
	
	public String getGate_pass() {
		return gate_pass;
	}

	public void setGate_pass(String gate_pass) {
		this.gate_pass = gate_pass;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@ManyToOne
	@JoinColumn(name="id_document", foreignKey = @ForeignKey(name="fk_id_document"),
	insertable = false, updatable = false)
	private TD_DOCUMENT td_document;

	@ManyToOne
	@JoinColumn(name="id_platform", foreignKey = @ForeignKey(name="fk_id_platform"),
	insertable = false, updatable = false)
	private MST_PLATFORM mst_platform;

	@JsonManagedReference
	@OneToMany(mappedBy = "idDocumentSp2",cascade = CascadeType.ALL)
	private Set<DOCUMENT_SP2_CONTAINER> container;
	
	@OneToMany(mappedBy = "id_document_sp2",cascade = CascadeType.ALL)
	@OrderBy(value = "id_sp2_log ASC")
	private Set<DOCUMENT_SP2_LOG> sp2_log;

	public String getId_document_sp2() {
		return id_document_sp2;
	}

	public void setId_document_sp2(String id_document_sp2) {
		this.id_document_sp2 = id_document_sp2;
	}

	public Timestamp getRequest_date() {
		return request_date;
	}

	public void setRequest_date(Timestamp request_date) {
		this.request_date = request_date;
	}

	public Integer getIdDocument() {
		return idDocument;
	}

	public void setIdDocument(Integer id_document) {
		this.idDocument = id_document;
	}

	public String getId_platform() {
		return id_platform;
	}

	public void setId_platform(String id_platform) {
		this.id_platform = id_platform;
	}

	public String getId_ff_ppjk() {
		return id_ff_ppjk;
	}

	public void setId_ff_ppjk(String id_ff_ppjk) {
		this.id_ff_ppjk = id_ff_ppjk;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public Date getPaid_thrud_date() {
		return paid_thrud_date;
	}

	public void setPaid_thrud_date(Date paid_thrud_date) {
		this.paid_thrud_date = paid_thrud_date;
	}

	public String getProforma() {
		return proforma;
	}

	public void setProforma(String proforma) {
		this.proforma = proforma;
	}

	public Date getProforma_date() {
		return proforma_date;
	}

	public void setProforma_date(Date proforma_date) {
		this.proforma_date = proforma_date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getParty() {
		return party;
	}

	public void setParty(Integer party) {
		this.party = party;
	}

	public TD_DOCUMENT getTd_document() {
		return td_document;
	}

	public void setTd_document(TD_DOCUMENT td_document) {
		this.td_document = td_document;
	}

	public MST_PLATFORM getMst_platform() {
		return mst_platform;
	}

	public void setMst_platform(MST_PLATFORM mst_platform) {
		this.mst_platform = mst_platform;
	}

	public Set<DOCUMENT_SP2_CONTAINER> getContainer() {
		return container;
	}

	public void setContainer(Set<DOCUMENT_SP2_CONTAINER> container) {
		this.container = container;
	}

	public Set<DOCUMENT_SP2_LOG> getSp2_log() {
		return sp2_log;
	}

	public void setSp2_log(Set<DOCUMENT_SP2_LOG> sp2_log) {
		this.sp2_log = sp2_log;
	}

	public Integer getIs_finished() {
		return is_finished;
	}

	public void setIs_finished(Integer is_finished) {
		this.is_finished = is_finished;
	}

	public String getPayment_url() {
		return payment_url;
	}

	public void setPayment_url(String payment_url) {
		this.payment_url = payment_url;
	}
	
}
