package com.xa.dev_backendv2.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name="document_sp2_container")
public class DOCUMENT_SP2_CONTAINER {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "document_sp2_container_generator")
	@SequenceGenerator(name = "document_sp2_container_generator", sequenceName = "seq_document_sp2_container", allocationSize = 1)
	@Column(name="id_sp2_container")
	private Integer id_sp2_container;

	@JsonBackReference
	@JoinColumn(name = "id_document_sp2", referencedColumnName = "id_document_sp2" )
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonProperty("id_document_sp2")
	private DOCUMENT_SP2 idDocumentSp2;
	
	@Column(name="container_no", length = 255, nullable = true)
	private String container_no;
	
	@Column(name="container_size", length = 255)
	private String container_size;
	
	@Column(name="container_type", length = 255)
	private String container_type;

	public Integer getId_sp2_container() {
		return id_sp2_container;
	}

	public void setId_sp2_container(Integer id_sp2_container) {
		this.id_sp2_container = id_sp2_container;
	}

	public DOCUMENT_SP2 getIdDocumentSp2() {
		return idDocumentSp2;
	}

	public void setIdDocumentSp2(DOCUMENT_SP2 id_document_sp2) {
		this.idDocumentSp2 = id_document_sp2;
	}

	public String getContainer_no() {
		return container_no;
	}

	public void setContainer_no(String container_no) {
		this.container_no = container_no;
	}

	public String getContainer_size() {
		return container_size;
	}

	public void setContainer_size(String container_size) {
		this.container_size = container_size;
	}

	public String getContainer_type() {
		return container_type;
	}

	public void setContainer_type(String container_type) {
		this.container_type = container_type;
	}

}
