package com.xa.dev_backendv2.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="document_trucking_detil")
public class DOCUMENT_TRUCKING_DETIL {
	@Id
	@Column(name="idRequestBooking")
	private String idRequestBooking;
	
	@Column(name = "id_platform")
	private String idPlatform;
	
	@Column(name = "idServiceOrder")
	private String idServiceOrder;
	
	@Column(name = "hargaPenawaran")
	private Integer hargaPenawaran;
	
	@Column(name = "waktuPenawaran")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp waktuPenawaran;
	
	@Column(name = "timestamp")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp timestamp;
	
	@Column(name = "status", insertable = false, updatable = true)
	private String status;
	
	@Column(name = "paidStatus", columnDefinition = "integer default 0", 
			insertable = false, updatable = true)
	private Integer paidStatus;
	
	@Column(name = "isBooked", columnDefinition = "integer default 0", 
			insertable = false,updatable = true)
	private Integer isBooked;
	
	@Column(name= "booking_note")
	private String booking_note;
	
	@Column(name = "created_date")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp created_date;
	
	@Column(name = "booked_date")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp booked_date;
	
	@OneToOne
	@JoinColumn(name="idRequestBooking", 
	insertable = false, updatable = false)
	private DOCUMENT_TRUCKING booking;
	
	@OneToOne
	@JoinColumn(name="idRequestBooking",
	insertable = false, updatable = false)
	private DATA_TRANSPORTER data_transporter;
	
	public DATA_TRANSPORTER getData_transporter() {
		return data_transporter;
	}

	public void setData_transporter(DATA_TRANSPORTER data_transporter) {
		this.data_transporter = data_transporter;
	}

	public DOCUMENT_TRUCKING getBooking() {
		return booking;
	}

	public void setBooking(DOCUMENT_TRUCKING booking) {
		this.booking = booking;
	}

	public Timestamp getWaktuPenawaran() {
		return waktuPenawaran;
	}

	public void setWaktuPenawaran(Timestamp waktuPenawaran) {
		this.waktuPenawaran = waktuPenawaran;
	}

	public String getIdRequestBooking() {
		return idRequestBooking;
	}

	public void setIdRequestBooking(String idRequestBooking) {
		this.idRequestBooking = idRequestBooking;
	}

	public String getIdServiceOrder() {
		return idServiceOrder;
	}

	public void setIdServiceOrder(String idServiceOrder) {
		this.idServiceOrder = idServiceOrder;
	}

	public Integer getPaidStatus() {
		return paidStatus;
	}

	public void setPaidStatus(Integer paidStatus) {
		this.paidStatus = paidStatus;
	}

	public Integer getIsBooked() {
		return isBooked;
	}

	public void setIsBooked(Integer isBooked) {
		this.isBooked = isBooked;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBooking_note() {
		return booking_note;
	}

	public void setBooking_note(String booking_note) {
		this.booking_note = booking_note;
	}

	public Timestamp getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}

	public Timestamp getBooked_date() {
		return booked_date;
	}

	public void setBooked_date(Timestamp booked_date) {
		this.booked_date = booked_date;
	}

	public String getIdPlatform() {
		return idPlatform;
	}

	public void setIdPlatform(String idPlatform) {
		this.idPlatform = idPlatform;
	}

	public Integer getHargaPenawaran() {
		return hargaPenawaran;
	}

	public void setHargaPenawaran(Integer hargaPenawaran) {
		this.hargaPenawaran = hargaPenawaran;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
}
