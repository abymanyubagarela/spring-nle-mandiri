package com.xa.dev_backendv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="document_trucking_container")
public class DOCUMENT_TRUCKING_CONTAINER {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "document_trucking_container_generator")
	@SequenceGenerator(name = "document_trucking_container_generator", sequenceName = "seq_document_trucking_container", allocationSize = 1)
	@Column(name="idContainer")
	private Integer idContainer;
	
	@Column(name="Container_no")
	private String container_no;
	
	@Column(name="Container_size")
	private String container_size;
	
	@Column(name="Container_type")
	private String container_type;
	
	@Column(name="idRequestBooking")
	private String idRequestBooking;
	
	@Column(name="over_height")
	private Integer over_height;
	
	@Column(name="over_width")
	private Integer over_width;
	
	@Column(name="over_length")
	private Integer over_length;
	
	@Column(name="over_weight")
	private Integer over_weight;
	
	@Column(name="temperatur")
	private Integer temperatur;
	
	@Column(name="dangerous_type")
	private String dangerous_type;
	
	@Column(name="dangerous_material")
	private String dangerous_material;

	public Integer getIdContainer() {
		return idContainer;
	}

	public void setIdContainer(Integer idContainer) {
		this.idContainer = idContainer;
	}

	public String getContainer_no() {
		return container_no;
	}

	public void setContainer_no(String container_no) {
		this.container_no = container_no;
	}

	public String getContainer_size() {
		return container_size;
	}

	public void setContainer_size(String container_size) {
		this.container_size = container_size;
	}

	public String getContainer_type() {
		return container_type;
	}

	public void setContainer_type(String container_type) {
		this.container_type = container_type;
	}

	public String getIdRequestBooking() {
		return idRequestBooking;
	}

	public void setIdRequestBooking(String idRequestBooking) {
		this.idRequestBooking = idRequestBooking;
	}

	public Integer getOver_height() {
		return over_height;
	}

	public void setOver_height(Integer over_height) {
		this.over_height = over_height;
	}

	public Integer getOver_width() {
		return over_width;
	}

	public void setOver_width(Integer over_width) {
		this.over_width = over_width;
	}

	public Integer getOver_length() {
		return over_length;
	}

	public void setOver_length(Integer over_length) {
		this.over_length = over_length;
	}

	public Integer getOver_weight() {
		return over_weight;
	}

	public void setOver_weight(Integer over_weight) {
		this.over_weight = over_weight;
	}

	public Integer getTemperatur() {
		return temperatur;
	}

	public void setTemperatur(Integer temperatur) {
		this.temperatur = temperatur;
	}

	public String getDangerous_type() {
		return dangerous_type;
	}

	public void setDangerous_type(String dangerous_type) {
		this.dangerous_type = dangerous_type;
	}

	public String getDangerous_material() {
		return dangerous_material;
	}

	public void setDangerous_material(String dangerous_material) {
		this.dangerous_material = dangerous_material;
	}
}
