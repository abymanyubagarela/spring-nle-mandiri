package com.xa.dev_backendv2.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="payment_billing", 
	indexes = {
		@Index(name = "module_index", columnList = "module", unique = false),
		@Index(name = "module_unique_id_index", columnList = "module_unique_id", unique = false)
	}
)
public class PAYMENT_BILLING {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "payment_billing_generator")
	@SequenceGenerator(name="payment_billing_generator", sequenceName = "seq_payment_billing", allocationSize = 1)
	@Column(name = "billing_id")
	private Integer billing_id;
	
	@NotNull
	@Column(name = "module", length = 20, nullable = true)
	private String module;
	
	@NotNull
	@Column(name = "module_unique_id", length = 60, nullable = true)
	@JsonProperty("module_unique_id")
	private String moduleUniqueId;
	
	@Column(name="id_service")
	private Integer id_service;
	
	@ManyToOne
	@JoinColumn(name = "id_service", foreignKey = @ForeignKey(name = "fk_id_service"),
	insertable = false, updatable = false)
	private MST_SERVICES services;
	
	@Column(name="service_unique_id")
	private String service_unique_id;
	
	@Column(name="item_info", columnDefinition = "text")
	private String item_info;
	
	@NotNull
	@Column(name="amount", precision = 14, scale = 2, nullable = true)
	private BigDecimal amount;
	
	@Column(name="paid_date")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp paid_date;
	
	@Column(name="payment_via", columnDefinition = "varchar default 'NLE'", length = 20)
	private String payment_via;
	
	@NotNull
	@Column(name="created_date", nullable = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp created_date;

	@ManyToOne
	@JoinColumn(name = "id_platform", foreignKey = @ForeignKey(name = "fk_id_platform"))
	private MST_PLATFORM idPlatform;

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getModuleUniqueId() {
		return moduleUniqueId;
	}

	public void setModuleUniqueId(String module_unique_id) {
		this.moduleUniqueId = module_unique_id;
	}

	public String getService_unique_id() {
		return service_unique_id;
	}

	public void setService_unique_id(String service_unique_id) {
		this.service_unique_id = service_unique_id;
	}

	public Integer getBilling_id() {
		return billing_id;
	}

	public void setBilling_id(Integer billing_id) {
		this.billing_id = billing_id;
	}

	public Integer getId_service() {
		return id_service;
	}

	public void setId_service(Integer id_service) {
		this.id_service = id_service;
	}

	public MST_SERVICES getServices() {
		return services;
	}

	public void setServices(MST_SERVICES services) {
		this.services = services;
	}

	public String getItem_info() {
		return item_info;
	}

	public void setItem_info(String item_info) {
		this.item_info = item_info;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Timestamp getPaid_date() {
		return paid_date;
	}

	public void setPaid_date(Timestamp paid_date) {
		this.paid_date = paid_date;
	}

	public String getPayment_via() {
		return payment_via;
	}

	public void setPayment_via(String payment_via) {
		this.payment_via = payment_via;
	}

	public Timestamp getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}

	public MST_PLATFORM getIdPlatform() {
		return idPlatform;
	}

	public void setIdPlatform(MST_PLATFORM idPlatform) {
		this.idPlatform = idPlatform;
	}
}
