package com.xa.dev_backendv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="mst_kawasan")
public class MST_KAWASAN {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "mst_kawasan_generator")
	@SequenceGenerator(name="mst_kawasan_generator", sequenceName = "seq_mst_kawasan", allocationSize = 1)
	@Column(name="id_kawasan")
	private Integer id_kawasan;
	
	@JsonBackReference
	@JoinColumn(name = "id_port", referencedColumnName = "id_port", nullable = true)
	@ManyToOne(fetch = FetchType.EAGER)
	private MST_PORT id_port;
	
	@NotNull
	@Column(name="nama_kawasan", nullable = true)
	private String nama_kawasan;
	
	@Column(name="nama_kota")
	private String nama_kota;

	public Integer getId_kawasan() {
		return id_kawasan;
	}

	public void setId_kawasan(Integer id_kawasan) {
		this.id_kawasan = id_kawasan;
	}

	public MST_PORT getId_port() {
		return id_port;
	}

	public void setId_port(MST_PORT id_port) {
		this.id_port = id_port;
	}

	public String getNama_kawasan() {
		return nama_kawasan;
	}

	public void setNama_kawasan(String nama_kawasan) {
		this.nama_kawasan = nama_kawasan;
	}

	public String getNama_kota() {
		return nama_kota;
	}

	public void setNama_kota(String nama_kota) {
		this.nama_kota = nama_kota;
	}
}
