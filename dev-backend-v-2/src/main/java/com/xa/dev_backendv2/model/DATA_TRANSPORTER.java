package com.xa.dev_backendv2.model;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="data_transporter")
public class DATA_TRANSPORTER {
	@Id
	@Column(name = "id_request_booking")
	private String idRequestBooking; 
	
	@Column(name = "timestamp")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp timestamp;
	
	@Column(name = "billing_code")
	private String billingCode;
	
	@Column(name = "created_date")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp created_date;
	
	@OneToMany(mappedBy = "idRequestBooking",cascade = CascadeType.ALL)
	@OrderBy(value = "idDataTransporterDetil ASC")
	private Set<DATA_TRANSPORTER_DETIL> transporterDetil = new HashSet<>();

	public String getIdRequestBooking() {
		return idRequestBooking;
	}

	public void setIdRequestBooking(String idRequestBooking) {
		this.idRequestBooking = idRequestBooking;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public String getBillingCode() {
		return billingCode;
	}

	public void setBillingCode(String billingCode) {
		this.billingCode = billingCode;
	}

	public Timestamp getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}

	public Set<DATA_TRANSPORTER_DETIL> getTransporterDetil() {
		return transporterDetil;
	}

	public void setTransporterDetil(Set<DATA_TRANSPORTER_DETIL> transporterDetil) {
		this.transporterDetil = transporterDetil;
	}
	
}
