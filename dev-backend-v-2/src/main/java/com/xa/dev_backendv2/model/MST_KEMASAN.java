package com.xa.dev_backendv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="mst_kemasan")
public class MST_KEMASAN {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "mst_kemasan_generator")
	@SequenceGenerator(name="mst_kemasan_generator", sequenceName = "seq_mst_kemasan", allocationSize = 1)
	@Column(name="id_kemasan")
	private Integer id_kemasan;
	
	@NotNull
	@Column(name="nama_kemasan", nullable = true)
	private String nama_kemasan;

	public Integer getId_kemasan() {
		return id_kemasan;
	}

	public void setId_kemasan(Integer id_kemasan) {
		this.id_kemasan = id_kemasan;
	}

	public String getNama_kemasan() {
		return nama_kemasan;
	}

	public void setNama_kemasan(String nama_kemasan) {
		this.nama_kemasan = nama_kemasan;
	}
	
}
