package com.xa.dev_backendv2.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name="mst_platform")
public class MST_PLATFORM {
	@Id
	@Column(name="id_platform", length = 5)
	private String id_platform;
	
	@NotNull
	@Column(name="nama_platform", nullable = true)
	private String nama_platform;
	
	@Column(name="company")
	private String company;
	
	@Column(name="tc_url")
	private String tc_url;
	
	@Column(name="payment_url")
	private String payment_url;
	
	@Column(name="live_tracking_url")
	private String live_tracking_url;
	
	@Column(name="no_rekening")
	private String no_rekening;

	@JsonManagedReference
	@OneToMany(mappedBy = "mst_platform", cascade = CascadeType.ALL)
	private Set<MST_PLATFORM_SERVICES> mstPlatformServices;

	public String getNo_rekening() {
		return no_rekening;
	}

	public void setNo_rekening(String no_rekening) {
		this.no_rekening = no_rekening;
	}

	public String getId_platform() {
		return id_platform;
	}

	public void setId_platform(String id_platform) {
		this.id_platform = id_platform;
	}

	public String getNama_platform() {
		return nama_platform;
	}

	public void setNama_platform(String nama_platform) {
		this.nama_platform = nama_platform;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getTc_url() {
		return tc_url;
	}

	public void setTc_url(String tc_url) {
		this.tc_url = tc_url;
	}

	public String getPayment_url() {
		return payment_url;
	}

	public void setPayment_url(String payment_url) {
		this.payment_url = payment_url;
	}

	public String getLive_tracking_url() {
		return live_tracking_url;
	}

	public void setLive_tracking_url(String live_tracking_url) {
		this.live_tracking_url = live_tracking_url;
	}

	public Set<MST_PLATFORM_SERVICES> getMstPlatformServices() {
		return mstPlatformServices;
	}

	public void setMstPlatformServices(Set<MST_PLATFORM_SERVICES> mstPlatformServices) {
		this.mstPlatformServices = mstPlatformServices;
	}
	
	public String toString() 
    { 
        return id_platform + " " + nama_platform + " " + company + " " + tc_url + " " + payment_url + " "; 
    } 
}
