package com.xa.dev_backendv2.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.NotNull;

@Entity
@Table(name="document_do_log")
public class DOCUMENT_DO_LOG {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "document_do_log_generator")
	@SequenceGenerator(name = "document_do_log_generator", sequenceName = "seq_document_do_log", allocationSize = 1)
	@Column(name="id_do_log")
	private Integer id_do_log;
	
	@NotNull
	@Column(name="id_document_do", length = 255, nullable = true)
	private String id_document_do;
	
	@NotNull
	@Column(name="created_date", nullable = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp created_date;

	@Column(name="status", length = 255, nullable = true)
	private String status;

	public Integer getId_do_log() {
		return id_do_log;
	}

	public void setId_do_log(Integer id_do_log) {
		this.id_do_log = id_do_log;
	}

	public String getId_document_do() {
		return id_document_do;
	}

	public void setId_document_do(String id_document_do) {
		this.id_document_do = id_document_do;
	}

	public Timestamp getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
