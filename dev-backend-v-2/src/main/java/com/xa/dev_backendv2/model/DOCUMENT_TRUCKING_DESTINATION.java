package com.xa.dev_backendv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="document_trucking_destination")
public class DOCUMENT_TRUCKING_DESTINATION {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "document_trucking_destination_generator")
	@SequenceGenerator(name = "document_trucking_destination_generator", sequenceName = "seq_document_trucking_destination", allocationSize = 1)
	@Column(name="id")
	private Integer id;
	
	@Column(name="idRequestBooking")
	private String idRequestBooking;
	
	@Column(name="urutan")
	private Integer urutan;
	
	@Column(name="destination")
	private String destination;
	
	@Column(name="latitude")
	private String latitude;
	
	@Column(name="longitude")
	private String longitude;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdRequestBooking() {
		return idRequestBooking;
	}

	public void setIdRequestBooking(String idRequestBooking) {
		this.idRequestBooking = idRequestBooking;
	}

	public Integer getUrutan() {
		return urutan;
	}

	public void setUrutan(Integer urutan) {
		this.urutan = urutan;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
}
