package com.xa.dev_backendv2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "mst_platform_configuration")
public class MST_PLATFORM_CONFIGURATION {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "mst_platform_configuration_generator")
	@SequenceGenerator(name="mst_platform_configuration_generator", sequenceName = "seq_mst_platform_configuration", allocationSize = 1)
	@Column(name = "id")
	private Integer id;
	
	@NotNull
	@JsonBackReference
	@JoinColumn(name = "id_platform", referencedColumnName = "id_platform", nullable = true)
	@ManyToOne(fetch = FetchType.EAGER)
	private MST_PLATFORM id_platform;
	
	@Column(name="category", length = 50)
	private String category;
	
	@NotNull
	@Column(name="key", length = 50, nullable = true)
	private String key;
	
	@Column(name="value")
	private String value;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MST_PLATFORM getId_platform() {
		return id_platform;
	}

	public void setId_platform(MST_PLATFORM id_platform) {
		this.id_platform = id_platform;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
